<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{freeshippingalert}default-bootstrap>blockfreeship_cde139c95e6a735d6cce92fd373a5440'] = 'Free shipping';
$_MODULE['<{freeshippingalert}default-bootstrap>blockfreeship_e0c59b98715123d23ce9ae1870b154b1'] = 'You still have to order';
$_MODULE['<{freeshippingalert}default-bootstrap>blockfreeship_fc0b168cd3271f652b74b5b844b5b261'] = 'for a free shipping';
$_MODULE['<{freeshippingalert}default-bootstrap>freeshippingalert_654e887157351401e1e870bdfb84f9c2'] = 'Minimum cart';
$_MODULE['<{freeshippingalert}default-bootstrap>freeshippingalert_dad1f8d794ee0dd7753fe75e73b78f31'] = 'Zones';
$_MODULE['<{freeshippingalert}default-bootstrap>freeshippingalert_81f620545faeb419cae4932272def201'] = 'Groups';
$_MODULE['<{freeshippingalert}default-bootstrap>freeshippingalert_1c3aef2983b7c25b9a14b2b40481ddb9'] = 'URL redirection';
$_MODULE['<{freeshippingalert}default-bootstrap>freeshippingalert_aed88202e2432ee2a613f9120ef7486b'] = 'Save';
