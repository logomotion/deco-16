<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{quickorderform}default-bootstrap>quickorderform-form_b31cf4e8452cb906ef1ef5769484b1d1'] = 'Please enter reference or name';
$_MODULE['<{quickorderform}default-bootstrap>quickorderform-form_7d9b384a7df4a5f2e720b8f1b81ea353'] = 'Remove this product';
$_MODULE['<{quickorderform}default-bootstrap>quickorderform-form_1063e38cb53d94d386f21227fcd84717'] = 'Remove';
$_MODULE['<{quickorderform}default-bootstrap>quickorderform-form_12d3c7a4296542c62474856ec452c045'] = 'Ref.';
$_MODULE['<{quickorderform}default-bootstrap>quickorderform-form_49ee3087348e8d44e1feda1917443987'] = 'Name';
$_MODULE['<{quickorderform}default-bootstrap>quickorderform-form_68a6edd654bf46bdca814b63c7bd35d1'] = 'Combinaison';
$_MODULE['<{quickorderform}default-bootstrap>quickorderform-form_694e8d1f2ee056f98ee488bdc4982d73'] = 'Quantity';
$_MODULE['<{quickorderform}default-bootstrap>quickorderform-form_3837cb7cbf8a4ee6231126ba1eb663da'] = 'Add 5 new lines';
$_MODULE['<{quickorderform}default-bootstrap>quickorderform-form_ec211f7c20af43e742bf2570c3cb84f9'] = 'Add';
$_MODULE['<{quickorderform}default-bootstrap>quickorderform_96e9f766df00ffcbd7c4600d16f90df3'] = 'Quick Order Form';
$_MODULE['<{quickorderform}default-bootstrap>quickorderform_37f40ca0969b0b1cd44758a5db34e05b'] = 'Adds a form to order quicker.';
$_MODULE['<{quickorderform}default-bootstrap>quickorderform_828aa8e02beb6a1e6ad5ec383e199f59'] = 'Quick order form';
$_MODULE['<{quickorderform}default-bootstrap>quickorderform_226ed577d0eff50725be6447bcd5a2f0'] = 'Error move uploaded file';
$_MODULE['<{quickorderform}default-bootstrap>quickorderform_b78a3223503896721cca1303f776159b'] = 'Title';
$_MODULE['<{quickorderform}default-bootstrap>quickorderform-form_1d9baf077ee87921f57a8fe42d510b65'] = 'Subtract';
$_MODULE['<{quickorderform}default-bootstrap>quickorderform-form_2d0f6b8300be19cf35e89e66f0677f95'] = 'Add to cart';
$_MODULE['<{quickorderform}default-bootstrap>left-column_ef0c6dc825df5280fe98c1bb42c310c7'] = 'Quick Order';
$_MODULE['<{quickorderform}default-bootstrap>left-column_9b14229fd538c73ed714ec4a9aa66996'] = 'Order quicker !';
$_MODULE['<{quickorderform}default-bootstrap>left-column_08ce78d09a131e90159b67381a7ebcee'] = 'Go to Quick Order Form';
