<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{toolcartmin}default-bootstrap>blockcartmin_2d8f3d3ca086eb77793eef99a902ec15'] = 'Minimum order';
$_MODULE['<{toolcartmin}default-bootstrap>blockcartmin_bbf5bc49e8b4e675757e66f95c6e4e1e'] = 'Minimum order is defined';
$_MODULE['<{toolcartmin}default-bootstrap>blockcartmin_1ec33b80127419bbc4f8f743f151c1a1'] = 'purchases';
$_MODULE['<{toolcartmin}default-bootstrap>blockcartmin_e0c59b98715123d23ce9ae1870b154b1'] = 'You still have to order';
$_MODULE['<{toolcartmin}default-bootstrap>toolcartmin_665ee5fccac789e654964e0f111b4679'] = 'Minimum cart';
$_MODULE['<{toolcartmin}default-bootstrap>toolcartmin_654e887157351401e1e870bdfb84f9c2'] = 'Minimum cart';
$_MODULE['<{toolcartmin}default-bootstrap>toolcartmin_4c96619779761c1b4b03d075d8e5bb1f'] = 'Minimum amount to order';
$_MODULE['<{toolcartmin}default-bootstrap>toolcartmin_dad1f8d794ee0dd7753fe75e73b78f31'] = 'Zones';
$_MODULE['<{toolcartmin}default-bootstrap>toolcartmin_81f620545faeb419cae4932272def201'] = 'Groups';
$_MODULE['<{toolcartmin}default-bootstrap>toolcartmin_1c3aef2983b7c25b9a14b2b40481ddb9'] = 'URL redirection';
$_MODULE['<{toolcartmin}default-bootstrap>toolcartmin_aed88202e2432ee2a613f9120ef7486b'] = 'Save';
