	
	Message de {shop_name}


	
		
			
				
			
		
		 
		
			Bonjour {firstname} {lastname},
		
		 
		
			Historique de votre commande n°{id_order}
		
		 
		
			
				Nous avons bien enregistré votre commande {id_order}. Celle-ci vous sera envoyée dès réception de votre paiement.
			
		
		 
		
			
				Pour rappel, vous avez sélectionné le devis et celui-ci est à envoyer à :
				
				{quote_owner}
				
				{quote_details}
				
				{quote_address}
				
				Le montant total est de {total_paid}
			
		
		 
		
			
				Vous pouvez accéder au suivi de votre commande et télécharger votre facture dans "Historique des commandes" de la rubrique "Mon compte" sur notre site.
			
		
		 
		
			
				{shop_name} réalisé avec PrestaShop™
			
		
	


