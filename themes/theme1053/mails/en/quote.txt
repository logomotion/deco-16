Hi {firstname} {lastname},

Your order #{id_order} has been placed successfully. Your order will be shipped as soon as payment is received.

Please note that you have selected the quote and this one will be sent to : 

{quote_owner} 

{quote_details} 

{quote_address} 

Order total amount is {total_paid}

You can review this order and download your invoice from the "Order history" section of your account by clicking "My account" on our Website.

Thank you for shopping at {shop_name}.

{shop_name} - {shop_url}


{shop_url} powered by PrestaShop™