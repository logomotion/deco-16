{if !isset($content_only) || !$content_only}
					</div><!-- #center_column -->
					{if isset($right_column_size) && !empty($right_column_size)}
						<div id="right_column" class="col-xs-12 col-sm-{$right_column_size|intval} column">{$HOOK_RIGHT_COLUMN}</div>
					{/if}
					</div><!-- .row -->
				</div><!-- #columns -->
			</div><!-- .columns-container -->
			{if isset($HOOK_FOOTER)}
				<!-- Footer -->
				<div class="footer-container">
					<span class="mask"></span>
					<div class="container">
						{if isset($HOOK_HOME) && $HOOK_HOME|trim}
							<div class="row">{$HOOK_HOME}</div>
						{/if}
					</div>
					<div class="wrap-footer">
						<footer id="footer"  class="container">
							<div class="row">
								<section class="footer-block col-xs-12 col-sm-2">
									<h4>{l s='Information'}</h4>
									<ul class="toggle-footer">
										<li><a href="/{if $lang_iso == 'en'}en{else}fr{/if}/content/1-{l s='livraison'}">{l s='Livraisons et retours'}</a></li>
										<li><a href="/{if $lang_iso == 'en'}en{else}fr{/if}/content/5-{l s='paiement-securise'}">{l s='Paiement sécurisé'}</a></li>
										<li><a href="/{if $lang_iso == 'en'}en{else}fr{/if}/plan-du-site">{l s='Plan du site'}</a></li>
										<li><a href="/{if $lang_iso == 'en'}en{else}fr{/if}/content/7-{l s='export-conditions'}">{l s='Export (Conditions)'}</a></li>
									</ul>
								</section>
								<section class="footer-block col-xs-12 col-sm-2">
									<h4>{l s='Produits'}</h4>
									<ul class="toggle-footer">
										<li><a href="/{if $lang_iso == 'en'}en{else}fr{/if}/promotions">{l s='Promotions'}</a></li>
										<li><a href="/{if $lang_iso == 'en'}en{else}fr{/if}/nouveaux-produits">{l s='Nouveautés'}</a></li>
										<li><a href="/{if $lang_iso == 'en'}en{else}fr{/if}/content/8-{l s='evenements'}">{l s='Événements'}</a></li>
										<li><a href="/{if $lang_iso == 'en'}en{else}fr{/if}/content/9-{l s='catalogue-deco-relief'}">{l s='Catalogue'}</a></li>
										<li><a href="/{if $lang_iso == 'en'}en{else}fr{/if}/content/10-{l s='pieces-artistiques'}">{l s='Pièces artistiques'}</a></li>
									</ul>
								</section>
								<section class="footer-block col-xs-12 col-sm-2">
									<h4>{l s='Société'}</h4>
									<ul class="toggle-footer">
										<li><a href="/{if $lang_iso == 'en'}en{else}fr{/if}/content/4-{l s='a-propos'}">{l s='Qui sommes-nous'}</a></li>
										<li><a href="/{if $lang_iso == 'en'}en{else}fr{/if}/content/2-{l s='mentions-legales'}">{l s='Mentions légales'}</a></li>
										<li><a href="/{if $lang_iso == 'en'}en{else}fr{/if}/content/3-{l s='conditions-generales-de-ventes'}">{l s='Conditions générales'}</a></li>
										<li><a href="/{if $lang_iso == 'en'}en{else}fr{/if}/contactez-nous">{l s='Contactez-nous'}</a></li>
										<li><a>{l s='Suivez-nous!'}</a><a href="https://twitter.com/DecoRelief_SAS" title="{l s='Suivez nous sur Twitter'}" target="_blank">&nbsp;&nbsp;<i class="fa fa-twitter fa-2x"></i></a> <a href="https://www.facebook.com/decorelief.france/" title="{l s='Suivez nous sur Facebook'}" target="_blank"><i class="fa fa-facebook fa-2x"></i></a></li>
									</ul>
								</section>
								{$HOOK_FOOTER}
							</div><!-- .row -->
						</footer>
					</div>
				</div><!-- #footer -->
			{/if}
		</div><!-- #page -->
{/if}
{include file="$tpl_dir./global.tpl"}
<script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="/themes/theme1053/js/jquery.reel-min.js"></script>
	</body>
</html>