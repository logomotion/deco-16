{if isset($orderProducts) && count($orderProducts)}
    <section id="crossselling" class="page-product-box">
    	<h3 class="productscategory_h2 page-product-heading">
            {if $page_name == 'product'}
                {l s='Customers who bought this product also bought:' mod='crossselling'}
            {else}
                {l s='We recommend' mod='crossselling'}
            {/if}
        </h3>
    	<div id="crossselling_list">
            <ul id="crossselling_list_car" class="clearfix">
                {foreach from=$orderProducts item='orderProduct' name=orderProduct}
                    <li class="product-box item">
                        {if $orderProduct.quantity <= 0}
                        <span class="out-of-stock">
                            <link itemprop="availability" href="http://schema.org/OutOfStock" />{l s='Out of stock' mod='crossselling'}
                        </span>
                        {/if}
                        <a href="{$orderProduct.link|escape:'html':'UTF-8'}" class="lnk_img product-image" title="{$orderProduct.name|htmlspecialchars}"><img src="{$orderProduct.image}" alt="{$orderProduct.name|htmlspecialchars}" /></a>
                        <p class="product-ref">{$orderProduct.reference}</p>
                        <h5 class="product-name">
                            <a href="{$orderProduct.link|escape:'html':'UTF-8'}" title="{$orderProduct.name|htmlspecialchars}">{$orderProduct.name|truncate:20:'...':true|escape:'html':'UTF-8'}</a>
                        </h5>
                        <p class="product-desc" itemprop="description">
                            <small>{$orderProduct.description_short|strip_tags:'UTF-8'|truncate:360:'...'}</small>
                        </p>

                        {if $orderProduct.show_price == 1 AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}
                            <p class="price_display">
                                {if isset($orderProduct.price_without_reduction) && $orderProduct.price_without_reduction != $orderProduct.displayed_price}<span class="old-price">{displayWtPrice p=$orderProduct.price_without_reduction}</span>{/if}<span class="price-tincl price{if isset($orderProduct.specific_prices) && $orderProduct.specific_prices} special-price{/if}">{convertPrice price=$orderProduct.price_with_taxes} {l s='tax incl.' mod='crossselling'}</span>
                                <span class="price-texcl">({convertPrice price=$orderProduct.price_without_taxes} {l s='tax excl.' mod='crossselling'})</span>
                                
                            </p>
                        {else}
                            <br />
                        {/if}
                        <div class="clearfix" style="margin-top:5px">
                            <div class="no-print">
                                <a class="btn btn-default ajax_add_to_cart_button" href="{$link->getPageLink('cart', true, NULL, "qty=1&amp;id_product={$orderProduct.id_product|intval}&amp;token={$static_token}&amp;add")|escape:'html':'UTF-8'}" data-id-product="{$orderProduct.id_product|intval}" title="{l s='Add to cart' mod='crossselling'}">
                                    <span>{l s='Add to cart' mod='crossselling'}</span>
                                </a>
                            </div>
                        </div>
                    </li>
                {/foreach}
            </ul>
        </div>
    </section>
{/if}
