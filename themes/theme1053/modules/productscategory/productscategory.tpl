{if count($categoryProducts) > 0 && $categoryProducts !== false}
    <section class="page-product-box blockproductscategory">
        <h3 class="productscategory_h3 page-product-heading">{$categoryProducts|@count} {l s='other products in the same category:' mod='productscategory'}</h3>
        <div id="productscategory_list" class="clearfix">
            <ul id="bxslider1" class="bxslider clearfix">
                 {foreach from=$categoryProducts item='categoryProduct' name=categoryProduct}
                    <li class="product-box item">
                        {if $orderProduct.quantity <= 0}
                        <span class="out-of-stock">
                            <link itemprop="availability" href="http://schema.org/OutOfStock" />{l s='Out of stock' mod='productscategory'}
                        </span>
                        {/if}
                        <a href="{$link->getProductLink($categoryProduct.id_product, $categoryProduct.link_rewrite, $categoryProduct.category, $categoryProduct.ean13)}" class="lnk_img product-image" title="{$categoryProduct.name|htmlspecialchars}"><img src="{$link->getImageLink($categoryProduct.link_rewrite, $categoryProduct.id_image, 'tm_home_default')|escape:'html':'UTF-8'}" alt="{$categoryProduct.name|htmlspecialchars}" /></a>
                        <p class="product-ref">{$categoryProduct.reference}</p>
                        <h5 class="product-name">
                            <a href="{$link->getProductLink($categoryProduct.id_product, $categoryProduct.link_rewrite, $categoryProduct.category, $categoryProduct.ean13)|escape:'html':'UTF-8'}" title="{$categoryProduct.name|htmlspecialchars}">{$categoryProduct.name|truncate:20:'...':true|escape:'html':'UTF-8'}</a>
                        </h5>
                        <p class="product-desc" itemprop="description">
                            <small>{$categoryProduct.description_short|strip_tags:'UTF-8'|truncate:360:'...'}</small>
                        </p>
                        {if $ProdDisplayPrice AND $categoryProduct.show_price == 1 AND !isset($restricted_country_mode) AND !$PS_CATALOG_MODE}
                            <p class="price_display">
                                {if isset($categoryProduct.specific_prices) && $categoryProduct.specific_prices}<span class="old-price">{displayWtPrice p=$categoryProduct.price_without_reduction}</span>{/if}<span class="price-tincl price{if isset($categoryProduct.specific_prices) && $categoryProduct.specific_prices} special-price{/if}">{convertPrice price=$categoryProduct.price} {l s='tax incl.' mod='productscategory'}</span>
                                <span class="price-texcl">({convertPrice price=$categoryProduct.price_tax_exc} {l s='tax excl.' mod='productscategory'})</span>
                                
                            </p>
                        {else}
                            <br />
                        {/if}
                        <div class="clearfix" style="margin-top:5px">
                            <div class="no-print">
                                <a class="btn btn-default ajax_add_to_cart_button" href="{$link->getPageLink('cart', true, NULL, "qty=1&amp;id_product={$categoryProduct.id_product|intval}&amp;token={$static_token}&amp;add")|escape:'html':'UTF-8'}" data-id-product="{$categoryProduct.id_product|intval}" title="{l s='Add to cart' mod='productscategory'}">
                                    <span>{l s='Add to cart' mod='productscategory'}</span>
                                </a>
                            </div>
                        </div>
                    </li>
                {/foreach}
            </ul>
        </div>
    </section>
{/if}