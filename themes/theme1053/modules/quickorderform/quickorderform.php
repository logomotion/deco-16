<?php

if (!defined('_PS_VERSION_'))
	exit;

class QuickOrderForm extends Module
{
	/* Title associated to the image */
	public $qof_title;

	/* Name of the image without extension */
	public $qof_imgname;

	/* Image path with extension */
	public $qof_img;

	public function __construct()
	{
		$this->name = 'quickorderform';
		$this->tab = 'checkout';
		$this->bootstrap = true;
		$this->display = 'view';
		$this->version = '1.5';
		$this->module_key = '9021e2d24ac6c563b7204df1c388fc3d';
		$this->need_instance = 0;
		$this->author = 'Ixycom';

		parent::__construct();

		$this->displayName = $this->l('Quick Order Form');
		$this->description = $this->l('Adds a form to order quicker.');

		$this->initialize();
	}

	/*
	* Set the properties of the module, like the link to the image and the title (contextual to the current shop context)
	*/

	protected function initialize()
	{
		$this->qof_imgname = 'quickorderform';
		if ((Shop::getContext() == Shop::CONTEXT_GROUP || Shop::getContext() == Shop::CONTEXT_SHOP)
			&& file_exists(_PS_MODULE_DIR_.$this->name.'/img/'.$this->qof_imgname.'-g'.$this->context->shop->getContextShopGroupID().'.'
				.Configuration::get('BLOCKQUICKORDER_IMG_EXT'))
		)
			$this->qof_imgname .= '-g'.$this->context->shop->getContextShopGroupID();

		if (Shop::getContext() == Shop::CONTEXT_SHOP
			&& file_exists(_PS_MODULE_DIR_.$this->name.'/img/'.$this->qof_imgname.'-s'
				.$this->context->shop->getContextShopID().'.'.Configuration::get('BLOCKQUICKORDER_IMG_EXT'))
		)
			$this->qof_imgname .= '-s'.$this->context->shop->getContextShopID();

		$this->qof_img = Tools::getMediaServer($this->name)._MODULE_DIR_.$this->name.'/img/'.$this->qof_imgname
			.'.'.Configuration::get('BLOCKQUICKORDER_IMG_EXT');
		$this->qof_title = htmlentities(Configuration::get('BLOCKQUICKORDER_TITLE'), ENT_QUOTES, 'UTF-8');
	}

	public function install()
	{
		Configuration::updateGlobalValue('BLOCKQUICKORDER_TITLE', $this->l('Quick order form'));
		Configuration::updateGlobalValue('BLOCKQUICKORDER_QUANTITY', 0);
		Configuration::updateGlobalValue('BLOCKQUICKORDER_NBLINES', 5);
		Configuration::updateGlobalValue('BLOCKQUICKORDER_EMPTY', 0);

		foreach (scandir(_PS_MODULE_DIR_.$this->name.'/img/') as $file)
			if (in_array($file, array('quickorderform.jpg', 'quickorderform.gif', 'quickorderform.png')))
				Configuration::updateGlobalValue('BLOCKQUICKORDER_IMG_EXT', Tools::substr($file, strrpos($file, '.') + 1));

		$groups = Group::getGroups($this->context->language->id);
		$list_group = '';
		foreach ($groups as $group)
			$list_group .= intval($group['id_group']).'-';
		$list_group = rtrim($list_group, '-');
		Configuration::updateGlobalValue('BLOCKQUICKORDER_GROUPS', $list_group);

		return (parent::install() && $this->registerHook('displayLeftColumn') && $this->registerHook('displayHeader') && $this->registerHook('displayTop'));
	}

	public function uninstall()
	{
		Configuration::deleteByName('BLOCKQUICKORDER_TITLE');
		Configuration::deleteByName('BLOCKQUICKORDER_IMG_EXT');
		Configuration::deleteByName('BLOCKQUICKORDER_QUANTITY');
		Configuration::deleteByName('BLOCKQUICKORDER_NBLINES');
		Configuration::deleteByName('BLOCKQUICKORDER_EMPTY');
		Configuration::deleteByName('BLOCKQUICKORDER_GROUPS');

		return (parent::uninstall());
	}

	public function getContent()
	{
		$output = $this->postProcess();
		$output .= $this->renderForm();
		$output .= $this->getCredit();

		return $output;
	}

	public function getCredit()
	{
		return '<div class="row text-center"><a class="center" href="http://www.ixycom.com" title="Agence web à Lille" target="_blank"><img
src="http://www.ixycom.com/images/logo-300.jpg" alt="Logo Ixycom" width="300" height="134" /></a></div>';
	}

	public function postProcess()
	{
		if (Tools::isSubmit('submitDeleteImgConf'))
			$this->deleteCurrentImg();

		$output = '';
		if (Tools::isSubmit('submitAdvConf'))
		{
			if (isset($_FILES['qof_img']) && isset($_FILES['qof_img']['tmp_name'])
				&& !empty($_FILES['qof_img']['tmp_name'])
			)
			{
				if (!ImageManager::isCorrectImageFileExt($_FILES['qof_img']['name']))
					$output .= $this->displayError($this->l('Error with file extension'));
				elseif ($_FILES['qof_img']['size'] > Tools::convertBytes(ini_get('upload_max_filesize')))
					$output .= $this->displayError($this->l('Error with file size'));
				else
				{
					Configuration::updateValue('BLOCKQUICKORDER_IMG_EXT',
						Tools::substr($_FILES['qof_img']['name'],
							strrpos($_FILES['qof_img']['name'], '.') + 1));

					$this->qof_imgname = 'quickorderform';
					if (Shop::getContext() == Shop::CONTEXT_GROUP)
						$this->qof_imgname = 'quickorderform-g'.(int)$this->context->shop->getContextShopGroupID();
					elseif (Shop::getContext() == Shop::CONTEXT_SHOP)
						$this->qof_imgname = 'quickorderform-s'.(int)$this->context->shop->getContextShopID();

					if (!move_uploaded_file($_FILES['qof_img']['tmp_name'],
						_PS_MODULE_DIR_.$this->name.'/img/'.$this->qof_imgname.'.'
						.Configuration::get('BLOCKQUICKORDER_IMG_EXT'))
					)
						$output .= $this->displayError($this->l('Error move uploaded file'));
				}
			}

			$active_groups = array();
			$groups = Group::getGroups($this->context->language->id);
			foreach ($groups as $group)
			{
				if (Tools::getValue('qof_groups_'.$group['id_group']))
					$active_groups[] = $group['id_group'];
			}

			if (count($active_groups))
				Configuration::updateValue('BLOCKQUICKORDER_GROUPS', implode('-', $active_groups));

			if (Tools::getValue('qof_title'))
				Configuration::updateValue('BLOCKQUICKORDER_TITLE', Tools::getValue('qof_title'));

			Configuration::updateValue('BLOCKQUICKORDER_QUANTITY', Tools::getValue('qof_quantity'));
			Configuration::updateValue('BLOCKQUICKORDER_NBLINES', Tools::getValue('qof_nblines'));
			Configuration::updateValue('BLOCKQUICKORDER_EMPTY', Tools::getValue('qof_empty'));

			$output .= $this->displayConfirmation($this->l('Settings updated'));

			$this->initialize();
		}

		return $output;
	}

	private function deleteCurrentImg()
	{
		if ($this->qof_imgname != 'quickorderform'
			&& file_exists(_PS_MODULE_DIR_.$this->name.'/img/'.$this->qof_imgname.'.'
				.Configuration::get('BLOCKQUICKORDER_IMG_EXT'))
		)
			unlink(_PS_MODULE_DIR_.$this->name.'/img/'.$this->qof_imgname.'.'
				.Configuration::get('BLOCKQUICKORDER_IMG_EXT'));

		Configuration::deleteFromContext('BLOCKQUICKORDER_IMG_EXT');
		Configuration::updateValue('BLOCKQUICKORDER_IMG_EXT', 'jpg');

		$this->initialize();
	}

	public function renderForm()
	{
		if (_PS_VERSION_ >= 1.6)
		{
			$fields_form = array(
				'form' => array(
					'legend' => array(
						'title' => $this->l('Display'),
						'icon'  => 'icon-cogs'
					),
					'input'  => array(
						array(
							'type'    => 'switch',
							'label'   => $this->l('Buttons for quantity fields'),
							'name'    => 'qof_quantity',
							'is_bool' => true,
							'desc'    => $this->l('Will be displayed in the form'),
							'values'  => array(
								array(
									'id'    => 'active_on',
									'value' => 1,
									'label' => $this->l('Enabled')
								),
								array(
									'id'    => 'active_off',
									'value' => 0,
									'label' => $this->l('Disabled')
								)
							),
						),
						array(
							'type'    => 'switch',
							'label'   => $this->l('Empty fields'),
							'name'    => 'qof_empty',
							'is_bool' => true,
							'desc'    => $this->l('Will empty fields after add to cart'),
							'values'  => array(
								array(
									'id'    => 'active_on',
									'value' => 1,
									'label' => $this->l('Enabled')
								),
								array(
									'id'    => 'active_off',
									'value' => 0,
									'label' => $this->l('Disabled')
								)
							),
						),
						array(
							'type'  => 'text',
							'label' => $this->l('Title'),
							'name'  => 'qof_title',
							'class' => 'fixed-width-xxl',
							'desc'  => $this->l('Will be displayed with the picture'),
						),
						array(
							'type'  => 'text',
							'label' => $this->l('# Lines'),
							'class' => 'fixed-width-xs',
							'name'  => 'qof_nblines',
							'desc'  => $this->l('x lines will be displayed on loading'),
						),
						array(
							'type'    => 'file',
							'label'   => $this->l('Picture for column block'),
							'name'    => 'qof_img',
							'is_bool' => false,
							'desc'    => $this->l('Will be displayed as 155x163'),
						),
						array(
							'type'   => 'checkbox',
							'label'  => $this->l('Customer groups'),
							'name'   => 'qof_groups',
							'values' => array(
								'query' => Group::getGroups($this->context->language->id),
								'id'    => 'id_group',
								'name'  => 'name',
								'value' => 1
							),
							'desc'   => $this->l('Will access to QuickOrderForm'),
						)
					),
					'submit' => array(
						'title' => $this->l('Save'),
						'class' => 'btn btn-default pull-right')
				),
			);
		}
		else
		{
			$fields_form = array(
				'form' => array(
					'legend' => array(
						'title' => $this->l('Display'),
						'icon'  => 'icon-cogs'
					),
					'input'  => array(
						array(
							'type'    => 'radio',
							'label'   => $this->l('Buttons for quantity fields'),
							'name'    => 'qof_quantity',
							'is_bool' => true,
							'class'   => 't',
							'desc'    => $this->l('Will be displayed in the form'),
							'values'  => array(
								array(
									'id'    => 'active_on',
									'value' => 1,
									'label' => $this->l('Enabled')
								),
								array(
									'id'    => 'active_off',
									'value' => 0,
									'label' => $this->l('Disabled')
								)
							),
						),
						array(
							'type'    => 'radio',
							'label'   => $this->l('Empty fields'),
							'name'    => 'qof_empty',
							'is_bool' => true,
							'class'   => 't',
							'desc'    => $this->l('Will empty fields after add to cart'),
							'values'  => array(
								array(
									'id'    => 'active_on2',
									'value' => 1,
									'label' => $this->l('Enabled')
								),
								array(
									'id'    => 'active_off2',
									'value' => 0,
									'label' => $this->l('Disabled')
								)
							),
						),
						array(
							'type'  => 'text',
							'label' => $this->l('Title'),
							'name'  => 'qof_title',
							'class' => 'fixed-width-xxl',
							'desc'  => $this->l('Will be displayed with the picture'),
						),
						array(
							'type'  => 'text',
							'label' => $this->l('# Lines'),
							'class' => 'fixed-width-xs',
							'name'  => 'qof_nblines',
							'desc'  => $this->l('x lines will be displayed on loading'),
						),
						array(
							'type'    => 'file',
							'label'   => $this->l('Picture for column block'),
							'name'    => 'qof_img',
							'is_bool' => false,
							'desc'    => $this->l('Will be displayed as 155x163'),
						),
						array(
							'type'   => 'checkbox',
							'label'  => $this->l('Customer groups'),
							'name'   => 'qof_groups',
							'values' => array(
								'query' => Group::getGroups($this->context->language->id),
								'id'    => 'id_group',
								'name'  => 'name',
								'value' => 1
							),
							'desc'   => $this->l('Will access to QuickOrderForm'),
						)
					),
					'submit' => array(
						'title' => $this->l('Save'),
						'class' => 'btn btn-default pull-right')
				),
			);
		}


		$helper = new HelperForm();
		$helper->title = $this->l('Quick Order Form');
		$helper->show_toolbar = true;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang =
			Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->toolbar_btn = array(
			'save' => array('href' => '#', 'desc' => $this->l('Save'))
		);

		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitAdvConf';
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
			.'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages'    => $this->context->controller->getLanguages(),
			'id_language'  => $this->context->language->id
		);

		return $helper->generateForm(array($fields_form));
	}

	public function getConfigFieldsValues()
	{
		$values = array(
			'qof_quantity' => Tools::getValue('qof_quantity', Configuration::get('BLOCKQUICKORDER_QUANTITY')),
			'qof_empty'    => Tools::getValue('qof_empty', Configuration::get('BLOCKQUICKORDER_EMPTY')),
			'qof_nblines'  => Tools::getValue('qof_nblines', Configuration::get('BLOCKQUICKORDER_NBLINES')),
			'qof_title'    => Tools::getValue('qof_title', Configuration::get('BLOCKQUICKORDER_TITLE')));

		if (Configuration::get('BLOCKQUICKORDER_GROUPS'))
		{
			$groups = explode('-', Configuration::get('BLOCKQUICKORDER_GROUPS'));
			foreach ($groups as $group)
				$values['qof_groups_'.$group['id_group']] = 1;
		}

		return $values;
	}

	public function hookDisplayHeader()
	{
		if (QuickOrderForm::hasAccess())
		{
			$this->context->controller->addCSS($this->_path.'css/quickorderform.css', 'all');
			$this->context->controller->addCSS(_PS_CSS_DIR_.'jquery.autocomplete.css', 'all');
			$this->context->controller->addJS(_PS_JS_DIR_.'jquery/jquery.autocomplete.js');
		}
	}

	public static function hasAccess()
	{
		$active_groups = explode('-', Configuration::get('BLOCKQUICKORDER_GROUPS'));
		$groups = Customer::getGroupsStatic(Context::getContext()->customer->id);

		foreach ($groups as $group)
			if (in_array($group, $active_groups))
				return true;

		return false;
	}

	public function hookDisplayTop($params)
	{
	    if (QuickOrderForm::hasAccess())
    		{
    			$this->smarty->assign(array('image'     => $this->context->link->protocol_content.$this->qof_img,
    										'qof_title' => $this->qof_title));

    			return $this->display(__FILE__, 'top.tpl');
    		}
	}

	public function hookDisplayRightColumn($params)
	{
		return $this->hookDisplayLeftColumn($params);
	}

	public function hookDisplayLeftColumn()
	{
		if (QuickOrderForm::hasAccess())
		{
			$this->smarty->assign(array('image'     => $this->context->link->protocol_content.$this->qof_img,
										'qof_title' => $this->qof_title));

			return $this->display(__FILE__, 'left-column.tpl');
		}
	}
}