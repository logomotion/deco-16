{capture name=path}{l s='Send the Quote' mod='quote'}{/capture}
<div class="row">
	<div id="center_column" class="center_column col-xs-12 col-sm-12">
		<h1 class="page-heading">{l s='Order summary' mod='quote'}</h1>
		
		{assign var='current_step' value='payment'}
		{include file="$tpl_dir./order-steps.tpl"}
		
		{if $nbProducts <= 0}
			<p class="warning">{l s='Your shopping cart is empty.'}</p>
		{else}
		
		<div class="box">
			<h3 class="page-subheading">{l s='Send the Quote' mod='quote'}</h3>
			<form action="{$this_path_ssl}validation.php" method="post">
			<p>
				<strong class="dark">{l s='You have chosen to send  the Quote.' mod='quote'}</strong>
			</p>
			<p>{l s='Here is a short summary of your order:' mod='quote'}</p>
			<p>
				- {l s='The total amount of your order is' mod='quote'}
				<span id="amount" class="price">{displayPrice price=$total}</span>
				{l s='(tax incl.)' mod='quote'}
			</p>
			<p> - {l s='Please confirm your order by clicking \'I confirm my order\'' mod='quote'}.</p>
		</div>
		 
		<p class="cart_navigation clearfix" id="cart_navigation">
			<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html':'UTF-8'}" class="btn btn-default icon-left" title="{l s='Other payment methods' mod='cheque'}">
                {l s='Other payment methods' mod='quote'}
			</a>
			<button type="submit" class="btn btn-default btn-md icon-right">
				<span>
                	{l s='I confirm my order' mod='quote'}
                </span>
			</button>
		</p>
		
		</form>
		{/if}
		</div>
</div>
