<!-- Block user information module NAV  -->
<div class="user_info-block-top">
    <div{if $is_logged} class="current"{/if}>
         {if $is_logged}
            <span>{$cookie->customer_firstname|truncate:2:'.'|escape:'html':'UTF-8'} {$cookie->customer_lastname}</span>
         {else}
          <span>
            <a class="login" href="{$link->getPageLink('my-account', true)|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Login to your customer account' mod='blockuserinfo'}">
                {l s='Sign in' mod='blockuserinfo'}
            </a>
        </span>
        {/if}
    </div>
    {if $is_logged}
    <ul class="toogle_content">
        <li>
            <span>
                <a class="logout" href="{$link->getPageLink('index', true, NULL, "mylogout")|escape:'html':'UTF-8'}" rel="nofollow" title="{l s='Log me out' mod='blockuserinfo'}">
                    {l s='Sign out' mod='blockuserinfo'}
                </a>
            </span>
        </li>
        <li>
            <span>
                <a href="{$link->getPageLink('my-account', true)}" title="{l s='View my customer account' mod='blockuserinfo'}" rel="nofollow">
                    {l s='Your Account' mod='blockuserinfo'}
                </a>
            </span>
        </li>
    </ul>
    {/if}
</div>
<!-- /Block usmodule NAV -->
