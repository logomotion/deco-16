{if isset($htmlitems) && $htmlitems}
    {assign var='hookName' value={$hook|escape:'htmlall':'UTF-8'}}
    <div id="htmlcontent_{$hookName}">
        <ul class="htmlcontent-home clearfix row">
            <li class="home-spinner-desc">{l s='Artistic Masterpieces'}</li>
            <li class="home-spinner-slider">
                <img src="/img/360-home.jpg" width="420" height="340" alt="" class="reel" data-image="/img/360-home-sprite.jpg" data-frames="72" data-footage="72" data-delay="2" data-responsive="true" data-start="1" data-speed="0.2">
            </li>
            {foreach name=items from=$htmlitems item=hItem}
                <li class="htmlcontent-item-{$smarty.foreach.items.iteration|escape:'htmlall':'UTF-8'}
                {if $hookName =='home'} col-xs-4{elseif $hookName =='footer'} {/if} 
                {if $hookName =='top'}{if $smarty.foreach.items.index == 0}col-xs-12 {else}col-xs-4{/if}{/if} ">
                    {if $hItem.url}
                        <a href="{$hItem.url|escape:'htmlall':'UTF-8'}" class="item-link"{if $hItem.target == 1} onclick="return !window.open(this.href);"{/if} title="{$hItem.title|escape:'htmlall':'UTF-8'}">
                    {/if}
                        {if $hItem.image}
                            <img src="{$link->getMediaLink("`$module_dir`img/`$hItem.image`")}" class="item-img" title="{$hItem.title|escape:'htmlall':'UTF-8'}" alt="{$hItem.title|escape:'htmlall':'UTF-8'}" width="{if $hItem.image_w}{$hItem.image_w|intval}{else}100%{/if}" height="{if $hItem.image_h}{$hItem.image_h|intval}{else}100%{/if}"/>
                        {/if}
                        {if $hItem.title && $hItem.title_use == 1}
                            <h3 class="item-title">{$hItem.title|escape:'htmlall':'UTF-8'}</h3>
                        {/if}
                        {if $hItem.html}
                            <div class="item-html">
                                {$hItem.html}
                            </div>
                        {/if}
                    {if $hItem.url}
                        </a>
                    {/if}
                </li>
            {/foreach}
        </ul>
    </div>
{/if}
