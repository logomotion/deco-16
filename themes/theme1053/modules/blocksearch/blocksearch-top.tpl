<!-- Block search module TOP -->
<div id="search_block_top">
	<form id="searchbox" method="get" action="{$link->getPageLink('search')|escape:'html':'UTF-8'}" >
		<input type="hidden" name="controller" value="search" />
		<input type="hidden" name="orderby" value="position" />
		<input type="hidden" name="orderway" value="desc" />
		<input class="search_query form-control" type="text" id="search_query_top" name="search_query" placeholder="{l s='Search' mod='blocksearch'}" value="{$search_query|escape:'htmlall':'UTF-8'|stripslashes}" />
		<button type="submit" name="submit_search" class="btn btn-default button-search">
			<span>{l s='Search' mod='blocksearch'}</span>
		</button>
	</form>
	<p class="top-contact">{l s='Call us' mod='blocksearch'} : <b>+33 (0)3 80 56 42 38</b></p>
	<p class="top-contact">E-mail :<a href="mailto:%63%6f%6e%74%61%63%74@%64%65%63%6f-%72%65%6c%69%65%66.%66%72"><b>contact@deco-relief.fr</b></a></p>
</div>
<!-- /Block search module TOP -->