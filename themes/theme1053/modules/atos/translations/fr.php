<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{atos}theme1053>atos_c8529ccddc224398922bcf29406c1b34'] = 'Atos';
$_MODULE['<{atos}theme1053>atos_d7c1e1da7e8d6ec700948769ecc5c67d'] = 'Module de paiement Atos SIPS';
$_MODULE['<{atos}theme1053>atos_2eb5abf818f329cdbd7b4585f0e09f63'] = 'Vous devez remplir le ID Commerçant avant de charger votre certificat';
$_MODULE['<{atos}theme1053>atos_4ed2e364121fec36663e42e2f8da10d7'] = 'Certificat de mise à jour';
$_MODULE['<{atos}theme1053>atos_7bfb74523cb51b2fc535756dc486e7d7'] = 'Erreur lors de la copie du certificat';
$_MODULE['<{atos}theme1053>atos_cb25441abb5f56c14d7c981aa29c2651'] = 'Vous devez remplir le ID Commerçant avant de pouvoir choisir votre banque';
$_MODULE['<{atos}theme1053>atos_092e44f497864b31374425a369e81090'] = 'Merci de mettre une adresse e-mail valide';
$_MODULE['<{atos}theme1053>atos_c888438d14855d7d96a2724ee9c306bd'] = 'Mise à jour réussie';
$_MODULE['<{atos}theme1053>atos_7ee46f979a84cdbfefdc0459f7ba03f8'] = 'Impossible de créer pathfile';
$_MODULE['<{atos}theme1053>atos_f98b49d8f68633bc31bb226235edb628'] = 'Impossible de créer parmcom';
$_MODULE['<{atos}theme1053>atos_75e207eed8452d13ebf1c661536ada4c'] = 'Votre banque a été sélectionné';
$_MODULE['<{atos}theme1053>atos_9ccd49aa503036139ee48e336fb228a6'] = 'Erreur Atos : impossible d\'exécuter le binaire';
$_MODULE['<{atos}theme1053>atos_263c1c536f5d6a7096a1477583df8e71'] = 'Erreur Atos :';
$_MODULE['<{atos}theme1053>atos_1bd7950e049d2adaed38585d79546c66'] = 'Erreur Atos : impossible d\'exécuter la requête';
$_MODULE['<{atos}theme1053>validation_f9093a540cd85a58d11d1a5d50a70f9e'] = 'Notification';
$_MODULE['<{atos}theme1053>validation_fa947589647308787c87b9de6b53cecc'] = 'exécution du module Atos impossible (fichier request)';
$_MODULE['<{atos}theme1053>validation_4d6f86b67e4b27cdf8a337aa898b1272'] = 'erreur Atos :';
$_MODULE['<{atos}theme1053>validation_57d5b6ac7b2a08b1d26f60e02fa6da51'] = 'Identifiant de transaction :';
$_MODULE['<{atos}theme1053>validation_6893c11c0d6e123d79fe34c5e5f80553'] = 'Moyen de paiement :';
$_MODULE['<{atos}theme1053>validation_b5b9fc7e87145d3b413c953d47eed33a'] = 'Paiement débuté à';
$_MODULE['<{atos}theme1053>validation_50916fd9ae82c9f780bb525273f1f75f'] = 'Paiement reçu à';
$_MODULE['<{atos}theme1053>validation_511e2806c2d06d90982867948dd0362c'] = 'Authorisation n°';
$_MODULE['<{atos}theme1053>validation_77295c7d814e7397c55f64ec06313984'] = 'Devise';
$_MODULE['<{atos}theme1053>validation_ada34f3d104b11150cd0374709bd68fd'] = 'Adresse IP du client :';
$_MODULE['<{atos}theme1053>validation_0d698fffffee7ffeb5d468acdf03c71c'] = 'ID panier:';
$_MODULE['<{atos}theme1053>validation_180aefaca118cec52c4b60350c20e7ea'] = 'Atos Total :';
$_MODULE['<{atos}theme1053>validation_06d8daf49727770af2481e3e180a44e7'] = 'Version d\'Atos :';
$_MODULE['<{atos}theme1053>validation_2bc35addf2334f3ab299e080ff6d7840'] = 'devrait être';
$_MODULE['<{atos}theme1053>validation_c70977d700b54b743479a6776f13b83e'] = 'Identifiant marchand invalide';
$_MODULE['<{atos}theme1053>validation_66adeff140c48aac3a699fb1490d63dc'] = 'Devise incorrecte (devrait être ';
$_MODULE['<{atos}theme1053>validation_107be4d407ce56b56a7dbc6f24fe1a15'] = 'Identifiant marchand invalide';
$_MODULE['<{atos}theme1053>validation_af5a810dabf78fd5255eb3ea7e4384be'] = 'La banque a rejeté le paiement';
$_MODULE['<{atos}theme1053>validation_058d2d9fde6b1fb1ff3d290875a6933d'] = 'Format incorrect';
$_MODULE['<{atos}theme1053>validation_dea375f0d8f3a2f27d97b5c772431681'] = 'La banque indique que la transaction peut être frauduleuse';
$_MODULE['<{atos}theme1053>validation_639e53d8342f059349b8cbc6758db24c'] = 'Le client a epuisé ses tentatives de paiement';
$_MODULE['<{atos}theme1053>validation_51862b3dfdeff86953c9590ab1c5667d'] = 'Service de paiement indisponible';
$_MODULE['<{atos}theme1053>configuration_b4e2b4f8d80ba6e4a336cfb641aa0ac3'] = 'Vous avez une mauvaise configuration';
$_MODULE['<{atos}theme1053>configuration_41c620e6cb3a13259d54e7285fa3302f'] = 'safe_mode est activé, contactez votre hébergeur.';
$_MODULE['<{atos}theme1053>configuration_8a9e9a044b481d5598f82d7a661a9425'] = 'la fonction exec() est interdit, contactez votre hébergeur.';
$_MODULE['<{atos}theme1053>configuration_4c54df2f4334953ab6e6097cc283fc55'] = 'n\'est pas exécutable, contactez votre hébergeur';
$_MODULE['<{atos}theme1053>configuration_5dd2b962a8bdeb455cbe97ce688e87dc'] = 'Votre boutique est en maintenance. Votre banque ne pourra pas accéder au fichier de validation des commandes.';
$_MODULE['<{atos}theme1053>configuration_5b6cf869265c13af8566f192b4ab3d2a'] = 'Documentation';
$_MODULE['<{atos}theme1053>configuration_254f642527b45bc260048e30704edb39'] = 'Configuration';
$_MODULE['<{atos}theme1053>configuration_bbaff12800505b22a853e8b7f4eb6a22'] = 'Contact';
$_MODULE['<{atos}theme1053>header_10fbc567758bc1bec3c8aadf6519894b'] = 'Configurez votre Atos';
$_MODULE['<{atos}theme1053>header_f1206f9fadc5ce41694f69129aecac26'] = 'Configurez';
$_MODULE['<{atos}theme1053>header_bcfaccebf745acfd5e75351095a5394a'] = 'Désactiver';
$_MODULE['<{atos}theme1053>header_2faec1f9f8cc7f8f40d521c4dd574f49'] = 'Activer';
$_MODULE['<{atos}theme1053>header_a27dfe771799a09fd55fea73286eb6ab'] = 'Désinstaller';
$_MODULE['<{atos}theme1053>header_526d688f37a86d3c3f27d0c5016eb71d'] = 'Réinitialiser';
$_MODULE['<{atos}theme1053>header_53103fcc4656f55c219b600ded3c7438'] = 'Points d\'accroche';
$_MODULE['<{atos}theme1053>header_0557fa923dcee4d0f86b1409f5c2167f'] = 'Retour';
$_MODULE['<{atos}theme1053>translations_1311e5ddedf25a4f6cd6059cbf04c21e'] = 'Gérer les traductions';
$_MODULE['<{atos}theme1053>conf_254f642527b45bc260048e30704edb39'] = 'Configuration';
$_MODULE['<{atos}theme1053>conf_f4c5e691601b8ae2af407683d6319ff4'] = 'Configuration de votre ID Commerçant';
$_MODULE['<{atos}theme1053>conf_229a7ec501323b94db7ff3157a7623c9'] = 'ID Commerçant';
$_MODULE['<{atos}theme1053>conf_c9cc8cce247e49bae79f15173ce97354'] = 'Sauvegarder';
$_MODULE['<{atos}theme1053>conf_d7508770aa22ec47274ecdbfcc042f3f'] = 'Téléchargez votre certificat';
$_MODULE['<{atos}theme1053>conf_6c5fc8cc9a9d7d265d67f0b2f1a4a48b'] = 'Certificat';
$_MODULE['<{atos}theme1053>conf_eb0f48a107df1a0f343d4cd513b555e6'] = 'Certificat';
$_MODULE['<{atos}theme1053>conf_13dd6c46d01e7b7ee9a8f033a5bb3549'] = 'Configuration de votre Banque';
$_MODULE['<{atos}theme1053>conf_030e8378cd8ec5104559875a0f66a1ea'] = 'Activer';
$_MODULE['<{atos}theme1053>conf_183620fe64a642f3f366990b89707ac2'] = 'Désactiver';
$_MODULE['<{atos}theme1053>conf_10178fd56b6572d58d85bd884ba949e8'] = 'Configuration de vos options';
$_MODULE['<{atos}theme1053>conf_b447848808d90545c1302579a79f503a'] = 'Redirection après paiement';
$_MODULE['<{atos}theme1053>conf_953fe01a902813ca1db664e9a7dd7249'] = 'Vers votre boutique';
$_MODULE['<{atos}theme1053>conf_32c92fa350d9d5ba8f2cea975eefbb68'] = 'Vers la page de confirmation d\'Atos';
$_MODULE['<{atos}theme1053>conf_03cd96fbbda0f8411f55e5ddc488c29c'] = 'Comportement des erreurs de paiement';
$_MODULE['<{atos}theme1053>conf_25d0ac473ce89d3ee421a98768b2d903'] = 'Enregistrer la commande comme une erreur de paiement';
$_MODULE['<{atos}theme1053>conf_6ab13883b6200d3a2d2dd3f4e9ca2891'] = 'Envoyez-moi un e-mail';
$_MODULE['<{atos}theme1053>conf_cd88ce600576fcc630878b5e374835a5'] = 'Ne rien faire';
$_MODULE['<{atos}theme1053>conf_e480686f42911546de1f314c272014e8'] = 'Notification par e-mail ';
$_MODULE['<{atos}theme1053>conf_9f8cb005f3d8218d5ac44a663923ae7a'] = 'Votre e-mail';
$_MODULE['<{atos}theme1053>conf_b5050affc000abd8eeeca19be0645866'] = 'Activer Paylib';
$_MODULE['<{atos}theme1053>conf_d18592b67ca82bf897286d0b737cfc96'] = 'Vérifiez auprès de votre banque si vous pouvez activer cette option';
$_MODULE['<{atos}theme1053>conf_816c23cabb09b8f4b62b303a99e5c42a'] = 'Activer AMEX';
$_MODULE['<{atos}theme1053>conf_5feff954236c661b891538c270e1281c'] = 'Personnalisation';
$_MODULE['<{atos}theme1053>conf_cc04a45e9d671b3c27e7b0d1a21fdb5e'] = 'Pour activer l\'option de personnalisation de la page de Paiement, vous devez d\'abord envoyer un mail avec votre \"ID Commerçant\" ainsi que l\'URL de votre site  à votre banque sans oublier de joindre votre fichier \"custom_tpl\" ainsi que les images qui l\'accomagne';
$_MODULE['<{atos}theme1053>conf_16dd73a38883e6d06b007ca90a696606'] = 'Certaines banques vous enverront un email de confirmation pour vous préciser la date à laquelle le Template sera installé, d\'autres banques vous contacteront seulement en cas d\'échec de l\'installation du Template.';
$_MODULE['<{atos}theme1053>conf_003fdbd373bdfb6f2babe3b7dcdc8419'] = 'Une fois que le Template est installé sur le serveur de votre banque vous pourrez activer cette option. ';
$_MODULE['<{atos}theme1053>contact_a3fa607b49ad809e8ca8f58abe958102'] = 'Merci d\'avoir choisi un module développé par l\'équipe Addons de PrestaShop.';
$_MODULE['<{atos}theme1053>contact_7b74161261a4ae2dd6c5ecc60fe1279f'] = 'Vous souhaitez confier l’installation de votre module à un développeur PrestaShop ? Nos experts s’occupent de tout !';
$_MODULE['<{atos}theme1053>contact_936ccdb97115e9f35a11d35e3d5b5cad'] = 'Cliquez ici';
$_MODULE['<{atos}theme1053>contact_9886df5ae168c20c3926e7e9f41adb45'] = 'Si vous rencontrez un problème lors de l\'utilisation de ce module, notre équipe est à votre service via le';
$_MODULE['<{atos}theme1053>contact_23372c0d3713719764670087006fc1b6'] = 'formulaire de contact';
$_MODULE['<{atos}theme1053>contact_7b3c61e9446620d8aa80f473a661254f'] = 'Pour gagner du temps, avant de nous contacter :';
$_MODULE['<{atos}theme1053>contact_47fad5151819f320629572659a9bb648'] = 'Assurez-vous que vous avez bien lu la documentation.';
$_MODULE['<{atos}theme1053>contact_da771cda19f288952ad7816d268d2753'] = 'Dans le cas où vous nous contactez via le formulaire, n\'hésitez pas à nous transmettre votre premier message, avec un maximum de détails sur le problème et son origine (captures d\'écrans, reproduire les actions pour trouver le bug,...)';
$_MODULE['<{atos}theme1053>contact_1b9441f293cd7307b59296f616bb858a'] = 'Ce module a été développé par PrestaShop et ne peut être vendu que sur ';
$_MODULE['<{atos}theme1053>documentation_5b6cf869265c13af8566f192b4ab3d2a'] = 'Documentation';
$_MODULE['<{atos}theme1053>documentation_fb8f41889792f184c2e50cdefb47c94a'] = 'Le module Atos (Worldline) est une solution de paiement à distance pour permettre aux clients d\'utiliser toutes ses fonctionnalités sur tous les canaux de vente à distance.';
$_MODULE['<{atos}theme1053>documentation_f52a0a03cde669652b6278ab9a7492ee'] = 'Conçu pour fournir un niveau de sécurité très élevé.';
$_MODULE['<{atos}theme1053>documentation_abb83f23ae40c258ca77e1ec60fdca87'] = 'PayLib et 3D Secure inclus.';
$_MODULE['<{atos}theme1053>documentation_b042fffc54e4c3b35332ae26c214cffc'] = 'En pièce jointe, vous trouverez la documentation pour ce module. N\'hésitez pas à la consulter afin de configurer correctement le module.';
$_MODULE['<{atos}theme1053>documentation_52d694a25184ecd9cd6c3474123ff84a'] = 'Guide d\'intégration d\'Atos (uniquement en français)';
$_MODULE['<{atos}theme1053>documentation_efef4e9130a9ba602a62de8cec5eb938'] = 'Guide de personnalisation d\'Atos  (uniquement en français)';
$_MODULE['<{atos}theme1053>documentation_91dd73dcc51182410b2533f548926fa6'] = 'Accès à la documentation gratuite de PrestaShop :';
$_MODULE['<{atos}theme1053>documentation_936ccdb97115e9f35a11d35e3d5b5cad'] = 'Cliquez ici';
$_MODULE['<{atos}theme1053>documentation_8a2b846b44e12930dbf6ef37d3b63590'] = 'Besoin d\'aide ? Veuillez aller sur ';
$_MODULE['<{atos}theme1053>documentation_4bd9ef088a14b5f6fd9ef86e6f33037f'] = 'Besoin d\'aide ? ';
$_MODULE['<{atos}theme1053>documentation_d8fe2675b1d7b2e812e118360a8663b5'] = 'l\'onglet Contact';
$_MODULE['<{atos}theme1053>hookorderconfirmation_2e2117b7c81aa9ea6931641ea2c6499f'] = 'Votre commande sur';
$_MODULE['<{atos}theme1053>hookorderconfirmation_75fbf512d744977d62599cc3f0ae2bb4'] = 'est terminée.';
$_MODULE['<{atos}theme1053>hookorderconfirmation_30163d8fc3068e8297e7ab5bf32aec87'] = 'Votre commande vous sera expédiée dès que possible.';
$_MODULE['<{atos}theme1053>hookorderconfirmation_0db71da7150c27142eef9d22b843b4a9'] = 'Pour toute question complémentaire, n\'hésitez pas à contacter notre';
$_MODULE['<{atos}theme1053>hookorderconfirmation_64430ad2835be8ad60c59e7d44e4b0b1'] = 'support client';
$_MODULE['<{atos}theme1053>hookorderconfirmation_8de637e24570c1edb0357826a2ad5aea'] = 'Un problème est survenu avec votre commande. Si vous pensez que c\'est une erreur, veuillez contacter notre';
$_MODULE['<{atos}theme1053>payment_d7016236b3853e7b5a68adec30f537b4'] = 'Payer par carte bancaire';
$_MODULE['<{atos}theme1053>payment_8de6f0c3f79d1a4d8d7cb5c93e904549'] = 'Le total de votre commande doit être supérieur à';
$_MODULE['<{atos}theme1053>payment_e75a63937e96a4f0240d17d9f5429455'] = 'pour payer par carte bancaire';
