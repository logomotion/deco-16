<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{socialsharing}theme1053>socialsharing_7dba9afe5ed3bbd1683c2c1a9e168cab'] = 'Partage sur les réseaux sociaux';
$_MODULE['<{socialsharing}theme1053>socialsharing_622e2c510964bb1eb4e2e721ec6148c5'] = 'Affiche des boutons de partage sur les réseaux sociaux (Twitter, Facebook, Google+ et Pinterest) sur chaque page produit.';
$_MODULE['<{socialsharing}theme1053>socialsharing_c888438d14855d7d96a2724ee9c306bd'] = 'Mise à jour réussie';
$_MODULE['<{socialsharing}theme1053>socialsharing_00d23a76e43b46dae9ec7aa9dcbebb32'] = 'Activé';
$_MODULE['<{socialsharing}theme1053>socialsharing_b9f5c797ebbf55adccdd8539a65a0241'] = 'Désactivé';
$_MODULE['<{socialsharing}theme1053>socialsharing_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{socialsharing}theme1053>socialsharing_368140d2caf6b43106964fd615756d1b'] = 'Comparaison de produits';
$_MODULE['<{socialsharing}theme1053>socialsharing_compare_6e55f5917f1f34501fc37388eb4b780d'] = 'Partager cette comparaison avec vos amis :';
$_MODULE['<{socialsharing}theme1053>socialsharing_compare_3746d4aa96f19672260a424d76729dd1'] = 'Tweet';
$_MODULE['<{socialsharing}theme1053>socialsharing_compare_5a95a425f74314a96f13a2f136992178'] = 'Partager';
$_MODULE['<{socialsharing}theme1053>socialsharing_compare_5b2c8bfd1bc974966209b7be1cb51a72'] = 'Google+';
$_MODULE['<{socialsharing}theme1053>socialsharing_compare_86709a608bd914b28221164e6680ebf7'] = 'Pinterest';
$_MODULE['<{socialsharing}theme1053>socialsharing_3746d4aa96f19672260a424d76729dd1'] = 'Tweet';
$_MODULE['<{socialsharing}theme1053>socialsharing_5a95a425f74314a96f13a2f136992178'] = 'Partager';
$_MODULE['<{socialsharing}theme1053>socialsharing_5b2c8bfd1bc974966209b7be1cb51a72'] = 'Google+';
$_MODULE['<{socialsharing}theme1053>socialsharing_86709a608bd914b28221164e6680ebf7'] = 'Pinterest';
$_MODULE['<{socialsharing}theme1053>socialsharing_2107f6398c37b4b9ee1e1b5afb5d3b2a'] = 'Envoyer à un ami';
$_MODULE['<{socialsharing}theme1053>socialsharing_5d6103b662f41b07e10687f03aca8fdc'] = 'Destinataire';
$_MODULE['<{socialsharing}theme1053>socialsharing_bb6aa0be8236a10e6d3b315ebd5f2547'] = 'Nom de votre ami';
$_MODULE['<{socialsharing}theme1053>socialsharing_099bc8914b5be9e522a29e48cb3c01c4'] = 'Adresse e-mail de votre ami';
$_MODULE['<{socialsharing}theme1053>socialsharing_70397c4b252a5168c5ec003931cea215'] = 'Champs requis';
$_MODULE['<{socialsharing}theme1053>socialsharing_94966d90747b97d1f0f206c98a8b1ac3'] = 'Envoyer';
$_MODULE['<{socialsharing}theme1053>socialsharing_e81c4e4f2b7b93b481e13a8553c2ae1b'] = 'OU';
$_MODULE['<{socialsharing}theme1053>socialsharing_ea4788705e6873b424c65e91c2846b19'] = 'Annuler';
