{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{if $PS_SC_TWITTER || $PS_SC_FACEBOOK || $PS_SC_GOOGLE || $PS_SC_PINTEREST}

	<p class="socialsharing_product list-inline no-print">
		{if $PS_SC_TWITTER}
			<button data-type="twitter" type="button" class="btn btn-default btn-twitter social-sharing" title="twitter">
				<i class="fa fa-twitter"></i>
				<!-- <img src="{$link->getMediaLink("`$module_dir`img/twitter.gif")}" alt="Tweet" /> -->
			</button>
		{/if}
		{if $PS_SC_FACEBOOK}
			<button data-type="facebook" type="button" class="btn btn-default btn-facebook social-sharing" title="facebook">
				<i class="fa fa-facebook"></i>
				<!-- <img src="{$link->getMediaLink("`$module_dir`img/facebook.gif")}" alt="Facebook Like" /> -->
			</button>
		{/if}
		{if $PS_SC_GOOGLE}
			<button data-type="google-plus" type="button" class="btn btn-default btn-google-plus social-sharing" title="google plus">
				<i class="fa fa-google-plus"></i>
				<!-- <img src="{$link->getMediaLink("`$module_dir`img/google.gif")}" alt="Google Plus" /> -->
			</button>
		{/if}
		{if $PS_SC_PINTEREST}
			<button data-type="pinterest" type="button" class="btn btn-default btn-pinterest social-sharing" title="pinterest">
				<i class="fa fa-pinterest"></i>
				<!-- <img src="{$link->getMediaLink("`$module_dir`img/pinterest.gif")}" alt="Pinterest" /> -->
			</button>
		{/if}
		<a class="btn btn-default social-sharing" id="send_friend_button" href="#send_friend_form" title="email"><i class="fa fa-envelope"></i></a>
		<div style="display: none;">
			<div id="send_friend_form">
				<h2  class="page-subheading">
					{l s='Send to a friend' mod='socialsharing'}
				</h2>
				<div class="row">
					<div class="product clearfix col-xs-12 col-sm-6">
						<img src="{$link->getImageLink($stf_product->link_rewrite, $stf_product_cover, 'home_default')|escape:'html':'UTF-8'}" alt="{$stf_product->name|escape:'html':'UTF-8'}" />
						<div class="product_desc">
							<p class="product_name">
								<strong>{$stf_product->name}</strong>
							</p>
							{$stf_product->description_short}
						</div>
					</div><!-- .product -->
					<div class="send_friend_form_content col-xs-12 col-sm-6" id="send_friend_form_content">
						<div id="send_friend_form_error"></div>
						<div id="send_friend_form_success"></div>
						<div class="form_container">
							<p class="intro_form">
								{l s='Recipient' mod='socialsharing'} :
							</p>
							<p class="text">
								<label for="friend_name">
									{l s='Name of your friend' mod='socialsharing'} <sup class="required">*</sup> :
								</label>
								<input id="friend_name" name="friend_name" type="text" value=""/>
							</p>
							<p class="text">
								<label for="friend_email">
									{l s='E-mail address of your friend' mod='socialsharing'} <sup class="required">*</sup> :
								</label>
								<input id="friend_email" name="friend_email" type="text" value=""/>
							</p>
							<p class="txt_required">
								<sup class="required">*</sup> {l s='Required fields' mod='socialsharing'}
							</p>
						</div>
						<p class="submit">
							<button id="sendEmail" class="btn btn-sm" name="sendEmail" type="submit">
								<span>{l s='Send' mod='socialsharing'}</span>
							</button>&nbsp;
							{l s='or' mod='socialsharing'}&nbsp;
							<a class="closefb" href="#" title="{l s='Cancel' mod='socialsharing'}">
								{l s='Cancel' mod='socialsharing'}
							</a>
						</p>
					</div> <!-- .send_friend_form_content -->
				</div>
			</div>
		</div>
	</p>
{/if}
