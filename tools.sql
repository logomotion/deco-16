# Export products (Presta 1.4)
SELECT group_concat(c.id_category SEPARATOR ',') as categories , l.description, l.description_short, l.link_rewrite, l.meta_description, l.meta_keywords, l.meta_title, l.name, p.id_product, p.id_supplier, p.id_manufacturer, p.id_tax_rules_group, p.id_category_default, p.id_color_default, p.on_sale, p.online_only, p.ean13, p.upc, p.ecotax, p.quantity, p.minimal_quantity, p.price, p.wholesale_price, p.unity, p.unit_price_ratio, p.additional_shipping_cost, p.reference, p.supplier_reference, p.location, p.width, p.height, p.depth, p.weight, p.out_of_stock, p.quantity_discount, p.customizable, p.uploadable_files, p.text_fields, p.active, p.available_for_order, p.condition, p.show_price, p.indexed, p.cache_is_pack, p.cache_has_attachments, p.cache_default_attribute, p.date_add, p.date_upd
FROM ps_product p 
JOIN ps_product_lang l ON l.id_product=p.id_product
JOIN ps_category_product c ON c.id_product=p.id_product
 WHERE l.id_lang=1 group by p.id_product
# Export categories (Presta 1.4)
SELECT (l.id_category + 1) as id_category, l.name, l.description, l.link_rewrite, l.meta_title, l.meta_keywords, l.meta_description, (c.id_parent + 1) as id_parent, c.active
FROM ps_category_lang l
JOIN ps_category c ON c.id_category=l.id_category
WHERE l.id_lang=1 AND c.id_category>1