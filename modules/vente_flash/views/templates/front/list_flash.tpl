{capture name=path}{l s='Flash sales' mod='vente_flash'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}
{include file="$tpl_dir./errors.tpl"}

<ul id="product_list" class="clear">
    {foreach from=$sale key=myId item=i}
        <li class="ajax_block_product clearfix" style="min-height:40px;">
            <div class="center_block">
                <h3>{l s='Sale flash' mod='vente_flash'} : {$i.flash_name|escape:'html'}</h3>
                <p class="product_desc" style="float:left;">{l s='From' mod='vente_flash'} <b>{$i.date_start|escape:'html'}</b> {l s='To' mod='vente_flash'} <b>{$i.date_end|escape:'html'}</b></p>
               {* <!-- <div id="timerBlock"  class="timerBlock imgTime{$lang}" style="color:{$font_color}; background-color:{$bg_color}; font-family:{$font_style};" ></div> --> *}
            </div>
            <a class="button" href="{$link_module|escape:'html'}&id={$i.id_flash|escape:'html'}" style="float: right; margin-top: 7px;">{l s='See Products' mod='vente_flash'}</a>
        </li>
        {foreachelse}
        <h3>{l s='These is no flash sale actually' mod='vente_flash'}</h3>
    {/foreach}
</ul>
