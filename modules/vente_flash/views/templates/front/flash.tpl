{capture name=path}<a href="flashSale.php">{l s='Flash sales' mod='vente_flash'}</a> {$navigationPipe|escape:html:'UTF-8'} <b>{$name|escape:'html'}</b> {/capture}
    {literal}
    <script type="text/javascript">
    $(document).ready(function(){
        timerShowProduct();
    });
    </script>
    {/literal}
{if $message}<h3 style="text-align:center; color:{$font_color|escape:'html'}; font-family:{$font_style|escape:'html'};">{l s='Quick, only' mod='vente_flash'} :</h3>{/if}
<div id="timeProduct" style="color:{$font_color|escape:'html'}; font-family:{$font_style|escape:'html'};text-align:center;font-size:18px;"></div>
<div class="clear">&nbsp;</div>
{if $products}
	{include file="$tpl_dirr./product-sort.tpl"}
	{include file="$tpl_dirr./product-list.tpl" products=$products}
	{include file="$tpl_dirr./pagination.tpl"}
{/if}