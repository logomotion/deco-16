<?php
require(dirname(__FILE__).'../../../config/config.inc.php');
require ('vente_flash.php');
$select = '
	SELECT COUNT(*) count, id_flash id
	FROM `'._DB_PREFIX_.'flash` f';
$sale = Db::getInstance()->ExecuteS($select);


$controller = new FrontController();
$controller->init();


if ($sale[0]["count"] == 1 || Tools::getValue("id"))
	Tools::redirect(Context::getContext()->link->getModuleLink('vente_flash', 'show', array("id" => Tools::getValue("id"))));
else
	Tools::redirect(Context::getContext()->link->getModuleLink('vente_flash', 'list'));