<?php
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');
include(dirname(__FILE__).'/../../images.inc.php');

// We check if it's a hack attempt (direct call to the file)
$id_shop = Tools::getValue('id_shop');
if ($id_shop == "") die("Hack attempt");

$tokenOK = Configuration::get('VF_TOKEN', null, null, Tools::getValue('id_shop'));
if (Tools::getValue('tokenVF') != $tokenOK)
	die("Hack attempt");

if (Tools::getValue('action') == 'add')
{
	$info = verifForm();
	add_flash($info["products"], $info["name"], $info["date_start"], $info["date_end"], $info["remise"]);
}

if (Tools::getValue('action') == 'actFlash')
{
	$update = '
	UPDATE `'._DB_PREFIX_.'flash` SET `statut` = 1
	WHERE `id_flash` = '.  intval(Tools::getValue('id'));
	if (!(Db::getInstance()->Execute($update)))
		die("error,12");
	$select = '
	SELECT `id_product`,`discount`,`discount_type`,`date_start`, `date_end`
	FROM `'._DB_PREFIX_.'flash` f
	INNER JOIN `'._DB_PREFIX_.'flash_assoc` fa ON (f.`id_flash` = fa.`id_flash`)
	WHERE fa.`id_flash` = '.  intval(Tools::getValue('id'));
	if (!($products = Db::getInstance()->ExecuteS($select)))
		die("error,7");
	foreach ($products as $product) {
		reduction($product["id_product"], $product["discount"], $product["discount_type"], $product["date_start"], $product["date_end"]);
	}
	die("valid,11");
}

if (Tools::getValue('action') == 'update')
{
	$id = Tools::getValue('modif');
	$info = verifForm($id);
	update_flash($info["products"], $info["name"], $info["date_start"], $info["date_end"], $info["remise"]);
}

if (Tools::getValue('action') == 'delFlash')
{
	$update = '
	UPDATE `'._DB_PREFIX_.'flash` SET `statut` = 0
	WHERE `id_flash` = '.  intval(Tools::getValue('id'));
	if (!(Db::getInstance()->Execute($update)))
		die("error,9");
	delDiscount(Tools::getValue('id'));
	die("valid,10");
}

if (Tools::getValue('action') == 'deleteFlash')
{
	$languages = Language::getLanguages(true);
	if(isset($_POST["id"]))
	{
		foreach ($_POST["id"] as $id)
		{
			delDiscount($id);
			$select = Db::getInstance()->ExecuteS('
			SELECT `id_product`
			FROM `'._DB_PREFIX_.'flash_assoc`
			WHERE `id_flash` = '.(int)$id);
			$delete = '
			DELETE FROM `'._DB_PREFIX_.'flash_assoc`
			WHERE `id_flash` = '.(int)$id;
			if (!(Db::getInstance()->Execute($delete)))
				die("error,14");
			$delete = '
			DELETE FROM `'._DB_PREFIX_.'flash_lang`
			WHERE `id_flash` = '.(int)$id;
			if (!(Db::getInstance()->Execute($delete)))
				die("error,14");
			$delete = '
			DELETE FROM `'._DB_PREFIX_.'flash`
			WHERE `id_flash` = '.(int)$id;
			if (!(Db::getInstance()->Execute($delete)))
				die("error,14");
			
			foreach ($languages AS $language)
			{
				foreach (Currency::getCurrencies() as $currency)
				{
					if (!Validate::isFileName($select[0]["id_product"].".jpg"))
						die();
					$img = $currency['iso_code']."_".$select[0]["id_product"].".jpg";
					$imgs = $currency['iso_code']."_".$select[0]["id_product"]."_s.jpg";
					$addr_large = dirname(__FILE__).'/images/'.$language['iso_code'].'/'.$img;
					$addr_small = dirname(__FILE__).'/images/'.$language['iso_code'].'/'.$imgs;
					
					if (is_file($addr_large))
						unlink($addr_large);
					if (is_file($addr_small))
						unlink($addr_small);
				}
			}
		}
	}
}

if (Tools::getValue('action') == 'product')
{
    $return = ' ';
    $category = new Category(intval(Tools::getValue('cat')), Configuration::get('PS_LANG_DEFAULT'));
    $products = $category->getProducts(Configuration::get('PS_LANG_DEFAULT'), 1, 500);
    $products = Product::searchByName(Configuration::get('PS_LANG_DEFAULT'), Tools::getValue('name'));
    if ($products)
    	foreach ($products as $product)
    	{
    		$name = str_replace("'", "&#146;",$product['name']);
    		$name = str_replace('"', '\"',$name);
        	$return .=  '
        	<tr class="line">
           		<td>'.$product['id_product'].'</td>
            	<td>'.$product['name'].'</td>
            	<td>'.Tools::displayPrice($product['price_tax_incl'], Currency::getDefaultCurrency()).'</td>
           		<td align="center"><a onclick=\'javascript:moveProduct('.(int)$product['id_product'].',"'.$name.'","'.Tools::displayPrice($product['price_tax_incl'], Currency::getDefaultCurrency()).'");\'>
                    <img src="'._MODULE_DIR_.'vente_flash/css/add.png" title="Ajouter ce produit à la vente flash"></a></td>
        	</tr>';
    	}
    echo $return;
}

if (Tools::getValue('action') == 'deleteImg')
{
	$languages = Language::getLanguages(true);

	if (is_file(dirname(__FILE__)."/images/".Tools::getValue('img').'.jpg') && Validate::isFileName(Tools::getValue('img').'.jpg'))
	{
		unlink(dirname(__FILE__)."/images/".Tools::getValue('img').'.jpg');
		unlink(dirname(__FILE__)."/images/s".Tools::getValue('img').'.jpg');
	}
	else
	{
		foreach ($languages AS $language)
		{
			foreach (Currency::getCurrencies() as $currency)
			{
				if (!Validate::isFileName(Tools::getValue('img').".jpg"))
					die();
				$img = $currency['iso_code']."_".Tools::getValue('img').".jpg";
				$imgs = $currency['iso_code']."_".Tools::getValue('img')."_s.jpg";
				$addr_large = dirname(__FILE__).'/images/'.$language['iso_code'].'/'.$img;
				$addr_small = dirname(__FILE__).'/images/'.$language['iso_code'].'/'.$imgs;
				 
				// Supprimer images existantes
				if (is_file($addr_large))
					unlink($addr_large);
				if (is_file($addr_small))
					unlink($addr_small);
			}
		}
	}
}

function clearImage()
{
	$img = scandir(dirname(__FILE__).'/images/');
	$imgSelect = array();
	$select = '
	SELECT  `image`
	FROM `'._DB_PREFIX_.'flash_assoc`';
	$select = Db::getInstance()->ExecuteS($select);
	foreach ($select as $s){
		if ($s['image'] != NULL)
		{
			$imgSelect[] = $s['image'];
			$imgSelect[] = 's'.$s['image'];
		}
	}
	$diff = array_diff($img, $imgSelect);
	for ($i = 2; $i < count($diff); $i++)
		if (Validate::isFileName($diff[$i]))
			unlink(dirname(__FILE__).'/images/'.$diff[$i]);
}

	function getCover($id_product, $type = NULL)
	{
	global $cookie;
	$select  =  '
	SELECT *
	FROM `'._DB_PREFIX_.'product` p
	LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product`)
	LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
			LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.intval($cookie->id_lang).')
			WHERE p.`id_product` = '.intval($id_product).'
			AND pl.`id_lang` = '.intval($cookie->id_lang).'
			ORDER BY pl.`name`';
			$row = Db::getInstance()->ExecuteS($select);
			$row = Product::getProductProperties(intval($cookie->id_lang),$row[0] );
			$id_img = preg_match("/\-/", $row['id_image']) ? explode("-", $row['id_image']) : array('',$row['id_image']);
			$image = new Image($id_img[1], $cookie->id_lang);
			if (method_exists($image, "getExistingImgPath"))
			{

			return '/img/p/'.$image->getExistingImgPath()."-".$type.".jpg";
	}
	else
		return "img/p/".$row['id_image']."-".$type.".jpg";

	}

function verifForm($id = 0)
{
    $info = array();
    $languages = Language::getLanguages(true);

    if (Validate::isDate(Tools::getValue('date_start')) & Validate::isDate(Tools::getValue('date_end')))  {
        $info["date_start"] = Tools::getValue('date_start')." ".Tools::getValue('hour_start');
        $info["date_end"] = Tools::getValue('date_end')." ".Tools::getValue('hour_end');
    	foreach ($languages AS $language)
    		$info["name"][(int)$language['id_lang']] = Tools::getValue('flash_name_'.(int)$language['id_lang']);
        $dateS = mktime(0, 0, 0, (int)substr($info["date_start"], 5, 2), (int)substr($info["date_start"], 8, 2), (int)substr($info["date_start"], 0, 4));
        $dateE = mktime(0, 0, 0, (int)substr($info["date_end"], 5, 2), (int)substr($info["date_end"], 8, 2), (int)substr($info["date_end"], 0, 4));
        if ($dateS <= $dateE) {
            if (Tools::getValue('products') != "") {
                $info["products"] = explode(',', Tools::getValue('products'));
                foreach ($info["products"] as $product) {
                    $select = '
                        SELECT DISTINCT `flash_name`
                        FROM `'._DB_PREFIX_.'flash` f, `'._DB_PREFIX_.'flash_lang` fl
                        INNER JOIN `'._DB_PREFIX_.'flash_assoc` fa ON (fl.`id_flash` = fa.`id_flash`)
                        WHERE fa.`id_product` = '.(int)$product.'
                        AND ((f.`date_start`BETWEEN "'.pSQL(Tools::getValue('date_start')).'" AND "'.pSQL(Tools::getValue('date_end')).'")
                        OR (f.`date_end`BETWEEN "'.pSQL(Tools::getValue('date_start')).'" AND "'.pSQL(Tools::getValue('date_end')).'" ))';
                    if ($id != 0)
                        $select .= ' AND f.`id_flash` != '.(int)$id;
                    $return = Db::getInstance()->ExecuteS($select);
                    if (Db::getInstance()->numRows() == 1)
                        die ('error,5');
                    else 
                    {
                    	if (Tools::getValue('action') == 'add')
	                        if (Tools::getValue('euros_discount') != "" || Tools::getValue('percent_discount') != "" )
	                            $info["remise"] = (Tools::getValue('euros_discount') != "") ? Tools::getValue('euros_discount')."e" : Tools::getValue('percent_discount')."p";
		                    else
		                        die("error,4");
		                elseif (Tools::getValue('action') == 'update')
		                	foreach ($info["products"] as $idProduct)
			                	if (Tools::getValue('euros_discount_'.(int)$idProduct) != "" || Tools::getValue('percent_discount_'.(int)$idProduct) != "" )
			                		$info["remise"]["product_".(int)$idProduct] = (Tools::getValue('euros_discount_'.(int)$idProduct) != "") ? Tools::getValue('euros_discount_'.(int)$idProduct)."e" : Tools::getValue('percent_discount_'.(int)$idProduct)."p";
				                else
				                	$info["remise"]["product_".(int)$idProduct] = "0p";
                    }
                }
            }
            else
                die("error,3");
        }
        else
            die("error,6");
    }
    else
        die("error,2");
    return $info;
}

function reduction($product, $remise, $reduc_type, $date_start, $date_end)
{
    $spePrice = new SpecificPrice();
    $spePrice->id_product = $product;
    $spePrice->id_shop = Tools::getValue('id_shop');
    $spePrice->id_customer = 0;
    $spePrice->id_currency = 0;
    $spePrice->id_country = 0;
    $spePrice->id_group = 0;
    $spePrice->price = -1;
    $spePrice->from_quantity = 1;
    $spePrice->reduction = floatval($remise);
    $spePrice->reduction_type = $reduc_type;
    $spePrice->from = $date_start;
    $spePrice->to = $date_end;
    if(!($spePrice->add()))
        die ("error,7");
    return $spePrice->id;
}

function add_flash($products, $name, $date_start, $date_end, $remise)
{
	global $cookie;
	
	_PS_VERSION_ >= '1.5.1' ? $large_image = "large_default" : $large_image = "large";
	$languages = Language::getLanguages(true);
    $reduc_type = "amount";
	$id_shop = Tools::getValue('id_shop');

    if (preg_match("/p/",$remise))
    {
        $reduc_type = "percentage";
        $remise = floatval($remise) / 100;
    }
    
    $insert  = '
        INSERT INTO `'._DB_PREFIX_.'flash`(`id_shop`,`date_start`,`date_end`,`statut`)
        VALUES ("'.(int)$id_shop.'","'.pSQL($date_start).'","'.pSQL($date_end).'",1)';
    if (!(Db::getInstance()->Execute($insert)))
        die ("error,8");
    $id_flash = Db::getinstance()->Insert_ID();
    $i = 0;

	foreach ($products as $product)
	{

	    $img = NULL;
	    
	    $id_specific = reduction($product, $remise, $reduc_type, $date_start, $date_end);
	    $img = $product.".jpg";

    	foreach ($languages AS $language)
    	{
    		foreach (Currency::getCurrencies() as $currency)
    		{
    			if (!Validate::isFileName($product.".jpg"))
    				die();
	    		$img = $currency['iso_code']."_".$product.".jpg";
	    		$imgs = $currency['iso_code']."_".$product."_s.jpg";
		    	$addr_large = htmlspecialchars(dirname(__FILE__).'/images/'.$language['iso_code'].'/'.$img);
		    	$addr_small = htmlspecialchars(dirname(__FILE__).'/images/'.$language['iso_code'].'/'.$imgs);
		    	$separator_source = dirname(__FILE__).'/css/separator.png';
		    	$bgimage_source = dirname(__FILE__).'/css/bg_image.png';
		    	$source = _PS_ROOT_DIR_.getCover($product, $large_image);
		    	$img_source = imagecreatefromjpeg($source);
		    	$img_gen = imagecreatetruecolor(535, 210);
		    	$separator = imagecreatefrompng($separator_source);
		    	$bgimage = imagecreatefrompng($bgimage_source);
	
		    	// Supprimer images existantes
		    	if (is_file($addr_large))
		    		unlink($addr_large);
		    	if (is_file($addr_small))
		    		unlink($addr_small);
		    	
		    	list($width, $height) = getimagesize($source);
		    	if ($width > $height)
		    	{
		    		$x = 150;
		    		$y = ($height/$width)*$x;
		    	}
		    	elseif ($width <= $height)
		    	{
		    		$y = 205;
		    		$x = ($width/$height)*$y;
		    	}
		    	
	
		    	
		    	// CREATION HEXA
		    	$background_hex = sscanf("#FFFFFF", '#%2x%2x%2x');
		    	$background = imagecolorallocate($img_gen, $background_hex[0], $background_hex[1], $background_hex[2]);
		    	
		    	imagefilledrectangle($img_gen, 0, 0, 535, 210, $background);
		    	imagecopyresized($img_gen, $img_source, 0, 0, 0, 0, $x, $y, $width, $height);
		    	imagecopy($img_gen, $separator, 210, 0, 0, 0, 10, 210);
		    	imagecopy($img_gen, $bgimage, 255, 70, 0, 0, 250, 100);

		    	imagepng($img_gen, $addr_large);
				imageResize(_PS_ROOT_DIR_.getCover($product, $large_image), $addr_small,200,190);
				edit_image($addr_large, $addr_small, $product, (int)$currency["id_currency"], (int)$language['id_lang']);
    		}
    	}

	    $insert  = '
	    INSERT INTO `'._DB_PREFIX_.'flash_assoc`(`id_flash`,`id_product`,`id_specificprice`,`discount`,`discount_type`,`image`)
	    VALUES ('.(int)$id_flash.','.(int)$product.','.(int)$id_specific.','.floatval($remise).',"'.pSQL($reduc_type).'","'.pSQL($img).'")';
	    if (!(Db::getInstance()->Execute($insert)))
	    	die ("error,8");
	}
	
    foreach ($languages AS $language)
    {
    	$insert  = '
    	INSERT INTO `'._DB_PREFIX_.'flash_lang`(`id_flash`, `id_lang`, `flash_name`)
    	VALUES ('.(int)$id_flash.','.(int)$language['id_lang'].',"'.pSQL($name[(int)$language['id_lang']]).'")';
	    if (!(Db::getInstance()->Execute($insert)))
	    	die ("error,8");
    }

}

function delDiscount($id)
{
     $select = '
         SELECT `id_specificprice`
         FROM `'._DB_PREFIX_.'flash_assoc` fa
         INNER JOIN `'._DB_PREFIX_.'flash` f ON (f.`id_flash` = fa.`id_flash`)
         WHERE fa.`id_flash` = '.(int)$id;

     if (!($products = Db::getInstance()->ExecuteS($select)))
             die("error,Problème lors de la suppresion des réductions");

     foreach ($products as $product)
        deleteBySpecificPriceId(intval($product['id_specificprice']));
}

function edit_image($addr_large, $addr_small, $product_id, $currency_id, $lang_id)
{
	global $cookie;
	
	// RECUP DES INFOS PRIX
	$product = new Product((int)$product_id, (int)$lang_id);
	$price['old'] = Product::getPriceStatic((int)$product_id, true, NULL, 6, NULL, false, false);
	$price['new'] = Product::getPriceStatic((int)$product_id);
	
	$price['old'] = Product::convertAndFormatPrice($price['old'], Currency::getCurrencyInstance((int)$currency_id));
	$price['new'] = Product::convertAndFormatPrice($price['new'], Currency::getCurrencyInstance((int)$currency_id));

	$name_product = getProductsLang((int)$currency_id, (int)$product_id);
	
	// EDITION DE LA GRANDE IMAGE
	$image_large = imagecreatefrompng($addr_large);
	
	// CREATION HEXA
	$background_hex = sscanf("#FFFFFF", '#%2x%2x%2x');
	$background = imagecolorallocate($image_large, $background_hex[0], $background_hex[1], $background_hex[2]);
	
	$color_price_hex = sscanf(Configuration::get('VF_IMAGE_PRICE_COLOR', null, null, Tools::getValue('id_shop')), '#%2x%2x%2x');
	$color_name_hex = sscanf(Configuration::get('VF_IMAGE_NAME_COLOR', null, null, Tools::getValue('id_shop')), '#%2x%2x%2x');
	$color_price = imagecolorallocate($image_large, $color_price_hex[0], $color_price_hex[1], $color_price_hex[2]);
	$color_name = imagecolorallocate($image_large, $color_name_hex[0], $color_name_hex[1], $color_name_hex[2]);
		
	imagettftext($image_large, 34, 0, 300, 120, $color_price, dirname(__FILE__)."/police".Configuration::get('VF_IMAGE_FONT', null, null, Tools::getValue('id_shop')).".ttf", $price['new']);
	imagettftext($image_large, 15, 0, 300, 150, $color_name, dirname(__FILE__)."/police".Configuration::get('VF_IMAGE_FONT', null, null, Tools::getValue('id_shop')).".ttf", $price['old']);
	imagettftext($image_large, 17, 0, 220, 40, $color_name, dirname(__FILE__)."/police".Configuration::get('VF_IMAGE_FONT', null, null, Tools::getValue('id_shop')).".ttf", $product->name[$lang_id]);
	ImageLine($image_large, 300, 145, 300 + (strlen(Tools::displayPrice($price['old'], Currency::getDefaultCurrency())) * 8.5), 145, $color_price);
	
	// BORDURE
	$background_border_hex = sscanf("#D0D0D0", '#%2x%2x%2x');
	$background_border = imagecolorallocate($image_large, $background_border_hex[0], $background_border_hex[1], $background_border_hex[2]);
	ImageLine($image_large, 0, 0, 0, 210, $background_border);
	ImageLine($image_large, 535, 0, 0, 0, $background_border);
	ImageLine($image_large, 534, 0, 534, 210, $background_border);
	ImageLine($image_large, 0, 209, 535, 209, $background_border);
	
	imagepng($image_large, $addr_large);
	
	// EDITION DE LA PETITE IMAGE
	$image_small = imagecreatefromjpeg($addr_small);
		
	imagettftext($image_small, 22, 0, 15, 28, $color_price, dirname(__FILE__)."/police".Configuration::get('VF_IMAGE_FONT', null, null, Tools::getValue('id_shop')).".ttf", $price['new']);
	imagettftext($image_small, 15, 0, 15, 170, $color_name, dirname(__FILE__)."/police".Configuration::get('VF_IMAGE_FONT', null, null, Tools::getValue('id_shop')).".ttf", $product->name[$lang_id]);
	imagettftext($image_small, 11, 0, 15, 185, $color_name, dirname(__FILE__)."/police".Configuration::get('VF_IMAGE_FONT', null, null, Tools::getValue('id_shop')).".ttf", $price['old']);
	ImageLine($image_small, 15, 180, 15 + (strlen(Tools::displayPrice($price['old'], Currency::getDefaultCurrency())) * 7), 180, $color_price);
	
	ImageLine($image_small, 0, 0, 0, 190, $background_border);
	ImageLine($image_small, 200, 0, 0, 0, $background_border);
	ImageLine($image_small, 199, 0, 199, 190, $background_border);
	ImageLine($image_small, 0, 189, 200, 189, $background_border);
	
	imagepng($image_small, $addr_small);
}

function update_flash($products, $name, $date_start, $date_end, $remises)
{
	include_once('vente_flash.php');
	global $cookie;
	
	_PS_VERSION_ >= '1.5.1' ? $large_image = "large_default" : $large_image = "large";
	$i = 0;
    $id = (int)Tools::getValue('modif');
    $languages = Language::getLanguages(true);
    //$reduc_type = "amount";
    delDiscount($id);
    
    foreach ($remises as $remise)
    {
    	if (preg_match("/p/",$remise))
    	{
    		//$reduc_type = "percentage";
    		//$remise = (float)$remise / 100;
    		
    		$reduc[$i]["type"] = "percentage";
    		$reduc[$i]["remise"] = (float)$remise / 100;
    	}
    	else
    	{
    		$reduc[$i]["type"] = "amount";
    		$reduc[$i]["remise"] = (int)$remise;
    	}
    	$i++;
    }

   $update = '
       UPDATE `'._DB_PREFIX_.'flash`
       SET `date_start` = "'.pSQL($date_start).'" ,`date_end` = "'.pSQL($date_end).'"
       WHERE `id_flash` = '.  intval($id);
   
   $exp = "/^[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/";
   if (!preg_match($exp, $date_start))
   		die("error,13");
   
   if (!(Db::getInstance()->Execute($update)))
        die("error,13");
   $delete = '
       DELETE FROM `'._DB_PREFIX_.'flash_assoc`
       WHERE `id_flash` = '.intval($id);
   if (!(Db::getInstance()->Execute($delete)))
   		die("error,13");
   $delete = '
   	   DELETE FROM `'._DB_PREFIX_.'flash_lang`
       WHERE `id_flash` = '.intval($id);
   if (!(Db::getInstance()->Execute($delete)))
         die("error,13");
    
    $i = 0;

	foreach ($products as $product)
	{
	    $img = NULL;
	    $remise = $reduc[$i]["remise"];
	    $reduc_type = $reduc[$i]["type"];
	    echo $product." - ".$remise." - ".$reduc_type." - ".$date_start." - ".$date_end."\n";
	    $id_specific = reduction($product, $remise, $reduc_type, $date_start, $date_end);
	    $img = $product.".jpg";
		
    	foreach ($languages AS $language)
    	{
    		foreach (Currency::getCurrencies() as $currency)
    		{
	    		$img = $currency['iso_code']."_".$product.".jpg";
	    		$imgs = $currency['iso_code']."_".$product."_s.jpg";
		    	$addr_large = htmlspecialchars(dirname(__FILE__).'/images/'.$language['iso_code'].'/'.$img);
		    	$addr_small = htmlspecialchars(dirname(__FILE__).'/images/'.$language['iso_code'].'/'.$imgs);
		    	$separator_source = dirname(__FILE__).'/css/separator.png';
		    	$bgimage_source = dirname(__FILE__).'/css/bg_image.png';
		    	$source = _PS_ROOT_DIR_.getCover($product, $large_image);
		    	$img_source = imagecreatefromjpeg($source);
		    	$img_gen = imagecreatetruecolor(535, 210);
		    	$separator = imagecreatefrompng($separator_source);
		    	$bgimage = imagecreatefrompng($bgimage_source);
		    	
		    	// Supprimer images existantes
		    	if (is_file($addr_large))
		    		unlink($addr_large);
		    	if (is_file($addr_small))
		    		unlink($addr_small);
				
		    	// Nom de l'image pour la bdd
		    	if ((int)$cookie->id_currency == (int)$currency['id_currency'])
		    		$img_bdd = $img;
		    	
		    	list($width, $height) = getimagesize($source);
		    	if ($width > $height)
		    	{
		    		$x = 150;
		    		$y = ($height/$width)*$x;
		    	}
		    	elseif ($width <= $height)
		    	{
		    		$y = 205;
		    		$x = ($width/$height)*$y;
		    	}
		    	
	
		    	
		    	// CREATION HEXA
		    	$background_hex = sscanf("#FFFFFF", '#%2x%2x%2x');
		    	$background = imagecolorallocate($img_gen, $background_hex[0], $background_hex[1], $background_hex[2]);
		    	
		    	imagefilledrectangle($img_gen, 0, 0, 535, 210, $background);
		    	imagecopyresized($img_gen, $img_source, 0, 0, 0, 0, $x, $y, $width, $height);
		    	imagecopy($img_gen, $separator, 210, 0, 0, 0, 10, 210);
		    	imagecopy($img_gen, $bgimage, 255, 70, 0, 0, 250, 100);

		    	imagepng($img_gen, $addr_large);
				imageResize(_PS_ROOT_DIR_.getCover($product, $large_image), $addr_small,200,190);
				edit_image($addr_large, $addr_small, $product, (int)$currency["id_currency"], (int)$language['id_lang']);
    		}
    	}
	    $insert  = '
	        INSERT INTO `'._DB_PREFIX_.'flash_assoc`(`id_flash`,`id_product`,`id_specificprice`,`discount`,`discount_type`,`image`)
	        VALUES ('.(int)$id.','.(int)$product.','.(int)$id_specific.',"'.  (float)$remise.'","'.pSQL($reduc_type).'","'.pSQL($img_bdd).'")';
	    if (!(Db::getInstance()->Execute($insert)))
	        die ("error,13");
	    $i++;
	}
	
	foreach ($languages AS $language)
	{
		$insert  = '
		INSERT INTO `'._DB_PREFIX_.'flash_lang`(`id_flash`, `id_lang`, `flash_name`)
		VALUES ('.(int)$id.','.(int)$language['id_lang'].',"'.$name[(int)$language['id_lang']].'")';
		if (!(Db::getInstance()->Execute($insert)))
			die ("error,8");
	}
	Configuration::updateValue('VF_LAST_EDIT', (int)$id, null, null, Tools::getValue('id_shop'));
}

function getProductsLang($id_lang, $id_product)
{
	return Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
			SELECT p.`id_product`, pl.`name`
			FROM `'._DB_PREFIX_.'product` p
			LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product`)
			WHERE pl.`id_product` = '.(int)($id_product).'
			AND pl.`id_lang` = '.(int)($id_lang).'
			ORDER BY pl.`name`');
}

function deleteBySpecificPriceId($id_specificprice)
{
	return Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'specific_price` WHERE `id_specific_price` = '.(int)$id_specificprice);
}