<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{vente_flash}prestashop>vente_flash_a3262162d6eb51d3865d1c0eed267ce0'] = 'Ventes Flash';
$_MODULE['<{vente_flash}prestashop>vente_flash_8019fdd614013a4d9385f6dd89b0434d'] = 'Créez des ventes flashs pour votre boutique en ligne PrestaShop';
$_MODULE['<{vente_flash}prestashop>vente_flash_a8d69cdfad4c19ed22bf693b8871064e'] = 'Choisir la langue';
$_MODULE['<{vente_flash}prestashop>vente_flash_462390017ab0938911d2d4e964c0cab7'] = 'Configuration mise à jour avec succès';
$_MODULE['<{vente_flash}prestashop>vente_flash_79dceca8926e976181b59254e9367168'] = 'Configuration réinitialisé avec succès';
$_MODULE['<{vente_flash}prestashop>vente_flash_8adc821d7a616e14d99983e162fe214e'] = 'Aucune vente flash';
$_MODULE['<{vente_flash}prestashop>vente_flash_6eb5f02531662e5630367df1a166b1dd'] = 'Modifier la vente';
$_MODULE['<{vente_flash}prestashop>vente_flash_6d73d13fc974ee32b6e0a24ee833901f'] = 'Ajouter la vente';
$_MODULE['<{vente_flash}prestashop>vente_flash_a129f1571564b31bd035e3838ffd3914'] = 'Votre vente n\'a pas d\'identifiant.';
$_MODULE['<{vente_flash}prestashop>vente_flash_13a7f3a7c91be7915876a7e5d547e810'] = 'La date est incorrecte.';
$_MODULE['<{vente_flash}prestashop>vente_flash_31e9aae51059b24b6ff2a2a96968e537'] = 'Aucun produit !';
$_MODULE['<{vente_flash}prestashop>vente_flash_5c8a2468cdf88a870b2d9f5cedc28b15'] = 'Aucune Remise !';
$_MODULE['<{vente_flash}prestashop>vente_flash_f1581f523751a6179ae44aa9a203da57'] = 'Un produit de votre vnte est déja dans une vente flash en cours.';
$_MODULE['<{vente_flash}prestashop>vente_flash_b1c9c8714a3aedcc6988cc40e5674fde'] = 'La date de fin est plus grande que celle de début.';
$_MODULE['<{vente_flash}prestashop>vente_flash_f9a20179dc05d4e39d07a5b7f1806429'] = 'Une erreur c\'est produit durant la création de la réduction.';
$_MODULE['<{vente_flash}prestashop>vente_flash_c5627dd74fb72406bd6164e89e53f85e'] = 'Une erreur c\'est produit durant la création de la vente.';
$_MODULE['<{vente_flash}prestashop>vente_flash_9f3bd876b7aa9b564a45b1265d362ac7'] = 'La vente n\'a pu être désactivé.';
$_MODULE['<{vente_flash}prestashop>vente_flash_edd7f43069c06bc6e85f349d0b1fac61'] = 'La vente a été désactivé.';
$_MODULE['<{vente_flash}prestashop>vente_flash_276969710788179a6c281bac0d9dc7b9'] = 'La vente a été activé.';
$_MODULE['<{vente_flash}prestashop>vente_flash_a7a5d888680cfee2f5136dd1bfd5f577'] = 'La vente n\'a pu être activé.';
$_MODULE['<{vente_flash}prestashop>vente_flash_9601555ec920ee84884fa1243a986f9e'] = 'Une erreur c\'est produit durant la mise a jour de la vente.';
$_MODULE['<{vente_flash}prestashop>vente_flash_4b7e270642789d3bc10cb6232ceda044'] = 'Une erreur c\'est produit durant la suppresion de la vente.';
$_MODULE['<{vente_flash}prestashop>vente_flash_7601a7859de333f3d700c17550dd8b29'] = 'Voulez-vous supprimer ?';
$_MODULE['<{vente_flash}prestashop>vente_flash_16d8e397a41006305110d2bc29fcdfe6'] = 'Ce produit est déja dans la vente';
$_MODULE['<{vente_flash}prestashop>vente_flash_352c43b3bda7826caabafbf11c5343a5'] = 'Caractère interdit danss l\'ID';
$_MODULE['<{vente_flash}prestashop>vente_flash_9964ab3af05e4e03efedce90d2dc2d3a'] = 'Ajouter une vente flash';
$_MODULE['<{vente_flash}prestashop>vente_flash_c47880c8221bdce9c1f76e982e775555'] = 'Éditer une vente flash';
$_MODULE['<{vente_flash}prestashop>vente_flash_80f0de93c486a1ec423506c641b3f94c'] = 'Ventes flash créées';
$_MODULE['<{vente_flash}prestashop>vente_flash_b25d20689c70c9017dcd4510e5328bda'] = 'Image de fond \"Large\"';
$_MODULE['<{vente_flash}prestashop>vente_flash_56c83857597a00f7c2a7d84c5f3e67cc'] = 'Image de fond \"Block\"';
$_MODULE['<{vente_flash}prestashop>vente_flash_90589c47f06eb971d548591f23c285af'] = 'Personnaliser';
$_MODULE['<{vente_flash}prestashop>vente_flash_1f715a051f74be015cfe8a50a4987ab1'] = 'Les visuels générés ou téléchargés apparaîtront dans le slider de la vente flash sur la page d\'accueil de votre boutique. Si vous souhaitez que l\'image de la vente flash reste fixe, il vous suffit de supprimer les visuels des produits en cliquant sur le bouton de suppression de l\'image. Pour personnaliser davantage vos ventes flash, veuillez les éditer dans l\'onglet \"Ventes Flash créés\"';
$_MODULE['<{vente_flash}prestashop>vente_flash_17f989748c3ef9733fb1d4b94c55e5e5'] = 'Nouvelle vente';
$_MODULE['<{vente_flash}prestashop>vente_flash_0834dbec80c240be5b2fda7c96b79b50'] = 'Rechercher un produit';
$_MODULE['<{vente_flash}prestashop>vente_flash_49ee3087348e8d44e1feda1917443987'] = 'Nom';
$_MODULE['<{vente_flash}prestashop>vente_flash_9976b7013f168284faabd0fa41a19e5a'] = 'Changer la langue de la vente';
$_MODULE['<{vente_flash}prestashop>vente_flash_a3b5ce10a6ec3a16f8c712b69d0e7f3a'] = 'Date de début';
$_MODULE['<{vente_flash}prestashop>vente_flash_744ec887e6852aac591a45b9b70bbcc6'] = 'Date a laquelle la vente commence';
$_MODULE['<{vente_flash}prestashop>vente_flash_3b8dea39d7cf07402edd7270ae0a3ee2'] = 'Date de fin';
$_MODULE['<{vente_flash}prestashop>vente_flash_a129bc54facd237dacc83fb27c7b034a'] = 'Date a laquelle la vente se termine';
$_MODULE['<{vente_flash}prestashop>vente_flash_92153959f8d07bdb9e8d3315ca9bfbac'] = 'Choisir réduc';
$_MODULE['<{vente_flash}prestashop>vente_flash_e81c4e4f2b7b93b481e13a8553c2ae1b'] = 'ou';
$_MODULE['<{vente_flash}prestashop>vente_flash_1da583eacc5180e89664a64afd319538'] = 'Modifier réduc';
$_MODULE['<{vente_flash}prestashop>vente_flash_11e34930eb433d02e7323d9314cb2742'] = 'Produits pour la vente Flash';
$_MODULE['<{vente_flash}prestashop>vente_flash_aaae3188ca91e4eb6aba6491772bdbf8'] = 'Vous avez supprimé une image produit d\'une vente flash. Si vous le voulez, vous pouvez charger une autre image, ou utiliser l\'image auto générée.';
$_MODULE['<{vente_flash}prestashop>vente_flash_699a8137deca4ffbb8f18c9db1525654'] = 'Si vous souhaitez personnaliser le visuel de ce produit pour la vente flash,';
$_MODULE['<{vente_flash}prestashop>vente_flash_8d404dabaf17f6dc2966d4be47b78844'] = 'vous pouvez télécharger une image dont le format est :';
$_MODULE['<{vente_flash}prestashop>vente_flash_16d2b386b2034b9488996466aaae0b57'] = 'Historique';
$_MODULE['<{vente_flash}prestashop>vente_flash_fe58b8b1ca51c5edc44c00d5ea86295f'] = 'Liste des ventes Flashes';
$_MODULE['<{vente_flash}prestashop>vente_flash_ec53a8c4f07baed5d8825072c89799be'] = 'Statut';
$_MODULE['<{vente_flash}prestashop>vente_flash_7dce122004969d56ae2e0245cb754d35'] = 'Modifier';
$_MODULE['<{vente_flash}prestashop>vente_flash_59410256a1f46f32732eea8b9cffc530'] = 'Supprimer les ventes flashes';
$_MODULE['<{vente_flash}prestashop>vente_flash_1f59b29dbc500a6b0be23b57dde26005'] = 'Ajouter phrase en haut \"Pour modifier le positionnement du décompte afin la fin de la vente flash, utilisez le niveau horizontal et le niveau vertical';
$_MODULE['<{vente_flash}prestashop>vente_flash_a0b9406ce7fd3329aee17d4f3ceccaeb'] = 'Fond large';
$_MODULE['<{vente_flash}prestashop>vente_flash_bfc9d469c96b11c6b0c61c18e70f5676'] = 'Choisir en largeur la position du compteur';
$_MODULE['<{vente_flash}prestashop>vente_flash_4b813b735984a92cdc0e38dc404da595'] = 'Choisir en hauteur la position du compteur';
$_MODULE['<{vente_flash}prestashop>vente_flash_ccda5151c9dc05e5c66807d1d5b08563'] = 'Position temps restant';
$_MODULE['<{vente_flash}prestashop>vente_flash_eec6c4bdbd339edf8cbea68becb85244'] = 'Hauteur';
$_MODULE['<{vente_flash}prestashop>vente_flash_32954654ac8fe66a1d09be19001de2d4'] = 'Largeur';
$_MODULE['<{vente_flash}prestashop>vente_flash_e6616588c50bd82570b15e3fa57aa0be'] = 'Télécharger fond';
$_MODULE['<{vente_flash}prestashop>vente_flash_3e9de459bb4cb3586492022df32ef5cd'] = 'Vous pouvez charger un nouveau fond';
$_MODULE['<{vente_flash}prestashop>vente_flash_c9cc8cce247e49bae79f15173ce97354'] = 'Enregistrer';
$_MODULE['<{vente_flash}prestashop>vente_flash_7d6da219788a178f4f80c2237eb73995'] = 'Supprimer les fonds personnalisés';
$_MODULE['<{vente_flash}prestashop>vente_flash_d87fbeccd327e22281fe2b5a8e1ab3a5'] = 'Fond block';
$_MODULE['<{vente_flash}prestashop>vente_flash_eb713a98ae97395959c8028145560c87'] = 'Supprimer le fond personnalisé';
$_MODULE['<{vente_flash}prestashop>vente_flash_dd469f1bcc83bd6207398c6dc440dff0'] = 'Personnaliser ventes flash';
$_MODULE['<{vente_flash}prestashop>vente_flash_4fa7583010efb8b60fd65977cf0f23a1'] = 'Vous pouvez ici personnaliser la police, la couleur d\'écriture et la couleur de fond du compteur de temps restant pour la vente flash';
$_MODULE['<{vente_flash}prestashop>vente_flash_37d0ad7e2298085ba8125e470657e6c3'] = 'Police ventes flash';
$_MODULE['<{vente_flash}prestashop>vente_flash_05d0513a5e700fd4e2f9c7f2c42a8992'] = 'Couleur police';
$_MODULE['<{vente_flash}prestashop>vente_flash_54730b168eb999a337afbff5395f54a1'] = 'Couleur fond';
$_MODULE['<{vente_flash}prestashop>vente_flash_02803cf1949305c8d830453818be473d'] = 'Vous pouvez ici personnaliser la police des images auto générés pour les ventes flash. (Voici à droite un exemple de la police séléctionné)';
$_MODULE['<{vente_flash}prestashop>vente_flash_7b32f7c982f7db7671f2e957f3e3d834'] = 'Police image produit';
$_MODULE['<{vente_flash}prestashop>vente_flash_4525619d34d8949b1cb56c0a795d3790'] = 'Couleur police du prix';
$_MODULE['<{vente_flash}prestashop>vente_flash_16e1fc3be750f0d03211b7fa44ba2c41'] = 'Couleur police de l\'article';
$_MODULE['<{vente_flash}prestashop>vente_flash_b263612598a5940e833aeea3f851caa5'] = 'Vous avez modifié la police ou la couleur des images auto-générées ? Afin que ces modifications soient prises en compte, il vous faut';
$_MODULE['<{vente_flash}prestashop>vente_flash_0204c27217420c9c33173b74ca568222'] = 'Enregistrer (ci-dessous)';
$_MODULE['<{vente_flash}prestashop>vente_flash_c493adf70edcb8a3c2900f8580003d23'] = 'Cliquer sur \"Ventes flash créées\"';
$_MODULE['<{vente_flash}prestashop>vente_flash_6572c19a9c6518420ca4656b8e7f40af'] = 'Rechercher la vente flash concernée et cliquer sur \"Modifier\"';
$_MODULE['<{vente_flash}prestashop>vente_flash_c82fdb5a0d6b8f8570377c9b1c340a0c'] = 'Une fois sur la fiche de la vente flash, cliquer sur \"Modifier la vente\"';
$_MODULE['<{vente_flash}prestashop>vente_flash_12e74fc823ebcdb75fa92be6b5db2bbc'] = 'Réinitialiser personnalisations';
$_MODULE['<{vente_flash}prestashop>vente_flash_a3fa607b49ad809e8ca8f58abe958102'] = 'Merci d\'avoir choisi un module développé par l\'équipe Addons de PrestaShop.';
$_MODULE['<{vente_flash}prestashop>vente_flash_9886df5ae168c20c3926e7e9f41adb45'] = 'Vous rencontrez un problème d\'utilisation du module? Nos équipes sont à votre service via le ';
$_MODULE['<{vente_flash}prestashop>vente_flash_23372c0d3713719764670087006fc1b6'] = 'formulaire de contact';
$_MODULE['<{vente_flash}prestashop>vente_flash_e7076d26652d5484e3402a540fe7c68d'] = 'Vente Flash';
$_MODULE['<{vente_flash}prestashop>product_6f6732d8aa523c1f06ed41c3b2339f7c'] = 'Dépêchez-vous, plus que';
$_MODULE['<{vente_flash}prestashop>flash_4e9c772719526ed908eb319ef874844a'] = 'Ventes Flash';
$_MODULE['<{vente_flash}prestashop>flash_6f6732d8aa523c1f06ed41c3b2339f7c'] = 'Dépêchez-vous, plus que';
$_MODULE['<{vente_flash}prestashop>list_flash_4e9c772719526ed908eb319ef874844a'] = 'Ventes Flash';
$_MODULE['<{vente_flash}prestashop>list_flash_3b29415738f1e969be165f22bce8842b'] = 'Ventes Flash';
$_MODULE['<{vente_flash}prestashop>list_flash_5da618e8e4b89c66fe86e32cdafde142'] = 'Du';
$_MODULE['<{vente_flash}prestashop>list_flash_e12167aa0a7698e6ebc92b4ce3909b53'] = 'au';
$_MODULE['<{vente_flash}prestashop>list_flash_3b268addd1f273bf3ab264e5fc1e6931'] = 'Voir les produits';
$_MODULE['<{vente_flash}prestashop>list_flash_f046102b9837b474a94f029330453113'] = 'Il n\'y a pas de vente flash en cours';
$_MODULE['<{vente_flash}prestashop>no_flash_4e9c772719526ed908eb319ef874844a'] = 'Ventes flash';
$_MODULE['<{vente_flash}prestashop>no_flash_997423ac5449c80691c05dddfdd8981d'] = 'Cette vente flash est déjà terminée.';
