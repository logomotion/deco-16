<?php
if (!defined('_CAN_LOAD_FILES_'))
	exit;
if ( !defined('__DIR__') )
	define('__DIR__', dirname(__FILE__));

class vente_flash extends Module
{
	private $_html = '';

	public function __construct() {
		$this->name = 'vente_flash';
		$this->tab = 'pricing_promotion';
		$this->version =  '2.3.4';
		$this->author = 'PrestaShop';
		$this->module_key = '29ff37a1387455a1850291c1dc55c240';

		$this_context = Context::getContext();
		$this->id_shop = $this_context->shop->id;

		parent::__construct();

		$this->displayName = $this->l('Flash Sales');
		$this->description = $this->l('Create flash sales to your online shop PrestaShop');
		putenv('GDFONTPATH='.realpath('.'));
	}

	public function install() {
		return (parent::install() AND $this->registerHook('productActions')
		AND $this->registerHook('header')
		AND $this->registerHook('rightColumn')
		AND $this->registerHook('home')
		AND $this->updateModulePosition(Hook::get('home'), 1)
		AND $this->installSql()
		AND $this->updateAllConfigs());
	}

	public function updateAllConfigs()
	{
		$shops = Shop::getShops();
		foreach ($shops as $shop)
		{
			Configuration::updateValue('VF_TOKEN', Tools::passwdGen(10), null, null, $shop["id_shop"]);
			Configuration::updateValue('VF_COUNT_H', '42', null, null, $shop["id_shop"]);
			Configuration::updateValue('VF_COUNT_W', '90', null, null, $shop["id_shop"]);
			Configuration::updateValue('VF_COUNT_SH', '60', null, null, $shop["id_shop"]);
			Configuration::updateValue('VF_COUNT_SW', '60', null, null, $shop["id_shop"]);
			Configuration::updateValue('VF_LAST_EDIT', '0', null, null, $shop["id_shop"]);
			Configuration::updateValue('VF_FONT', '1', null, null, $shop["id_shop"]);
			Configuration::updateValue('VF_FONT_COLOR', '#9B0000', null, null, $shop["id_shop"]);
			Configuration::updateValue('VF_BG_COLOR', '#EEEEEE', null, null, $shop["id_shop"]);
			Configuration::updateValue('VF_IMAGE_FONT', '1', null, null, $shop["id_shop"]);
			Configuration::updateValue('VF_IMAGE_PRICE_COLOR', '#9B0000', null, null, $shop["id_shop"]);
			Configuration::updateValue('VF_IMAGE_NAME_COLOR', '#383838', null, null, $shop["id_shop"]);
		}
		return true;
	}

	public function installSql()
	{
		$flash = Db::getInstance()->Execute('
				CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'flash` (
				`id_flash` integer not null auto_increment,
				`id_shop` integer not null,
				`date_start` datetime,
				`date_end` datetime,
				`statut`    boolean,
				primary key (`id_flash`)
		) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

		$assoc = Db::getInstance()->Execute('
				CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'flash_assoc` (
				`id_assoc` integer not null auto_increment,
				`id_flash` integer not null,
				`id_product` integer not null,
				`id_specificprice` integer not null,
				`discount` float,
				`discount_type` varchar(20),
				`image` varchar(100),
				primary key (`id_assoc`)
		) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

		$lang = Db::getInstance()->Execute('
				CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'flash_lang` (
				`id_flash` integer not null,
				`id_lang` integer not null,
				`flash_name` varchar(50)
		) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;');

		return ($flash & $assoc & $lang);
	}

	public function uninstall()
	{
		$select = '
		SELECT `id_specificprice`
		FROM `'._DB_PREFIX_.'flash_assoc` fa
		INNER JOIN `'._DB_PREFIX_.'flash` f ON (f.`id_flash` = fa.`id_flash`)';
		$products = Db::getInstance()->ExecuteS($select);

		foreach ($products as $product)
			$this->deleteBySpecificPriceId(intval($product['id_specificprice']));

		$flash= 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'flash`';
		$assoc = 'DROP TABLE IF EXISTS `'._DB_PREFIX_.'flash_assoc`';
		$lang = 'DROP TABLE IF EXISTS`'._DB_PREFIX_.'flash_lang`';

		return (parent::uninstall()
					AND Db::getInstance()->Execute($flash)
					AND Db::getInstance()->Execute($assoc)
					AND Db::getInstance()->Execute($lang)
					AND Configuration::deleteByName('VF_TOKEN')
					AND Configuration::deleteByName('VF_COUNT_H')
					AND Configuration::deleteByName('VF_COUNT_W')
					AND Configuration::deleteByName('VF_COUNT_SH')
					AND Configuration::deleteByName('VF_COUNT_SW')
					AND Configuration::deleteByName('VF_LAST_EDIT')
					AND Configuration::deleteByName('VF_FONT')
					AND Configuration::deleteByName('VF_FONT_COLOR')
					AND Configuration::deleteByName('VF_BG_COLOR')
					AND Configuration::deleteByName('VF_IMAGE_FONT')
					AND Configuration::deleteByName('VF_IMAGE_PRICE_COLOR')
					AND Configuration::deleteByName('VF_IMAGE_NAME_COLOR')
				);
	}

	public function displayFlags($languages, $default_language, $ids, $id, $return = false, $use_vars_instead_of_ids = false)
	{
		if (sizeof($languages) == 1)
			return false;
		$output = '
		<div class="displayed_flag">
		<img src="../img/l/'.$default_language.'.jpg" class="pointer" id="language_current_'.$id.'" onclick="toggleLanguageFlags(this);" alt="" />
		</div>
		<div id="languages_'.$id.'" class="language_flags">
		'.$this->l('Choose language').':<br /><br />';
		foreach ($languages as $language)
			$output .= '<img src="../img/l/'.(int)($language['id_lang']).'.jpg" class="pointer" alt="'.$language['name'].'" title="'.$language['name'].'" onclick="my_changeLanguage(this, '.(int)($language['id_lang']).');" /> ';
		$output .= '</div>';

		return $output;
	}

	public function updateModulePosition($id_hook, $position)
	{
		$old_pos = $this->getPosition($id_hook);
		$update = 'UPDATE '._DB_PREFIX_.'hook_module set position = position + 1
		WHERE id_hook = '.intval($id_hook);

		$return = Db::getInstance()->Execute($update);

		$update = 'UPDATE '._DB_PREFIX_.'hook_module set position='.intval($position).'
		WHERE id_module='.intval($this->id);

		if($return)
			return Db::getInstance()->Execute($update);
		else
			return false;
		return true;
	}

	public function getContent()
	{
		$languages = Language::getLanguages(true);
		$message_confirm = "";

		// If new background submitted
		if (Tools::isSubmit('ChangeBackground'))
		{
			foreach ($languages AS $language)
				if ($_FILES["image_filename_".(int)$language['id_lang']]["name"] != "")
					if (checkImage($_FILES["image_filename_".(int)$language['id_lang']], 1048576) === false)
						move_uploaded_file($_FILES["image_filename_".(int)$language['id_lang']]['tmp_name'], __DIR__.'/images/'.$language['iso_code'].'/perso.jpg');

			Configuration::updateValue('VF_COUNT_H', htmlspecialchars(Tools::getValue('vf_count_h')), null, null, $this->id_shop);
			Configuration::updateValue('VF_COUNT_W', htmlspecialchars(Tools::getValue('vf_count_w')), null, null, $this->id_shop);
			if (!isset($_GET["conf"]))
				$message_confirm = $this->displayConfirmation($this->l('Settings updated successfully'));
			else
				$message_confirm = "";
		}
		else if (Tools::isSubmit('DeleteBackground'))
		{
			foreach ($languages AS $language)
				if (file_exists(__DIR__.'/images/'.$language['iso_code'].'/perso.jpg'))
					unlink(__DIR__.'/images/'.$language['iso_code'].'/perso.jpg');
			Configuration::updateValue('VF_COUNT_H', '36', null, null, $this->id_shop);
			Configuration::updateValue('VF_COUNT_W', '428', null, null, $this->id_shop);
			if (!isset($_GET["conf"]))
				$message_confirm = $this->displayConfirmation($this->l('Settings reseted successfully'));
			else
				$message_confirm = "";
		}
		else if (Tools::isSubmit('ChangeBackgroundSmall'))
		{
			foreach ($languages AS $language)
				if ($_FILES["image_filename_".(int)$language['id_lang']]["name"] != "")
					if (checkImage($_FILES["image_filename_".(int)$language['id_lang']], 1048576) === false)
						move_uploaded_file($_FILES["image_filename_".(int)$language['id_lang']]['tmp_name'], __DIR__.'/images/'.$language['iso_code'].'/perso_small.jpg');
			Configuration::updateValue('VF_COUNT_SH', htmlspecialchars(Tools::getValue('vf_count_sh')), null, null, $this->id_shop);
			Configuration::updateValue('VF_COUNT_SW', htmlspecialchars(Tools::getValue('vf_count_sw')), null, null, $this->id_shop);
			if (!isset($_GET["conf"]))
				$message_confirm = $this->displayConfirmation($this->l('Settings updated successfully'));
			else
				$message_confirm = "";
		}
		else if (Tools::isSubmit('DeleteBackgroundSmall'))
		{
			foreach ($languages AS $language)
				if (file_exists(__DIR__.'/images/'.$language['iso_code'].'/perso_small.jpg'))
					unlink(__DIR__.'/images/'.$language['iso_code'].'/perso_small.jpg');
			Configuration::updateValue('VF_COUNT_SH', '20', null, null, $this->id_shop);
			Configuration::updateValue('VF_COUNT_SW', '106', null, null, $this->id_shop);
			if (!isset($_GET["conf"]))
				$message_confirm = $this->displayConfirmation($this->l('Settings reseted successfully'));
			else
				$message_confirm = "";
		}
		else if (Tools::isSubmit('Custom'))
		{
			Configuration::updateValue('VF_FONT', (int)Tools::getValue('font'), null, null, $this->id_shop);
			Configuration::updateValue('VF_FONT_COLOR', htmlspecialchars(Tools::getValue('color_font')), null, null, $this->id_shop);
			Configuration::updateValue('VF_BG_COLOR', htmlspecialchars(Tools::getValue('color_bg')), null, null, $this->id_shop);
			Configuration::updateValue('VF_IMAGE_FONT', htmlspecialchars(Tools::getValue('pictures_font')), null, null, $this->id_shop);
			Configuration::updateValue('VF_IMAGE_PRICE_COLOR', htmlspecialchars(Tools::getValue('pictures_color_price')), null, null, $this->id_shop);
			Configuration::updateValue('VF_IMAGE_NAME_COLOR', htmlspecialchars(Tools::getValue('pictures_color_name')), null, null, $this->id_shop);
			if (!isset($_GET["conf"]))
				$message_confirm = $this->displayConfirmation($this->l('Settings updated successfully'));
			else
				$message_confirm = "";
		}
		else if (Tools::isSubmit('ResetCustom'))
		{
			Configuration::updateValue('VF_FONT', "1", null, null, $this->id_shop);
			Configuration::updateValue('VF_FONT_COLOR', "#9B0000", null, null, $this->id_shop);
			Configuration::updateValue('VF_BG_COLOR', "#EEEEEE", null, null, $this->id_shop);
			Configuration::updateValue('VF_IMAGE_FONT', "1", null, null, $this->id_shop);
			Configuration::updateValue('VF_IMAGE_PRICE_COLOR', "#9B0000", null, null, $this->id_shop);
			Configuration::updateValue('VF_IMAGE_NAME_COLOR', "#000000", null, null, $this->id_shop);
			if (!isset($_GET["conf"]))
				$message_confirm = $this->displayConfirmation($this->l('Settings reseted successfully'));
			else
				$message_confirm = "";
		}

		$this->context->smarty->assign('token', htmlspecialchars(Configuration::get('VF_TOKEN', null, null, $this->id_shop)));

		return $this->displayForm($message_confirm);
	}

	protected function displayForm($message_confirm)
	{
		global $currentIndex;

		$jquery = Media::getJqueryPath();
		if (!isset($message_confirm))
			$message_confirm = "";

		// Generate the iso code for currency
		$iso_currency = htmlspecialchars($this->context->currency->iso_code);

		// Multilanguage
		$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		$languages = Language::getLanguages(true);
		$divLangName = 'image_large¤image_block¤flash_name¤image_large_show¤image_block_show';

		// Generate the iso code with id lang
		foreach ($languages AS $language)
			if ($this->context->language->id == (int)$language['id_lang'])
				$lang_iso = htmlspecialchars($language['iso_code']);

		$currency = new Currency(Configuration::get("PS_CURRENCY_DEFAULT"));

		$url_redirect = $currentIndex.'&token='.htmlspecialchars(Tools::getValue('token')).'&configure=vente_flash';
		if (isset($_GET["modif"]))
			if ($this->ifFlashSaleExists(intval(Tools::getValue('modif'))) != true)
				return $this->l('No flash sale');

		if (Tools::getValue('modif')) {
			Configuration::updateValue('VF_LAST_EDIT', intval(Tools::getValue('modif')), null, null, $this->id_shop);
			$id_edit = "&modif=".intval(Configuration::get("VF_LAST_EDIT", null, null, $this->id_shop));
			$select = '
				 SELECT *
				 FROM `'._DB_PREFIX_.'flash` f
				 INNER JOIN `'._DB_PREFIX_.'flash_assoc` fa ON (f.`id_flash` = fa.`id_flash`)
				 WHERE fa.`id_flash` = '.intval(Tools::getValue('modif')).'
				 AND f.`id_shop` = '.$this->id_shop;
			$select_lang = '
				 SELECT *
				 FROM `'._DB_PREFIX_.'flash_lang` f
				 WHERE f.`id_flash` = '.intval(Tools::getValue('modif'));
			$products = Db::getInstance()->ExecuteS($select);
			$req_lang = Db::getInstance()->ExecuteS($select_lang);
			foreach ($languages as $language)
				foreach ($req_lang as $key => $val)
					if ((int)$language['id_lang'] == $val['id_lang'])
						foreach ($val as $attr => $attrval)
							$name_lang[(int)$language['id_lang']][$attr] = htmlspecialchars($attrval);
			$date_start = substr($products[0]["date_start"], 0, 10);
			$hour_start = substr($products[0]["date_start"], 11, 8);
			$date_end = substr($products[0]["date_end"], 0, 10);
			$hour_end = substr($products[0]["date_end"], 11, 8);
			for ($i = 0; $i < sizeof($products); $i++)
			{
				if ($products[$i]["discount_type"] == "percentage") {
					$remise_percent[$i] = $products[$i]["discount"] * 100;
					$remise_devise[$i] = "";
				}
				else {
					$remise_percent[$i] = "";
					$remise_devise[$i] = $products[$i]["discount"];
				}
				$submit = $this->l('Update Flash Sale');
				$action = "update";
				$modif = (int)Tools::getValue('modif');
			}
		}
		else {
			$id_edit = "";
			Configuration::updateValue('VF_LAST_EDIT', 0, null, null, $this->id_shop);
			$name = "";
			$date_start = "";
			$hour_start = "00:00:00";
			$date_end = "";
			$hour_end = "23:59:59";
			$remise_devise = "";
			$remise_percent = "";
			$submit = $this->l('Add Flash Sale');
			$action = "add";
			$modif = "";
		}

		$history = '
			SELECT *
			FROM `'._DB_PREFIX_.'flash`, `'._DB_PREFIX_.'flash_lang`
			WHERE `'._DB_PREFIX_.'flash`.id_flash = `'._DB_PREFIX_.'flash_lang`.id_flash
			AND id_lang = "'.(int)$this->context->language->id.'"
			AND id_shop = "'.(int)$this->id_shop.'"';
		$history = Db::getInstance()->ExecuteS($history);
		$category = Category::getSimpleCategories($this->context->language->id);
		$this->_html = '
		<link rel="stylesheet" href="'._MODULE_DIR_.'vente_flash/css/uploadify.css" type="text/css" />
		<link type="text/css" rel="stylesheet" href="'._MODULE_DIR_.'vente_flash/css/jquery-ui.css" />
		<link type="text/css" rel="stylesheet" href="'._MODULE_DIR_.'vente_flash/css/jquery.miniColors.css" />
		<style type="text/css">
			div.floatLeft {float:left;margin-right:24px;min-height:300px;}
			#productList {font-size:15px;margin-top:1px;}
			label.form {float: left;font-weight: bold;padding: 0.2em 0.5em 0 0;text-align: left;width:100px;}
			div.margin-form1 {color: #7F7F7F;font-size: 0.85em;padding: 0 0 1em 120px}
			#addBtn {float:right;margin-top:-20px;}
			div.none {display:none;}
			table.productFlash {max-width:489px;}
			img.display {width:480px;}
			#arrow {margin-left: 5px;margin-top: 3px;}
		</style>
		<script type="text/javascript">
			var id_shop = "'.$this->id_shop.'";
		</script>
		<script src="http://code.jquery.com/ui/1.9.0/jquery-ui.js"></script>

		<script type="text/javascript" src="'._MODULE_DIR_.'vente_flash/js/swfobject.js"/></script>
		<script type="text/javascript" src="'._MODULE_DIR_.'vente_flash/js/uploadify.js"/></script>
		<script type="text/javascript" src="'._MODULE_DIR_.'vente_flash/js/bo.js"/></script>

		<script src="'._MODULE_DIR_.'vente_flash/js/jquery.miniColors.js"></script>
		<script src="'._MODULE_DIR_.'vente_flash/js/jquery.miniColors.init.js"></script>

		<script type="text/javascript">
			var trad = new Array();
			trad["1"] = "'.$this->l('Your sale doesn\'t have an ID.').'";
			trad["2"] = "'.$this->l('Incorrect Date.').'";
			trad["3"] = "'.$this->l('No product !').'";
			trad["4"] = "'.$this->l('No Discount !').'";
			trad["5"] = "'.$this->l('A product is also in another flash sale during the same date.').'";
			trad["6"] = "'.$this->l('Date start is greater than Date end.').'";
			trad["7"] = "Prestashop : '.$this->l('An error occurred during discount creation.').'";
			trad["8"] = "Prestashop : '.$this->l('An error occurred during sale creation.').'";
			trad["9"] = "Prestashop : '.$this->l('The sale can\'t be disable.').'";
			trad["10"] = "'.$this->l('The sale has been disable.').'";
			trad["11"] = "'.$this->l('The sale has been enable.').'";
			trad["12"] = "Prestashop : '.$this->l('The sale can\'t be enable.').'";
			trad["13"] = "Prestashop : '.$this->l('An error occurred during update sale.').'";
			trad["14"] = "Prestashop : '.$this->l('An error occurred during delete sale.').'";
			trad["15"] = "'.$this->l('Do you want to delete ?').'";
			trad["16"] = "'.$this->l('This product is already on the flash sale').'";
			trad["17"] = "'.$this->l('Invalid character on your ID').'";

			// Usefull for DisplayLang
			function my_changeLanguage(el, id_language)
			{
				$(".multilangs_fields").hide();
				$(".multilangs_fields").each(function(){
					id = $(this).attr("id");
					tmp = id.split("_");
					if (tmp[tmp.length - 1] == id_language)
					{
						$("#"+id).show();
						id = id.replace("_"+id_language, "");
						changeLanguage(id, id, id_language, "");
					}
				});
			}

			var token = "'.Configuration::get('VF_TOKEN', null, null, $this->id_shop).'";
			var id_shop = "'.$this->id_shop.'";

			$(document).ready(function() {
				$("#tabs").tabs();
				var id_font = $("#select_font").val();
				$("#show_font").html("<img style=float:right; src='._MODULE_DIR_.'vente_flash/css/sample_"+id_font+".png>");
				$(".color-picker").miniColors({
					letterCase: "uppercase",
					change: function(hex, rgb) {
						logData(hex, rgb);
					}
				});
				$("#select_font").change(function () {
					var id_font = $("#select_font").val();
					$("#show_font").html("<img style=float:right; src='._MODULE_DIR_.'vente_flash/css/sample_"+id_font+".png>");
				});
			});
		</script>';

		$jqui_tabs = Media::getJqueryUIPath('ui.datepicker', false, true);
		foreach ($jqui_tabs["js"] as $jqui)
			$this->_html .= '<script type="text/javascript" src="'.$jqui.'"></script>';

		$this->_html .= '<script type="text/javascript">'.$this->bindDatepicker('sales_begindate', false).'</script>';
		$this->_html .= '<script type="text/javascript">'.$this->bindDatepicker('sales_enddate', false).'</script>';

		/////////////////////////////////
		//TABS
		/////////////////////////////////
		$this->_html .= $message_confirm.'
		<h2>'.$this->l("Flash Sale").' V2</h2>
		<div id="tabs" style="display:block;font-size:1em;">
			<ul>';
				if ($action == "add")
					$this->_html .= '<li><a href="#tabs-1"><span><img src="'._MODULE_DIR_.'vente_flash/css/add_flash.png"> '.$this->l('Add a Flash Sale').'</span></a></li>';
				else
					$this->_html .= '<li><a href="#tabs-1"><span><img src="'._MODULE_DIR_.'vente_flash/css/edit_flash.png"> '.$this->l('Edit a Flash Sale').'</span></a></li>';
				$this->_html .= '
				<li><a href="#tabs-2"><span><img src="'._MODULE_DIR_.'vente_flash/css/history.png"> '.$this->l('Flash sales created').'</span></a></li>
				<li><a href="#tabs-3"><span><img src="'._MODULE_DIR_.'vente_flash/css/large.png"> '.$this->l('Large Background Options').'</span></a></li>
				<li><a href="#tabs-4"><span><img src="'._MODULE_DIR_.'vente_flash/css/block.png"> '.$this->l('Block Background Options').'</span></a></li>
				<li style="float:right;"><a href="#tabs-5"><span><img src="'._MODULE_DIR_.'vente_flash/css/font.png"> '.$this->l('Custom').'</span></a></li>
			</ul>';
		/////////////////////////////////
		//TAB 1 : ADD A FLASH SALE
		/////////////////////////////////
		$edit_url = htmlspecialchars($currentIndex.'&token='.Tools::getValue('token').'&configure=vente_flash'.$id_edit);
		$new_url = htmlspecialchars($currentIndex.'&token='.Tools::getValue('token').'&configure=vente_flash');
		$i = 0;
		$this->_html .= '
		<div id="tabs-1">
			<div class="error none" id="errorFlash"></div>
			<div class="conf none" id="validFlash"></div>
			<fieldset>';
			if ($action == "add")
				$this->_html .= '<legend>'.$this->l('Add a Flash Sale').'</legend>';
			else
				$this->_html .= '<legend>'.$this->l('Edit a Flash Sale').'</legend>';
			$this->_html .= '<p>'.$this->l('The pictures generated or uploaded will appear in the slider of the flash sale on the home page of your online shop. If you want the image of the flash sale stay fixed, just remove visual products by clicking on the image delete button. To custom more your flash sales, you can edit them on tab "Flash sales created"').'.</p>';
			if ($id_edit != "" && (int)(Configuration::get('VF_LAST_EDIT', null, null, $this->id_shop) != 0))
				$this->_html .= '<a href="'.$new_url.'"><span><img src="'._MODULE_DIR_.'vente_flash/css/add.png"> '.$this->l('New sale').'</span></a>';
				$this->_html .= '<hr>
				<form method="post" id="submitForm" action="'.htmlentities($_SERVER["REQUEST_URI"]."#fragment-1").'">
					<input type="hidden" name="action" value="'.$action.'">
					<input type="hidden" name="modif" value="'.htmlspecialchars($modif).'">
					<input type="hidden" id="dir" value="'._MODULE_DIR_.'vente_flash/" />
					<input type="hidden" id="currentPage" value="'.$edit_url.'" >
					<input type="hidden" id="indexPage" value="'.$new_url.'" >
					<div class="floatLeft">
					'.$this->l('Search product').' :
					<input type="text" id="product_name" />
					<br /><br />
					<table class="table" width="310px" cellspacing=0 cellpadding=0 id="tabProduct">
						<tr>
							<th>ID</th>
							<th>'.$this->l("Name").'</th>
							<th width="60px">'.$this->l("Price").'</th>
							<th>Action</th>
						</tr>
					  </table>
					  <hr>
					<br />
					<label class="form">'.$this->l('Name').'</label>
					<div class="margin-form1">';
						foreach ($languages AS $language)
							$this->_html .= '
							<div class="multilangs_fields" id="flash_name_'.(int)$language['id_lang'].'" style="display: '.((int)$language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
								<input type="text" name="flash_name_'.(int)$language['id_lang'].'" id="flash_name_'.(int)$language['id_lang'].'" value="'.(isset($name_lang[(int)$language['id_lang']]['flash_name']) ? $name_lang[(int)$language['id_lang']]['flash_name'] : '').'"/>
								<p>'.$this->l('Change the flash name language').':</p>
							</div>
						<div class="clear"></div>';
						$this->_html .= $this->displayFlags($languages, $defaultLanguage, $divLangName, 'flash_name', true);
					$this->_html .= '</div><div class="clear"></div><br/>

						<label class="form">'.$this->l('Date Start').'</label>
						<div class="margin-form1">
							<input type="text" id="sales_begindate" name="date_start" value="'.htmlspecialchars($date_start).'" size="10"/>
							<input type="text" id="sales_begindate" name="hour_start" value="'.htmlspecialchars($hour_start).'" size="8"/>
							<sup>*</sup>
							<p>'.$this->l('Date for starting the flash sale').'.<br /> Format: AAAA-MM-JJ HH:MM:SS</p>
						</div>
					<div class="clear"></div>
						<label class="form">'.$this->l('Date End').'</label>
						<div class="margin-form1">
							<input type="text" id="sales_enddate" name="date_end" value="'.htmlspecialchars($date_end).'" size="10" />
							<input type="text" id="sales_begindate" name="hour_end" value="'.htmlspecialchars($hour_end).'" size="8"/>
							 <sup>*</sup>
							<p>'.$this->l('Date for ending the flash sale').'.<br /> Format: AAAA-MM-JJ HH:MM:SS</p>
						</div>
					<hr>
					';
				if ($action != "update")
					$this->_html .= '
							<label class="form">'.$this->l('Select discount').'</label>
							<div class="margin-form1">
								<input type="text" size="4" name="percent_discount" id="percent" value="'.htmlspecialchars($remise_percent).'" /> % '.$this->l('or').'
								<input type="text" size="4" name="euros_discount" id="euros" value="'.htmlspecialchars($remise_devise).'"/> '.$currency->sign.'
							</div>';
				else
				{
					foreach ($products as $product)
					{
						$p = new Product($product["id_product"], false, $this->context->language->id);
						$name = str_replace("'", "&#146;",$p->name);
						$name = str_replace('"', '\"',$name);
						$this->_html .= '
							<label class="form">'.$this->l('Modify discount').'</label>
							<div class="margin-form1">
								'.$this->subStrWithStrip($p->name, 23).' : <br/>
								<input type="text" size="4" name="percent_discount_'.(int)$product["id_product"].'" value="'.htmlspecialchars($remise_percent[$i]).'"> % '.$this->l('or').'
								<input type="text" size="4" name="euros_discount_'.(int)$product["id_product"].'" value="'.htmlspecialchars($remise_devise[$i]).'"> '.$currency->sign.'
							</div>';
						$i++;
					}
				}
				$this->_html .= '
				<br/>
				   <input style="width:100%;" id="addBtn" type="submit" class="button" value="'.$submit.'" />
				</div>
				<div class="floatLeft">
					<h3 id="productList">'.$this->l('Products for Flash Sale').'</h3>
					<p id="warning_delete" class="warning" style="display: none; width: 459px;">'.$this->l('You have deleted a product\'s picture of a flash sale. You can upload another picture if you want, or use the auto-generated picture.').'</p>
					<table class="table" width="499px" productFlash" id="tabFlash" cellspacing = 0 cellpadding = 0>
						<tr>
							<th>ID</th>
							<th width="350">'.$this->l('Name').'</th>
							<th>Actions</th>
							<th>Image</th>
						</tr>';
		 if ($name != "")
			 foreach ($products as $product)
			 {
				 $p = new Product((int)$product["id_product"], false, $this->context->language->id);
				 $name = str_replace("'", "&#146;",$p->name);
				 $name = str_replace('"', '\"',$name);
				 $this->_html .= '
					<input type="hidden" id="prevImg'.(int)$product["id_product"].'" value="'._MODULE_DIR_.'vente_flash/images/'.$lang_iso.'/'.$iso_currency.'_'.(int)$product["id_product"].'.jpg">
					  <script type="text/javascript">
					   moveProduct('.(int)$product["id_product"].', "'.$name.'");';
				 if ($product["image"] != NULL && file_exists(dirname(__FILE__).'/images/'.(int)$product["id_product"].'.jpg'))
					$this->_html .= 'affImg('.(int)$product["id_product"].', "'._MODULE_DIR_.'vente_flash/images/'.(int)$product["id_product"].'.jpg");';
				 else if ($product["image"] != NULL && file_exists(dirname(__FILE__).'/images/'.$lang_iso.'/'.$iso_currency.'_'.(int)$product["id_product"].'.jpg'))
					$this->_html .= 'affImg('.(int)$product["id_product"].', "'._MODULE_DIR_.'vente_flash/images/'.$lang_iso.'/'.$iso_currency.'_'.(int)$product["id_product"].'.jpg");';
				 else
					$this->_html .= 'affImg('.(int)$product["id_product"].', "'._MODULE_DIR_.'vente_flash/css/notexist.png");';
				 $this->_html .= '</script>';
			}
				$this->_html .= '
					</table>
					<hr>
					<p>'.$this->l('If you prefer custom product picture for this flash sale, you can upload a new').'<br/>'.$this->l('picture with this size :').'  535*210.</p>
				   </div>
				</form>
			</fieldset>
		</div>';

		/////////////////////////////////
		//TAB 2 : HISTORY
		/////////////////////////////////
		$this->_html .= '
		<div id="tabs-2">
			<div class="error none" id="errorHistory"></div>
			<div class="conf none" id="validHistory"></div>
			<fieldset><legend>'.$this->l('History').'</legend>
				<h3 id="productList">'.$this->l('Flash sales List').'</h3>
				<table class="table" width="100%" cellspacing = 0 cellpadding = 0>
					<tr>
						<th></th>
						<th>ID</th>
						<th width="200">'.$this->l('Name').'</th>
						<th>'.$this->l('Date Start').'</th>
						<th>'.$this->l('Date End').'</th>
						<th>'.$this->l('Status').'</th>
						<th>'.$this->l('Edit').'</th>
					</tr>';
		foreach ($history as $sale) {
			$this->_html .= '
				<tr>
					 <td><input type="checkbox" name="id_flash" value='.$sale["id_flash"].'></td>
					 <td>'.$sale["id_flash"].'</td>
					 <td>'.$sale["flash_name"].'</td>
					 <td>'.$sale["date_start"].'</td>
					 <td>'.$sale["date_end"].'</td>';

			if ($sale['statut'] == 1)
					$this->_html .= '
					<td align="center" id="statut'.(int)$sale["id_flash"].'"><a onClick="javascript:delFlash('.(int)$sale["id_flash"].');"><img  src="'._MODULE_DIR_.'vente_flash/css/act.png"></a></td>';
			else
				$this->_html .= '
					<td align="center"  id="statut'.(int)$sale["id_flash"].'"><a onClick="javascript:actFlash('.(int)$sale["id_flash"].');"><img id="statut'.(int)$sale["id_flash"].'" src="'._MODULE_DIR_.'vente_flash/css/del.png"></a></td>';
			$this->_html .= '<td align="center"><a href="'.$currentIndex.'&token='.htmlspecialchars(Tools::getValue('token')).'&configure=vente_flash&modif='.(int)$sale["id_flash"].'"><img src="'._MODULE_DIR_.'vente_flash/css/edit.png"></a></td>';
		}

		$this->_html .= '</table>
			<img src="'._MODULE_DIR_.'vente_flash/css/arrow_ltr.png" id="arrow"><a href="javascript:deleteFlash();">'.$this->l('Delete Flash Sale').'</a>
			</fieldset>
		</div>';

		/////////////////////////////////
		//TAB 3 : MANAGE LARGE BACKGROUND
		/////////////////////////////////
		if (file_exists('../modules/vente_flash/images/'.$lang_iso.'/perso.jpg'))
			$image_info = getimagesize('../modules/vente_flash/images/'.$lang_iso.'/perso.jpg');
		else
			$image_info = getimagesize('../modules/vente_flash/images/'.$lang_iso.'/ban.jpg');
		$this->_html .= '
		<div id="tabs-3">
			<fieldset>
			<p>'.$this->l('You can modify position of the countdown before the end of the flash sale, use the horizontal slider and the vertical slider').'.</p>
			<hr>
			<legend>'.$this->l('Large Background Options').'</legend>

			<label>'.$this->l('Large background').' (209*535px) :</label>

		<script type="text/javascript">
		$(document).ready(function() {
			$( "#slider-vertical" ).slider({
				orientation: "vertical",
				range: "min",
				min: 0,
				max: '.$image_info[1].',
				value: '.(int)Configuration::get('VF_COUNT_H', null, null, $this->id_shop).',
				slide: function( event, ui ) {
					$( "#amount_h" ).val( ui.value );
				}
			});
			$( "#slider-horizontal" ).slider({
				orientation: "horizontal",
				range: "min",
				min: 0,
				max: '.$image_info[0].',
				value: '.(int)Configuration::get('VF_COUNT_W', null, null, $this->id_shop).',
				slide: function( event, ui ) {
					$( "#amount_w" ).val( ui.value );
				}
			});
			$( "#amount_h" ).val( $( "#slider-vertical" ).slider( "value" ) );
			$( "#amount_w" ).val( $( "#slider-horizontal" ).slider( "value" ) );
		});
		</script>

			<table>
				<tr>
					<td></td>
					<td>
						<div id="slider-horizontal" style="display:block;width:'.$image_info[0].'px;" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
						<div class="ui-slider-range ui-widget-header ui-slider-range-min" style="height: 100%; ">
						</div>
							<a class="ui-slider-handle ui-state-default ui-corner-all" title="'.$this->l('Custom the width of counter position').'" style="bottom: 100%; "></a>
						</div>
					</td>
				</tr>
				<tr>
					<td>
						<div id="slider-vertical" style="display:block;height:'.$image_info[1].'px;" class="ui-slider ui-slider-vertical ui-widget ui-widget-content ui-corner-all">
						<div class="ui-slider-range ui-widget-header ui-slider-range-min" style="height: 100%; ">
						</div>
							<a class="ui-slider-handle ui-state-default ui-corner-all" title="'.$this->l('Custom the height of counter position').'" style="bottom: 100%; "></a>
						</div>
					</td>
					<td>';

		foreach ($languages AS $language)
			if (file_exists('../modules/vente_flash/images/'.$language['iso_code'].'/perso.jpg'))
				$this->_html .= '
				<div class="multilangs_fields" id="image_large_show_'.(int)$language['id_lang'].'" style="display: '.((int)$language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
					<img src="../modules/vente_flash/images/'.htmlspecialchars($language['iso_code']).'/perso.jpg" name="image_filename_'.(int)$language['id_lang'].'" id="image_filename_'.(int)$language['id_lang'].'"/>
				</div>';
			else
				$this->_html .= '
				<div class="multilangs_fields" id="image_large_show_'.(int)$language['id_lang'].'" style="display: '.((int)$language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
					<img src="../modules/vente_flash/images/'.htmlspecialchars($language['iso_code']).'/ban.jpg" name="image_filename_'.(int)$language['id_lang'].'" id="image_filename_'.(int)$language['id_lang'].'"/>
				</div>';
		$this->_html .= $this->displayFlags($languages, $defaultLanguage, $divLangName, 'image_large_show', true);
		$this->_html .= '<div class="clear"></div>
		</div>';

		$this->_html .= '</td>
				</tr>
				</table>
			<form method="post" action="'.htmlentities($_SERVER["REQUEST_URI"]."#fragment-3").'" enctype="multipart/form-data">
				<label>'.$this->l('Time left Position').' :</label>
				<div class="margin-form">
				'.$this->l('Height').': <input type="text" id="amount_h" value="'.(int)Configuration::get('VF_COUNT_H', null, null, $this->id_shop).'" name="vf_count_h" size="6" />
				'.$this->l('Width').': <input type="text" id="amount_w" value="'.(int)Configuration::get('VF_COUNT_W', null, null, $this->id_shop).'" name="vf_count_w" size="6" />
				('.$image_info[1].'*'.$image_info[0].')
				</div>

				<label>'.$this->l('Upload background').' :</label>
				<div class="margin-form">';
					foreach ($languages AS $language)
						$this->_html .= '
						<div class="multilangs_fields" id="image_large_'.(int)$language['id_lang'].'" style="display: '.((int)$language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
							<input type="file" name="image_filename_'.(int)$language['id_lang'].'" id="image_filename_'.(int)$language['id_lang'].'"/>
							<p>'.$this->l('You can upload a new background').' <b>(209*535px)</b></p>
						</div>';
					$this->_html .= $this->displayFlags($languages, $defaultLanguage, $divLangName, 'image_large', true);
					$this->_html .= '<div class="clear"></div>
				</div>
				<div class="margin-form">
					<input type="submit" class="button" name="ChangeBackground" value="'.$this->l('Save').'"/>
					<input type="submit" class="button" name="DeleteBackground" value="'.$this->l('Delete the customs backgrounds').'" />
				</div>
			</form>
			</fieldset>
		</div>';

		/////////////////////////////////
		//TAB 4 : MANAGE BLOCK BACKGROUND
		/////////////////////////////////
		if (file_exists('../modules/vente_flash/images/'.$lang_iso.'/perso_small.jpg'))
			$image_info = getimagesize('../modules/vente_flash/images/'.$lang_iso.'/perso_small.jpg');
		else
			$image_info = getimagesize('../modules/vente_flash/images/'.$lang_iso.'/banBlock.png');
		$this->_html .= '
		<div id="tabs-4">
		<fieldset><legend>'.$this->l('Block Background Options').'</legend>
			<p>'.$this->l('You can modify position of the countdown before the end of the flash sale, use the horizontal slider and the vertical slider').'.</p>
			<hr>
			<label>'.$this->l('Block background').' (180*190px) :</label>

		<script type="text/javascript">
		$(document).ready(function() {
			$( "#slider-vertical2" ).slider({
				orientation: "vertical",
				range: "min",
				min: 0,
				max: '.$image_info[1].',
				value: '.(int)Configuration::get('VF_COUNT_SH', null, null, $this->id_shop).',
				slide: function( event, ui ) {
					$( "#amount_sh" ).val( ui.value );
				}
			});
			$( "#slider-horizontal2" ).slider({
				orientation: "horizontal",
				range: "min",
				min: 0,
				max: '.$image_info[0].',
				value: '.(int)Configuration::get('VF_COUNT_SW', null, null, $this->id_shop).',
				slide: function( event, ui ) {
					$( "#amount_sw" ).val( ui.value );
				}
			});
			$( "#amount_sh" ).val( $( "#slider-vertical2" ).slider( "value" ) );
			$( "#amount_sw" ).val( $( "#slider-horizontal2" ).slider( "value" ) );
		});
		</script>

		<table>
			<tr>
				<td></td>
				<td>
					<div id="slider-horizontal2" style="display:block;width:'.(int)$image_info[0].'px;" class="ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all">
					<div class="ui-slider-range ui-widget-header ui-slider-range-min" style="height: 100%; ">
					</div>
					<a class="ui-slider-handle ui-state-default ui-corner-all" title="'.$this->l('Custom the width of counter position').'" style="bottom: 100%; "></a>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div id="slider-vertical2" style="display:block;height:'.(int)$image_info[1].'px;" class="ui-slider ui-slider-vertical ui-widget ui-widget-content ui-corner-all">
					<div class="ui-slider-range ui-widget-header ui-slider-range-min" style="height: 100%; ">
					</div>
					<a class="ui-slider-handle ui-state-default ui-corner-all" title="'.$this->l('Custom the height of counter position').'" style="bottom: 100%; "></a>
					</div>
				</td>
				<td>';
		foreach ($languages AS $language)
			if (file_exists('../modules/vente_flash/images/'.htmlspecialchars($language['iso_code']).'/perso_small.jpg'))
				$this->_html .= '
				<div class="multilangs_fields" id="image_block_show_'.(int)$language['id_lang'].'" style="display: '.((int)$language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
					<img src="../modules/vente_flash/images/'.htmlspecialchars($language['iso_code']).'/perso_small.jpg" name="image_filename_'.(int)$language['id_lang'].'" id="image_filename_'.(int)$language['id_lang'].'"/>
				</div>';
			else
				$this->_html .= '
				<div class="multilangs_fields" id="image_block_show_'.(int)$language['id_lang'].'" style="display: '.((int)$language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
					<img src="../modules/vente_flash/images/'.htmlspecialchars($language['iso_code']).'/banBlock.png" name="image_filename_'.(int)$language['id_lang'].'" id="image_filename_'.(int)$language['id_lang'].'"/>
				</div>';
		$this->_html .= $this->displayFlags($languages, $defaultLanguage, $divLangName, 'image_block_show', true);
		$this->_html .= '<div class="clear"></div>
		</div>';
		$this->_html .= '
				</td>
			</tr>
		</table>


			<form method="post" action="'.htmlentities($_SERVER["REQUEST_URI"]."#fragment-4").'" enctype="multipart/form-data">
			<label>'.$this->l('Time left Position').' :</label>
				<div class="margin-form">
				'.$this->l('Height').': <input type="text" id="amount_sh" value="'.(int)Configuration::get('VF_COUNT_SH', null, null, $this->id_shop).'" name="vf_count_sh" size="6" />
				'.$this->l('Width').': <input type="text" id="amount_sw" value="'.(int)Configuration::get('VF_COUNT_SW', null, null, $this->id_shop).'" name="vf_count_sw" size="6" />
				('.$image_info[1].'*'.$image_info[0].')
				</div>

				<label>'.$this->l('Upload background').' :</label>
				<div class="margin-form">';
					foreach ($languages AS $language)
						$this->_html .= '
						<div class="multilangs_fields" id="image_block_'.(int)$language['id_lang'].'" style="display: '.((int)$language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
							<input type="file" name="image_filename_'.(int)$language['id_lang'].'" id="image_filename_'.(int)$language['id_lang'].'"/>
							<p>'.$this->l('You can upload a new background').' <b>(180*190px)</b></p>
						</div>';
					$this->_html .= $this->displayFlags($languages, $defaultLanguage, $divLangName, 'image_block', true);
					$this->_html .= '<div class="clear"></div>
				</div>
				<div class="margin-form">
					<input type="submit" class="button" name="ChangeBackgroundSmall" value="'.$this->l('Save').'"/>
					<input type="submit" class="button" name="DeleteBackgroundSmall" value="'.$this->l('Delete the custom background').'" />
				</div>
			</form>
			</fieldset>
		</div>';

		/////////////////////////////////
		//TAB 5 : CUSTOM
		/////////////////////////////////
		$tabPolice = array(
				1 => 'Arial',
				2 => 'Georgia',
				3 => 'Times New Roman',
				4 => 'Tahoma',
				5 => 'Verdana'
			);
		$this->_html .= '
		<div id="tabs-5">
			<fieldset><legend>'.$this->l('Custom flash sales').'</legend>
			<p>'.$this->l('Here you can custom the police, font color and background color of time left counter').'.</p>
				<form method="post" action="'.htmlentities($_SERVER["REQUEST_URI"]."#fragment-5").'">
					<label>'.$this->l('Flash sales font').' :</label>
					<div class="margin-form">
						<select name="font" value="'.(int)Configuration::get('VF_FONT', null, null, $this->id_shop).'">';
						for ($i = 1; $i <= 5; $i++)
							if ((int)Configuration::get('VF_FONT', null, null, $this->id_shop) == $i)
								$this->_html .= '<option selected value="'.$i.'">'.htmlspecialchars($tabPolice[$i]).'</option>';
							else
								$this->_html .= '<option value="'.$i.'">'.htmlspecialchars($tabPolice[$i]).'</option>';
						$this->_html .= '
						</select>
					</div>
					<label>'.$this->l('Flash sales font color').' :</label>
					<div class="margin-form">
						<input type="text" name="color_font" class="color-picker" size="6" value="'.htmlspecialchars(Configuration::get('VF_FONT_COLOR', null, null, $this->id_shop)).'" />
					</div>
					<label>'.$this->l('Flash sales background color').' :</label>
					<div class="margin-form">
						<input type="text" name="color_bg" class="color-picker" size="6" value="'.htmlspecialchars(Configuration::get('VF_BG_COLOR', null, null, $this->id_shop)).'" />
					</div>
					<hr>
					<p>'.$this->l('Here you can custom the police of generated pictures of flash sales (This is on the right an example of the selected font)').'.</p>
					<label>'.$this->l('Pictures font').' :</label>
					<div class="margin-form">
						<select id="select_font" name="pictures_font" value="'.(int)Configuration::get('VF_IMAGE_FONT', null, null, $this->id_shop).'">';
						for ($i = 1; $i <= 6; $i++)
							if ((int)Configuration::get('VF_IMAGE_FONT', null, null, $this->id_shop) == $i)
								$this->_html .= '<option selected value="'.$i.'">Police '.$i.'</option>';
							else
								$this->_html .= '<option value="'.$i.'">Police '.$i.'</option>';
						$this->_html .= '
						</select>
					</div>
					<div id="show_font"></div>
					<label>'.$this->l('Pictures font price color').' :</label>
					<div class="margin-form">
						<input type="text" name="pictures_color_price" class="color-picker" size="6" value="'.htmlspecialchars(Configuration::get('VF_IMAGE_PRICE_COLOR', null, null, $this->id_shop)).'" />
					</div>
					<label>'.$this->l('Pictures font name color').' :</label>
					<div class="margin-form">
						<input type="text" name="pictures_color_name" class="color-picker" size="6" value="'.htmlspecialchars(Configuration::get('VF_IMAGE_NAME_COLOR', null, null, $this->id_shop)).'" />
					</div>
					<div class="warning" style="width:820px;">
						'.$this->l('You have changed the font or generated image\'s colors ? So for the changes take effect, you must').' :
						<ul>
							<li>'.$this->l('Save (below this)').',</li>
							<li>'.$this->l('Click on the tab "Flash sales created"').',</li>
							<li>'.$this->l('Select your flash sales and click on "Edit"').'</li>
							<li>'.$this->l('Once on the flash sales details, click directly on "Update Flash Sale"').'</li>
						</ul>
					</div>
					<div class="margin-form">
						<input type="submit" class="button" name="Custom" value="'.$this->l('Save').'"/>
						<input type="submit" class="button" name="ResetCustom" value="'.$this->l('Reset customs').'"/>
					</div>
				</form>
			</fieldset>
		</div>';

		$this->_html .= '</div><br/>';

		/////////////////////////////////
		//PRESTASHOP
		/////////////////////////////////
		$this->_html .=  '<fieldset>
			<legend>Prestashop Addons</legend>
					<b>'.$this->l('Thank you for choosing a module developed by the Addons Team of PrestaShop.').'</a></b><br /><br />
					'.$this->l('If you encounter a problem using the module, our team is at your service via the ').'<a href="http://addons.prestashop.com/contact-form.php">'.$this->l('contact form').'</a>.
				</fieldset>';

			$theme = Tools::getValue('mainTheme');

			return $this->_html;
		}

		private function getLastFlash()
		{
			$date = date("Y-m-d H:i:s");
			$select = '
				SELECT `date_end`, `image`,`id_product`
				FROM `'._DB_PREFIX_.'flash` f
				INNER JOIN `'._DB_PREFIX_.'flash_assoc` fa ON (f.`id_flash` = fa.`id_flash`)
				WHERE (f.`date_start`<= "'.pSQL($date).'"
				AND f.`date_end` >= "'.pSQL($date).'")
				AND f.`statut` = 1
				AND f.`id_shop` = '.$this->id_shop.'
				ORDER BY f.`date_end` ASC';
			return Db::getInstance()->ExecuteS($select);
		}

		private function getLastFlashId($id)
		{
			$date = date("Y-m-d H:i:s");
			$select = '
				SELECT *
				FROM `'._DB_PREFIX_.'flash` f
				INNER JOIN `'._DB_PREFIX_.'flash_assoc` fa ON (f.`id_flash` = fa.`id_flash`)
				WHERE (f.`date_start`<= "'.pSQL($date).'"
				AND f.`date_end` >= "'.pSQL($date).'")
				AND f.`statut` = 1
				AND fa.`id_product` = '.(int)$id.'
				ORDER BY f.`date_end` ASC';
			return Db::getInstance()->ExecuteS($select);
		}

		public function hookHome($params)
		{
			$this->context->smarty->assign('lang', $this->context->language->id);

			$html = '';

			// Generate the iso code for currency
			$iso_currency = htmlspecialchars($this->context->currency->iso_code);

			// Generate the iso code with id lang
			$languages = Language::getLanguages(true);
			foreach ($languages AS $language)
				if ($this->context->language->id == (int)$language['id_lang'])
					$lang_iso = htmlspecialchars($language['iso_code']);

			if (($product = $this->getLastFlash())) {
				$this->context->smarty->assign('flash', true);
				$year = substr($product[0]["date_end"], 0, 4);
				$month = intval(substr($product[0]["date_end"], 5, 2));
				$day = intval(substr($product[0]["date_end"], 8, 2));
				$hour = substr($product[0]["date_end"], 11, 2);
				$minute = substr($product[0]["date_end"], 14,2);
				$second = substr($product[0]["date_end"], 17, 2);
				$input = '
					<input type="hidden" value="'._MODULE_DIR_.'" id="dir">
					<input type="hidden" value="'.$this->context->language->id.'" id="lang">
					<input id="year" type="hidden" value="'.$year.'" />
					<input id="month" type="hidden" value="'.$month.'" />
					<input id="day" type="hidden" value="'.$day.'" />
					<input id="hour" type="hidden" value="'.$hour.'" />
					<input id="minute" type="hidden" value="'.$minute.'" />
					<input id="second" type="hidden" value="'.$second.'" />';
				$this->context->smarty->assign('input', $input);

				$size['w'] = 535;
				$size['h'] = 209;
				$size['h2'] = 209 - 3;

				if (file_exists('modules/vente_flash/images/'.$lang_iso.'/perso.jpg'))
				{
					$this->context->smarty->assign('img_perso', _MODULE_DIR_ . 'vente_flash/images/'.$lang_iso.'/perso.jpg');
					$size_data = getimagesize('modules/vente_flash/images/'.$lang_iso.'/perso.jpg');
				}
				else
				{
					$this->context->smarty->assign('img_perso', _MODULE_DIR_ . 'vente_flash/images/'.$lang_iso.'/ban.jpg');
					$size_data = getimagesize('modules/vente_flash/images/'.$lang_iso.'/ban.jpg');
				}
				$size['w'] = $size_data[0];
				$size['h'] = $size_data[1];
				$size['h2'] = $size_data[1] - 3;

				$this->context->smarty->assign('vf_size', $size);

				$this->context->smarty->assign('vf_count_h', (int)Configuration::get('VF_COUNT_H', null, null, $this->id_shop));
				$this->context->smarty->assign('vf_count_w', (int)Configuration::get('VF_COUNT_W', null, null, $this->id_shop));

				$html  .= '<li><a href="#"><img src="'._MODULE_DIR_.'vente_flash/css/clear.png"  width="'.$size['w'].'" height="'.$size['h2'].'" alt="" /></a></li>';
				foreach ($product as $p)
				{
					$link = $this->context->link->getProductLink((int)$p['id_product']);

					if (file_exists(dirname(__FILE__).'/images/'.(int)$p["id_product"].'.jpg'))
					{
						$html .= '<li><a href="'.$link.'"><img src="'._MODULE_DIR_.'vente_flash/images/'.(int)$p["id_product"].'.jpg"  width="'.$size['w'].'" height="'.$size['h'].'" class="imgSlide"  alt="" /></a></li>';
						$html  .= '<li><a href="#"><img src="'._MODULE_DIR_.'vente_flash/css/clear.png"  width="'.$size['w'].'" height="'.$size['h'].'" alt="" /></a></li>';
					}
					elseif (file_exists(dirname(__FILE__).'/images/'.$lang_iso.'/'.$iso_currency.'_'.(int)$p["id_product"].'.jpg'))
					{
						$html .= '<li><a href="'.$link.'"><img src="'._MODULE_DIR_.'vente_flash/images/'.$lang_iso.'/'.$iso_currency.'_'.(int)$p["id_product"].'.jpg"  width="'.$size['w'].'" height="'.$size['h'].'" class="imgSlide"  alt="" /></a></li>';
						$html  .= '<li><a href="#"><img src="'._MODULE_DIR_.'vente_flash/css/clear.png"  width="'.$size['w'].'" height="'.$size['h'].'" alt="" /></a></li>';
					}
				}
			}
			else {
				$this->context->smarty->assign('flash', false);
			}

		$this->context->smarty->assign(array(  'html'      => $html,
								'dir'       => _MODULE_DIR_));

		$this->context->smarty->assign(array(
				'font_color' => htmlspecialchars(Configuration::get('VF_FONT_COLOR', null, null, $this->id_shop)),
				'bg_color' => htmlspecialchars(Configuration::get('VF_BG_COLOR', null, null, $this->id_shop)),
				'tpl_dirr' => _PS_THEME_DIR_
		));

		return $this->display(__FILE__, 'templates/home.tpl');
	}

	public function hookRightColumn($params)
	{
		$html = "";
		$this->context->smarty->assign('lang', $this->context->language->id);

		// Generate the iso code for currency
		$iso_currency = htmlspecialchars($this->context->currency->iso_code);

		// Generate the iso code with id lang
		$languages = Language::getLanguages(true);
		foreach ($languages AS $language)
			if ($this->context->language->id == (int)$language['id_lang'])
			$lang_iso = htmlspecialchars($language['iso_code']);

		if (($product = $this->getLastFlash()))
		{
			$this->context->smarty->assign('flash', true);
			$year = substr($product[0]["date_end"], 0, 4);
			$month = intval(substr($product[0]["date_end"], 5, 2));
			$day = intval(substr($product[0]["date_end"], 8, 2));
			$hour = substr($product[0]["date_end"], 11, 2);
			$minute = substr($product[0]["date_end"], 14,2);
			$second = substr($product[0]["date_end"], 17, 2);
			$input = '
				<input id="year" type="hidden" value="'.$year.'" />
				<input id="month" type="hidden" value="'.$month.'" />
				<input id="day" type="hidden" value="'.$day.'" />
				<input id="hour" type="hidden" value="'.$hour.'" />
				<input id="minute" type="hidden" value="'.$minute.'" />
				<input id="second" type="hidden" value="'.$second.'" />';
			$this->context->smarty->assign('input', $input);
			$html  .= '<li><a href="#"><img src="'._MODULE_DIR_.'vente_flash/css/clear.png"  width="190" height="180" alt="" /></a></li>';

			foreach ($product as $p)
			{
				if (file_exists(dirname(__FILE__).'/images/s'.$p["id_product"].'.jpg'))
				{
					$html .= '<li><a href="'.$this->context->link->getPageLink('product.php').'?id_product='.(int)$p["id_product"].'"><img border="0" src="'._MODULE_DIR_.'vente_flash/images/s'.(int)$p["id_product"].'.jpg" width="190" height="180" class="imgSlide" alt="" /></a></li>';
					$html .= '<li><a href="#"><img src="'._MODULE_DIR_.'vente_flash/css/clear.png"  width="190" height="180" alt="" /></a></li>';
				}
				else if (file_exists(dirname(__FILE__).'/images/'.$lang_iso.'/'.$iso_currency.'_'.(int)$p["id_product"].'_s.jpg'))
				{
					$html .= '<li><a href="'.$this->context->link->getPageLink('product.php').'?id_product='.(int)$p["id_product"].'"><img src="'._MODULE_DIR_.'vente_flash/images/'.$lang_iso.'/'.$iso_currency.'_'.(int)$p["id_product"].'_s.jpg" width="190" height="180" class="imgSlide"  alt="" /></a></li>';
					$html .= '<li><a href="#"><img src="'._MODULE_DIR_.'vente_flash/css/clear.png" width="190" height="180" alt="" /></a></li>';
				}
			}
		}
		else
			$this->context->smarty->assign('flash', false);

		$this->context->smarty->assign(array(  'html'  => $html,
								'title' => $this->l('Flash Sale'),
								'dir'   => _MODULE_DIR_));

		$size_small['w'] = 190;
		$size_small['h'] = 180;
		$size_small['h2'] = 180 - 3;



		if (file_exists(dirname(__FILE__).'/images/'.$lang_iso.'/perso_small.jpg'))
		{
			$this->context->smarty->assign('img_perso_small', _MODULE_DIR_ .'vente_flash/images/'.$lang_iso.'/perso_small.jpg');
			$size_data = getimagesize(__DIR__.'/images/'.$lang_iso.'/perso_small.jpg');
		}
		else
		{
			$this->context->smarty->assign('img_perso_small', _MODULE_DIR_ .'vente_flash/images/'.$lang_iso.'/banBlock.png');
			$size_data = getimagesize(__DIR__.'/images/'.$lang_iso.'/banBlock.png');
		}
		$size_small['w'] = $size_data[0];
		$size_small['h'] = $size_data[1];
		$size_small['h2'] = $size_data[1] - 3;

		$this->context->smarty->assign('vf_size_small', $size_small);
		$this->context->smarty->assign('vf_count_sh', (int)Configuration::get('VF_COUNT_SH', null, null, $this->id_shop));
		$this->context->smarty->assign('vf_count_sw', (int)Configuration::get('VF_COUNT_SW', null, null, $this->id_shop));

		$this->context->smarty->assign(array(
				'font_color' => htmlspecialchars(Configuration::get('VF_FONT_COLOR', null, null, $this->id_shop)),
				'bg_color' => htmlspecialchars(Configuration::get('VF_BG_COLOR', null, null, $this->id_shop)),
				'font_style' => $this->getPolice((int)Configuration::get('VF_FONT', null, null, $this->id_shop)),
				'tpl_dirr' => _PS_THEME_DIR_
		));
		return $this->display(__FILE__, 'templates/block.tpl');
	}

	public function hookProductActions($params)
	{
		$id_product = Tools::getValue('id_product');
		if (Product::getQuantity((int)$id_product) < 1)
			return '';

		$this->context->smarty->assign('lang', $this->context->language->id);
		if (($product = $this->getLastFlashId((int)$id_product)))
		{
			$this->context->smarty->assign('flash', true);
			$year = substr($product[0]["date_end"], 0, 4);
			$month = intval(substr($product[0]["date_end"], 5, 2));
			$day = intval(substr($product[0]["date_end"], 8, 2));
			$hour = substr($product[0]["date_end"], 11, 2);
			$minute = substr($product[0]["date_end"], 14,2);
			$second = substr($product[0]["date_end"], 17, 2);
			$en_date = $month."/".$day."/".$year." ";
			$hour_format = $hour;
			if ($hour > 12)
			{
				$hour_format = $hour - 12;
				$hour_suff = "PM";
			}
			else
				$hour_suff = "AM";
			$en_date .= $hour_format.":".$minute.":".$second." ".$hour_suff;

			$input = '
				<input id="show_year" type="hidden" value="'.$year.'" />
				<input id="show_month" type="hidden" value="'.$month.'" />
				<input id="show_day" type="hidden" value="'.$day.'" />
				<input id="show_hour" type="hidden" value="'.$hour.'" />
				<input id="show_minute" type="hidden" value="'.$minute.'" />
				<input id="show_second" type="hidden" value="'.$second.'" />';
			$this->context->smarty->assign('input', $input);
		}
		else
			$this->context->smarty->assign('flash', false);
			$this->context->smarty->assign(array(
				'font_color' => htmlspecialchars(Configuration::get('VF_FONT_COLOR', null, null, $this->id_shop)),
				'bg_color' => htmlspecialchars(Configuration::get('VF_BG_COLOR', null, null, $this->id_shop)),
				'font_style' => $this->getPolice((int)Configuration::get('VF_FONT', null, null, $this->id_shop)),
				'tpl_dirr' => _PS_THEME_DIR_
				));
		return $this->display(__FILE__, 'templates/product.tpl');
	}

	public function hookLeftColumn($params)
	{
		return $this->hookRightColumn($params);
	}

	public function hookHeader($params)
	{
		$this->context->controller->addJs(_MODULE_DIR_.'vente_flash/js/countdown_show.js');
		$this->context->controller->addJs(_MODULE_DIR_."vente_flash/js/vente.js");
		$this->context->controller->addJs(_MODULE_DIR_."vente_flash/js/easyslider.js");
		$this->context->controller->addCSS(_MODULE_DIR_."vente_flash/css/vente_fo.css");
		$this->context->smarty->assign(array(
				'font_color' => htmlspecialchars(Configuration::get('VF_FONT_COLOR', null, null, $this->id_shop)),
				'font_style' => $this->getPolice((int)Configuration::get('VF_FONT', null, null, $this->id_shop)),
				'tpl_dirr' => _PS_THEME_DIR_)
				);
	}

	function subStrWithStrip($string, $length)
	{
		if ($length == NULL)
			$length = 50;

		$stringDisplay = substr(strip_tags($string), 0, $length);
		if (strlen(strip_tags($string)) > $length)
			$stringDisplay .= ' ...';
		return $stringDisplay;
	}

	function getPolice($idFont)
	{
		if ($idFont == 1)
			$font = 'Arial, sans-serif';
		else if ($idFont == 2)
			$font = 'Georgia';
		else if ($idFont == 3)
			$font = 'Times New Roman, serif';
		else if ($idFont == 4)
			$font = 'Tahoma, Geneva, Kalimati, sans-serif';
		else if ($idFont == 5)
			$font = 'Verdana, sans-serif';
		else
			$font = 'Times New Roman", serif';
		return $font;
	}

	function getCover($id_product, $type = NULL)
	{
		$select = '
		SELECT *
		FROM `'._DB_PREFIX_.'product` p
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product`)
		LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
		LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.intval($this->context->language->id).')
		WHERE p.`id_product` = '.intval($id_product).'
		AND pl.`id_lang` = '.intval($this->context->language->id).'
		ORDER BY pl.`name`';

		$row = Db::getInstance()->ExecuteS($select);
		$row = Product::getProductProperties(intval($this->context->language->id),$row[0]);

		return __PS_BASE_URI__."img/p/".$row['id_image']."-".$type.".jpg";
	}

	function ifFlashSaleExists($id)
	{
		$select  =  '
		SELECT id_flash
		FROM `'._DB_PREFIX_.'flash` p
		WHERE `id_flash` = '.$id.'
		AND `id_shop` = '.$this->id_shop;

		$req = Db::getInstance()->ExecuteS($select);

		if (!empty($req))
			return true;
		else
			return false;
	}

	function deleteBySpecificPriceId($id_specificprice)
	{
		return Db::getInstance()->Execute('DELETE FROM `'._DB_PREFIX_.'specific_price` WHERE `id_specific_price` = '.(int)$id_specificprice);
	}

	function bindDatepicker($id, $time)
	{
		$return = "";
		if ($time)
			$return .= '
			var dateObj = new Date();
			var hours = dateObj.getHours();
			var mins = dateObj.getMinutes();
			var secs = dateObj.getSeconds();
			if (hours < 10) { hours = "0" + hours; }
			if (mins < 10) { mins = "0" + mins; }
			if (secs < 10) { secs = "0" + secs; }
			var time = " "+hours+":"+mins+":"+secs;';

		$return .= '
		$(function() {
			$("#'.Tools::htmlentitiesUTF8($id).'").datepicker({
				prevText:"",
				nextText:"",
				dateFormat:"yy-mm-dd"'.($time ? '+time' : '').'});
		});';

		return $return;
	}
}
