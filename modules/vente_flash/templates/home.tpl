{if $flash == 'true'}
{literal}
<script type="text/javascript">
$(document).ready(function(){
	timer();
	$("#slider").click(function(){
		window.location.href = $("input#dir").val() + "vente_flash/flashSale.php";
	});
});
</script>
{/literal}
<div class="flashsale">
	<div class="block_products_block">
		<div class="block_content">
			{$input}
			<div id="slider" class="slider img{$lang|escape:'html'}" style="{if isset($img_perso)}background: url('{$img_perso|escape:'html'}') no-repeat;{/if}">
				<div style="height:{$vf_size.h|escape:'html'}px;">
					<ul>
						{$html}
					</ul>
				</div>
				<div id="time" style="margin-top:-{$vf_count_h|escape:'html'}px; margin-left: {$vf_count_w|escape:'html'}px; color:{$font_color|escape:'html'}; font-family:{$font_style|escape:'html'};"></div>
			</div>
			<div style="clear:both;"></div>
		</div>
	</div>
</div>
{/if}
