{if $flash == 'true'}
{literal}
<script type="text/javascript">
$(document).ready(function(){
	timerShowProduct();
});
</script>
{/literal}
<br/><br/>
<div>
	<div class="block_content">
		{$input}
	    <div id="timerBlock"  class="timerBlock imgTime{$lang|escape:'html'}" style="color:{$font_color|escape:'html'}; background-color:{$bg_color|escape:'html'}; font-family:{$font_style|escape:'html'};" >
	    	<p>{l s='Quick, only' mod='vente_flash'} :</p><div id="timeProduct"></div>
	    </div>
	</div>
</div>
{/if}