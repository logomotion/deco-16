{if $flash == 'true'}
{literal}
<script type="text/javascript">
$(document).ready(function(){
	var dir = {/literal}"{$dir}"{literal};
	timer();
	$("#blockSlider").click(function(){
		
		window.location.href = dir+"vente_flash/flashSale.php";
	});
});
</script>
{/literal}
<div class="block">
	<h4><a href="{$dir|escape:'html'}vente_flash/flashSale.php"> {$title|escape:'html'}</a></h4>
	<div class="block_content" id="block" style="height:220px;"><br />
		{$input}
		<div id="blockSlider" class="sliderBlock imgBlock{$lang|escape:'html'}" style="width:{$vf_size_small.w|escape:'html'}px;height:{$vf_size_small.h|escape:'html'}px;{if isset($img_perso_small)}background: url('{$img_perso_small|escape:'html'}');{/if}">
			<ul>
				{$html}
			</ul>
		<div class="timeBlock" style="margin-top:-{$vf_count_sh|escape:'html'}px; margin-left: {$vf_count_sw|escape:'html'}px; color:{$font_color|escape:'html'}; font-family:{$font_style|escape:'html'};"></div>
		</div>
	</div>
</div>
{/if}