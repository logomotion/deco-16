<?php

require_once(dirname(__FILE__).'/../../vente_flash.php');

class vente_flashlistModuleFrontController extends ModuleFrontController
{

    /**
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();

        $this->displayContent();
    }

    public function setMedia()
	{
		parent::setMedia();
		$this->context->controller->addCSS(array(
			_PS_CSS_DIR_.'jquery.cluetip.css' => 'all',
			_THEME_CSS_DIR_.'scenes.css' => 'all',
			_THEME_CSS_DIR_.'category.css' => 'all',
			_THEME_CSS_DIR_.'product_list.css' => 'all'));

		if ((int)Configuration::get('PS_COMPARATOR_MAX_ITEM') > 0)
			$this->context->controller->addJS(_THEME_JS_DIR_.'products-comparison.js');
	}

    public function displayContent()
	{
            global $cookie;
            $date = date("Y-m-d H:i:s");
            $select = '
                SELECT *
                FROM `'._DB_PREFIX_.'flash` f, `'._DB_PREFIX_.'flash_lang` fl
                WHERE `f`.id_flash = `fl`.id_flash
                AND (f.`date_start`<= "'.pSQL($date).'"
                AND f.`date_end` >= "'.pSQL($date).'")
                AND f.`statut` = 1
                AND f.id_shop = '.(int)$this->context->shop->id.'
                AND id_lang = "'.(int)$cookie->id_lang.'"
                ORDER BY f.`date_end` ASC';
            $sale = Db::getInstance()->ExecuteS($select);
            parent::displayContent();

            $link = $this->context->link->getModuleLink('vente_flash', 'show');
            self::$smarty->assign("sale", $sale);
            self::$smarty->assign("link_module", $link);

            $this->setTemplate('list_flash.tpl');
	}
}
