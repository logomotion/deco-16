<?php

require_once(dirname(__FILE__).'/../../vente_flash.php');

class vente_flashshowModuleFrontController extends ModuleFrontController
{

    /**
     * @see FrontController::initContent()
     */
    public function initContent()
    {
        parent::initContent();
        
        $this->displayContent();
    }

    public function setMedia()
    {
            parent::setMedia();
            $this->context->controller->addCSS(array(
                    _PS_CSS_DIR_.'jquery.cluetip.css' => 'all',
                    _THEME_CSS_DIR_.'scenes.css' => 'all',
                    _THEME_CSS_DIR_.'category.css' => 'all',
                    _THEME_CSS_DIR_.'product_list.css' => 'all')
            );

            if ((int)Configuration::get('PS_COMPARATOR_MAX_ITEM') > 0)
                    $this->context->controller->addJs(_THEME_JS_DIR_.'products-comparison.js');
    }

    public function displayContent()
    {
            $id = (int)Tools::getValue('id');
            if ($id == 0)
            {
            	$select = '
	            	SELECT *
	            	FROM `'._DB_PREFIX_.'flash` f';
            	$sale = Db::getInstance()->ExecuteS($select);
            	$id = (int)$sale[0]["id_flash"];
            }

            $date = date("Y-m-d H:i:s");
            
            parent::displayContent();
            $sql ='SELECT p.*, pl.`description_short`, pl.`link_rewrite`, pl.`name`, tax.`rate`, i.`id_image`, il.`legend`, m.`name` manufacturer_name, 1 position
            FROM `'._DB_PREFIX_.'product` p
            INNER JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)$this->context->language->id.')
            LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
            LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$this->context->language->id.')
            LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (p.`id_tax_rules_group` = tr.`id_tax_rules_group`
                                                       AND tr.`id_country` = '.(int)$this->context->country->id_zone.'
                                                       AND tr.`id_state` = 0)
            LEFT JOIN `'._DB_PREFIX_.'tax` tax ON (tax.`id_tax` = tr.`id_tax`)
            LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (m.`id_manufacturer` = p.`id_manufacturer`)

            LEFT JOIN `'._DB_PREFIX_.'product_shop` ps ON (p.`id_product` = ps.`id_product`)
            LEFT JOIN `'._DB_PREFIX_.'flash_assoc` fa ON (ps.`id_product` = fa.`id_product`)
            LEFT JOIN `'._DB_PREFIX_.'flash` f ON (fa.id_flash = f.`id_flash`)

            WHERE p.`active` = 1
            AND ps.id_shop = '.(int)$this->context->shop->id.'
            AND f.id_shop = '.(int)$this->context->shop->id.'
            AND pl.id_shop = '.(int)$this->context->shop->id.'
            AND f.id_flash = '.$id;

            $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS($sql);

            $this->getChronoWithId($id);
            $products = Product::getProductsProperties($this->context->language->id, $result);
            
            $select = '
	            SELECT *
	            FROM `'._DB_PREFIX_.'flash` f, `'._DB_PREFIX_.'flash_lang` fl
	            WHERE `f`.id_flash = `fl`.id_flash
	            AND (f.`date_start`<= "'.pSQL($date).'"
	            AND f.`date_end` >= "'.pSQL($date).'")
	            AND f.`statut` = 1
	            AND fl.id_lang = "'.$this->context->language->id.'"
                AND f.id_shop = '.(int)$this->context->shop->id.'
	            AND f.id_flash = '.(int)$id.'
	            ORDER BY f.`date_end` ASC';
            $sale = Db::getInstance()->ExecuteS($select);
            if (empty($sale[0]["flash_name"]))
            	$sale[0]["flash_name"] = "";
            $this->context->smarty->assign(array(
                'products' => $products,
                'name' => $sale[0]["flash_name"]));

            /* Test if flash sale is finished */
            $ifFinished = '
                SELECT *
                FROM `'._DB_PREFIX_.'flash` f
                WHERE f.id_flash = '.$id.'
                AND f.id_shop = '.(int)$this->context->shop->id.'
                AND f.`date_end` > "'.pSQL($date).'"';
                //d($ifFinished);
            $testFinished = Db::getInstance()->ExecuteS($ifFinished);
            //d($testFinished);
            if (empty($testFinished))
            {
                $this->setTemplate('no_flash.tpl');
                return;
            }
            $this->setTemplate('flash.tpl');
    }
    
    private function getChronoWithId($id)
    {
    	$this->context->smarty->assign("message", true);
    	$date = date("Y-m-d H:i:s");
    	$select = '
    	SELECT *
    	FROM `'._DB_PREFIX_.'flash` f
    	INNER JOIN `'._DB_PREFIX_.'flash_assoc` fa ON (f.`id_flash` = fa.`id_flash`)
    	WHERE (f.`date_start`<= "'.pSQL($date).'"
    	AND f.`date_end` >= "'.pSQL($date).'")
    	AND f.`statut` = 1
        AND f.`id_shop` = '.(int)$this->context->shop->id.'
    	AND fa.`id_flash` = '.(int)$id.'
    	LIMIT 1';

    	if ($product = Db::getInstance()->ExecuteS($select))
    	{
	    	$year = substr($product[0]["date_end"], 0, 4);
	    	$month = substr($product[0]["date_end"], 5, 2);
	    	$day = substr($product[0]["date_end"], 8, 2);
	    	$hour = substr($product[0]["date_end"], 11, 2);
	    	$minute = substr($product[0]["date_end"], 14,2);
	    	$second = substr($product[0]["date_end"], 17, 2);
	    	$en_date = $month."/".$day."/".$year." ";
            $hour_format = $hour;
	    	if ($hour > 12)
	    	{
	    		$hour_format = $hour - 12;
	    		$hour_suff = "PM";
	    	}
	    	else
	    		$hour_suff = "AM";
	    	$en_date .= $hour_format.":".$minute.":".$second." ".$hour_suff;
	    	echo '
                <input id="show_year" type="hidden" value="'.$year.'" />
                <input id="show_month" type="hidden" value="'.$month.'" />
                <input id="show_day" type="hidden" value="'.$day.'" />
                <input id="show_hour" type="hidden" value="'.$hour.'" />
                <input id="show_minute" type="hidden" value="'.$minute.'" />
                <input id="show_second" type="hidden" value="'.$second.'" />
	    	';
    	}
    	else
    	{
    		$this->context->smarty->assign("message", false);
    		return '';
    	}
    }
}
?>