var tabFlash = new Array();
var req = false;

function getProduct()
{
	dir = $("input#dir").val();
	$("#tabProduct tr.line").remove();
	$("#tabProduct").append('<tr class="line"><td colspan=4><center><img src="'+ dir +'css/loader.gif"></center></td></tr>');
    var name = $("input#product_name").val();
    if (req)
    	req.abort();
    req = $.ajax({
        type: "POST",
        url: $("input#dir").val() + "ajax.php?tokenVF="+token+"&id_shop="+id_shop,
        data: "action=product&name=" + name,
        success: function(msg){
            $("#tabProduct tr.line").remove();
            $("#tabProduct").append(msg);
        }
    });
}

function addProduct(id)
{
    tabFlash.push(id);
}

function moveProduct(id, name)
{
    dir = $("input#dir").val();
    if (isFlash(id) == false) {
        tabFlash.push(id);
        tab = '<tr id="' + id + '">';
        tab += "<td>" + id + "</td>";
        tab += "<td>" + name + "</td>";
        tab += '<td align="center"><a onClick=\'javascript:dropProduct('+ id +');\'><img src="' + dir + '/css/del.png" title="Supprimer l\'article"></a><a onClick=\'javascript:delImg('+ id +');\'><img src="' + dir + '/css/imgSup.png" title="Supprimer l\'image de l\article"></a></td>';
        tab += '<td width="20" align="center"><span title="Uploader une nouvelle image pour ce produit"><input type="file" name="upload" id="upload' + id + '"/><span></td>';
        tab += '</tr><tr style="display:none;"  id="trFile' + id +'"><td colspan="3" id="file' + id +'"><b>Cliquez sur Ok à droite.</b></td>';
        tab += '<td align="center"><a href="javascript:$(\'#upload' + id + '\').uploadifyUpload();"><b>Ok</b></a></td></tr>';
         tab += '<tr id="trImg' + id + '" style="display:none;"><td colspan="5"><img class="display" id="img'+ id +'" src=""></a></td></tr>';
        $("#tabFlash").append(tab);
        $("#upload" + id).uploadify({
		'uploader'       : dir + '/js/uploadify.swf',
		'script'         : dir + '/js/uploadify.php?id=' + id,
		'cancelImg'      :  dir + '/css/cancel.png',
		'folder'         : dir + '/images',
		'multi'          : false,
		'buttonImg'     :  dir + '/css/arrow.png',
                'width' : 16,
                'height' : 16,
                'queueID' : 'file' + id,
                'fileExt'     : '*.jpg;*.gif;*.png',
                'fileDesc'    : 'Image Files',
                'onSelect' : function() {
                    $('#trFile' + id).attr("style", "");
                    $('#img' + id).attr("src", "");
                },
                'onComplete' : function(event, ID, fileObj, response, data) {
                    date = new Date();
                    $('#trFile' + id).attr("style", "display:none;");
                    $('#img' + id).attr("src","/" + response + "?time=" + date.getTime());
                    $('#trImg' + id).attr("style", "");
                },
                'onCancel' : function() {
                    $('#trFile' + id).attr("style", "display:none;");
                }

	});
    }
}


function affImg(id, chem)
{
    $('#img' + id).attr("src",chem );
    $('#trImg' + id).attr("style", "");
}



function dropProduct(id)
{
    $("#tabFlash tr#" + id).remove();
    $("#tabFlash tr#trFile" + id).remove();
    $("#tabFlash tr#trImg" + id).remove();
    tabFlash = jQuery.grep(tabFlash, function(value) {
        return value != id;
   });

}


function isFlash(id)
{
    for (i = 0; i < tabFlash.length; i++)
    {
        if (tabFlash[i] == id) 
        {
                $("#errorFlash").html('<img src="../img/admin/error2.png" >' + trad[16]);
                $("#errorFlash").fadeIn("slow");
                return true;
        } 
    }
    
    return false;
}


function delFlash(id)
{
    $.ajax({
    type: "POST",
    url: $("input#dir").val() + "ajax.php?tokenVF="+token+"&id_shop="+id_shop,
    data: "action=delFlash&id=" + id,
    success: function(msg)
    {
        tab = msg.split(',');
            if (tab[0] == "error") {
                $("#errorHistory").html('<img src="../img/admin/error2.png" >' + trad[tab[1]]);
                $("#errorHistory").fadeIn("slow");
            }
            else {
                 $("#validHistory").html('<img src="../img/admin/ok2.png" >' + trad[tab[1]]);
                 $("#validHistory").fadeIn("slow");
                 $('#statut' + id).html('<a href="javascript:actFlash(' + id +');"><img  src="'+ $("input#dir").val() +'css/del.png"></a>');
            }
    }
 });
}

function actFlash(id)
{
    $.ajax({
    type: "POST",
    url: $("input#dir").val() + "ajax.php?tokenVF="+token+"&id_shop="+id_shop,
    data: "action=actFlash&id=" + id,
    success: function(msg){
        tab = msg.split(',');
            if (tab[0] == "error") {
                $("#errorHistory").html('<img src="../img/admin/error2.png" >' + trad[tab[1]]);
                $("#errorHistory").fadeIn("slow");
            }
            else {
                $("#validHistory").html('<img src="../img/admin/ok2.png" >' + trad[tab[1]]);
                $("#validHistory").fadeIn("slow");
                $('#statut' + id).html('<a href="javascript:delFlash('+ id +');"><img  src="'+ $("input#dir").val() +'css/act.png"></a>');
            }
    }
 });
}


function deleteFlash()
{
    if(confirm(trad[15])) {
        var tab =  "";
        $("input:checked[type='checkbox'][name='id_flash']").each( function(index, value){tab += "&id[]=" + value.value;});
        $.ajax({
            type: "POST",
            url: $("input#dir").val() + "ajax.php?tokenVF="+token+"&id_shop="+id_shop,
            data: "action=deleteFlash" + tab,
            success: function(msg){
                document.location.href = $("input#indexPage").val() + "&conf=1";
            }
        });
    }
}


function delImg(id)
{
	var chem = $('#prevImg' + id).val();
	if(confirm(trad[15])) {
    dateN = new Date();
    if ((dateN.getMonth() +1) < 10 )
        dateM = "0" + (dateN.getMonth() +1);
    else
        dateM = (dateN.getMonth() + 1);

    codeImg = id;
    $.ajax({
            type: "POST",
            url: $("input#dir").val() + "ajax.php?tokenVF="+token+"&action=deleteImg&id_shop="+id_shop,
            data:  { img: codeImg },
            success: function(msg){
                $('#trImg' + id).attr("style", "");
                if ($('#prevImg' + id).val() != "")
                {
                	$('#img' + id).hide().fadeIn(2000).attr("src", chem).attr("style", "width:480px;");
                	$('#prevImg' + id).val("");
                }
                else
                	$('#img' + id).hide();
                	
                $('#warning_delete').show("slow");
            }
        });
	}
}

$(document).ready(function() {
    $("input#product_name").keyup(function(){
    	getProduct();
    });
    $("#submitForm").bind("submit", function(){
        $("#errorFlash").fadeOut("slow");
        $("#validFlash").fadeOut("slow");
		$('#addBtn').click(function(){
			
		    $(':submit', this).click(function() {
		    	
		        return false;
		    });
		});
        products = tabFlash.join(',');
        form = "";
        $('#submitForm input').each (function(nb, chp){form += chp.name + "=" + chp.value + "&";})
        if (confirm("La génération d'image prendra quelques instants. Voulez-vous continuer ?"))
        {
        	if ($('#addBtn').val('Patientez...'))
	        $.ajax({
	        type: "POST",
	        async:false,
	        url: $("input#dir").val() + "ajax.php?tokenVF="+token+"&id_shop="+id_shop,
	        data: "action="+ $("input#action").val() +"&products=" + products + "&" + form,
	        success: function(msg){
	                tab = msg.split(',');
	                if (tab[0] == "error") {
	                    $("#errorFlash").html('<img src="../img/admin/error2.png" >' + trad[tab[1]]);
	                    $("#errorFlash").fadeIn("slow");
	                }
	                else if (tab[0] == "edit")
	                {
	                	document.location.href = $("input#currentPage").val() + "&conf=3&modif="+tab[1];
	                }
	                else {
						console.log(msg);
	                    document.location.href = $("input#currentPage").val() + "&conf=3";
	                }
	            }
	        });
        }
        return false;
   });
   $("input#percent").change( function(){
       if ($("input#percent").val().trim() != "")
           $("input#euros").attr("disabled",true);
       else
          $("input#euros").removeAttr("disabled");
   });
   $("input#euros").change( function(){
       if ($("input#euros").val().trim() != "")
           $("input#percent").attr("disabled",true);
       else
           $("input#percent").removeAttr("disabled");
   });
   
   $("input#percent2").change( function(){
       if ($("input#percent2").val().trim() != "")
           $("input#euros2").attr("disabled",true);
       else
          $("input#euros2").removeAttr("disabled");
   });
   $("input#euros2").change( function(){
       if ($("input#euros2").val().trim() != "")
           $("input#percent2").attr("disabled",true);
       else
           $("input#percent2").removeAttr("disabled");
   });
});




