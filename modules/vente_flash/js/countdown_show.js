var show_year;
var show_month;
var show_day;
var show_hour;
var show_minute;
var show_second;
var show_timeLeft = 1;
//FO

$(document).ready(function()
{
      show_year = $("input#show_year").val();
      show_day = $("input#show_day").val();
      show_month = $("input#show_month").val();
      show_hour = $("input#show_hour").val();
      show_minute = $("input#show_minute").val();
      show_second = $("input#show_second").val();
});

function timerShowProduct()
 {


   var target = new Date(parseInt(show_year),(parseInt(show_month) - 1),parseInt(show_day),show_hour,show_minute,show_second);
   if (show_timeLeft > 0)
       {
           var mms_day = 24 * 60 * 60 * 1000;
           var mms_hour = 60 * 60 * 1000;
           var mms_minute = 60 * 1000;
           var mms_second = 1000;
           var today = new Date();

           var diff_mms = target.getTime() - today.getTime();
           timeLeft = diff_mms;

           var diff_day = Math.floor(diff_mms / mms_day);
           diff_mms -= diff_day * mms_day;

           var diff_hour = Math.floor(diff_mms / mms_hour);
           diff_mms -= diff_hour * mms_hour;

           var diff_minute = Math.floor(diff_mms / mms_minute);
           diff_mms -= diff_minute * mms_minute;

           var diff_second = Math.floor(diff_mms / mms_second);
           diff_mms -= diff_second * mms_second;

           //diff_hour += diff_day * 24;
           if (diff_hour < 10)
               diff_hour = "0" + diff_hour;
           if (diff_minute < 10)
               diff_minute = "0" + diff_minute;
           if (diff_second < 10)
               diff_second = "0" + diff_second;
           
           $("#timeProduct").html(diff_day + ":" + diff_hour + ":" + diff_minute + ":" + diff_second);

          setTimeout("timerShowProduct()",100);
       }
       else
         {
           window.location.reload();
         }
 }