var year;
var month;
var day;
var hour;
var minute;
var second;
var timeLeft = 1;
//FO

$(document).ready(function(){
      year = $("input#year").val();
      day = $("input#day").val();
      month = $("input#month").val();
      hour = $("input#hour").val();
      minute = $("input#minute").val();
      second = $("input#second").val();
     
     $("#slider").easySlider({
              prevId: 'prevBtn',
              prevText: '',
              nextId: 'nextBtn',
              nextText: '',
              auto: true,
              continuous: true
	});
      $("div.sliderBlock").easySlider({
              prevId: 'prevBtn',
              prevText: '',
              nextId: 'nextBtn',
              nextText: '',
              auto: true,
              continuous: true
	});

      }
);

function timer()
 {

   var target = new Date(parseInt(year),(parseInt(month) - 1),parseInt(day),hour,minute,second);
   if (timeLeft > 0)
       {
           var mms_day = 24 * 60 * 60 * 1000;
           var mms_hour = 60 * 60 * 1000;
           var mms_minute = 60 * 1000;
           var mms_second = 1000;
           var today = new Date();

           var diff_mms = target.getTime() - today.getTime();
           timeLeft = diff_mms;

           var diff_day = Math.floor(diff_mms / mms_day);
           diff_mms -= diff_day * mms_day;

           var diff_hour = Math.floor(diff_mms / mms_hour);
           diff_mms -= diff_hour * mms_hour;

           var diff_minute = Math.floor(diff_mms / mms_minute);
           diff_mms -= diff_minute * mms_minute;

           var diff_second = Math.floor(diff_mms / mms_second);
           diff_mms -= diff_second * mms_second;
           
           //diff_hour += diff_day * 24;
           if (diff_hour < 10)
               diff_hour = "0" + diff_hour;
           if (diff_minute < 10)
               diff_minute = "0" + diff_minute;
           if (diff_second < 10)
               diff_second = "0" + diff_second;
           $("#time").html(diff_day + ":" + diff_hour + ":" + diff_minute + ":" + diff_second);
           $("div.timeBlock").html(diff_day + ":" + diff_hour + ":" + diff_minute + ":" + diff_second);

          setTimeout("timer()",100);
       }
       else
         {
           window.location.reload();
         }
 }
