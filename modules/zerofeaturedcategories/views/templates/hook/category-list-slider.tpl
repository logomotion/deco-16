{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

{if !isset($categoriesPerRow)}
{assign var='categoriesPerRow' value=2}
{/if}

<div id="zFeaturedCategories" class="category-list-slider row per-row-{$categoriesPerRow|intval}">
	{foreach from=$featuredCategories item=featuredCategory name=featuredCategories}
	<div class="category-block col-xs-12">
		{include file="./category-container.tpl" category=$featuredCategory['category'] subCategories=$featuredCategory['subCategories']}
	</div>
	{/foreach}
</div>

<script type="text/javascript">
    $(document).ready(function(){
      $('#zFeaturedCategories').slick({
        slidesToShow: {$categoriesPerRow|intval},
		slidesToScroll: {$categoriesPerRow|intval},
		adaptiveHeight: true,
		infinite: true,
		speed: 1000,
		autoplay: false,
		dots: false,
		arrows: true,
		responsive: [
			{
			  breakpoint: 992,
			  settings: {
				slidesToShow: {math equation="min(3, $categoriesPerRow)"},
				slidesToScroll: {math equation="min(3, $categoriesPerRow)"},
			  }
			},
			{
			  breakpoint: 768,
			  settings: {
				slidesToShow: {math equation="min(2, $categoriesPerRow)"},
				slidesToScroll: {math equation="min(2, $categoriesPerRow)"},
			  }
			},
			{
			  breakpoint: 480,
			  settings: {
				slidesToShow: 1,
				slidesToScroll: 1
			  }
			}
		],
      });
    });
</script>