ZERO - FEATURED CATEGORIES module will display several Categories and Subcategories on Homepage.

Hook can be added:
+ displayHome (default)
+ displayTopcolumn
+ displayLeftColumn
+ displayRightColumn

- Version: 1.0
- Author: Mr.Zero
- Documentation: read file documentation.pdf to configure this module.