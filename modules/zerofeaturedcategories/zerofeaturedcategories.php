<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

class ZeroFeaturedCategories extends Module
{
	public function __construct()
	{
		$this->name = 'zerofeaturedcategories';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Mr.Zero';
		$this->need_instance = 0;
		$this->module_key = '38e00592c363d95939c14c0bb204245b';

		$this->bootstrap = true;

		parent::__construct();

		$this->displayName = $this->l('Zero - Featured Categories');
		$this->description = $this->l('Displays several Categories and Subcategories on Homepage.');

		$this->confirmUninstall = $this->l('Are you sure you want to uninstall this module?');

		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}

	public function install()
	{
		$settings = array(
			'enableSlider' => true,
			'categoriesPerRow' => 2,
			'imageName' => ImageType::getFormatedName('category'),
			'featuredCategories' => array(),
			'layoutStyle' => 'basic',
			'subCategoriesLimit' => 4,
		);

		Configuration::updateValue('ZEROFEATUREDCATEGORIES_SETTINGS', serialize($settings));

		return parent::install() &&
			$this->registerHook('header') &&
			$this->registerHook('backOfficeHeader') &&
			$this->registerHook('actionObjectCategoryUpdateAfter') &&
			$this->registerHook('displayHome');
	}

	public function uninstall()
	{
		Configuration::deleteByName('ZEROFEATUREDCATEGORIES_SETTINGS');

		$this->_clearCache('*');

		return parent::uninstall();
	}

	protected function about()
	{
		return '
		<div class="panel">
			<h3><i class="icon icon-circle"></i> '.$this->displayName.'</h3>
			<p><strong>'.$this->displayName.'</strong> module will display several Categories and Subcategories on Homepage.</p>
			<p>Easy to configure!</p>
			<p>	- Version: 1.0<br/>
				- Author: Mr.Zero<br/>
				- Documentation: read file <a href="'._MODULE_DIR_.$this->name.'/documentation.pdf" target="_blank">documentation.pdf</a> to configure this module.
			</p>
		</div>		
		';
	}

	public function getContent()
	{
		$about = $this->about();

		if (Tools::isSubmit('submitSettings'))
		{
			$this->processSaveSettings();

			return $this->displayConfirmation($this->l('Settings updated')).$this->renderSettingsForm().$about;
		}
		else
			return $this->renderSettingsForm().$about;
	}

	protected function processSaveSettings()
	{
		$settings = array(
			'enableSlider' => Tools::getValue('enableSlider'),
			'categoriesPerRow' => (int)Tools::getValue('categoriesPerRow'),
			'imageName' => Tools::getValue('imageName', ImageType::getFormatedName('category')),
			'featuredCategories' => Tools::getValue('featuredCategories', array()),
			'layoutStyle' => Tools::getValue('layoutStyle'),
			'subCategoriesLimit' => (int)Tools::getValue('subCategoriesLimit'),
		);

		Configuration::updateValue('ZEROFEATUREDCATEGORIES_SETTINGS', serialize($settings));

		$this->_clearCache('*');
	}

	protected function renderSettingsForm()
	{
		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->module = $this;
		$helper->default_form_language = $this->context->language->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitSettings';
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');

		$helper->tpl_vars = array(
			'fields_value' => $this->getSettingsFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id,
		);

		return $helper->generateForm(array($this->getSettingsForm()));
	}

	protected function getSettingsForm()
	{
		$categories_per_row_options = array(
			'query' => array(
				array('id' => 2, 'name' => 2),
				array('id' => 3, 'name' => 3),
				array('id' => 4, 'name' => 4),
				array('id' => 5, 'name' => 5),
				array('id' => 6, 'name' => 6),
			),
			'id' => 'id',
			'name' => 'name'
		);
		$layout_style_options = array(
			'query' => array(
				array('id' => 'small', 'name' => $this->l('Category and Thumbnail')),
				array('id' => 'medium', 'name' => $this->l('Category, Thumbnail and Subcategories')),
			),
			'id' => 'id',
			'name' => 'name'
		);

		$selected_categories = array();
		$settings = Tools::unSerialize(Configuration::get('ZEROFEATUREDCATEGORIES_SETTINGS'));
		if (isset($settings['featuredCategories']))
			$selected_categories = $settings['featuredCategories'];

		$cats_images_types = ImageType::getImagesTypes('categories');
		if ($cats_images_types)
			foreach ($cats_images_types as &$cit)
				$cit['label'] = $cit['name'].' ('.$cit['width'].'x'.$cit['height'].'px)';
		else
			$cats_images_types = array(array('name' => '', 'label' => $this->l('Not found!')));
		$image_name_options = array(
			'query' => $cats_images_types,
			'id' => 'name',
			'name' => 'label'
		);

		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs',
				),
				'input' => array(
					array(
						'type' => 'switch',
						'label' => $this->l('Enable Slider'),
						'name' => 'enableSlider',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'enableSlider_on',
								'value' => true,
								'label' => $this->l('Enabled')
							),
							array(
								'id' => 'enableSlider_off',
								'value' => false,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type' => 'select',
						'label' => $this->l('Categories per Row'),
						'name' => 'categoriesPerRow',
						'options' => $categories_per_row_options,
						'hint' => $this->l('The number of category block per row.'),
					),
					array(
						'type' => 'select',
						'label' => $this->l('Category Image Type'),
						'name' => 'imageName',
						'options' => $image_name_options,
					),
					array(
						'type' => 'html',
						'name' => 'html',
						'html_content' => '<a href="'.$this->context->link->getAdminLink('AdminImages').'" target="_blank">'.$this->l('Add new Image Type').'</a>',
					),
					array(
						'type' => 'categories',
						'label' => $this->l('Category Selection'),
						'name' => 'featuredCategories',
						'tree' => array(
							'use_search' => false,
							'id' => 'featuredCategories',
							'use_checkbox' => true,
							'selected_categories' => $selected_categories,
						),
						'col' => 6,
					),
					array(
						'type' => 'select',
						'label' => $this->l('Layout Style'),
						'name' => 'layoutStyle',
						'options' => $layout_style_options,
					),
					array(
						'type' => 'text',
						'label' => $this->l('Number of Subcategories'),
						'name' => 'subCategoriesLimit',
						'hint' => $this->l('Limit number of subcategories of a category. If set to zero, no limit is imposed.'),
						'col' => 1,
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		return $fields_form;
	}

	protected function getSettingsFieldsValues()
	{
		$settings = Tools::unSerialize(Configuration::get('ZEROFEATUREDCATEGORIES_SETTINGS'));

		$fields_value = array(
			'enableSlider' => Tools::getValue('enableSlider', $settings['enableSlider']),
			'categoriesPerRow' => Tools::getValue('categoriesPerRow', $settings['categoriesPerRow']),
			'imageName' => Tools::getValue('imageName', $settings['imageName']),
			'layoutStyle' => Tools::getValue('layoutStyle', $settings['layoutStyle']),
			'subCategoriesLimit' => Tools::getValue('subCategoriesLimit', $settings['subCategoriesLimit']),
		);

		return $fields_value;
	}

	public function hookBackOfficeHeader()
	{
		$this->context->controller->addJS($this->_path.'views/js/back.js');
		$this->context->controller->addCSS($this->_path.'views/css/back.css');
	}

	public function hookHeader()
	{
		$this->context->controller->addJS($this->_path.'views/js/slick.min.js');
		$this->context->controller->addJS($this->_path.'views/js/front.js');
		$this->context->controller->addCSS($this->_path.'views/css/slick.css');
		$this->context->controller->addCSS($this->_path.'views/css/slick-theme.css');
		$this->context->controller->addCSS($this->_path.'views/css/front.css');
	}

	public function hookActionObjectCategoryUpdateAfter()
	{
		$this->_clearCache('*');
	}

	protected function preProcess()
	{
		$id_lang = (int)$this->context->language->id;
		$settings = Tools::unSerialize(Configuration::get('ZEROFEATUREDCATEGORIES_SETTINGS'));
		$featured_categories = false;

		if (!empty($settings['featuredCategories']) && is_array($settings['featuredCategories']))
		{
			$featured_categories = array();

			foreach ($settings['featuredCategories'] as $id_category)
			{
				$category = new Category($id_category, $id_lang);
				if (!$category->id_image)
					$category->id_image = Language::getIsoById($id_lang).'-default';

				if (Validate::isLoadedObject($category))
				{
					$subcategories = $category->getSubCategories($id_lang);
					if ((int)$settings['subCategoriesLimit'])
						$subcategories = array_slice($subcategories, 0, (int)$settings['subCategoriesLimit']);

					$featured_categories[] = array(
						'category' => $category,
						'subCategories' => $subcategories,
					);
				}
			}
		}

		$this->smarty->assign(array(
			'enableSlider' => $settings['enableSlider'],
			'categoriesPerRow' => $settings['categoriesPerRow'],
			'layoutStyle' => $settings['layoutStyle'],
			'featuredCategories' => $featured_categories,
			'imageName' => $settings['imageName'],
			'imageSize' => Image::getSize($settings['imageName']),
		));
	}

	public function hookDisplayHome()
	{
		if (!$this->isCached('zerofeaturedcategories.tpl', $this->getCacheId()))
			$this->preProcess();

		return $this->display(__FILE__, 'zerofeaturedcategories.tpl', $this->getCacheId());
	}

	public function hookDisplayTopColumn()
	{
		return $this->hookDisplayHome();
	}

	public function hookDisplayLeftColumn()
	{
		if (!$this->isCached('zerofeaturedcategories_sidebar.tpl', $this->getCacheId()))
			$this->preProcess();

		return $this->display(__FILE__, 'zerofeaturedcategories_sidebar.tpl', $this->getCacheId());
	}

	public function hookDisplayRightColumn()
	{
		return $this->hookDisplayLeftColumn();
	}
}
