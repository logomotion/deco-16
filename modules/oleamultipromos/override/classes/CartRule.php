<?php

class CartRule extends CartRuleCore
{
	public $oleamultipromo_id_cart_generating;
	
	public function __construct($id = null, $id_lang = null, $id_shop = null)
	{
		self::$definition['fields']['oleamultipromo_id_cart_generating'] = array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId');
	
		return parent::__construct($id, $id_lang, $id_shop);
	}
	
	public static function autoAddToCart(Context $context = null)
	{
		if ($context === null)
			$context = Context::getContext();
		$oleamultipromos_module = Module::getInstanceByName('oleamultipromos');
		if (Validate::isLoadedObject($oleamultipromos_module) && $oleamultipromos_module->active)
			$oleamultipromos_module->oleaCartRefreshForMultiPromo15(array('cart'=>$context->cart));
		return parent::autoAddToCart($context);
	}

	public function checkValidity(Context $context, $alreadyInCart = false, $display_error = true)
	{
		if (!CartRule::isFeatureActive())
			return false;

		if ($this->oleamultipromo_id_cart_generating <> 0 AND $this->oleamultipromo_id_cart_generating <> $context->cart->id)
			return false;
			
		return parent::checkValidity($context, $alreadyInCart, $display_error);
	}
}
