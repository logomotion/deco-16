<?php

class OleaDiscountMulti14 extends Discount {
	
	const SENDING_METHOD_CURRENT_CART = 0;
	const SENDING_METHOD_MAIL = 1;
	
	const SEND_NONE = 0;
	const SEND_PENDING = 1;
	const SEND_DONE = 2;
	
	public $is_for_oleamultipromo;
	public $oleamultipromo_sending_method = self::SENDING_METHOD_CURRENT_CART;
	public $oleamultipromo_id_cart_generating = 0;
	public $oleamultipromo_id_order_generating = 0;
	public $oleamultipromo_is_sent_by_email = self::SEND_NONE;
	public $oleamultipromo_date_from_of_order = 1;
	public $oleamultipromo_validity_days = 365;
	public $oleamultipromo_mail_message;
	
/*	protected	$fieldsValidate = array('id_customer' => 'isUnsignedId', 'id_group' => 'isUnsignedId', 'id_discount_type' => 'isUnsignedId', 'id_currency' => 'isUnsignedId',
		'name' => 'isDiscountName', 'value' => 'isPrice', 'quantity' => 'isUnsignedInt', 'quantity_per_user' => 'isUnsignedInt',
		'cumulable' => 'isBool', 'cumulable_reduction' => 'isBool', 'date_from' => 'isDate',
		'date_to' => 'isDate', 'minimal' => 'isUnsignedFloat', 'active' => 'isBool', 'is_for_oleamultipromo' => 'isUnsignedId');
*/
	public function __construct($id = NULL, $id_lang = NULL) {
		$this->is_for_oleamultipromo = 1;
		parent::__construct($id, $id_lang);
	}
	
	public function getFields()
	{
		$fields = parent::getFields();
		$fields['is_for_oleamultipromo'] = (int)($this->is_for_oleamultipromo);
		$fields['oleamultipromo_sending_method'] = (int)($this->oleamultipromo_sending_method);
		$fields['oleamultipromo_id_order_generating'] = (int)($this->oleamultipromo_id_order_generating);
		$fields['oleamultipromo_id_cart_generating'] = (int)($this->oleamultipromo_id_cart_generating);
		$fields['oleamultipromo_is_sent_by_email'] = (int)($this->oleamultipromo_is_sent_by_email);
		$fields['oleamultipromo_date_from_of_order'] = (int)($this->oleamultipromo_date_from_of_order);
		$fields['oleamultipromo_validity_days'] = (int)($this->oleamultipromo_validity_days);
		$fields['oleamultipromo_mail_message'] = pSQL($this->oleamultipromo_mail_message);
		return $fields;
	}	
	
	static public function getAllDiscountsInCart($id_cart) {
		$sqlReq = 'SELECT DISTINCT cd.id_discount
				FROM `'._DB_PREFIX_.'cart_discount` cd
				LEFT JOIN `'._DB_PREFIX_.'discount` d on (d.id_discount = cd.id_discount)
				WHERE cd.id_cart = '.(int)$id_cart;
		$res = Db::getInstance()->ExecuteS($sqlReq);
		
		$retour = array();
		foreach ($res as $info)
			$retour[$info['id_discount']] = new self($info['id_discount']);
		return $retour;
	}
	
	static public function getMultiDiscountsOfCart($id_cart, $id_lang=null) {
		if ((int)$id_lang == 0)
			$id_lang = Configuration::get('PS_LANG_DEFAULT');
		$sqlReq = 'SELECT  d.*, dl.*, count(d.id_discount) as occurrence
				FROM `'._DB_PREFIX_.'discount` d
				LEFT JOIN `'._DB_PREFIX_.'discount_lang` dl ON (d.id_discount = dl.id_discount)
				LEFT JOIN `'._DB_PREFIX_.'cart_discount` cd ON (cd.id_discount = d.id_discount)
				WHERE d.oleamultipromo_id_cart_generating = '.(int)$id_cart.'
					AND dl.id_lang = '.(int)$id_lang.'
				GROUP BY d.id_discount';
		$res = Db::getInstance()->ExecuteS($sqlReq);
		return $res;
	}
	
	static public function getAllPendingOrSentMailDiscountOfOrder ($id_order, $id_lang=null) {
		if ((int)$id_lang == 0)
			$id_lang = Configuration::get('PS_LANG_DEFAULT');
		$sqlReq = 'SELECT d.*, dl.*
				FROM `'._DB_PREFIX_.'discount` d
				LEFT JOIN `'._DB_PREFIX_.'discount_lang` dl ON (dl.id_discount = d.id_discount)
				WHERE d.oleamultipromo_id_order_generating = '.(int)$id_order.'
					AND d.oleamultipromo_sending_method = '.(int)self::SENDING_METHOD_MAIL.'
					AND oleamultipromo_is_sent_by_email IN ( '.(int)self::SEND_PENDING.', '.(int)self::SEND_DONE.')
					AND dl.id_lang = '.(int)$id_lang;
		$res = Db::getInstance()->ExecuteS($sqlReq);
		
		return (array)$res;
	}
	
	static public function buildDiscountOfPromoinfo ($discount_name, $promoinfo, $oleadiscounts_in_cart, $cart, $oleadiscounts_associated_to_cart) {
		if (!Validate::isDiscountName($discount_name))
			return null;
			
		$returned_discount = null;
		foreach ($oleadiscounts_in_cart as $discount) {
			if ($discount->name == $discount_name)
				$returned_discount = $discount;
		}
		if ($returned_discount == null) {
			foreach ($oleadiscounts_associated_to_cart as $discount) {
				if ($discount['name'] == $discount_name) 
					$returned_discount = new self((int)$discount['id_discount']);
			}
		}
		
		if ($returned_discount == null)
			$returned_discount = new self((int)Discount::getIdByName($discount_name));
			
		$languages = Language::getLanguages();
			
		require_once dirname(__FILE__).'/Oleapromo.php';
		
		$promo = $promoinfo['promo'];
		if ($promo->sending_method == Oleapromo::SENDING_METHOD_MAIL) {
			if ($promo->mail_discount_type==Oleapromo::MAIL_DISCOUNT_TYPE_COMPUTED) {
				$returned_discount->id_discount_type = 2;  // by Amount
				$returned_discount->value = round($promoinfo['amount'], 2);
				$hashV = Tools::displayPrice($returned_discount->value);
			} elseif ($promo->mail_discount_type==Oleapromo::MAIL_DISCOUNT_TYPE_AMOUNT) {
				$returned_discount->id_discount_type = 2;  // by Amount
				$returned_discount->value = round(($promo->mail_discount_value<0) ?0 :$promo->mail_discount_value, 2);
				$hashV = Tools::displayPrice($returned_discount->value);
			} elseif ($promo->mail_discount_type==Oleapromo::MAIL_DISCOUNT_TYPE_PERCENT) {
				$returned_discount->id_discount_type = 1;  // by Percent
				$discount_value = round($promo->mail_discount_value, 2);
				$returned_discount->value = ($discount_value<0) ?0 :(($discount_value<100) ?$discount_value :100);
				$hashV = $returned_discount->value.'%';
			} elseif ($promo->mail_discount_type==Oleapromo::MAIL_DISCOUNT_TYPE_FDP) {
				$returned_discount->id_discount_type = 3;  // free shipping
				$returned_discount->value = 0;
				$hashV = '';
			} else {
				$returned_discount->id_discount_type = 2;  // by Amount
				$returned_discount->value = 0;
				$hashV = '--';
			}
		} else {
			if ($promoinfo['free_shipping']) {
				$returned_discount->id_discount_type = 3;  // free shipping
				$returned_discount->value = 0;
			} else {
				$returned_discount->id_discount_type = 2;  // By amount
				if (version_compare('1.3', _PS_VERSION_) <= 0)
					$returned_discount->value = round($promoinfo['amount'], 2);
				else {
					global $cookie;
					$currency = new Currency($cookie->id_currency);
					// Must be converted into default currency on 1.2	conversion_rate
					//$returned_discount->value = ($currency->conversion_rate) ?(round($promoinfo['amount'] / $currency->conversion_rate, 2)):0;
					$returned_discount->value = round($promoinfo['amount'], 2);
				}
			}			
		}
		
		$returned_discount->oleamultipromo_id_cart_generating = (int)$cart->id;
		
		$returned_discount->name = $discount_name;
		$returned_discount->id_customer = (int)($cart->id_customer);
		//$groups = Customer::getGroupsStatic((int)($cart->id_customer));
		$returned_discount->id_group = 0;/*((int)($cart->id_customer)) ?$groups[0] :0;*/
		$returned_discount->id_currency = (int)($cart->id_currency);
		$returned_discount->quantity = 1;
		$returned_discount->quantity_per_user = 1;
		$returned_discount->active = 1;
		$returned_discount->cart_display = 0;
		$returned_discount->behavior_not_exhausted = 1; // reduce voucher to order amount
		$now = time()-10;
		$returned_discount->date_from = date('Y-m-d H:i:s', $now);
		$mail_categories = false;
		if ($promo->sending_method == Oleapromo::SENDING_METHOD_MAIL) {
			$returned_discount->date_from = $promo->mail_date_from;//date('Y-m-d H:i:s', $promo->mail_date_from); 
			$returned_discount->date_to = $promo->mail_date_to;//date('Y-m-d H:i:s', $promo->mail_date_to); 
			$returned_discount->date_to = date('Y-m-d H:i:s', $now + (3600 * 24 * $promo->mail_validity_days)); 
			$returned_discount->oleamultipromo_sending_method = self::SENDING_METHOD_MAIL;
			$returned_discount->oleamultipromo_date_from_of_order = $promo->mail_date_from_of_order;
			$returned_discount->oleamultipromo_validity_days = $promo->mail_validity_days;
			foreach ($languages as $language)
				$returned_discount->description[$language['id_lang']] = (($promo->mail_description<>'') ?str_replace('#V#', $hashV, $promo->mail_description) :'Automatic'); /*(isset($promo->discountobj_description[$language['id_lang']])) ?$promo->discountobj_description[$language['id_lang']] :'--';*/
			
			$returned_discount->oleamultipromo_mail_message = str_replace('#V#', $hashV, $promo->mail_message);
			$returned_discount->cumulable = (int)$promo->mail_cumulable;
			$returned_discount->cumulable_reduction = (int)$promo->mail_cumulable_reduction;
			$returned_discount->minimal = (float)$promo->mail_minimal;
			$mail_categories = $promo->getMailCategories();
		} else {
			$returned_discount->date_to = date('Y-m-d H:i:s', $now + (3600 * 24 * 365)); 
			foreach ($languages as $language)
				$returned_discount->description[$language['id_lang']] = $promo->discountobj_description.(($promoinfo['nb_attributions']>1) ?' (x'.$promoinfo['nb_attributions'].')' :''); /*(isset($promo->discountobj_description[$language['id_lang']])) ?$promo->discountobj_description[$language['id_lang']] :'--';*/
			$returned_discount->cumulable = 1;
			$returned_discount->cumulable_reduction = 1;
			$returned_discount->minimal = 0;
			$returned_discount->oleamultipromo_date_from_of_order = 0;
			$returned_discount->oleamultipromo_validity_days = 0;
		}
		if ((int)($returned_discount->id) > 0 )
			$sav = $returned_discount->update(false, false, $mail_categories); 
		else 
			$sav = $returned_discount->add(true, false, (($mail_categories===false) ?null :$mail_categories));
		if (!$sav)
			error_log('error saving discount ('.(int)$returned_discount->id.')');
			
			//		if (!$returned_discount->save())
//			error_log('Error saving discount');
		return $returned_discount;
	}
	
	static public function buildDiscountName ($id_promo, $id_cart, $id_association) {
		list($id_product, $id_product_attribute) = explode('_', $id_association.'_0');
		return strtoupper('PR'.base_convert($id_promo, 10, 34).'CZ'.base_convert($id_cart, 10, 34).'Z'.base_convert((int)$id_product, 10, 34).'Z'.base_convert((int)$id_product_attribute, 10, 34));
		//return 'PR'.(int)$id_promo.'CA'.(int)$id_cart.'A'.$id_association;  // Should be less than 32 characters
	}
	
	static public function countOldDiscount ($nb_days) {
		$sql = 'SELECT count(*) as total
				FROM `'._DB_PREFIX_.'discount` d
				WHERE   (d.is_for_oleamultipromo = 1)
					AND (d.oleamultipromo_sending_method = '.self::SENDING_METHOD_CURRENT_CART.')
					AND (d.date_from <= SUBDATE(NOW(), '.(int)$nb_days.'))
					';

		$total = Db::getInstance()->getValue($sql);
		return $total;
	}

	static public function deleteOldDiscount ($nb_days) {
		
		$sql = 'SELECT d.id_discount
				FROM `'._DB_PREFIX_.'discount` d
				WHERE   (d.is_for_oleamultipromo = 1)
					AND (d.oleamultipromo_sending_method = '.self::SENDING_METHOD_CURRENT_CART.')
					AND (d.date_from <= SUBDATE(NOW(), '.(int)$nb_days.'))
					';
		$res = Db::getInstance()->executeS($sql);
		
		if ($res AND is_array($res)) {
			$ids = array();
			foreach ($res as $line)
				$ids[] = $line['id_discount'];
			$ids_str = implode(',', $ids);
			$sql1 = 'DELETE FROM `'._DB_PREFIX_.'cart_discount`
					WHERE id_discount IN ('.$ids_str.')';
			$res1 = Db::getInstance()->execute($sql1);
			
			$sql2 = 'DELETE FROM `'._DB_PREFIX_.'discount_category`
					WHERE id_discount IN ('.$ids_str.')';
			$res2 = Db::getInstance()->execute($sql2);
			
			$sql3 = 'DELETE FROM `'._DB_PREFIX_.'discount`
					WHERE id_discount IN ('.$ids_str.')';
			$res3 = Db::getInstance()->execute($sql3);
			
		}

		return true;
	}
}