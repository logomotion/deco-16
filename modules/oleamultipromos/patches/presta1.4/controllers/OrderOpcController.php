<?php
class OrderOpcController extends OrderOpcControllerCore {
	
	public function preProcess()
	{
		// Oleacorner
		$params = array();
		if (Tools::isSubmit('submitAddDiscount') OR Tools::isSubmit('deleteDiscount') )
			Module::hookExec('oleaCartRefreshForMultiPromo', array('removeOleapromo' => 1));
		// Fin Oleacorner	

		parent::preProcess ();

		// Oleacorner
		Module::hookExec('oleaCartRefreshForMultiPromo');
		// Fin Oleacorner	
	}
}