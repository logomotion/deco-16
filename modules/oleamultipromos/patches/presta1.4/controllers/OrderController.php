<?php
class OrderController extends OrderControllerCore {
	
	public function preProcess()
	{
		// Oleacorner
		$params = array();
		if (Tools::isSubmit('submitAddDiscount') OR Tools::isSubmit('deleteDiscount') )
			Module::hookExec('oleaCartRefreshForMultiPromo', array('removeOleapromo' => 1));
		// Fin Oleacorner	

		parent::preProcess ();

		// Oleacorner
		if ($this->step <= 2)  {
			Module::hookExec('oleaCartRefreshForMultiPromo');
		}
		// Fin Oleacorner	
	}
}