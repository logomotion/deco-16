<?php

class Product extends ProductCore {
	
	public static function getProductsProperties($id_lang, $query_result)
	{
		// OLEACORNER
		$retour_hook = ModuleCore::hookExec('oleaMultiPromoProductsProperties', array('id_lang'=>$id_lang, 'query_result'=>$query_result));
		if ($retour_hook <> '')
			$query_result = unserialize($retour_hook);
		// END OLEACORNER
			
		$retour = parent::getProductsProperties($id_lang, $query_result);
		return $retour;

	}
	
}