<?php

class Cart extends CartCore {
	
	private static $_carts_list = array();
	
	public function __construct($id = NULL, $id_lang = NULL)
	{
		parent::__construct($id, $id_lang);
		
		if (version_compare('1.4.9', _PS_VERSION_) <= 0 )
			return;
			
		/* Oleacorner : workaround for PSCFI-6187
		 * Optimization of producst cache management in case several cart objects with same IDs are created */
		if (isset(self::$_carts_list[$id.'_'.$id_lang])) {
			$this->_products = self::$_carts_list[$id.'_'.$id_lang]->_products;
		} else 	
			self::$_carts_list[$id.'_'.$id_lang] = $this;
		/* End Oleacorner */
	}
	
	public function getDiscounts($lite = false, $refresh = false)
	{
		if (!$this->id)
			return array();
			
		// OLEACORNER fix for PSCFI-3260, hopping to be fix in version>=1.4.5	
		if ($refresh AND version_compare(_PS_VERSION_, '1.4.5') < 0) {
			unset (self::$_discounts[$this->id]);
			unset (self::$_discountsLite[$this->id]);
		}
		// END OLEACORNER
		
		return parent::getDiscounts($lite, $refresh);
	}
}