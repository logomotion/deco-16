/**
 * Please do not edit or add any code in this file without the permission of PIBBLU
 *
 * @author    Pibblu
 * @copyright PIBBLU TECHNOLOGY SOLUTIONS
 * @license   http://www.pibblu.in
 * Prestashop version 1.6+
 * ZIPCHECK 6.0.0
 * July 2015
 */

function ProductTabsManager(){
	var self = this;
	this.product_tabs = [];
	this.current_request;
	this.stack_error = [];
	this.page_reloading = false;
	this.has_error_loading_tabs = false;

	/**
	* Show / Hide languages semaphore
	*/
	this.allow_hide_other_languages = true;

	this.setTabs = function(tabs){
		this.product_tabs = tabs;
	}

	/**
	 * Schedule execution of onReady() function for each tab and bind events
	 */
	this.init = function() {
		for (var tab_name in this.product_tabs) {
			if (this.product_tabs[tab_name].onReady !== undefined && this.product_tabs[tab_name] !== this.product_tabs['Pack'] )
				this.onLoad(tab_name, this.product_tabs[tab_name].onReady);
		}

		$('.shopList.chzn-done').on('change', function(){
			if (self.current_request)
			{
				self.page_reloading = true;
				self.current_request.abort();
			}
		});

		$(window).on('beforeunload', function() {
			self.page_reloading = true;
		});
	}

	/**
	 * Execute a callback function when a specific tab has finished loading or right now if the tab has already loaded
	 *
	 * @param tab_name name of the tab that is checked for loading
	 * @param callback_function function to call
	 */
	this.onLoad = function (tab_name, callback)
	{
		var container = $('#product-tab-content-' + tab_name);
		// Some containers are not loaded depending on the shop configuration
		if (container.length === 0)
			return;

		// onReady() is always called after the dom has been created for the tab (similar to $(document).ready())
		if (container.hasClass('not-loaded'))
			container.bind('loaded', callback);
		else
			callback();
	}

	/**
	 * Get a single tab or recursively get tabs in stack then display them
	 *
	 * @param string tab_name name of the tab
	 * @param boolean selected is the tab selected
	 */
	this.display = function (tab_name, selected)
	{
		/*In order to prevent mod_evasive DOSPageInterval (Default 1s)*/
		if (mod_evasive)
			sleep(1000);

		var tab_selector = $("#product-tab-content-" + tab_name);

		// Is the tab already being loaded?
		if (tab_selector.hasClass('not-loaded') && !tab_selector.hasClass('loading'))
		{
			// Mark the tab as being currently loading
			tab_selector.addClass('loading');

			if (selected)
				$('#product-tab-content-wait').show();

			// send $_POST array with the request to be able to retrieve posted data if there was an error while saving product
			var data;
			var send_type = 'GET';
			if (save_error)
			{
				send_type = 'POST';
				data = post_data;
				// set key_tab so that the ajax call returns the display for the current tab
				data.key_tab = tab_name;
			}

			return $.ajax({
				url : $('#link-'+tab_name).attr("href")+"&ajax=1" + '&rand=' + new Date().getTime(),
				async : true,
				cache: false, // cache needs to be set to false or IE will cache the page with outdated product values
				type: send_type,
				headers: { "cache-control": "no-cache" },
				data: data,
				timeout: 30000,
				success : function(data)
				{
					tab_selector.html(data).find('.dropdown-toggle').dropdown();
					tab_selector.removeClass('not-loaded');

					if (selected)
					{
						$("#link-"+tab_name).addClass('selected');
						tab_selector.show();
					}
					tab_selector.trigger('loaded');
				},
				complete : function(data)
				{
					tab_selector.removeClass('loading');
					if (selected)
					{
						$('#product-tab-content-wait').hide();
						tab_selector.trigger('displayed');
					}
				},
				beforeSend : function(data)
				{
					// don't display the loading notification bar
					if (typeof(ajax_running_timeout) !== 'undefined')
						clearTimeout(ajax_running_timeout);
				}
			});
		}
	}

	/**
	 * Send an ajax call for each tab in the stack, binding each call to the "complete" event of the previous call
	 *
	 * @param array stack contains tab names as strings
	 */
	this.displayBulk = function(stack){
		if (stack.length == 0)
		{
			$('[name="submitAddproductAndStay"]').each(function() {
				$(this).prop('disabled', false).find('i').removeClass('process-icon-loading').addClass('process-icon-save');
			});
			$('[name="submitAddproduct"]').each(function() {
				$(this).prop('disabled', false).find('i').removeClass('process-icon-loading').addClass('process-icon-save');
			});
			this.allow_hide_other_languages = true;

			return false;
		}

		this.current_request = 	this.display(stack[0], false);

		if (this.current_request !== undefined)
		{
			this.current_request.complete(function(request, status) {
				var wrong_status_code = new Array(400, 401, 403, 404, 405, 406, 408, 410, 413, 429, 499, 500, 502, 503, 504);

				if ((status === 'abort' || status === 'error' || request.responseText.length == 0 || in_array(request.status, wrong_status_code) || self.stack_error.length !== 0) && !self.page_reloading)
				{
					var current_tab = stack[0];
					self.stack_error.push(stack.shift());
					self.has_error_loading_tabs = true;

					jConfirm('Tab : ' + current_tab + ' (' + request.status + ')\n' + reload_tab_description, reload_tab_title, function(confirm) {
						if (confirm === true)
						{
							self.page_reloading = true;
							self.displayBulk(stack);
						}
						else
						{
							$('[name="submitAddproductAndStay"]').each(function() {
								$(this).prop('disabled', false).find('i').removeClass('process-icon-loading').addClass('process-icon-save');
							});
							$('[name="submitAddproduct"]').each(function() {
								$(this).prop('disabled', false).find('i').removeClass('process-icon-loading').addClass('process-icon-save');
							});
							self.allow_hide_other_languages = true;

							return false;
						}
					});
				}
				else if (stack.length !== 0 && status !== 'abort')
				{
					stack.shift();
					self.displayBulk(stack);
				}
			});
		}
		else
		{
			stack.shift();
			self.displayBulk(stack);
		}
	}
}

var product_tabs = [];

/**
 * hide save and save-and-stay buttons
 *
 * @access public
 * @return void
 */
function disableSave()
{
	//$('button[name="submitAddproduct"]').hide();
	//$('button[name="submitAddproductAndStay"]').hide();
}

/**
 * show save and save-and-stay buttons
 *
 * @access public
 * @return void
 */
function enableSave()
{
	$('button[name="submitAddproduct"]').show();
	$('button[name="submitAddproductAndStay"]').show();
}

function handleSaveButtons(e)
{
	msg = [];
	var i = 0;
	// relative to type of product
	if (product_type == product_type_pack)
		msg[i++] = handleSaveButtonsForPack();
	else if (product_type == product_type_pack)
		msg[i++] = handleSaveButtonsForVirtual();
	else
		msg[i++] = handleSaveButtonsForSimple();

	// common for all products
	$("#disableSaveMessage").remove();

	if ($("#name_" + id_lang_default).val() == "" && (!display_multishop_checkboxes || $('input[name=\'multishop_check[name][' + id_lang_default + ']\']').prop('checked')))
		msg[i++] = empty_name_msg;

	// check friendly_url_[defaultlangid] only if name is ok
	else if ($("#link_rewrite_" + id_lang_default).val() == "" && (!display_multishop_checkboxes || $('input[name=\'link_rewrite[name][' + id_lang_default + ']\']').prop('checked')))
		msg[i++] = empty_link_rewrite_msg;

	if (msg.length == 0)
	{
		$("#disableSaveMessage").remove();
		enableSave();
	}
	else
	{
		$("#disableSaveMessage").remove();
		do_not_save = false;
		for (var key in msg)
		{
			if (msg != "")
			{
				if (do_not_save == false)
				{
					$(".leadin").append('<div id="disableSaveMessage" class="alert alert-danger"></div>');
					warnDiv = $("#disableSaveMessage");
					do_not_save = true;
				}
				warnDiv.append('<p id="'+key+'">'+msg[key]+'</p>');
			}
		}
		if (do_not_save)
			disableSave();
		else
			enableSave();
	}
}

function handleSaveButtonsForSimple(){return '';}
function handleSaveButtonsForVirtual(){return '';}

function handleSaveButtonsForPack()
{
	// if no item left in the pack, disable save buttons
	if ($("#inputPackItems").val() == "")
		return empty_pack_msg;
	return '';
}

product_tabs['Pack'] = new function() {
	var self = this;

	this.bindPackEvents = function () {

		$('.delPackItem').on('click', function() {
			delPackItem($(this).data('delete'), $(this).data('delete-attr'));
		});

		function productFormatResult(item) {
			itemTemplate = "<div class='media'>";
			itemTemplate += "<div class='pull-left'>";
			itemTemplate += "<img class='media-object' width='40' src='" + item.image + "' alt='" + item.name + "'>";
			itemTemplate += "</div>";
			itemTemplate += "<div class='media-body'>";
			itemTemplate += "<h4 class='media-heading'>" + item.name + "</h4>";
			itemTemplate += "<span>REF: " + item.ref + "</span>";
			itemTemplate += "</div>";
			itemTemplate += "</div>";
			return itemTemplate;
		}

		function productFormatSelection(item) {
			return item.name;
		}

		var selectedProduct;
		$('#curPackItemName').select2({
			placeholder: search_product_msg,
			minimumInputLength: 2,
			width: '100%',
			dropdownCssClass: "bootstrap",
			ajax: {
				//url : "index.php?controller=AdminZipcheckProductZipcode" + "&token=" + token + "&id_product=" + id_product + "&action=Pack" + "&ajax_product_list=1",
				url :"index.php?controller=AdminModules&configure=layoutdesigner&" + "&token=" + token + "&id_product=" + id_product + "&action=Pack" + "&ajax_product_list=1",
				//url :"AdminModules&configure=layoutdesigner&addlayoutdesigner&id_layoutsectionlist=6&title=women&token=4bcbb5346c0cac815836135b6e335b7d",
				dataType: 'json',
				data: function (term) {
					return {
						q: term
					};
				},
				results: function (data) {
					var excludeIds = getSelectedIds();
					var returnIds = new Array();
					if (data) {
						for (var i = data.length - 1; i >= 0; i--) {
							var is_in = 0;
							for (var j = 0; j < excludeIds.length; j ++) {
								if (data[i].id == excludeIds[j][0] && (typeof data[i].id_product_attribute == 'undefined' || data[i].id_product_attribute == excludeIds[j][1]))
									is_in = 1;
							}
							if (!is_in)
								returnIds.push(data[i]);
						}
						return {
							results: returnIds
						}
					} else {
						return {
							results: []
						}
					}
				}
			},
			formatResult: productFormatResult,
			formatSelection: productFormatSelection,
		})
		.on("select2-selecting", function(e) {
			selectedProduct = e.object
		});

		$('#add_pack_item').on('click', addPackItem);

		function addPackItem() {

			if (selectedProduct) {
				selectedProduct.qty = $('#curPackItemQty').val();
				if (selectedProduct.id == '' || selectedProduct.name == '' && $('#curPackItemQty').valid()) {
					error_modal(error_heading_msg, msg_select_one);
					return false;
				} else if (selectedProduct.qty == '' || !$('#curPackItemQty').valid() || isNaN($('#curPackItemQty').val()) ) {
					error_modal(error_heading_msg, msg_set_quantity);
					return false;
				}

				if (typeof selectedProduct.id_product_attribute === 'undefined')
					selectedProduct.id_product_attribute = 0;
				//$('#divPackItems').empty();
				//var divContent = $('#divPackItems').empty();
				var divContent = $('#divPackItems').html();
				divContent += '<li class="product-pack-item media-product-pack" data-product-name="' + selectedProduct.name + '" data-product-qty="' + selectedProduct.qty + '" data-product-id="' + selectedProduct.id + '" data-product-id-attribute="' + selectedProduct.id_product_attribute + '">';
				divContent += '<img class="media-product-pack-img" src="' + selectedProduct.image +'"/>';
				divContent += '<span class="media-product-pack-title">' + selectedProduct.name + '</span>';
				divContent += '<span class="media-product-pack-ref">REF: ' + selectedProduct.ref + '</span>';
				divContent += '<span class="media-product-pack-quantity"><span class="text-muted">x</span> ' + selectedProduct.qty + '</span>';
				divContent += '<button type="button" class="btn btn-default delPackItem media-product-pack-action" data-delete="' + selectedProduct.id + '" data-delete-attr="' + selectedProduct.id_product_attribute + '"><i class="icon-trash"></i></button>';
				divContent += '</li>';

				// QTYxID-QTYxID
				// @todo : it should be better to create input for each items and each qty
				// instead of only one separated by x, - and ¤
				var line = selectedProduct.qty + 'x' + selectedProduct.id + 'x' + selectedProduct.id_product_attribute;
				var lineDisplay = selectedProduct.qty + 'x ' + selectedProduct.name;

				$('#id_product').val(selectedProduct.id);
				$('#product_name').val(selectedProduct.name);
				$('#divPackItems').html(divContent);
				$('#inputPackItems').val($('#inputPackItems').val() + line  + '-');
				$('#namePackItems').val($('#namePackItems').val() + lineDisplay + '¤');

				$('.delPackItem').on('click', function(e){
					e.preventDefault();
					e.stopPropagation();
					delPackItem($(this).data('delete'), $(this).data('delete-attr'));
				})
				selectedProduct = null;
				$('#curPackItemName').select2("val", "");
				$('.pack-empty-warning').hide();
			} else {
				error_modal(error_heading_msg, msg_select_one);
				return false;
			}
		}

		function delPackItem(id, id_attribute) {

			var reg = new RegExp('-', 'g');
			var regx = new RegExp('x', 'g');

			var input = $('#inputPackItems');
			var name = $('#namePackItems');

			var inputCut = input.val().split(reg);
			var nameCut = name.val().split(new RegExp('¤', 'g'));

			input.val(null);
			name.val(null);
			for (var i = 0; i < inputCut.length; ++i)
				if (inputCut[i]) {
					var inputQty = inputCut[i].split(regx);
					if (inputQty[1] != id || inputQty[2] != id_attribute) {
						input.val( input.val() + inputCut[i] + '-' );
						name.val( name.val() + nameCut[i] + '¤');
					}
				}

			var elem = $('.product-pack-item[data-product-id="' + id + '"][data-product-id-attribute="' + id_attribute + '"]');
			elem.remove();

			if ($('.product-pack-item').length === 0){
				$('.pack-empty-warning').show();
			}
		}

		function getSelectedIds()
		{
			var reg = new RegExp('-', 'g');
			var regx = new RegExp('x', 'g');

			var input = $('#inputPackItems');

			if (input.val() === undefined)
				return '';

			var inputCut = input.val().split(reg);

			var ints = new Array();

			for (var i = 0; i < inputCut.length; ++i)
			{
				var in_ints = new Array();
				if (inputCut[i]) {
					var inputQty = inputCut[i].split(regx);
					in_ints[0] = inputQty[1];
					in_ints[1] = inputQty[2];
				}
				ints[i] = in_ints;
			}

			return ints;
		}
	};

	this.onReady = function(){
		self.bindPackEvents();
	}
}

product_tabs['Images'] = new function(){
	this.onReady = function(){
		displayFlags(languages, id_language, allowEmployeeFormLang);
	}
}

/**
 * Update the product image list position buttons
 *
 * @param DOM table imageTable
 */
function refreshImagePositions(imageTable)
{
	var reg = /_[0-9]$/g;
	var up_reg  = new RegExp("imgPosition=[0-9]+&");

	imageTable.find("tbody tr").each(function(i,el) {
		$(el).find("td.positionImage").html(i + 1);
	});
	imageTable.find("tr td.dragHandle a:hidden").show();
	imageTable.find("tr td.dragHandle:first a:first").hide();
	imageTable.find("tr td.dragHandle:last a:last").hide();
}

/**
 * Generic ajax call for actions expecting a json return
 *
 * @param url
 * @param action
 * @param success_callback called if the return status is 'ok' (optional)
 * @param failure_callback called if the return status is not 'ok' (optional)
 */
function ajaxAction (url, action, success_callback, failure_callback){
	$.ajax({
		url: url,
		data: {
			id_product: id_product,
			action: action,
			ajax: true
		},
		dataType: 'json',
		context: this,
		async: false,
		success: function(data) {
			if (data.status == 'ok')
			{
				showSuccessMessage(data.confirmations);
				if (typeof success_callback == 'function')
					success_callback();
			}
			else
			{
				showErrorMessage(data.error);
				if (typeof failure_callback == 'function')
					failure_callback();
			}
		},
		error : function(data){
			showErrorMessage(("[TECHNICAL ERROR]"));
		}
	});
};

var tabs_manager = new ProductTabsManager();
tabs_manager.setTabs(product_tabs);

$(document).ready(function() {
	// The manager schedules the onReady() methods of each tab to be called when the tab is loaded
	tabs_manager.init();

	$('#product_form').delegate('input', 'keypress', function(e) {
			var code = null;
		code = (e.keyCode ? e.keyCode : e.which);
		return (code == 13) ? false : true;
	});

	loadPack();
});

function loadPack() {
	var container = $('#product-pack-container');
	var id_product = 1;
	var data;
	$.ajax({
		//url : "index.php?controller=AdminZipcheckProductZipcode" + "&token=" + token + "&id_product=" + id_product + "&action=Pack" + "&updateproduct" + "&ajax=1" + '&rand=' + new Date().getTime(),
		url :"index.php?controller=AdminModules&configure=layoutdesigner&" + "&token=" + token + "&id_product=" + id_product + "&action=Pack" + "&updateproduct" + "&ajax=1" + '&rand=' + new Date().getTime(),
		async : true,
		cache: false, // cache needs to be set to false or IE will cache the page with outdated product values
		type: 'GET',
		headers: { "cache-control": "no-cache" },
		data: data,
		success : function(data){
			$('#product-pack-container').html(data);
			product_tabs['Pack'].onReady();
		}
	});
}