/**
 * Please do not edit or add any code in this file without the permission of PIBBLU
 *
 * @author    Pibblu
 * @copyright PIBBLU TECHNOLOGY SOLUTIONS
 * @license   http://www.pibblu.in/terms-and-conditions
 * Prestashop version 1.6.0.8
 * jungleedata 5.0.0
 * March 2015
 */

    $(document).ready(function(){

     
      $(".image_form").css( "display", "none");
      $(".textarea_form").css( "display", "none");   
      $(".Products_carousel").css( "display", "none");
      $(".Specialcat_carousel").css( "display", "none");  
    $('.items_set option').trigger('onselect');


    $('#items_set').on('change', function() {

  //alert( this.value ); // or $(this).val()
  var formclass = this.value;

  if(formclass =='carousel_form'){
        $( ".carousel_form" ).css( "display", "block" );
        $(".image_form").css( "display", "none");
        $(".textarea_form").css( "display", "none"); 
  }
  else if(formclass =='image_form'){
        $( ".image_form" ).css( "display", "block" );
        $(".carousel_form").css( "display", "none");
        $(".textarea_form").css( "display", "none"); 
  }
  else if(formclass =='textarea_form'){
        $( ".textarea_form" ).css( "display", "block" );
        $(".carousel_form").css( "display", "none");
        $(".image_form").css( "display", "none");  
  }
  //$( '.'+formclass+'').css( "display", "block" );

});

    $('#carousel_type').on('change', function() {

  //alert( this.value ); // or $(this).val()
  var formclass = this.value;

  if(formclass =='Category_carousel'){
        $( ".Category_carousel" ).css( "display", "block" );
        $(".Products_carousel").css( "display", "none");
        $(".Specialcat_carousel").css( "display", "none"); 
  }
  else if(formclass =='Specialcat_carousel'){
        $( ".Specialcat_carousel" ).css( "display", "block" );
        $(".Category_carousel").css( "display", "none");
        $(".Products_carousel").css( "display", "none"); 
  }
  else if(formclass =='Products_carousel'){
        $( ".Products_carousel" ).css( "display", "block" );
        $(".Category_carousel").css( "display", "none");
        $(".Specialcat_carousel").css( "display", "none");  
  }
  //$( '.'+formclass+'').css( "display", "block" );

});
      $('#products_carousel_on').click(function(){
        
        $( ".carousel_form" ).css( "display", "block" );
        $(".image_form").css( "display", "none");
        $(".textarea_form").css( "display", "none");        
      });

      $('#products_carousel_off').click(function(){
        
        $( ".carousel_form" ).css( "display", "none" );;
      });

      $('#image_banner_on').click(function(){
        
        $( ".image_form" ).css( "display", "block" );
        $(".carousel_form").css( "display", "none");
        $(".textarea_form").css( "display", "none");        
      });

      $('#image_banner_off').click(function(){
        
        $( ".image_form" ).css( "display", "none" );;
      });      

      $('#text_banner_on').click(function(){
        
        $( ".textarea_form" ).css( "display", "block" );
        $(".carousel_form").css( "display", "none");
        $(".image_form").css( "display", "none");        
      });
      
      $('#text_banner_off').click(function(){
        
        $( ".textarea_form" ).css( "display", "none" );;
      }); 

         
    });