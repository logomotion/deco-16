{*
* Please do not edit or add any code in this file without the permission of PIBBLU
* @copyright PIBBLU TECHNOLOGY SOLUTIONS
* Prestashop version 1.6+
* ZIPCHECK 6.0.0
* July 2015
*}

		<script type="text/javascript">
			var id_product = {if isset($product->id)}{$product->id|escape:'htmlall':'UTF-8'}{else}0{/if};
			var id_lang_default = {$id_lang_default|escape:'htmlall':'UTF-8'};
			var product_type_pack = {Product::PTYPE_PACK|escape:'htmlall':'UTF-8'};
			var toload = new Array();
			var empty_pack_msg = '{l s='This pack is empty. You will need to add one product to the pack before you can save.'  mod='layoutdesigner' js=1}';
			var empty_name_msg = '{l s='The product name is empty. You will at least need to enter a name for the default language before you can save the product.'  mod='layoutdesigner' js=1}';
			$('#product-tab-content-wait').show();
			var post_data = '';
			var save_error = false;
			var error_heading_msg = '{l s='Error'  mod='layoutdesigner' js=1}';
			var error_continue_msg = '{l s='Continue'  mod='layoutdesigner' js=1}';
			var product_type = {$product_type|escape:'htmlall':'UTF-8'};
			var tabs_preloaded = new Array();
			var tabs_to_preload = new Array();
			var mod_evasive = {if isset($mod_evasive) && $mod_evasive}true{else}false{/if};
			var mod_security = {if isset($mod_security) && $mod_security}true{else}false{/if};

			// Listen to the load event that is fired each time an ajax call to load a tab has completed
			$(window).bind("load", function() {
				{* Fill an array with tabs that need to be preloaded *}
				var tabs_to_preload = new Array();
				{foreach $tabs_preloaded as $tab_name => $value}
					{* If the tab was not given a loading priority number it will not be preloaded *}
					{if (is_numeric($value))}
						if ($("#product-tab-content-"+'{$tab_name|escape:'htmlall':'UTF-8'}').hasClass('not-loaded'))
							tabs_to_preload.push('{$tab_name|escape:'htmlall':'UTF-8'}');
					{/if}
				{/foreach}

				// Recursively load tabs starting with the first element of stack
				tabs_manager.displayBulk(tabs_to_preload);
				$('.productTabs').show();
				$('#product-tab-content-wait').hide();

				function checkIfProductTypeIsPack() {
					var typeIsPack = $('#pack_product').is(':checked');
					if (typeIsPack && $('#inputPackItems').val()=='' ) {
						$('.pack-empty-warning').removeClass('alert-warning').addClass('alert-danger');
						$('#curPackItemName').select2('open');
					}
					return typeIsPack;
				}
				$("#product_form").validate({
					ignore: '.updateCurrentText',
					rules: {
						inputPackItems: {
							required: {
								depends: checkIfProductTypeIsPack
							},
						}
					},
					messages: {
						inputPackItems: {
							required: ""
						}
					},
					submitHandler: function(form) {
						form.submit();
					},
					// override jquery validate plugin defaults for bootstrap 3
					highlight: function(element) {
						$(element).closest('.form-group').addClass('has-error');
					},
					unhighlight: function(element) {
						$(element).closest('.form-group').removeClass('has-error');
					},
					errorElement: 'span',
					errorClass: 'help-block',
					errorPlacement: function(error, element) {
						if(element.parent('.input-group').length) {
							error.insertAfter(element.parent());
						} else {
							error.insertAfter(element);
						}
					}
				});
			});

			tb_pathToImage = "../img/loadingAnimation.gif";
		</script>
<form id="product_form" class="form-horizontal" action="{$form_action|escape:'html':'UTF-8'}" method="post" enctype="multipart/form-data" name="product" novalidate>
<div class="panel"><div class="panel-heading"><i class="icon-cogs"></i> Add Product Zipcode Combinations: Step 1: Select Product</div>
<div class="form-group">
<label class="control-label col-lg-3 required">Carousel Tab Title</label>
<div class="col-lg-9 ">
<input type="text" name="hometab_title" id="hometab_title" value="{$hometab_title|escape:'htmlall':'UTF-8'}" class="" required="required">
</div>
</div>
<div class="form-group listOfPack">
	<label class="control-label col-lg-3 product_description">
		{l s='Selected product' mod='layoutdesigner'}
	</label>
	<div class="col-lg-9">

		<ul id="divPackItems" class="list-unstyled">
			{foreach $pack_items as $pack_item}
				<li class="product-pack-item media-product-pack" data-product-name="{$pack_item.name|escape:'htmlall':'UTF-8'}" data-product-qty="{$pack_item.pack_quantity|escape:'htmlall':'UTF-8'}" data-product-id="{$pack_item.id|escape:'htmlall':'UTF-8'}" data-product-id-attribute="{$pack_item.id_product_attribute|escape:'htmlall':'UTF-8'}">
					<img class="media-product-pack-img" src="{$pack_item.image|escape:'htmlall':'UTF-8'}"/>
					<span class="media-product-pack-title">{$pack_item.name|escape:'htmlall':'UTF-8'}</span>
					<span class="media-product-pack-ref">REF: {$pack_item.reference|escape:'htmlall':'UTF-8'}</span>
					<span class="media-product-pack-quantity" style="display:none;"><span class="text-muted">x</span>{$pack_item.pack_quantity|escape:'htmlall':'UTF-8'}</span>
					<button type="button" class="btn btn-default delPackItem media-product-pack-action" data-delete="{$pack_item.id|escape:'htmlall':'UTF-8'}" data-delete-attr="{$pack_item.id_product_attribute|escape:'htmlall':'UTF-8'}"><i class="icon-trash"></i></button>
				</li>
			{/foreach}
		</ul>
	</div>
</div>
<div class="form-group addProductToPack">
	<label class="control-label col-lg-3" for="curPackItemName">
		<span class="label-tooltip" data-toggle="tooltip" title="{l s='Start by typing the first letters of the product name, then select the product from the drop-down list.' mod='layoutdesigner'}">
			{l s='Add a product list of Products Carousel'  mod='layoutdesigner'}
		</span>
	</label>
	<div class="col-lg-9">
		<div class="row">
			<div class="col-lg-6">
				<input type="text" id="curPackItemName" name="curPackItemName" class="form-control" />
			</div>
			<div class="col-lg-2" style="display:none;">
				<div class="input-group">
					<span class="input-group-addon">&times;</span>
					<input type="number" name="curPackItemQty" id="curPackItemQty" class="form-control" min="1" value="1"/>
				</div>
			</div>
			<div class="col-lg-2">
				<button type="button" id="add_pack_item" class="btn btn-default">
					<i class="icon-plus-sign-alt"></i> {l s='Add this product'  mod='layoutdesigner'}
				</button>
			</div>
		</div>
	</div>
</div>
<div class="panel-footer">
	<button type="submit" value="1" id="module_form_submit_btn_2" name="addcarouselitem" class="button pull-right"><i class="process-icon-save"></i> Save</button>
</div>
<input type="hidden" name="inputPackItems" id="inputPackItems" value="{$input_pack_items|escape:'htmlall':'UTF-8'}" placeholder="inputs"/>
<input type="hidden" name="namePackItems" id="namePackItems" value="{$input_namepack_items|escape:'htmlall':'UTF-8'}" placeholder="name"/>

<div id="product-pack-container"></div>
</div>
</form>