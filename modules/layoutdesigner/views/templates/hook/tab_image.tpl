{*
* Please do not edit or add any code in this file without the permission of PIBBLU
*
* @author    Pibblu 
* @copyright PIBBLU
* @license   http://www.pibblu.com  
* Prestashop version 1.6.0.6
* layoutdesigner 3.1.0
* OCT 2015
*}

<div class="image_banner col-xs-12 col-sm-6 {$tab_image_detail['set_col_md']|escape:'html':'UTF-8'}">				
	<div class="image_banner1">
		{foreach from=$image_tab[$home_tab['id_layoutsectionlist']] key="image_tab_key" item="image_tab_list"} 

			<div class=" col-xs-12 col-sm-6 {$image_tab_list['imgwidth']|escape:'html':'UTF-8'} {$tab_image_detail['image_position']|escape:'html':'UTF-8'}">
				<a href="{$image_tab_list['url']|escape:'html':'UTF-8'}" class="item-link" title="">
					<img class="replace-2x img-responsive lazy" src="{$link->getMediaLink("`$module_dir`views/img/`$image_tab_list['image']|escape:'html':'UTF-8'`")}" class="item-img " title="">	
				</a>
				<a>
					<p> {$image_tab_list['description']|escape:'html':'UTF-8'} </p>
				</a>
			</div>
		{/foreach}									
	</div>

	<script type="text/javascript">
		$(".image_banner1 > :first-child").addClass("image_first");
		$(".image_banner1 > :last-child").addClass("image_last");
	</script>

</div>