{*
* Please do not edit or add any code in this file without the permission of PIBBLU
*
* @author    Pibblu 
* @copyright PIBBLU
* @license   http://www.pibblu.com  
* Prestashop version 1.6.0.6
* layoutdesigner 3.1.0
* OCT 2015
*}
<div class="container">

	<div class="row">
		{if isset($hometablists)}	
			{foreach from=$hometablists key="home_tab_key1" item="home_tab"}

				{assign var='hometab_type' value=$home_tab['hometab_type']}

				{if isset($hometab_type) && $hometab_type == carousel}			
					{assign var='tab_carousel_detail' value=$tab_carousel_details[$home_tab['id_layoutsectionlist']]}

					{if $tab_carousel_detail['carousel_enabled'] == 'true'}

						{include file="./tab_carousel.tpl"}

					{/if}

				{/if}

				{if isset($hometab_type) && $hometab_type == image}			
					{assign var='tab_image_detail' value=$tab_image_details[$home_tab['id_layoutsectionlist']]}

					{if $tab_image_detail['image_enabled'] == 'true'}

						{include file="./tab_image.tpl"}

					{/if}

				{/if}

				{if isset($hometab_type) && $hometab_type == text}			
					{assign var='tab_text_detail' value=$tab_text_details[$home_tab['id_layoutsectionlist']]}

					{if $tab_text_detail['auto_play'] == 'true'}

						{include file="./tab_textarea.tpl"}

					{/if}

				{/if}					

			{/foreach}
		{/if}
	</div>
</div>

<style type="text/css">
.custom_blocks .tt-box, .custom_blocks .tt-box a{ background-color: {$layoutdesigner_css_values['text_background_color']|escape:'html':'UTF-8'};}
.custom_blocks .tt-box .icon_circle.dark {
	background-color: {$layoutdesigner_css_values['icon_backgroud_color']|escape:'html':'UTF-8'};
}
.animate-delay{
	color: {$layoutdesigner_css_values['text_color']|escape:'html':'UTF-8'};
}
.yt-tab-listing{
	background-color: {$layoutdesigner_css_values['carsoul_tab_color']|escape:'html':'UTF-8'};
}
.yt-tab-listing .respl-tabs-wrap ul li.active a{
	color: {$layoutdesigner_css_values['carsoul_tabactive_color']|escape:'html':'UTF-8'};
}
.yt-tab-listing .respl-items-container .respl-items-inner{
	box-shadow: 0px 0px 5px {$layoutdesigner_css_values['layout_border_color']|escape:'html':'UTF-8'};
}
.custom_blocks .tt-box, .custom_blocks .tt-box a , .custom_blocks .tt-box .inside{
	    border-color: {$layoutdesigner_css_values['layout_border_color']|escape:'html':'UTF-8'};
}

</style>

<!--</div>
</div>-->