{*
* Please do not edit or add any code in this file without the permission of PIBBLU
*
* @author    Pibblu 
* @copyright PIBBLU
* @license   http://www.pibblu.com  
* Prestashop version 1.6.0.6
* layoutdesigner 3.1.0
* OCT 2015
*}
<div class="tab-listing col-xs-12 {$tab_carousel_detail['set_col_md']|escape:'html':'UTF-8'}">


	<script type="text/javascript">
		jQuery(function() {

			var selected = $("#container_{$home_tab['id_layoutsectionlist']|escape:'html':'UTF-8'}").children().first().attr("id");
			var active_selected =('.'+selected);
			$(active_selected).addClass('active');

			$('#' + selected).addClass('active');
		});
	</script>


	<div id="" class="yt-tab-listing first-load">



		<!-- Start Tabs-->
		<div class="respl-tabs-wrap">

			<ul class="respl-tabs cf">
				{assign var='home_tab_key' value=$home_tab['id_layoutsectionlist']}
				{foreach from=$home_category_name[$home_tab_key] key="tab_title_key" item="category_nameid"}
					{assign var='tab_title' value=$home_tab_titles[$home_tab_key]}

					<li class="respl-tab  tab-loaded {$category_nameid|escape:'html':'UTF-8'}">
						<a  data-toggle="tab"  href="#{$category_nameid|escape:'html':'UTF-8'}" >
							<span class="respl-tab-label">{$tab_title[$tab_title_key]|escape:'html':'UTF-8'}</span>
						</a>
					</li>							
				{/foreach}					
			</ul>
		</div>
		<!-- End Tabs-->




		<div class="respl-items-container" id="container_{$home_tab['id_layoutsectionlist']|escape:'html':'UTF-8'}">
			{foreach from=$home_product_lists[$home_tab_key] key="product_list_key" item="product_list"} 

				{assign var='category_nameid' value=$home_category_name[$home_tab_key]}

				


				<div class="respl-items items-category-created_at " id="{$category_nameid[$product_list_key]|escape:'html':'UTF-8'}">
					<div class="respl-items-inner">	
							
						{if !empty($product_list)}

						<section class="featured-widget">

							{assign var='tab_id' value=$home_tab_ids[$home_tab_key]}
							<div id="producted_carousel">
								<a id="productscategory_scroll_left" title="{l s='Previous' mod='layoutdesigner'}" href="javascript:{ldelim}{rdelim}">{l s='Previous' mod='layoutdesigner'}</a>

								<div id="producted_carousel_list" class="list-featured responsive">
									{assign var='products' value=$product_list}	
										{include file="$tpl_dir./product-list.tpl" class='featured-news clearfix' id="bxslider-{$tab_id[$product_list_key]|escape:'html':'UTF-8'}"}
								</div>


								
								<a id="productscategory_scroll_right" title="{l s='Next' mod='layoutdesigner'}" href="javascript:{ldelim}{rdelim}">{l s='Next' mod='layoutdesigner'}</a>
							</div>														
						</section>



						{assign var='category_links' value=$home_category_link[$home_tab_key]}
						<div class="view_all">
						{if isset($category_links[$product_list_key]) && $category_links[$product_list_key] == new_products}
							<a href="{$link->getPageLink('new-products')|escape:'html':'UTF-8'}"> View All </a>	
						{elseif isset($category_links[$product_list_key]) && $category_links[$product_list_key] == featured_products}
							<a href=""> </a>	
						{elseif isset($category_links[$product_list_key]) && $category_links[$product_list_key] == best_sellers}
							<a href="{$link->getPageLink('best-sales')|escape:'html':'UTF-8'}"> View All </a>
						{elseif isset($category_links[$product_list_key]) && $category_links[$product_list_key] == Products_carousel}
																						
							{else}
							<a href="{$category_links[$product_list_key]|escape:'html':'UTF-8'}" > View All  </a>							
							{/if}
						
						</div>
						{else}
						<p class="alert alert-warning">NO PRODUCTS</p>
						{/if}


						<script type="text/javascript">
							$('#producted_carousel').trigger('goto', [8 - 3]);
						</script>
        <script type="text/javascript">
                $('#bxslider-{$tab_id[$product_list_key]|escape:'html':'UTF-8'}').bxSlider({
            minSlides: 1,
            maxSlides: {$tab_carousel_detail['limit_cols']|escape:'html':'UTF-8'},
            slideWidth: {$tab_carousel_detail['slide_width']|escape:'html':'UTF-8'},
            pager: {$tab_carousel_detail['slide_pager']|escape:'html':'UTF-8'},
            nextText: '#loffnext-{$tab_id[$product_list_key]|escape:'html':'UTF-8'}',
            prevText: '',
            moveSlides:1,
            infiniteLoop:false,
            responsive: true,
            auto: {$tab_carousel_detail['auto_play']|escape:'html':'UTF-8'}
        });
    	</script>

					</div>	
				</div>
				

			{/foreach}
		</div>

	</div>



</div>