{*
 * Please do not edit or add any code in this file without the permission of PIBBLU
 *
 * @author    Pibblu 
 * @copyright PIBBLU
 * @license   http://www.pibblu.com  
 * Prestashop version 1.6.0.6
 * layoutdesigner 3.1.0
 * OCT 2015
 *}
<div class="panel"><h3><i class="icon-list-ul"></i> {$layoutsectionlist_title|escape:'html':'UTF-8'}{l s=' list' mod='layoutdesigner'}
	<span class="panel-heading-action">
		<span>
		<a id="desc-layoutdesigner-new" class="btn " href="{$new|escape:'html':'UTF-8'}">
			<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="Add new" data-html="true">
				<i class="process-icon-new "></i>
			</span>
		</a>
		</span>
		<span>
	<a id="desc-layoutdesigner-back" class="btn " href="{$back|escape:'html':'UTF-8'}">
	<span title="" data-toggle="tooltip" class="label-tooltip" data-original-title="back To List" data-html="true">		
		<i class="process-icon-back "></i>
		</span>
	</a>		
	</span>
	</span>
	</h3>

	<div id="slidesContent" class="tableDnD ">
		<ul id="slides" class="list-unstyled ui-sortable">
			{foreach from=$slides item=slide}
				<li id="slides_{$slide.id_layout_imagelist|escape:'html':'UTF-8'}" class="panel item well">
					<div class="row">
						<div class="col-lg-1">
							<span><i class="icon-arrows "></i></span>
						</div>
						<div class="col-md-3">
							<img src="{$image_baseurl|escape:'html':'UTF-8'}{$slide.image|escape:'html':'UTF-8'}" alt="{$slide.description|escape:'html':'UTF-8'}" class="img-thumbnail" style="height: 250px;"/>
						</div>
						<div class="col-md-8">
							<h4 class="pull-left">#{$slide.id_layout_imagelist|escape:'html':'UTF-8'} - {$slide.description|escape:'html':'UTF-8'}</h4>
							<div class="btn-group-action pull-right">
								
								
								<a class="btn btn-default"
									href="{$link->getAdminLink('AdminModules')|escape:'htmlall':'UTF-8'}&configure=layoutdesigner&edit_id_image={$slide.id_layout_imagelist|escape:'htmlall':'UTF-8'}&editeimagelist&id_layoutsectionlist={$layoutsectionlist_id|escape:'htmlall':'UTF-8'}">
									<i class="icon-edit"></i>
									{l s='Edit' mod='layoutdesigner'}
								</a>

								<a class="btn btn-default"
									href="{$link->getAdminLink('AdminModules')|escape:'htmlall':'UTF-8'}&configure=layoutdesigner&delete_id_image={$slide.id_layout_imagelist|escape:'htmlall':'UTF-8'}&deleteimagelist&id_layoutsectionlist={$layoutsectionlist_id|escape:'htmlall':'UTF-8'}">
									<i class="icon-trash"></i>
									{l s='Delete' mod='layoutdesigner'}
								</a>
							</div>
						</div>
					</div>
				</li>
			{/foreach}
		</ul>
	</div>
</div>