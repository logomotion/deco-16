{*
 * Please do not edit or add any code in this file without the permission of PIBBLU
 *
 * @author    Pibblu 
 * @copyright PIBBLU
 * @license   http://www.pibblu.com  
 * Prestashop version 1.6.0.6
 * pibblublockinfolinks 3.1.0
 * OCT 2014
 *}
 
 

    <div class="tab-listing custom_blocks col-xs-12 col-sm-6 {$tab_text_detail['set_col_md']|escape:'html':'UTF-8'}  animate-delay-outer" id="master"> 
        
          {foreach from=$textarea_tab[$home_tab['id_layoutsectionlist']] key="textarea_tab_key" item="textarea_tab_list"} 
           {if $textarea_tab_list['text']|escape:html:'UTF-8'}    
            <ul class="tt-box  col-md-3 animate-delay scale" >

              <li class="inside"> 
                  {if $textarea_tab_list['link']}
                  <a href="{$textarea_tab_list['link']|escape:'html':'UTF-8'}" target="_blank">
                  {/if}
                    <div class="text_align">
                    {if $textarea_tab_list['icon']}
                      <span class="icon_circle dark"><i class="{$textarea_tab_list['icon']|escape:'html':'UTF-8'}"></i></span>        
                    {/if}
                    </div>

                      <div class="text_align">
                    <span class="text">{$textarea_tab_list['text']|escape:'html':'UTF-8'}</span>
                  </div>
                 
                  {if $textarea_tab_list['link']}
                   </a>
                  {/if} 

              </li>
    
            </ul>
            {/if}               
          {/foreach}               
        
  </div>

            <script type="text/javascript">
      $("#master > :first-child").addClass( "alpha");
$("#master > :last-child").addClass("omega");
   </script>         