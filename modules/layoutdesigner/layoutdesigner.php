<?php
/**
 * Please do not edit or add any code in this file without the permission of PIBBLU
 *
 * @author    Pibblu 
 * @copyright PIBBLU
 * @license   http://www.pibblu.com  
 * Prestashop version 1.6.0.6
 * layoutdesigner 3.0.1
 * OCT 2015
 */

if (!defined('_PS_VERSION_')) exit;

require_once(_PS_MODULE_DIR_.'layoutdesigner/layoutdesignermodel.php');

class LayoutDesigner extends Module
{

    protected $max_image_size = 1048576;
    public $html;
    protected $position_identifier = 'id_layoutsectionlist';

    public function __construct()
    {
        $this->name = 'layoutdesigner'; // internal identifier, unique and lowercase
        $this->tab = 'front_office_features';
        $this->version = '3.2.1'; // version number for the module
        $this->author = 'pibblu theme'; // module author
        $this->module_key = '81f3fee8a0d32d5f1064d019d2ae3b3f';
        $this->need_instance = 0; // load the module when displaying the "Modules" page 
        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Home Page Layout Designer.Product Carousel, Image block'); // public name
        $this->description = $this->l('Home Page Layout Designer.  Product Carousel,  Image block'); // public description
        $this->ps_versions_compliancy = array(
        'min' => '1.6',
        'max' => _PS_VERSION_);
        $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');
        $this->_tabsArray = array(
        'AdminLayoutSettings' => 'LAYOUT SETTINGS',
        );
    }

    /**
     * Install this module
     * @return boolean
     */
    public function install()
    {
        $ret = parent::install() && $this->installDB() && $this->registerHook('header') && $this->registerHook('home') && $this->registerHook('displayHomeTab') &&
        $this->registerHook('displayHomeTabContent') && $this->registerHook('addproduct') && $this->registerHook('updateproduct') && $this->registerHook('deleteproduct') && $this->registerHook('categoryUpdate');
        $this->_clearCache('hometab_carousel.tpl');
        return $ret;
    }

    public function installDB()
    {
        $return = true;

        include(dirname(__FILE__).'/sql/install.php');
        return $return;
    }
 
    /**
     * Uninstall this module
     * @return boolean
     */
    public function uninstall()
    {
        $ret = $this->uninstallDB() && Configuration::deleteByName('layoutdesigner') && parent::uninstall();
        $this->_clearCache('hometab_carousel.tpl');
        return $ret;
    }

    public function uninstallDB()
    {
        //include(dirname(__FILE__).'/sql/uninstall.php');
        return Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'layout_carousellist`') && Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'layoutsectionlist`') &&
        Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'layout_imagelist`') && Db::getInstance()->execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'layout_textarealist`');
    }




    public function getContent()
    {

        if (Tools::isSubmit('save_carousel')) {
            $carousel_title = Tools::getValue('tab_title');
            $hometab_details = array(
            );

            if ($carousel_title == '' || $carousel_title == null) {
                $carousel_title = 'home tab carousel';
            }

            switch (Tools::getValue('type')) {
                case 'carousel':
                    $hometab_details = array(
                    'slide_width' => Tools::getValue('slide_width'),
                    'tab_title' => $carousel_title,
                    'items_number' => Tools::getValue('items_number'),
                    'limit_cols' => Tools::getValue('limit_cols'),
                    'slide_pager' => Tools::getValue('slide_pager') ? 'true' : 'false',
                    'auto_play' => Tools::getValue('auto_play') ? 'true' : 'false',
                    'carousel_enabled' => Tools::getValue('carousel_enabled') ? 'true' : 'false',
                    'set_col_md' => Tools::getValue('items_set'),
                    'type' => Tools::getValue('type'),
                    );
                    break;
                case 'image':
                    $hometab_details = array(
                    'image_items' => Tools::getValue('image_items'),
                    'tab_title' => $carousel_title,
                    'image_items' => Tools::getValue('image_items'),
                    'image_position' => Tools::getValue('image_position'),
                    'image_enabled' => Tools::getValue('image_enabled') ? 'true' : 'false',
                    'set_col_md' => Tools::getValue('tab_width'),
                    'type' => Tools::getValue('type'),
                    );
                    break;
                case 'text':
                    $hometab_details = array(
                    'tab_title' => $carousel_title,
                    'auto_play' => Tools::getValue('auto_play') ? 'true' : 'false',
                    'set_col_md' => Tools::getValue('textarea_width'),
                    'type' => Tools::getValue('type'),
                    );
                    break;
                default:
                    $hometab_details = array(
                    );
            }
            $newtablist = array(
            );
            $newtablist['id_shop'] = (int)Shop::getContextShopID();
            $newtablist['id_lang'] = (int)$this->context->language->id;
            $newtablist['tab_title'] = $carousel_title;
            $newtablist['hometab_details'] = $hometab_details;
            $newtablist['hometab_type'] = Tools::getValue('type');


            if (Tools::getValue('id_layoutsectionlist')) {
                $newtablist['position'] = $this->getPosition(Tools::getValue('id_layoutsectionlist'));
                $this->updatehometablist($newtablist, Tools::getValue('id_layoutsectionlist'));
            } else {
                $newtablist['position'] = $this->getMaxPosition();
                $this->addhometablist($newtablist);
            }
            $this->html .= $this->displayConfirmation($this->l('Settings updated'));
            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
        } elseif (Tools::isSubmit('addhometablayoutdesigner')) {
            $this->html .='<div class="">'.$this->homeTabList().'</div>';
            $this->html .='	<div class="panel col-lg-12">
								<div id="render" class="left1">
										<div>'.$this->renderForm().'</div>
									</div>									
									<div class="carousel_form">
									<div class="display_img" style="margin-left:20%;width: 50%;padding-bottom:30px;">
										<span id="layout_section_img_span"><img id="layout_section_img" itemprop="image" src="'.__PS_BASE_URI__.'modules/layoutdesigner/views/img/title_img.png"></span></div>									
										<div class=" ">'.$this->carousel_form().'</div>
									</div>
									<div class="image_form ">
									<div class="display_img" style="margin-left:20%;width: 50%;padding-bottom:30px;">
										<span id="layout_section_img_span"><img id="layout_section_img" itemprop="image" src="'.__PS_BASE_URI__.'modules/layoutdesigner/views/img/image_banner_disp.png"></span></div>									
										<div class=" ">'.$this->image_form().'</div>
									</div>
									<div class="textarea_form ">
									<div class="display_img" style="margin-left:5%;width: 50%;padding-bottom:30px;">
										<span id="layout_section_img_span"><img id="layout_section_img" itemprop="image" src="'.__PS_BASE_URI__.'modules/layoutdesigner/views/img/textarea.png"></span></div>
										<div class=" ">'.$this->textarea_form().'</div>
									</div>																		
								</div>
							</div>';
        } elseif (Tools::isSubmit('viewlayoutdesigner')) {
            $id_layoutsectionlist = Tools::getValue('id_layoutsectionlist');
            $hometab_type = $this->getHomeTab_type($id_layoutsectionlist);
            $layoutsectionlist_title = $this->getHomeTab_title($id_layoutsectionlist);

            switch ($hometab_type) {
                case 'carousel':
                    $this->html .='<div class="">'.$this->CarouselList($id_layoutsectionlist, $layoutsectionlist_title).'</div>';
                    break;
                case 'image':
                    $this->html .='<div class="">'.$this->ImageList($id_layoutsectionlist, $layoutsectionlist_title).'</div>';
                    break;
                case 'text':
                    $this->html .='<div class="">'.$this->TextList($id_layoutsectionlist, $layoutsectionlist_title).'</div>';
                    break;
                default:
                    $this->html .='<div class="">'.$this->CarouselList($id_layoutsectionlist, $layoutsectionlist_title).'</div>';
            }
        } elseif (Tools::isSubmit('addlayoutdesigner')) {
            $id_layoutsectionlist = Tools::getValue('id_layoutsectionlist');
            $layoutsectionlist_title = $this->getHomeTab_title($id_layoutsectionlist);
            $this->html .='<div class="">'.$this->CarouselList($id_layoutsectionlist, $layoutsectionlist_title).'</div>';

            $this->html .='	<div class="panel col-lg-12">
								<div id="render" class="left1">
										<div>'.$this->carouselTypeForm().'</div>
									</div>									
									<div class="Category_carousel">									<div class=" ">'.$this->addcarouseltab().'</div>
									</div>
									<div class="Products_carousel">								
										<div class=" ">'.$this->initFormPack().'</div>
									</div>
									<div class="Specialcat_carousel">
										<div class=" ">'.$this->addSpecialcat_carousel().'</div>	
									</div>																		
								</div>
							</div>';


            $this->context->controller->addJS(($this->_path).'/views/js/products.js');
            
            $this->context->controller->addJqueryUI(array(
            'ui.core',
            'ui.widget'
            ));
            $this->context->controller->addjQueryPlugin(array(
            'autocomplete',
            'fancybox',
            'typewatch',
            'tablednd',
            'thickbox',
            'ajaxfileupload',
            'date',
            'tagify',
            'select2',
            'validate'
            ));

            $this->context->controller->addJS(array(
            _PS_JS_DIR_.'tiny_mce/tiny_mce.js',
            _PS_JS_DIR_.'admin/tinymce.inc.js',
            _PS_JS_DIR_.'admin/dnd.js',
            _PS_JS_DIR_.'jquery/ui/jquery.ui.progressbar.min.js',
            _PS_JS_DIR_.'vendor/spin.js',
            _PS_JS_DIR_.'vendor/ladda.js'
            ));
            $this->context->controller->addJS(_PS_JS_DIR_.'jquery/plugins/select2/select2_locale_'.$this->context->language->iso_code.'.js');
            $this->context->controller->addJS(_PS_JS_DIR_.'jquery/plugins/validate/localization/messages_'.$this->context->language->iso_code.'.js');

            $this->context->controller->addCSS(array(
            _PS_JS_DIR_.'jquery/plugins/timepicker/jquery-ui-timepicker-addon.css',
            ));
        } elseif (Tools::isSubmit('addimagelayoutdesigner')) {
            $id_layoutsectionlist = Tools::getValue('id_layoutsectionlist');
            $layoutsectionlist_title = $this->getHomeTab_title($id_layoutsectionlist);
            $this->html .='<div>'.$this->imageAddForm().'</div>';
            $this->html .='<div class="">'.$this->ImageList($id_layoutsectionlist, $layoutsectionlist_title).'</div>';
        } elseif (Tools::isSubmit('addtextlayoutdesigner')) {
            $id_layoutsectionlist = Tools::getValue('id_layoutsectionlist');
            $layoutsectionlist_title = $this->getHomeTab_title($id_layoutsectionlist);
            $this->html .='<div class="">'.$this->TextList($id_layoutsectionlist, $layoutsectionlist_title).'</div>';
            $this->html .='<div>'.$this->textAddForm().'</div>';
        } elseif (Tools::isSubmit('updatelayoutdesigner')) {

            $id_layout_carousellist = Tools::getValue('id_layout_carousellist');
            $id_layoutsectionlist = Tools::getValue('id_layoutsectionlist');
            $id_layout_textarealist = Tools::getValue('id_layout_textarealist');

            if (!empty($id_layout_carousellist)) {
                $carousel_type = $this->getCarousel_type($id_layout_carousellist);
                $id_layoutsectionlist = $carousel_type['id_hometab'];
                $hometab_title = $carousel_type['hometab_title'];
                $layoutsectionlist_title = $this->getHomeTab_title($id_layoutsectionlist);
                $this->html .='<div class="">'.$this->CarouselList($id_layoutsectionlist, $layoutsectionlist_title).'</div>';

                switch ($carousel_type['carousel_type']) {
                    case 'Category_carousel':
                        $this->html .='<div class="">'.$this->addcarouseltab($id_layout_carousellist).'</div>';
                        break;
                    case 'Products_carousel':
                        $this->html .='<div class="">'.$this->initFormPack($id_layout_carousellist, $hometab_title, $id_layoutsectionlist).'</div>';
                        $this->context->controller->addJS(($this->_path).'/views/js/products.js');
                        
                        $this->context->controller->addJqueryUI(array(
                        'ui.core',
                        'ui.widget'
                        ));
                        $this->context->controller->addjQueryPlugin(array(
                        'autocomplete',
                        'fancybox',
                        'typewatch',
                        'tablednd',
                        'thickbox',
                        'ajaxfileupload',
                        'date',
                        'tagify',
                        'select2',
                        'validate'
                        ));

                        $this->context->controller->addJS(array(
                        _PS_JS_DIR_.'tiny_mce/tiny_mce.js',
                        _PS_JS_DIR_.'admin/tinymce.inc.js',
                        _PS_JS_DIR_.'admin/dnd.js',
                        _PS_JS_DIR_.'jquery/ui/jquery.ui.progressbar.min.js',
                        _PS_JS_DIR_.'vendor/spin.js',
                        _PS_JS_DIR_.'vendor/ladda.js'
                        ));
                        $this->context->controller->addJS(_PS_JS_DIR_.'jquery/plugins/select2/select2_locale_'.$this->context->language->iso_code.'.js');
                        $this->context->controller->addJS(_PS_JS_DIR_.'jquery/plugins/validate/localization/messages_'.$this->context->language->iso_code.'.js');

                        $this->context->controller->addCSS(array(
                        _PS_JS_DIR_.'jquery/plugins/timepicker/jquery-ui-timepicker-addon.css',
                        ));
                        break;
                    default:
                        $this->html .='<div class="">'.$this->addSpecialcat_carousel($id_layout_carousellist, $id_layoutsectionlist,
                        $layoutsectionlist_title).'</div>';
                }
            } elseif (!empty($id_layout_textarealist)) {
                $id_layoutsectionlist = $this->getIdHomeTab($id_layout_textarealist);
                $layoutsectionlist_title = $this->getHomeTab_title($id_layoutsectionlist);
                $this->html .='<div class="">'.$this->TextList($id_layoutsectionlist, $layoutsectionlist_title).'</div>';
                $this->html .='<div>'.$this->textAddForm($id_layoutsectionlist).'</div>';
            } elseif (!empty($id_layoutsectionlist)) {
                $id_layoutsectionlist = Tools::getValue('id_layoutsectionlist');
                $hometab_type = $this->getHomeTab_type($id_layoutsectionlist);
                $this->html .='<div class="">'.$this->homeTabList().'</div>';
                switch ($hometab_type) {
                    case 'carousel':
                        $this->html .='<div class="">'.$this->carousel_form().'</div>';
                        break;
                    case 'image':
                        $this->html .='<div class="">'.$this->image_form().'</div>';
                        break;
                    case 'text':
                        $this->html .='<div class="">'.$this->textarea_form().'</div>';
                        break;
                    default:
                        $this->html .='<div class="">'.$this->carousel_form().'</div>';
                }
            }
        } elseif (Tools::isSubmit('updateBtn')) {
            $carousel_details = array(
            'slide_width' => Tools::getValue('slide_width'),
            'tab_title' => Tools::getValue('tab_title'),
            'items_number' => Tools::getValue('items_number'),
            'limit_cols' => Tools::getValue('limit_cols'),
            'slide_pager' => Tools::getValue('slide_pager') ? 'true' : 'false',
            'auto_play' => Tools::getValue('auto_play') ? 'true' : 'false',
            'auto_play' => Tools::getValue('auto_play') ? 'true' : 'false',
            'set_col_md' => Tools::getValue('items_set')
            );

            $newtablist = array(
            );
            $newtablist['id_shop'] = (int)Shop::getContextShopID();
            $newtablist['id_lang'] = (int)$this->context->language->id;
            $newtablist['tab_title'] = Tools::getValue('tab_title');
            $newtablist['carousel_details'] = $carousel_details;
            $newtablist['id_layoutsectionlist'] = Tools::getValue('id_layoutsectionlist');
            $this->updatehometablist($newtablist);
            $this->html .= $this->displayConfirmation($this->l('Settings updated'));

            $this->html .='<div class="">'.$this->homeTabList().'</div>';
        } elseif (Tools::isSubmit('deletelayoutdesigner')) {
            $hometab_id = Tools::getValue('id_layout_carousellist');
            $layoutsectionlist_id = Tools::getValue('id_layoutsectionlist');

            if (!empty($hometab_id)) {
                $this->deletehometab($hometab_id);
            } elseif (!empty($layoutsectionlist_id)) {
                $this->deletehometablist($layoutsectionlist_id);
            }
            Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
        } elseif (Tools::isSubmit('addcarouselitem')) {
            $newItems_array = array(
            );
            $newItems_array['id_shop'] = (int)Shop::getContextShopID();
            $newItems_array['id_lang'] = (int)$this->context->language->id;
            $newItems_array['hometab_title'] = Tools::getValue('hometab_title');
            $newItems_array['id_layoutsectionlist'] = Tools::getValue('id_layoutsectionlist');
            $item_category = Tools::getValue('item_category');

            $carousel_type = Tools::getValue('type');
            $newItems_array['carousel_type'] = $carousel_type;
            $id_layout_carousellist = Tools::getValue('id_layout_carousellist');

            switch ($carousel_type) {
                case 'Category_carousel':
                    $category = new Category((int)Tools::getValue('item_category'), $this->context->language->id);
                    if (Validate::isLoadedObject($category)) $newItems_array['carousel_name'] = $category->name;
                    $hometab_details = array(
                    'category_name' => $category->name,
                    'hometab_title' => Tools::getValue('hometab_title'),
                    'id_category' => Tools::getValue('item_category'));
                    $newItems_array['carousellist_details'] = serialize($hometab_details);
                    break;
                case 'Specialcat_carousel':
                    $newItems_array['carousel_name'] = Tools::getValue('item_category');
                    $hometab_details = array(
                    'category_name' => Tools::getValue('item_category'),
                    'hometab_title' => Tools::getValue('hometab_title'),
                    'id_category' => Tools::getValue('item_category'));
                    $newItems_array['carousellist_details'] = serialize($hometab_details);
                    break;
                case 'Products_carousel':
                    $pack_items = $this->getProductItems();
                    $newItems_array['carousel_name'] = 'Product_ids';
                    $product_ids = array(
                    );
                    $product_pack_quantity = array(
                    );
                    $product_name = array(
                    );
                    foreach ($pack_items as $key => $pack_item) {
                        $product_ids[] = $pack_item['ids'];
                        $product_pack_quantity[] = $pack_item['pack_quantity'];
                        $product_name[] = $pack_item['name'];
                    }

                    $hometab_details = array(
                    'Product_ids' => $product_ids,
                    'product_pack_quantity' => $product_pack_quantity,
                    'product_name' => $product_name,
                    'id_category' => $pack_items[0],
                    'hometab_title' => Tools::getValue('hometab_title'),
                    'category_name' => Tools::getValue('hometab_title'));
                    $newItems_array['carousellist_details'] = serialize($hometab_details);
                    break;
            }

            if ($id_layout_carousellist) {
                $carousel_type = $this->getCarousel_type($id_layout_carousellist);
                $id_layoutsectionlist = $carousel_type['id_hometab'];
                $newItems_array['id_layoutsectionlist'] = $id_layoutsectionlist;
                $layoutsectionlist_title = $this->getHomeTab_title($id_layoutsectionlist);
                $this->updatehometab($newItems_array, $id_layout_carousellist);
            } else {
                $id_layoutsectionlist = Tools::getValue('id_layoutsectionlist');
                $this->addhometab($newItems_array);
            }

            $this->html .= $this->displayConfirmation($this->l('Settings updated'));
            $layoutsectionlist_title = $this->getHomeTab_title($id_layoutsectionlist);
            $this->html .='<div class="">'.$this->CarouselList($id_layoutsectionlist, $layoutsectionlist_title).'</div>';
        }

        /* Processes Slide */ 
        elseif (Tools::isSubmit('submitimage')) {
            /* Sets active */
            $active = (int)Tools::getValue('active_slide');
            $errors = '';
            /* Sets each langue fields */
            $url = Tools::getValue('url');
            $imgwidth = Tools::getValue('imgwidth');
            $description = Tools::getValue('description');

            $imagetablist = array(
            );
            /* Processes if no errors  */
            if (!$errors) {
                $imagetablist['imgwidth'] = $imgwidth;
                $imagetablist['url'] = $url;
                $imagetablist['description'] = $description;
                $imagetablist['active_image'] = $active;
                $imagetablist['id_shop'] = (int)Shop::getContextShopID();
                $imagetablist['id_lang'] = (int)$this->context->language->id;
                $imagetablist['id_layoutsectionlist'] = Tools::getValue('id_layoutsectionlist');
                //ppp($imagetablist);
                //die();					
            }
            if (Tools::getValue('edit_id_image')) {

                $image_id = (int)Tools::getValue('edit_id_image');
                if (!empty($_FILES['image']['name'])) {
                    if ($old_image = Db::getInstance()->getValue('SELECT image FROM `'._DB_PREFIX_.'layout_imagelist` WHERE id_layout_imagelist = '.(int)$image_id))
                            if (file_exists(dirname(__FILE__).'/views/img/'.$old_image))
                                @unlink(dirname(__FILE__).'/views/img/'.$old_image);
                    if (!$image = $this->uploadImage($_FILES['image'])) return false;
                    $new_image = 'image = \''.pSQL($image).'\',';
                }
                else {
                    $image = Db::getInstance()->getValue('SELECT image FROM `'._DB_PREFIX_.'layout_imagelist` WHERE id_layout_imagelist = '.(int)$image_id);
                }
                $imagetablist['image'] = $image;
                $this->updateimagetab($imagetablist);
            } else {
                if (!empty($_FILES['image']['name'])) {
                    if (!$image = $this->uploadImage($_FILES['image'])) return false;
                }
                else {
                    $image = '';
                }

                $position = Db::getInstance()->getValue('SELECT COUNT(*)
                FROM `'._DB_PREFIX_.'layout_imagelist`
                WHERE id_hometab = \''.pSQL(Tools::getValue('id_layoutsectionlist')).'\'');
                $imagetablist['image'] = $image;
                $imagetablist['position'] = $position + 1;
                $this->addimagetab($imagetablist);
            }

            $this->html .= $this->displayConfirmation($this->l('Settings updated'));
            $id_layoutsectionlist = Tools::getValue('id_layoutsectionlist');
            $layoutsectionlist_title = $this->getHomeTab_title($id_layoutsectionlist);
            $this->html .='<div class="">'.$this->ImageList($id_layoutsectionlist, $layoutsectionlist_title).'</div>';
        } elseif (Tools::isSubmit('deleteimagelist')) {
            $hometab_id = Tools::getValue('delete_id_image');
            $id_layoutsectionlist = Tools::getValue('id_layoutsectionlist');
            if (!empty($hometab_id)) {
                $this->deletetabimage($hometab_id);
            }
            $layoutsectionlist_title = $this->getHomeTab_title($id_layoutsectionlist);
            $this->html .='<div class="">'.$this->ImageList($id_layoutsectionlist, $layoutsectionlist_title).'</div>';
        } elseif (Tools::isSubmit('editeimagelist')) {
            $hometab_id = Tools::getValue('edit_id_image');
            $id_layoutsectionlist = Tools::getValue('id_layoutsectionlist');
            //ppp($hometab_id);
            //die();		
            $layoutsectionlist_title = $this->getHomeTab_title($id_layoutsectionlist);
            $this->html .='<div>'.$this->imageAddForm().'</div>';
            $this->html .='<div class="">'.$this->ImageList($id_layoutsectionlist, $layoutsectionlist_title).'</div>';
        } elseif (Tools::isSubmit('addtextitem')) {
            $newItems_array = array(
            );
            $newItems_array['id_shop'] = (int)Shop::getContextShopID();
            $newItems_array['id_lang'] = (int)$this->context->language->id;
            $newItems_array['text'] = Tools::getValue('text');
            $newItems_array['icon'] = Tools::getValue('icon');
            $newItems_array['link'] = Tools::getValue('link');
            $newItems_array['id_layoutsectionlist'] = Tools::getValue('id_layoutsectionlist');

            if (Tools::getValue('id_layout_textarealist')) {
                //$this->updatehometablist($newtablist, Tools::getValue('id_layout_textarealist'));
                $this->updateTextArea($newItems_array, Tools::getValue('id_layout_textarealist'));
            } else {
                $this->addTextArea($newItems_array);
            }


            $this->html .= $this->displayConfirmation($this->l('Settings updated'));
            $id_layoutsectionlist = Tools::getValue('id_layoutsectionlist');
            $layoutsectionlist_title = $this->getHomeTab_title($id_layoutsectionlist);
            $this->html .='<div class="">'.$this->TextList($id_layoutsectionlist, $layoutsectionlist_title).'</div>';
        } elseif (Tools::getIsset('ajax_product_list')) {
            $this->processAjaxProductsList();
        } elseif (Tools::getIsset('save_color')) {
            $layoutdesigner_css_value = array(
            'carsoul_tab_color' => Tools::getValue('carsoul_tab_color'),
            'text_background_color' => Tools::getValue('text_background_color'),
            'text_color' => Tools::getValue('text_color'),
            'carsoul_tabactive_color' => Tools::getValue('carsoul_tabactive_color'),
            'layout_border_color' => Tools::getValue('layout_border_color'),
            'icon_backgroud_color' => Tools::getValue('icon_backgroud_color'));
            Configuration::updateValue('layoutdesigner_css_value', serialize($layoutdesigner_css_value));
            $this->_clearCache('hometab_carousel.tpl');
            $this->html .='<div class="">'.$this->homeTabList().'</div>';
            $this->html .='<div class="">'.$this->color_renderForm().'</div>';
        } elseif (Tools::getIsset('ajaxProcessUpdatePositions')) {
            $this->ajaxProcessUpdatePositions();
        }elseif (Tools::getIsset('ajax')) {
            $this->ajaxProcessUpdatePositions();
        } 

        elseif (Tools::isSubmit('id_layoutsectionlist') && Tools::isSubmit('way') && Tools::isSubmit('position') && Tools::isSubmit('location'))
                $this->changePosition();
        elseif (Tools::isSubmit('updatePositions')) $this->updatePositionsDnd();
        else {
            $this->html .='<div class="">'.$this->homeTabList().'</div>';
            $this->html .='<div class="">'.$this->color_renderForm().'</div>';
        }
        //$this->homeTabList();
        $this->_postProcess();
        $this->context->controller->addJS(($this->_path).'views/js/admin.js');
        $this->context->controller->addJS(($this->_path).'views/js/click.js');
        $this->context->controller->addCSS(($this->_path).'views/css/admin.css');


        return $this->html;
    }

    public function getContent123()
    {
        $this->_html = '';
        $this->_postProcess();

        if (Tools::isSubmit('addBlockCMS') || Tools::isSubmit('editBlockCMS')) $this->displayAddForm();
        else $this->homeTabList();

        return $this->_html;
    }

    protected function homeTabList()
    {
        $this->context->controller->addJqueryPlugin('tablednd');
        //$this->context->controller->addJS(($this->_path).'views/js/admin-dnd1.js');
        $this->context->controller->addJS(($this->_path).'views/js/admin_dnd.js');        
        if (version_compare(_PS_VERSION_, '1.6.0.11', '>=') === true)
        //$this->context->controller->addJS(_PS_JS_DIR_.'admin/dnd.js');
                $this->context->controller->addJS(($this->_path).'views/js/dnd.js');
        else $this->context->controller->addJS(_PS_JS_DIR_.'admin-dnd.js');

        $current_index = AdminController::$currentIndex.'&configure='.$this->name;
        ppp($current_index);
        $token = Tools::getAdminTokenLite('AdminModules');

        $this->_display = 'index';

        $this->fields_form[0]['form'] = array(
        'legend' => array(
        'title' => $this->l('HOME PAGE LAYOUT SECTIONS / FRAMES'),
        'icon' => 'icon-list-alt'
        ),
        'input' => array(
        array(
        'type' => 'cms_blocks',
        'label' => $this->l('CMS Blocks'),
        'name' => 'cms_blocks',
        'values' => array(
        0 => LayoutDesignerModel::getCMSBlocksByLocation(Shop::getContextShopID()))
        )
        ),
        'buttons' => array(
        'newBlock' => array(
        'title' => $this->l('NEW LAYOUT SECTIONS'),
        'href' => $current_index.'&amp;configure='.$this->name.'&amp;token='.$token.'&amp;addhometablayoutdesigner',
        'class' => 'pull-right',
        'icon' => 'process-icon-new'
        )
        )
        );


        $this->context->controller->getLanguages();
        $tmp = Configuration::get('FOOTER_CMS');
        $footer_boxes = explode('|', $tmp);

        if ($footer_boxes && is_array($footer_boxes)) foreach ($footer_boxes as $key => $value) $this->fields_value[$value] = true;

        foreach ($this->context->controller->_languages as $language) {
            $footer_text = Configuration::get('FOOTER_CMS_TEXT_'.$language['id_lang']);
            $this->fields_value['footer_text'][$language['id_lang']] = $footer_text;
        }

        $helper = $this->initForm();
        $helper->submit_action = '';
        $helper->title = $this->l('HOME PAGE LAYOUT SECTIONS / FRAMES');

        $helper->fields_value = $this->fields_value;
        //$this->_html .= $helper->generateForm($this->fields_form);

        return $helper->generateForm($this->fields_form);
    }

    protected function initForm()
    {
        $helper = new HelperForm();

        $helper->module = $this;
        $helper->name_controller = 'layoutdesigner';
        $helper->identifier = $this->identifier;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->languages = $this->context->controller->_languages;
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $helper->default_form_language = $this->context->controller->default_form_language;
        $helper->allow_employee_form_lang = $this->context->controller->allow_employee_form_lang;
        $helper->toolbar_scroll = true;
        //$helper->toolbar_btn = $this->initToolbar();

        return $helper;
    }

    public function initToolbar()
    {
        $current_index = AdminController::$currentIndex;
        $token = Tools::getAdminTokenLite('AdminModules');
        $back = Tools::safeOutput(Tools::getValue('back', ''));
        if (!isset($back) || empty($back)) $back = $current_index.'&amp;configure='.$this->name.'&token='.$token;

        switch ($this->_display) {
            case 'add':
                $this->toolbar_btn['cancel'] = array(
                'href' => $back,
                'desc' => $this->l('Cancel')
                );
                break;
            case 'edit':
                $this->toolbar_btn['cancel'] = array(
                'href' => $back,
                'desc' => $this->l('Cancel')
                );
                break;
            case 'index':
                $this->toolbar_btn['new'] = array(
                'href' => $current_index.'&amp;configure='.$this->name.'&amp;token='.$token.'&amp;addBlockCMS',
                'desc' => $this->l('Add new')
                );
                break;
            default:
                break;
        }
        return $this->toolbar_btn;
    }

    protected function _postProcess()
    {


        $this->_errors = array(
        );

        if (Tools::isSubmit('id_cms_block') && Tools::isSubmit('way') && Tools::isSubmit('position') && Tools::isSubmit('location')) {
            ;
            $this->changePosition();
        } elseif (Tools::isSubmit('updatePositions')) $this->updatePositionsDnd();
        if (count($this->_errors)) {
            foreach ($this->_errors as $err) $this->_html .= '<div class="alert error">'.$err.'</div>';
        }
    }

    protected function updatePositionsDnd()
    {

        //die();
        if (Tools::getValue('cms_block_0')) $positions = Tools::getValue('cms_block_0');
        elseif (Tools::getValue('cms_block_1')) $positions = Tools::getValue('cms_block_1');
        else $positions = array(
            );

        foreach ($positions as $position => $value) {
            $pos = explode('_', $value);

            if (isset($pos[2])) LayoutDesignerModel::updateCMSBlockPosition($pos[2], $position);
        }
        $this->_clearCache('hometab_carousel.tpl');
    }

    public function deletehometab($hometab_id)
    {
        $db_result = Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'layout_carousellist` WHERE  id_layout_carousellist =\''.$hometab_id.'\'');
        $this->_clearCache('hometab_carousel.tpl');
    }

    public function clearCache()
    {
        $this->_clearCache('hometab_carousel.tpl');
    }

    public function deletetabimage($hometab_id)
    {
        $db_result = Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'layout_imagelist` WHERE  id_layout_imagelist =\''.$hometab_id.'\'');
        $this->_clearCache('hometab_carousel.tpl');
    }

    public function deletehometablist($layoutsectionlist_id)
    {

        $db_img_lists = Db::getInstance()->executeS('
			SELECT `image`FROM `'._DB_PREFIX_.'layout_imagelist`
			WHERE `id_hometab` = '.(int)$layoutsectionlist_id.'');

        foreach ($db_img_lists as $old_image) {
            if (file_exists(dirname(__FILE__).'/views/img/'.$old_image['image'])) @unlink(dirname(__FILE__).'/views/img/'.$old_image);
        }

        $db_result = Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'layout_carousellist` WHERE  id_hometab =\''.$layoutsectionlist_id.'\'');
        $db_result = Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'layout_imagelist` WHERE  id_hometab =\''.$layoutsectionlist_id.'\'');
        $db_result = Db::getInstance()->execute('DELETE FROM `'._DB_PREFIX_.'layoutsectionlist` WHERE  id_layoutsectionlist =\''.$layoutsectionlist_id.'\'');
        $this->_clearCache('hometab_carousel.tpl');
    }

    /**
     * Configuration page
     */
    public function addhometablist($newtablist)
    {
        $sql = 'INSERT INTO '._DB_PREFIX_.'layoutsectionlist(layoutsectionlist_title,hometab_type,position,id_shop,id_lang,carousel_details)
						VALUES
						(\''.pSQL($newtablist['tab_title']).'\',
						\''.pSQL($newtablist['hometab_type']).'\',
						\''.pSQL($newtablist['position']).'\',	
						\''.pSQL($newtablist['id_shop']).'\',
						\''.pSQL($newtablist['id_lang']).'\',
						\''.pSQL(serialize($newtablist['hometab_details'])).'\'
						)';
        $sql_result = Db::getInstance()->execute($sql);
        $this->_clearCache('hometab_carousel.tpl');
    }

    public function updatehometablist($newtablist, $id_layoutsectionlist)
    {
        $sql = 'UPDATE `'._DB_PREFIX_.'layoutsectionlist` SET 
			`layoutsectionlist_title`= \''.pSQL($newtablist['tab_title']).'\',
			`hometab_type`= \''.pSQL($newtablist['hometab_type']).'\',
			`id_shop`= \''.pSQL($newtablist['id_shop']).'\',
			`id_lang`= \''.pSQL($newtablist['id_lang']).'\',
			`carousel_details` = \''.pSQL(serialize($newtablist['hometab_details'])).'\'
			WHERE `id_layoutsectionlist`= \''.pSQL($id_layoutsectionlist).'\'';
        $sql_result = Db::getInstance()->execute($sql);
        $this->_clearCache('hometab_carousel.tpl');
    }

    public function addhometab($newItems_array)
    {
        $sql = 'INSERT INTO '._DB_PREFIX_.'layout_carousellist (hometab_title,carousel_type,carousel_name,id_shop,id_lang,id_hometab,carousellist_details)
						VALUES
						(\''.pSQL($newItems_array['hometab_title']).'\',
						\''.pSQL($newItems_array['carousel_type']).'\',
						\''.pSQL($newItems_array['carousel_name']).'\',
						\''.pSQL($newItems_array['id_shop']).'\',
						\''.pSQL($newItems_array['id_lang']).'\',
						\''.pSQL($newItems_array['id_layoutsectionlist']).'\',
						\''.pSQL($newItems_array['carousellist_details']).'\'
						)';
        $sql_result = Db::getInstance()->execute($sql);
        $this->_clearCache('hometab_carousel.tpl');
    }

    public function updatehometab($newItems_array, $id_layout_carousellist)
    {
        $sql = 'UPDATE `'._DB_PREFIX_.'layout_carousellist` SET 
			`hometab_title`= \''.pSQL($newItems_array['hometab_title']).'\',
			`carousel_type`= \''.pSQL($newItems_array['carousel_type']).'\',
			`carousel_name`= \''.pSQL($newItems_array['carousel_name']).'\',
			`id_shop`= \''.pSQL($newItems_array['id_shop']).'\',
			`id_lang`= \''.pSQL($newItems_array['id_lang']).'\',
			`id_hometab`= \''.pSQL($newItems_array['id_layoutsectionlist']).'\',
			`carousellist_details` = \''.pSQL($newItems_array['carousellist_details']).'\'
			WHERE `id_layout_carousellist`= \''.pSQL($id_layout_carousellist).'\'';
        $sql_result = Db::getInstance()->execute($sql);
        $this->_clearCache('hometab_carousel.tpl');
    }

    public function addimagetab($imagetablist)
    {

        $sql = 'INSERT INTO '._DB_PREFIX_.'layout_imagelist (image,imgwidth,url,description,id_shop,id_lang,active_image,id_hometab,position)
						VALUES
						(\''.pSQL($imagetablist['image']).'\',
						\''.pSQL($imagetablist['imgwidth']).'\',	
						\''.pSQL($imagetablist['url']).'\',
						\''.pSQL($imagetablist['description']).'\',
						\''.pSQL($imagetablist['id_shop']).'\',
						\''.pSQL($imagetablist['id_lang']).'\',
						\''.pSQL($imagetablist['active_image']).'\',
						\''.pSQL($imagetablist['id_layoutsectionlist']).'\',
                        \''.pSQL($imagetablist['position']).'\'
						)';
        $sql_result = Db::getInstance()->execute($sql);
        $this->_clearCache('hometab_carousel.tpl');
    }

    public function addTextArea($newItems_array)
    {
        $sql = 'INSERT INTO '._DB_PREFIX_.'layout_textarealist (id_lang,id_shop,text,icon,link,id_hometab)
						values
						(\''.pSQL($newItems_array['id_lang']).'\',
						\''.pSQL($newItems_array['id_shop']).'\',	
						\''.pSQL($newItems_array['text']).'\',
						\''.pSQL($newItems_array['icon']).'\',
						\''.pSQL($newItems_array['link']).'\',
						\''.pSQL($newItems_array['id_layoutsectionlist']).'\'
						)';
        $sql_result = Db::getInstance()->execute($sql);
        $this->_clearCache('hometab_carousel.tpl');
    }

    public function updateTextArea($newItems_array, $id_layout_textarealist)
    {
        //die();
        $sql = 'UPDATE `'._DB_PREFIX_.'layout_textarealist` SET 
			`id_lang`= \''.pSQL($newItems_array['id_lang']).'\',
			`id_shop`= \''.pSQL($newItems_array['id_shop']).'\',
			`text`= \''.pSQL($newItems_array['text']).'\',
			`icon`= \''.pSQL($newItems_array['icon']).'\',
			`link`= \''.pSQL($newItems_array['link']).'\',		
			`id_hometab` = \''.pSQL($newItems_array['id_layoutsectionlist']).'\'
			WHERE `id_layout_textarealist`= \''.pSQL($id_layout_textarealist).'\'';
        $sql_result = Db::getInstance()->execute($sql);
        $this->_clearCache('hometab_carousel.tpl');
    }

    public function updateimagetab($imagetablist)
    {
        $sql = 'UPDATE `'._DB_PREFIX_.'layout_imagelist` SET 
			`image`= \''.pSQL($imagetablist['image']).'\',
			`imgwidth`= \''.pSQL($imagetablist['imgwidth']).'\',
			`url`= \''.pSQL($imagetablist['url']).'\',
			`description`= \''.pSQL($imagetablist['description']).'\',
			`id_shop`= \''.pSQL($imagetablist['id_shop']).'\',
			`id_lang`= \''.pSQL($imagetablist['id_lang']).'\',
			`active_image` = \''.pSQL($imagetablist['active_image']).'\',
			`id_hometab` = \''.pSQL($imagetablist['id_layoutsectionlist']).'\'
			WHERE `id_layout_imagelist`= \''.pSQL(Tools::getValue('edit_id_image')).'\'';
        $sql_result = Db::getInstance()->execute($sql);
        $this->_clearCache('hometab_carousel.tpl');
    }

    protected function uploadImage($image)
    {
        $res = false;
        if (is_array($image) && (ImageManager::validateUpload($image, $this->max_image_size) === false) && ($tmp_name = tempnam(_PS_TMP_IMG_DIR_,
        'PS')) && move_uploaded_file($image['tmp_name'], $tmp_name)) {
            $salt = sha1(microtime());
            $pathinfo = pathinfo($image['name']);
            $img_name = $salt.'_'.Tools::str2url($pathinfo['filename']).'.'.$pathinfo['extension'];

            if (ImageManager::resize($tmp_name, dirname(__FILE__).'/views/img/'.$img_name, null, null)) $res = true;
        }

        if (!$res) {
            $this->context->smarty->assign('error', $this->l('An error occurred during the image upload.'));
            return false;
        }

        return $img_name;
    }

    protected function homeTabList_test()
    {
        $this->fields_list = array(
        'id_layoutsectionlist' => array(
        'title' => $this->l('LAYOUT SECTION ID'),
        'width' => 120,
        'type' => 'text',
        'search' => false,
        //'orderby' => false
        ),
        'layoutsectionlist_title' => array(
        'title' => $this->l('LAYOUT SECTION TITLE'),
        'width' => 140,
        'type' => 'text',
        'search' => false,
        //'orderby' => false
        ),
        'hometab_type' => array(
        'title' => $this->l('LAYOUT SECTION TYPE'),
        'width' => 140,
        'type' => 'text',
        'search' => false,
        //'orderby' => false
        ),
        'position' => array(
        'title' => $this->l('Position'),
        'width' => 40,
        'filter_key' => 'a!position',
        'search' => false,
        'position' => 'position'
        )
        );

        if (Shop::isFeatureActive())
                $this->fields_list['id_shop'] = array(
            'title' => $this->l('ID Shop'),
            'align' => 'center',
            'width' => 25,
            'type' => 'int');

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        $helper->identifier = 'id_layoutsectionlist';
        //$helper->table = 'layoutsectionlist';
        $helper->actions = array(
        'view',
        'edit',
        'delete');
        $helper->orderBy = 'position';
        //$helper->_orderBy = 'position';
        $helper->className = 'LayoutDesignerModel';
        $helper->position_identifier = 'position';
        $helper->name_controller = $this->name;
        $helper->show_toolbar = true;
        $helper->imageType = 'jpg';
        $helper->toolbar_btn['new'] = array(
        'href' => AdminController::$currentIndex.'&configure='.$this->name.'&addhometab'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
        'desc' => $this->l('Add new')
        );
        $helper->toolbar_btn['back'] = array(
        'href' => AdminController::$currentIndex.'&configure='.$this->name.'&addhometab'.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
        'desc' => $this->l('Back')
        );
        $helper->title = 'Home Page Layout Sections / Frames';
        $helper->table = $this->name;
        //$helper->table = $this->name.'_categories';
        $helper->toolbar_scroll = true;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        $id_shop = (int)$this->context->shop->id;
        $helper->name_controller = $this->name;
        //$helper->fields_value = $this->fields_value;
        //$this->_html .= $helper->generateForm($this->fields_form);
        //return $helper;
        return $helper->generateList($this->gettabListContent(), $this->fields_list);
        //$helper->generateList($this->gettabListContent(), $this->fields_list);
    }

    protected function gettabListContent()
    {
        $sql = Db::getInstance()->executeS('
			SELECT `id_layoutsectionlist`, `layoutsectionlist_title`, `hometab_type`,`id_shop`,`id_lang`,`position`
			FROM `'._DB_PREFIX_.'layoutsectionlist`
			ORDER BY `position` '.Shop::addSqlRestrictionOnLang());

        //ppp($sql);
        //die();

        return $sql;
    }

    public function ajaxProcess()
    {
        $query = 'SELECT * FROM mytable';
        echo Tools::jsonEncode(array(
        'data' => Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($query),
        'fields_display' => $this->fieldsDisplay
        ));
        // die();
    }

    public function ajaxProcessUpdatePosition()
    {
        $items = Tools::getValue('item');
        $total = count($items);
        $success = true;
        for ($i = 1; $i <= $total; $i++){
            $pos = explode('_', $items[$i - 1]);
            $success &= Db::getInstance()->update('layout_imagelist', array('position' => $i), '`id_layout_imagelist` = '.$pos[1]);
    }
            $this->_clearCache('hometab_carousel.tpl');
        if (!$success)
            die(Tools::jsonEncode(array('error' => 'Update Fail')));
        die(Tools::jsonEncode(array('success' => 'Update Success !', 'error' => false)));

    }

    public function ajaxProcessUpdatePositions()
    {
        $way = (int)(Tools::getValue('way'));
        $id_layoutsectionlist = (int)(Tools::getValue('id'));
        $positions = Tools::getValue('layoutsectionlist');
        foreach ($positions as $position => $value) {
            $pos = explode('_', $value);

            $id_layoutsectionlist = (int)$pos[2];

            if ((int)$id_layoutsectionlist > 0) {
                if ($LayoutDesignerModel = new LayoutDesignerModel($id_layoutsectionlist)) {
                    $LayoutDesignerModel->position = $position + 1;
                    if ($LayoutDesignerModel->update())
                            echo 'ok position '.(int)$position.' for category '.(int)$LayoutDesignerModel->id.'\r\n';
                }
                else {
                    echo '{"hasError" : true, "errors" : "This category ('.(int)$id_layoutsectionlist.') cant be loaded"}';
                }
            }
        }
    }

    protected function CarouselList($id_layoutsectionlist, $layoutsectionlist_title)
    {
        $this->fields_list = array(
        'id_layout_carousellist' => array(
        'title' => $this->l('TAB ID'),
        'width' => 120,
        'type' => 'text',
        'search' => false,
        'orderby' => false
        ),
        'hometab_title' => array(
        'title' => $this->l('CAROUSEL TAB TITLE'),
        'width' => 140,
        'type' => 'text',
        'search' => false,
        'orderby' => false
        ),
        'carousel_type' => array(
        'title' => $this->l('PRODUCTS CAROUSEL TYPE'),
        'width' => 140,
        'type' => 'text',
        'search' => false,
        'orderby' => false
        ),
        'carousel_name' => array(
        'title' => $this->l('PRODUCTS CAROUSEL NAME'),
        'width' => 140,
        'type' => 'text',
        'search' => false,
        'orderby' => false
        ),
        );

        if (Shop::isFeatureActive())
                $this->fields_list['id_shop'] = array(
            'title' => $this->l('ID Shop'),
            'align' => 'center',
            'width' => 25,
            'type' => 'int');

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        $helper->identifier = 'id_layout_carousellist';
        $helper->table = 'layout_carousellist';
        $helper->actions = array(
        'edit',
        'delete');
        $helper->show_toolbar = true;
        $helper->listTotal;
        $helper->imageType = 'jpg';
        $helper->toolbar_btn['new'] = array(
        'href' => AdminController::$currentIndex.'&configure='.$this->name.'&add'.$this->name.'&id_layoutsectionlist='.$id_layoutsectionlist.'&title='.$layoutsectionlist_title.'&token='.Tools::getAdminTokenLite('AdminModules'),
        'desc' => $this->l('Add new')
        );

        $helper->toolbar_btn['back'] = array(
        'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
        'desc' => $this->l('back To List')
        );

        $helper->title = ' carousel tabs in "'.$layoutsectionlist_title.'" layout section';
        $helper->table = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        return $helper->generateList($this->getCarouselListContent((int)Configuration::get('PS_LANG_DEFAULT'), $id_layoutsectionlist),
        $this->fields_list);
    }

    public function ImageList($id_layoutsectionlist, $layoutsectionlist_title)
    {

        $this->context->controller->addJqueryUI('ui.sortable');
        $this->context->controller->addJquery();
        $this->context->controller->addJS($this->_path.'js/admin.js');
        $slides = $this->getImageListContent((int)Configuration::get('PS_LANG_DEFAULT'), $id_layoutsectionlist);
        $new = AdminController::$currentIndex.'&configure='.$this->name.'&addimage'.$this->name.'&id_layoutsectionlist='.$id_layoutsectionlist.'&token='.Tools::getAdminTokenLite('AdminModules');

        $back = AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules');

        foreach ($slides as $key => $slide) $slides[$key]['status'] = $this->displayStatus($slide['id_layout_imagelist'],
            $slide['active_image']);

        $this->context->smarty->assign(
        array(
        'link' => $this->context->link,
        'slides' => $slides,
        'theme_url' => AdminController::$currentIndex.'&configure='.$this->name.'&ajax=1&token='.Tools::getAdminTokenLite('AdminModules'),
        //'theme_url' => $this->context->link->getAdminLink('AdminLayoutdesigner'),
        'image_baseurl' => $this->_path.'views/img/',
        'new' => $new,
        'back' => $back,
        'layoutsectionlist_id' => $id_layoutsectionlist,
        'layoutsectionlist_title' => $layoutsectionlist_title,
        )
        );

        return $this->display(__FILE__, 'list.tpl');
    }

    protected function TextList($id_layoutsectionlist, $layoutsectionlist_title)
    {
        $this->fields_list = array(
        'id_layout_textarealist' => array(
        'title' => $this->l('TAB ID'),
        'width' => 120,
        'type' => 'text',
        'search' => false,
        'orderby' => false
        ),
        'text' => array(
        'title' => $this->l('Text'),
        'width' => 140,
        'type' => 'text',
        'search' => false,
        'orderby' => false
        ),
        );

        if (Shop::isFeatureActive())
                $this->fields_list['id_shop'] = array(
            'title' => $this->l('ID Shop'),
            'align' => 'center',
            'width' => 25,
            'type' => 'int');

        $helper = new HelperList();
        $helper->shopLinkType = '';
        $helper->simple_header = false;
        $helper->identifier = 'id_layout_textarealist';
        $helper->table = 'textarealist';
        $helper->actions = array(
        'delete',
        'edit');
        $helper->show_toolbar = true;
        $helper->listTotal;
        $helper->imageType = 'jpg';
        $helper->toolbar_btn['new'] = array(
        'href' => AdminController::$currentIndex.'&configure='.$this->name.'&addtext'.$this->name.'&id_layoutsectionlist='.$id_layoutsectionlist.'&title='.$layoutsectionlist_title.'&token='.Tools::getAdminTokenLite('AdminModules'),
        'desc' => $this->l('Add new')
        );

        $helper->toolbar_btn['back'] = array(
        'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
        'desc' => $this->l('back To List')
        );

        $helper->title = ' carousel tabs in "'.$layoutsectionlist_title.'" layout section';
        $helper->table = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
        return $helper->generateList($this->getTextareaContent((int)Configuration::get('PS_LANG_DEFAULT'), $id_layoutsectionlist),
        $this->fields_list);
    }

    protected function getImageListContent($id_lang, $id_layoutsectionlist)
    {
        return Db::getInstance()->executeS('
			SELECT `id_layout_imagelist`,`image`,`imgwidth`, `url`, `description`, `id_shop`,`id_lang`, `active_image`
			FROM `'._DB_PREFIX_.'layout_imagelist`
			WHERE `id_hometab` = '.(int)$id_layoutsectionlist.' '.Shop::addSqlRestrictionOnLang().'ORDER BY position ASC');
    }

    public function displayStatus($id_slide, $active)
    {
        $title = ((int)$active == 0 ? $this->l('Disabled') : $this->l('Enabled'));
        $icon = ((int)$active == 0 ? 'icon-remove' : 'icon-check');
        $class = ((int)$active == 0 ? 'btn-danger' : 'btn-success');
        $html = '<a class="btn '.$class.'" href="'.AdminController::$currentIndex.
        '&configure='.$this->name.'
				&token='.Tools::getAdminTokenLite('AdminModules').'
				&changeStatus&id_slide='.(int)$id_slide.'" title="'.$title.'"><i class="'.$icon.'"></i> '.$title.'</a>';

        return $html;
    }

    protected function getCarouselListContent($id_lang, $id_layoutsectionlist)
    {

        return Db::getInstance()->executeS('
			SELECT `id_layout_carousellist`,`hometab_title`, `carousel_type`,`carousel_name`, `id_shop`,`id_lang`,`carousellist_details`
			FROM `'._DB_PREFIX_.'layout_carousellist`
			WHERE `id_hometab` = '.(int)$id_layoutsectionlist.' '.Shop::addSqlRestrictionOnLang());
    }

    protected function getCarousel_type($id_layout_carousellist)
    {
        $carousel_types = Db::getInstance()->executeS('
			SELECT `hometab_title`,`carousel_type`,`id_hometab`FROM `'._DB_PREFIX_.'layout_carousellist`
			WHERE `id_layout_carousellist` = '.(int)$id_layout_carousellist.' '.Shop::addSqlRestrictionOnLang());
        foreach ($carousel_types as $value) {
            $carousel_type = $value;
        }
        return $carousel_type;
    }

    protected function getIdHomeTab($id_layoutsectionlist)
    {
        return Db::getInstance()->getValue('
			SELECT `id_hometab`
			FROM `'._DB_PREFIX_.'layout_textarealist`
			WHERE `id_layout_textarealist` = '.(int)$id_layoutsectionlist);
    }

    protected function getTextareaContent($id_lang, $id_layoutsectionlist)
    {
        return Db::getInstance()->executeS('
			SELECT `id_layout_textarealist`, `id_lang`, `id_shop`, `text`, `icon`,`link`
			FROM `'._DB_PREFIX_.'layout_textarealist`
			WHERE `id_hometab` = '.(int)$id_layoutsectionlist.' '.Shop::addSqlRestrictionOnLang());
    }

    public function getHomeTab_title($id_layoutsectionlist)
    {
        $layoutsectionlist_title = Db::getInstance()->getValue('
					SELECT layoutsectionlist_title
					FROM '._DB_PREFIX_.'layoutsectionlist
					WHERE  id_layoutsectionlist = '.(int)$id_layoutsectionlist);
        return $layoutsectionlist_title;
    }

    public function getHomeTab_type($id_layoutsectionlist)
    {
        $hometab_type = Db::getInstance()->getValue('
					SELECT hometab_type
					FROM '._DB_PREFIX_.'layoutsectionlist
					WHERE  id_layoutsectionlist = '.(int)$id_layoutsectionlist);
        return $hometab_type;
    }

    public function renderForm()
    {
        $fields_form = array(
        'form' => array(
        'legend' => array(
        'title' => $this->l('CREATE A LAYOUT SECTION'),
        'icon' => 'icon-cogs'
        ),
        'input' => array(
        array(
        'type' => 'select',
        'label' => $this->l('SELECT LAYOUT SECTION TYPE:'),
        'name' => 'items_set',
        'required' => false,
        'default_value' => 'col-md-12',
        'desc' => $this->l('Section types are explained below:').'<div class="clearfix"><ol><li>Product Carousel: Create Category / Featured / Best Seller / New Products multi-tab slider spanning multiple page columns</li><li>Image: Add a sequence of Image links Horizontally / Vertically / spanning multiple page columns</li><li>Text Area: This feature implementation is in Progress. Please DO NOT SELECT</li></ol></div>',
        'options' => array(
        'query' => array(
        array(
        'set_id' => 'carousel_form',
        'set_name' => $this->l('Products Carousel')),
        array(
        'set_id' => 'image_form',
        'set_name' => $this->l('Image Banner')),
        array(
        'set_id' => 'textarea_form',
        'set_name' => $this->l('Text block')),
        ),
        'id' => 'set_id',
        'name' => 'set_name'
        )
        ),
        ),
        )
        );


        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
            : 0;
        $this->fields_form = array(
        );
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'save_carousel';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
        .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&type=Text';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
        'fields_value' => $this->getConfigFieldsValues(),
        'languages' => $this->context->controller->getLanguages(),
        'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array(
        $fields_form));
    }

    public function color_renderForm()
    {
        $fields_form = array(
        'form' => array(
        'legend' => array(
        'title' => $this->l('CREATE A LAYOUT SECTION'),
        'icon' => 'icon-cogs'
        ),
        'input' => array(
        array(
        'type' => 'color',
        'label' => $this->l('Carsoul Tab Background Color'),
        'name' => 'carsoul_tab_color',
        'desc' => $this->l('Products added to the cart will be highlighted with this color'),
        ),
        array(
        'type' => 'color',
        'label' => $this->l('Carsoul Tab Hover Color'),
        'name' => 'carsoul_tabactive_color',
        'desc' => $this->l('Products added to the cart will be highlighted with this color'),
        ),
        array(
        'type' => 'color',
        'label' => $this->l('Textarea Background Color'),
        'name' => 'text_background_color',
        'desc' => $this->l('Products added to the cart will be highlighted with this color'),
        ),
        array(
        'type' => 'color',
        'label' => $this->l('Highlight Color: Product Box'),
        'name' => 'text_color',
        'desc' => $this->l('Products added to the cart will be highlighted with this color'),
        ),
        array(
        'type' => 'color',
        'label' => $this->l('Icon Backgroud Color'),
        'name' => 'icon_backgroud_color',
        'desc' => $this->l('Products added to the cart will be highlighted with this color'),
        ),
        array(
        'type' => 'color',
        'label' => $this->l('LAYOUT Border Color'),
        'name' => 'layout_border_color',
        'desc' => $this->l('Products added to the cart will be highlighted with this color'),
        ),
        /* array(
          'type' => 'color',
          'label' => $this->l('Icon Backgroud Color'),
          'name' => 'icon_backgroud_color',
          'desc' => $this->l('Products added to the cart will be highlighted with this color'),
          ), */
        ),
        'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'button pull-right'
        )
        )
        );


        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
            : 0;
        $this->fields_form = array(
        );
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'save_color';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
        .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&type=Text';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
        'fields_value' => $this->getConfigFieldsValues(),
        'languages' => $this->context->controller->getLanguages(),
        'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array(
        $fields_form));
    }

    public function carousel_form()
    {
        $fields_form = array(
        'form' => array(
        'legend' => array(
        'title' => $this->l('Save Layout Section Details (Products Carousels)'),
        'icon' => 'icon-cogs'
        ),
        'input' => array(
        array(
        'label' => $this->l('Layout Section Title*:'),
        'type' => 'text',
        'name' => 'tab_title',),
        array(
        'type' => 'select',
        'label' => $this->l('Layout Section Width:'),
        'name' => 'items_set',
        'required' => false,
        'desc' => '<span id="layout_section_img_span"><img id="layout_section_img" itemprop="image" src="'.__PS_BASE_URI__.'modules/layoutdesigner/views/img/homepage_with_img.png"></span>',
        'default_value' => 'col-md-12',
        'options' => array(
        'query' => array(
        array(
        'set_id' => 'col-md-12',
        'set_name' => $this->l('100% of home page width')),
        array(
        'set_id' => 'col-md-9',
        'set_name' => $this->l('75% of home page width')),
        array(
        'set_id' => 'col-md-6',
        'set_name' => $this->l('50% of home page width')),
        array(
        'set_id' => 'col-md-3',
        'set_name' => $this->l('25% of home page width')),
        ),
        'id' => 'set_id',
        'name' => 'set_name'
        )
        ),
        array(
        'label' => $this->l('Carousel Item Width:'),
        'type' => 'text',
        'name' => 'slide_width',
        'class' => 'fixed-width-xs',
        'desc' => $this->l('Width for a single item in a Carousel. Default value: 280'),
        ),
        array(
        'label' => $this->l('Number of items in a Carousel:'),
        'type' => 'text',
        'name' => 'items_number',
        'class' => 'fixed-width-xs',
        'desc' => $this->l('Number of items retrived  from Database for the carsouel (Default value: 8)'),
        ),
        array(
        'label' => $this->l('Number of items in view port of a Carousel:'),
        'type' => 'text',
        'name' => 'limit_cols',
        'class' => 'fixed-width-xs',
        'desc' => $this->l('Number of  items in carousel displayed to user in visible area'),
        ),
        array(
        'type' => 'switch',
        'label' => $this->l('Pagination Icon:'),
        'name' => 'slide_pager',
        'is_bool' => true,
        'values' => array(
        array(
        'id' => 'active_on',
        'value' => 1,
        'label' => $this->l('Yes')
        ),
        array(
        'id' => 'active_off',
        'value' => 0,
        'label' => $this->l('No')
        )
        ),
        ),
        array(
        'type' => 'switch',
        'label' => $this->l('Auto-play:'),
        'name' => 'auto_play',
        'is_bool' => true,
        'values' => array(
        array(
        'id' => 'active_on',
        'value' => 1,
        'label' => $this->l('Yes')
        ),
        array(
        'id' => 'active_off',
        'value' => 0,
        'label' => $this->l('No')
        )
        ),
        ),
        array(
        'type' => 'switch',
        'label' => $this->l('Carousel Enabled:'),
        'name' => 'carousel_enabled',
        'is_bool' => true,
        'values' => array(
        array(
        'id' => 'active_on',
        'value' => 1,
        'label' => $this->l('Yes')
        ),
        array(
        'id' => 'active_off',
        'value' => 0,
        'label' => $this->l('No')
        )
        ),
        ),
        ),
        'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'button pull-right'
        )
        )
        );


        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
            : 0;
        $this->fields_form = array(
        );
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'save_carousel';
        $helper->toolbar_btn = array(
        'save' => array(
        'desc' => $this->l('Save'),
        'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
        '&token='.Tools::getAdminTokenLite('AdminModules'),
        ),
        'back' => array(
        'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
        'desc' => $this->l('Back to list')
        )
        );
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        if (Tools::getValue('id_layoutsectionlist')) {
            $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&type=carousel&id_layoutsectionlist='.Tools::getValue('id_layoutsectionlist');
            $helper->tpl_vars = array(
            'fields_value' => $this->getcarouselFieldsVal(Tools::getValue('id_layoutsectionlist')),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
            );
        } else {
            $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&type=carousel';
            $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
            );
        }
        return $helper->generateForm(array(
        $fields_form));
    }

    public function image_form()
    {
        $fields_form = array(
        'form' => array(
        'legend' => array(
        'title' => $this->l('Create An Image Section'),
        'icon' => 'icon-cogs'
        ),
        'input' => array(
        array(
        'label' => $this->l('Image Section Title*:'),
        'type' => 'text',
        'name' => 'tab_title',
        ),
        array(
        'type' => 'select',
        'label' => $this->l('Image Section Width:'),
        'name' => 'tab_width',
        'required' => false,
        'desc' => '<span id="layout_section_img_span" style="margin-left: 25%;"><img id="layout_section_img" itemprop="image" src="'.__PS_BASE_URI__.'modules/layoutdesigner/views/img/layoutsize.png"></span>',
        'default_value' => 'col-md-12',
        'default_value' => 'col-md-12',
        'options' => array(
        'query' => array(
        array(
        'set_id' => 'col-md-12',
        'set_name' => $this->l('12/12 - 100% of width')),
        array(
        'set_id' => 'col-md-11',
        'set_name' => $this->l('10/12 - 83.33% of width')),
        array(
        'set_id' => 'col-md-10',
        'set_name' => $this->l('9.6/12 - 80.00% of width')),
        array(
        'set_id' => 'col-md-9',
        'set_name' => $this->l('9/12 - 75.00% of width')),
        array(
        'set_id' => 'col-md-8',
        'set_name' => $this->l('8/12 - 66.67% of width')),
        array(
        'set_id' => 'col-md-7',
        'set_name' => $this->l('7.2/12 - 60.00% of width')),
        array(
        'set_id' => 'col-md-6',
        'set_name' => $this->l('6/12 - 50.00% of width')),
        array(
        'set_id' => 'col-md-5',
        'set_name' => $this->l('4.8/12 - 40.00% of width')),
        array(
        'set_id' => 'col-md-4',
        'set_name' => $this->l('4/12 - 33.33% of width')),
        array(
        'set_id' => 'col-md-3',
        'set_name' => $this->l('3/12 - 25.00% of width')),
        array(
        'set_id' => 'col-md-2',
        'set_name' => $this->l('2/12 - 20.00% of width')),
        array(
        'set_id' => 'col-md-1',
        'set_name' => $this->l('1/12 - 8.33% of width')),
        ),
        'id' => 'set_id',
        'name' => 'set_name'
        )
        ),
        array(
        'type' => 'select',
        'label' => $this->l('Image Section Position:'),
        'name' => 'image_position',
        'required' => false,
        'desc' => '<span id="layout_section_img_span"><img id="layout_section_img" itemprop="image" src="'.__PS_BASE_URI__.'modules/layoutdesigner/views/img/ImagePosition.png"></span>',
        'default_value' => 'Horizontal',
        'options' => array(
        'query' => array(
        array(
        'set_id' => 'horizontal',
        'set_name' => $this->l('Horizontal')),
        array(
        'set_id' => 'vertical',
        'set_name' => $this->l('Vertical')),
        ),
        'id' => 'set_id',
        'name' => 'set_name'
        )
        ),
        array(
        'type' => 'switch',
        'label' => $this->l('Enabled:'),
        'name' => 'image_enabled',
        'is_bool' => true,
        'values' => array(
        array(
        'id' => 'active_on',
        'value' => 1,
        'label' => $this->l('Yes')
        ),
        array(
        'id' => 'active_off',
        'value' => 0,
        'label' => $this->l('No')
        )
        ),
        ),
        ),
        'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'button pull-right'
        )
        )
        );


        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
            : 0;
        $this->fields_form = array(
        );
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'save_carousel';
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        if (Tools::getValue('id_layoutsectionlist')) {
            $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&type=image&id_layoutsectionlist='.Tools::getValue('id_layoutsectionlist');
            $helper->tpl_vars = array(
            'fields_value' => $this->getimageFieldsVal(Tools::getValue('id_layoutsectionlist')),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
            );
        } else {
            $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&type=image';
            $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
            );
        }

        return $helper->generateForm(array(
        $fields_form));
    }

    public function textarea_form()
    {
        $fields_form = array(
        'form' => array(
        'legend' => array(
        'title' => $this->l('Create Home Page Textarea'),
        'icon' => 'icon-cogs'
        ),
        'input' => array(
        array(
        'label' => $this->l('Textarea Title*:'),
        'type' => 'text',
        'name' => 'tab_title',
        ),
        array(
        'type' => 'select',
        'label' => $this->l('Textarea Width:'),
        'name' => 'textarea_width',
        'required' => false,
        'default_value' => 'col-md-12',
        'options' => array(
        'query' => array(
        array(
        'set_id' => 'col-md-12',
        'set_name' => $this->l('100% of home page width')),
        array(
        'set_id' => 'col-md-9',
        'set_name' => $this->l('75% of home page width')),
        array(
        'set_id' => 'col-md-6',
        'set_name' => $this->l('50% of home page width')),
        array(
        'set_id' => 'col-md-3',
        'set_name' => $this->l('25% of home page width')),
        ),
        'id' => 'set_id',
        'name' => 'set_name'
        )
        ),
        array(
        'type' => 'switch',
        'label' => $this->l('Enable Textarea:'),
        'name' => 'auto_play',
        'is_bool' => true,
        'values' => array(
        array(
        'id' => 'active_on',
        'value' => 1,
        'label' => $this->l('Yes')
        ),
        array(
        'id' => 'active_off',
        'value' => 0,
        'label' => $this->l('No')
        )
        ),
        ),
        ),
        'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'button pull-right'
        )
        )
        );


        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
            : 0;
        $this->fields_form = array(
        );
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'save_carousel';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
        .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
        'fields_value' => $this->getConfigFieldsValues(),
        'languages' => $this->context->controller->getLanguages(),
        'id_language' => $this->context->language->id
        );

        if (Tools::getValue('id_layoutsectionlist')) {
            $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&type=text&id_layoutsectionlist='.Tools::getValue('id_layoutsectionlist');
            $helper->tpl_vars = array(
            'fields_value' => $this->gettextFieldsVal(Tools::getValue('id_layoutsectionlist')),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
            );
        } else {
            $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&type=text';
            $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
            );
        }
        return $helper->generateForm(array(
        $fields_form));
    }

    public function carouselTypeForm()
    {
        $fields_form = array(
        'form' => array(
        'legend' => array(
        'title' => $this->l('CREATE A CAROUSEL SECTION'),
        'icon' => 'icon-cogs'
        ),
        'input' => array(
        array(
        'type' => 'select',
        'label' => $this->l('SELECT PRODUCTS CAROUSEL TYPE:'),
        'name' => 'carousel_type',
        'required' => false,
        'default_value' => 'col-md-12',
        'desc' => $this->l('Section types are explained below:').'<div class="clearfix"><ol><li>Category Products Carousel: Display products of a selected Category</li><li>Featured Products Carousel: Display Featured / Best Seller / New Products</li><li>Custom Products Carousel: Display selected products list</li></ol></div>',
        'options' => array(
        'query' => array(
        array(
        'set_id' => 'Category_carousel',
        'set_name' => $this->l('Category Products Carousel')),
        array(
        'set_id' => 'Specialcat_carousel',
        'set_name' => $this->l('Featured Products Carousel')),
        array(
        'set_id' => 'Products_carousel',
        'set_name' => $this->l('Custom Products Carousel')),
        ),
        'id' => 'set_id',
        'name' => 'set_name'
        )
        ),
        ),
        )
        );


        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
            : 0;
        $this->fields_form = array(
        );
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'save_carousel';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
        .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&type=Text';
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->tpl_vars = array(
        'fields_value' => $this->getConfigFieldsValues(),
        'languages' => $this->context->controller->getLanguages(),
        'id_language' => $this->context->language->id
        );

        return $helper->generateForm(array(
        $fields_form));
    }

    public function addcarouseltab($id_layout_carousellist = null)
    {
        $fields_form = array(
        'form' => array(
        'legend' => array(
        'title' => $this->l('ADD NEW CAROUSEL TAB  IN "'.Tools::getValue('title').'" LAYOUT SECTION'),
        'icon' => 'icon-cogs'
        ),
        'input' => array(
        array(
        'type' => 'text',
        'label' => $this->l('Carousel Tab Title'),
        'name' => 'hometab_title',
        'required' => true,
        ),
        array(
        'type' => 'select',
        'label' => $this->l('Product List Category:'),
        'name' => 'item_category',
        'required' => true,
        'options' => array(
        'optiongroup' => array(
        'query' => $this->CategoryOptions(),
        'label' => 'name',
        'name' => 'name',
        'id' => 'id'),
        'options' => array(
        'query' => 'query',
        'id' => 'id',
        'name' => 'name')),
        ),
        /* array(
          'type'  => 'categories',
          'label' => $this->l('Parent category'),
          'name'  => 'item_category',
          'tree'  => array(
          'id'=> 'categories-tree',
          'selected_categories' => $selected_categories,
          'disabled_categories' => (!Tools::isSubmit('add'.$this->table) && !Tools::isSubmit('submitAdd'.$this->table)) ? array($this->_category->id) : null,
          'root_category' => $this->context->shop->getCategory(),
          )
          ), */
        ),
        'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'button pull-right',
        'desc' => $this->l('* Mandatory field'),
        )
        )
        );

        $helper = new HelperForm();
        $helper->show_toolbar = true;
        $helper->table = $this->table;
        //$helper->show_cancel_button = true;
        $back = Tools::safeOutput(Tools::getValue('back', ''));
        if (empty($back)) $back = AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules');
        if (!Validate::isCleanHtml($back)) die(Tools::displayError());
        $helper->back_url = $back;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
            : 0;
        $this->fields_form = array(
        );
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'addcarouselitem';
        /* $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
          .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&id_layoutsectionlist='.Tools::getValue('id_layoutsectionlist').'&type=Category_carousel'; */
        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->toolbar_btn = array(
        'save' => array(
        'desc' => $this->l('Save'),
        'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
        '&token='.Tools::getAdminTokenLite('AdminModules'),
        ),
        'back' => array(
        'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
        'desc' => $this->l('Back to list')
        )
        );

        if ($id_layout_carousellist) {
            $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&id_layout_carousellist='.Tools::getValue('id_layout_carousellist').'&type=Category_carousel&id_layoutsectionlist='.Tools::getValue('id_layoutsectionlist');
            $helper->tpl_vars = array(
            'fields_value' => $this->getCarouselFieldsValues($id_layout_carousellist),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
            );
        } else {
            $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&id_layoutsectionlist='.Tools::getValue('id_layoutsectionlist').'&type=Category_carousel';
            $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
            );
        }

        return $helper->generateForm(array(
        $fields_form));
    }

    public function addSpecialcat_carousel($id_layout_carousellist = null, $id_layoutsectionlist = null, $layoutsectionlist_title = null)
    {

        if ($layoutsectionlist_title) {
            $title = $layoutsectionlist_title;
        } else {
            $title = Tools::getValue('title');
        }

        $fields_form = array(
        'form' => array(
        'legend' => array(
        'title' => $this->l('ADD NEW CAROUSEL TAB  IN "'.$title.'" LAYOUT SECTION'),
        'icon' => 'icon-cogs'
        ),
        'input' => array(
        array(
        'type' => 'text',
        'label' => $this->l('Carousel Tab Title'),
        'name' => 'hometab_title',
        'required' => true,
        ),
        array(
        'type' => 'select',
        'label' => $this->l('Product List Category:'),
        'name' => 'item_category',
        'required' => true,
        'options' => array(
        'optiongroup' => array(
        'query' => $this->SpecialCategoryOptions(),
        'label' => 'name',
        'name' => 'name',
        'id' => 'id'),
        'options' => array(
        'query' => 'query',
        'id' => 'id',
        'name' => 'name')),
        ),
        /* array(
          'type'  => 'categories',
          'label' => $this->l('Parent category'),
          'name'  => 'item_category',
          'tree'  => array(
          'id'=> 'categories-tree',
          'selected_categories' => $selected_categories,
          'disabled_categories' => (!Tools::isSubmit('add'.$this->table) && !Tools::isSubmit('submitAdd'.$this->table)) ? array($this->_category->id) : null,
          'root_category' => $this->context->shop->getCategory(),
          )
          ), */
        ),
        'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'button pull-right',
        'desc' => $this->l('* Mandatory field'),
        )
        )
        );

        $helper = new HelperForm();
        $helper->show_toolbar = true;
        $helper->table = $this->table;
        //$helper->show_cancel_button = true;
        $back = Tools::safeOutput(Tools::getValue('back', ''));
        if (empty($back)) $back = AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules');
        if (!Validate::isCleanHtml($back)) die(Tools::displayError());
        $helper->back_url = $back;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
            : 0;
        $this->fields_form = array(
        );
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'addcarouselitem';

        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->toolbar_btn = array(
        'save' => array(
        'desc' => $this->l('Save'),
        'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
        '&token='.Tools::getAdminTokenLite('AdminModules'),
        ),
        'back' => array(
        'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
        'desc' => $this->l('Back to list')
        )
        );

        if ($id_layout_carousellist) {
            $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&id_layout_carousellist='.$id_layout_carousellist.'&type=Specialcat_carousel&id_layoutsectionlist='.$id_layoutsectionlist;

            $helper->tpl_vars = array(
            'fields_value' => $this->getCarouselFieldsValues($id_layout_carousellist),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
            );
        } else {
            $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&id_layoutsectionlist='.Tools::getValue('id_layoutsectionlist').'&type=Specialcat_carousel';
            $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
            );
        }

        return $helper->generateForm(array(
        $fields_form));
    }

    public function initFormPack($id_layout_carousellist = null, $hometab_title = null, $id_layoutsectionlist = null)
    {
        $data = $this->context->smarty->createTemplate(_PS_MODULE_DIR_.'layoutdesigner/views/templates/admin/pack.tpl',
        $this->context->smarty);

        if (Tools::getValue('namePackItems')) {
            $input_pack_items = Tools::getValue('inputPackItems');
            $input_namepack_items = Tools::getValue('namePackItems');
            $pack_items = $this->getPackItems();
        } else {
            $packGroupId = $id_layout_carousellist;
            $pack_items = $this->getPackItems($packGroupId);
            $input_namepack_items = '';
            $input_pack_items = '';
            foreach ($pack_items as $pack_item) {
                $input_pack_items .= $pack_item['pack_quantity'].'x'.$pack_item['id'].'x'.'0'.'-';
                $input_namepack_items .= $pack_item['pack_quantity'].' x '.$pack_item['name'].'¤';
            }
        }
        if (!empty($id_layout_carousellist)) {
            $data->assign(array(
            'input_pack_items' => $input_pack_items,
            'input_namepack_items' => $input_namepack_items,
            'pack_items' => $pack_items,
            'hometab_title' => $hometab_title,
            'product_type' => (int)Tools::getValue('type_product'),
            'id_lang_default' => Configuration::get('PS_LANG_DEFAULT'),
            'tabs_preloaded' => array(
            'Pack' => 7,),
            'form_action' => $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&id_layoutsectionlist='.$id_layoutsectionlist.'&type=Products_carousel&id_layout_carousellist='.$id_layout_carousellist.'&token='.Tools::getAdminTokenLite('AdminModules'),
            ));
            //ppp(Tools::getValue('id_layout_carousellist'));
            //die();
        } else {
            $data->assign(array(
            'input_pack_items' => $input_pack_items,
            'input_namepack_items' => $input_namepack_items,
            'pack_items' => $pack_items,
            'hometab_title' => $hometab_title,
            'product_type' => (int)Tools::getValue('type_product'),
            'id_lang_default' => Configuration::get('PS_LANG_DEFAULT'),
            'tabs_preloaded' => array(
            'Pack' => 7,),
            'form_action' => $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&id_layoutsectionlist='.Tools::getValue('id_layoutsectionlist').'&type=Products_carousel&token='.Tools::getAdminTokenLite('AdminModules'),
            ));
        }


        return $data->fetch();
    }

    /**
     * Get an array of pack items for display from the product object if specified, else from POST/GET values
     *
     * @param Product $product
     * @return array of pack items
     */
    public function getPackItems($packGroupId = null)
    {
        $pack_items = array(
        );
        if (!$packGroupId) {
            $names_input = Tools::getValue('namePackItems');
            $ids_input = Tools::getValue('inputPackItems');
            if (!$names_input || !$ids_input) return array(
                );

            // ids is an array of string with format : QTYxID
            $ids = array_unique(explode('-', $ids_input));
            $names = array_unique(explode('¤', $names_input));

            if (!empty($ids)) {
                $length = count($ids);
                for ($i = 0; $i < $length; $i++) {
                    if (!empty($ids[$i]) && !empty($names[$i])) {
                        list($pack_items[$i]['pack_quantity'], $pack_items[$i]['id']) = explode('x', $ids[$i]);
                        $exploded_name = explode('x', $names[$i]);
                        $pack_items[$i]['name'] = $exploded_name[1];
                    }
                }
            }
        } else {
            //$prod_zip_combs = getItems($packGroupId, $this->context->shop->id);
            $prod_zip_combs = $this->getProductsValues($packGroupId);
            //ppp($prod_zip_combs);
            //die();
            $pack_quantity = 1;
            $i = 0;
            foreach ($prod_zip_combs['Product_ids'] as $pack_item) {
                //ppp($pack_item['Product_ids']);

                $pack_items[$i]['id'] = $pack_item;
                //$pack_items[$i]['pack_quantity'] = 5;
                //$pack_items[$i]['name']	= 'demo1';
                //$pack_items[$i]['reference'] = 'demo reference';
                $pack_items[$i]['id_product_attribute'] = 0;
                $cover = Product::getCover($pack_item);
                $pack_items[$i]['image'] = Context::getContext()->link->getImageLink('', $cover['id_image']);
                $i++;
            }
            $j = 0;
            foreach ($prod_zip_combs['product_pack_quantity'] as $pack_item) {
                $pack_items[$j]['pack_quantity'] = $pack_item;
                $j++;
            }
            $k = 0;
            foreach ($prod_zip_combs['product_name'] as $pack_item) {
                $pack_items[$k]['name'] = $pack_item;
                $pack_items[$k]['reference'] = $pack_item;
                $k++;
            }
        }
        //die();
        return $pack_items;
    }

    public function getProductItems($packGroupId = null)
    {
        $pack_items = array(
        );
        $pack_ids = array(
        );
        if (!$packGroupId) {
            $names_input = Tools::getValue('namePackItems');
            $ids_input = Tools::getValue('inputPackItems');
            if (!$names_input || !$ids_input) return array(
                );

            // ids is an array of string with format : QTYxID
            $ids = array_unique(explode('-', $ids_input));
            $names = array_unique(explode('¤', $names_input));

            if (!empty($ids)) {
                $length = count($ids);
                //ppp($ids);
                //die();
                for ($i = 0; $i < $length; $i++) {
                    if (!empty($ids[$i]) && !empty($names[$i])) {
                        list($pack_items[$i]['pack_quantity'], $pack_items[$i]['id']) = explode('x', $ids[$i]);
                        $exploded_id = explode('x', $ids[$i]);
                        $pack_ids[$i]['ids'] = $exploded_id[1];
                        $exploded_name = explode('x', $names[$i]);
                        $pack_ids[$i]['name'] = $exploded_name[1];
                        $pack_ids[$i]['name'] = $exploded_name[1];
                    }
                }
                $i = 0;
                foreach ($pack_ids as $subArray) {
                    $pack_items[$i]['ids'] = $subArray['ids'];
                    $pack_items[$i]['name'] = $subArray['name'];
                    $pack_items[$i]['name'] = $subArray['name'];
                    $i++;
                }
            }
        }

        return $pack_items;
    }

    public function processAjaxProductsList()
    {
        $query = Tools::getValue('q', false);
        if (!$query || $query == '' || Tools::strlen($query) < 1) die();

        /*
         * In the SQL request the "q" param is used entirely to match result in database.
         * In this way if string:"(ref : #ref_pattern#)" is displayed on the return list,
         * they are no return values just because string:"(ref : #ref_pattern#)"
         * is not write in the name field of the product.
         * So the ref pattern will be cut for the search request.
         */

        if ($pos = strpos($query, ' (ref:')) $query = Tools::substr($query, 0, $pos);

        $excludeIds = Tools::getValue('excludeIds', false);

        if ($excludeIds && $excludeIds != 'NaN') $excludeIds = implode(',', array_map('intval', explode(',', $excludeIds)));
        else $excludeIds = '';

        // Excluding downloadable products from packs because download from pack is not supported
        $excludeVirtuals = (bool)Tools::getValue('excludeVirtuals', false);
        $exclude_packs = (bool)Tools::getValue('exclude_packs', false);

        $sql = 'SELECT p.`id_product`, pl.`link_rewrite`, p.`reference`, pl.`name`, MAX(image_shop.`id_image`) id_image, il.`legend`, p.`cache_default_attribute`
		FROM `'._DB_PREFIX_.'product` p
		'.Shop::addSqlAssociation('product',
        'p').'
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.id_product = p.id_product AND pl.id_lang = '.(int)Context::getContext()->language->id.Shop::addSqlRestrictionOnLang('pl').')
		LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product`)'.
        Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
		LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)Context::getContext()->language->id.')
		WHERE (pl.name LIKE \'%'.pSQL($query).'%\' OR p.reference LIKE \'%'.pSQL($query).'%\')'.
        (!empty($excludeIds) ? ' AND p.id_product NOT IN ('.$excludeIds.') ' : ' ').
        ($excludeVirtuals ? 'AND p.id_product NOT IN (SELECT pd.id_product FROM `'._DB_PREFIX_.'product_download` pd WHERE (pd.id_product = p.id_product))'
            : '').
        ($exclude_packs ? 'AND (p.cache_is_pack IS NULL OR p.cache_is_pack = 0)' : '').
        ' GROUP BY p.id_product';

        $items = Db::getInstance()->executeS($sql);

        if ($items && ($excludeIds || strpos($_SERVER['HTTP_REFERER'], 'AdminScenes') !== false))
                foreach ($items as $item) echo trim($item['name']).(!empty($item['reference']) ? ' (ref: '.$item['reference'].')' : '').'|'.(int)($item['id_product'])."\n";
        elseif ($items) {
            // packs
            $results = array(
            );
            foreach ($items as $item) {

                $product = array(
                'id' => (int)($item['id_product']),
                'name' => $item['name'],
                'ref' => (!empty($item['reference']) ? $item['reference'] : ''),
                'image' => str_replace('http://', Tools::getShopProtocol(),
                Context::getContext()->link->getImageLink($item['link_rewrite'], $item['id_image'])),
                );
                array_push($results, $product);
//}
            }
            $results = array_values($results);
            echo Tools::jsonEncode($results);
        }
        else echo Tools::jsonEncode(new stdClass);
        die();
    }

    /**
     *  New Work Process
     */
    public function imageAddForm()
    {
        if (Tools::getValue('edit_id_image')) $fields_value = $this->getImageFieldById(Tools::getValue('edit_id_image'));

        $fields_form = array(
        'form' => array(
        'legend' => array(
        'title' => $this->l('ADD NEW IMAGE'),
        'icon' => 'icon-cogs'
        ),
        'input' => array(
        array(
        'type' => 'file',
        'label' => $this->l('Select image file'),
        'name' => 'image',
        'display_image' => true,
        'desc' => $this->l(sprintf('Maximum image size: %s.', ini_get('upload_max_filesize'))).isset($fields_value['image']) && $fields_value['image'] !=
        '' ? '<div><img src="'.$this->_path.'views/img/'.$fields_value['image'].'" style="max-width: 200px;max-height: 250px"></div>' : '',
        ),
        array(
        'type' => 'select',
        'label' => $this->l('Image Width:'),
        'name' => 'imgwidth',
        'required' => false,
        'desc' => 'Width for a single item in a Image Banner. Default value: col-md-12',
        'default_value' => 'col-md-12',
        'default_value' => 'col-md-12',
        'options' => array(
        'query' => array(
        array(
        'set_id' => 'col-md-12',
        'set_name' => $this->l('12/12 - 100% of width')),
        array(
        'set_id' => 'col-md-11',
        'set_name' => $this->l('10/12 - 83.33% of width')),
        array(
        'set_id' => 'col-md-10',
        'set_name' => $this->l('9.6/12 - 80.00% of width')),
        array(
        'set_id' => 'col-md-9',
        'set_name' => $this->l('9/12 - 75.00% of width')),
        array(
        'set_id' => 'col-md-8',
        'set_name' => $this->l('8/12 - 66.67% of width')),
        array(
        'set_id' => 'col-md-7',
        'set_name' => $this->l('7.2/12 - 60.00% of width')),
        array(
        'set_id' => 'col-md-6',
        'set_name' => $this->l('6/12 - 50.00% of width')),
        array(
        'set_id' => 'col-md-5',
        'set_name' => $this->l('4.8/12 - 40.00% of width')),
        array(
        'set_id' => 'col-md-4',
        'set_name' => $this->l('4/12 - 33.33% of width')),
        array(
        'set_id' => 'col-md-3',
        'set_name' => $this->l('3/12 - 25.00% of width')),
        array(
        'set_id' => 'col-md-2',
        'set_name' => $this->l('2/12 - 20.00% of width')),
        array(
        'set_id' => 'col-md-1',
        'set_name' => $this->l('1/12 - 8.33% of width')),
        ),
        'id' => 'set_id',
        'name' => 'set_name'
        )
        ),
        array(
        'type' => 'text',
        'label' => $this->l('URL'),
        'name' => 'url',
        //'lang' => true,
        ),
        array(
        'type' => 'textarea',
        'label' => $this->l('Description'),
        'name' => 'description',
        'autoload_rte' => true,
        //'lang' => true,
        ),
        ),
        'submit' => array(
        'title' => $this->l('Save'),
        )
        ),
        );


        $helper = new HelperForm();
        $helper->show_toolbar = false;
        $helper->table = $this->table;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
            : 0;
        $this->fields_form = array(
        );
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'submitimage';
        $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&id_layoutsectionlist='.Tools::getValue('id_layoutsectionlist');
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $language = new Language((int)Configuration::get('PS_LANG_DEFAULT'));

        if (Tools::getValue('edit_id_image')) {

            $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&id_layoutsectionlist='.Tools::getValue('id_layoutsectionlist').'&edit_id_image='.Tools::getValue('edit_id_image');
            $helper->tpl_vars = array(
            'base_url' => $this->context->shop->getBaseURL(),
            'language' => array(
            'id_lang' => $language->id,
            'iso_code' => $language->iso_code
            ),
            'fields_value' => $fields_value,
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'image_baseurl' => $this->_path.'views/img/'
            );
            //ppp($helper->tpl_vars);
            //die();		
        } else {

            $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&id_layoutsectionlist='.Tools::getValue('id_layoutsectionlist');
            $helper->tpl_vars = array(
            'base_url' => $this->context->shop->getBaseURL(),
            'language' => array(
            'id_lang' => $language->id,
            'iso_code' => $language->iso_code
            ),
            'fields_value' => $this->getImageFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id,
            'image_baseurl' => $this->_path.'views/img/'
            );
        }
        return $helper->generateForm(array(
        $fields_form));
    }

    public function textAddForm($id_layoutsectionlist = false)
    {
        $fields_form = array(
        'form' => array(
        'legend' => array(
        'title' => $this->l('ADD NEW TEXT AREA  IN "'.Tools::getValue('title').'" LAYOUT SECTION'),
        'icon' => 'icon-cogs'
        ),
        'input' => array(
        array(
        'type' => 'text',
        'label' => $this->l('Text'),
        //'lang' => true,
        'name' => 'text',
        'cols' => 40,
        'rows' => 10,
        'desc' => $this->l('Enter Title'),
        ),
        array(
        'type' => 'textarea',
        'label' => $this->l('Icon'),
        //'lang' => true,
        'name' => 'icon',
        'cols' => 40,
        'rows' => 10,
        'desc' => $this->l('Set Icon Class name Ex: "icon-truck"').'<div><a href="http://fortawesome.github.io/Font-Awesome/3.2.1/icon/truck/" target="_blank">fortawesome/icons</a></div>',
        ),
        array(
        'type' => 'text',
        'label' => $this->l('Link'),
        //'lang' => true,
        'name' => 'link',
        'cols' => 40,
        'rows' => 10,
        'desc' => $this->l('Give Link to open a new page'),
        ),
        ),
        'submit' => array(
        'title' => $this->l('Save'),
        'class' => 'button pull-right',
        'desc' => $this->l('* Mandatory field'),
        )
        )
        );

        $helper = new HelperForm();
        $helper->show_toolbar = true;
        $helper->table = $this->table;
        //$helper->show_cancel_button = true;
        $back = Tools::safeOutput(Tools::getValue('back', ''));
        if (empty($back)) $back = AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules');
        if (!Validate::isCleanHtml($back)) die(Tools::displayError());
        $helper->back_url = $back;
        $lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
        $helper->default_form_language = $lang->id;
        $helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG')
            : 0;
        $this->fields_form = array(
        );
        $helper->module = $this;
        $helper->identifier = $this->identifier;
        $helper->submit_action = 'addtextitem';

        $helper->token = Tools::getAdminTokenLite('AdminModules');

        $helper->toolbar_btn = array(
        'save' => array(
        'desc' => $this->l('Save'),
        'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
        '&token='.Tools::getAdminTokenLite('AdminModules'),
        ),
        'back' => array(
        'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
        'desc' => $this->l('Back to list')
        )
        );

        if (Tools::getValue('id_layout_textarealist')) {
            $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&id_layoutsectionlist='.$id_layoutsectionlist.'&id_layout_textarealist='.Tools::getValue('id_layout_textarealist');
            $helper->tpl_vars = array(
            'fields_value' => $this->getTextarealistFieldsValues(Tools::getValue('id_layout_textarealist')),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
            );
        } else {
            $helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false)
            .'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name.'&id_layoutsectionlist='.Tools::getValue('id_layoutsectionlist');

            $helper->tpl_vars = array(
            'fields_value' => $this->getConfigFieldsValues(),
            'languages' => $this->context->controller->getLanguages(),
            'id_language' => $this->context->language->id
            );
        }

        return $helper->generateForm(array(
        $fields_form));
    }

    /**
     *  Display input values into the form after process
     */
    public function getConfigFieldsValues()
    {
        $layoutdesigner_css_values = unserialize(Configuration::get('layoutdesigner_css_value'));
        return array(
        'slide_width' => Tools::getValue('slide_width', 280),
        'tab_title' => Tools::getValue('tab_title'),
        'textarea_width' => Tools::getValue('textarea_width'),
        'items_number' => Tools::getValue('items_number', 8),
        'limit_cols' => Tools::getValue('limit_cols', 4),
        'scroll_items' => Tools::getValue('scroll_items', 4),
        'auto_play' => Tools::getValue('auto_play', true),
        'carousel_enabled' => Tools::getValue('carousel_enabled', true),
        'slide_pager' => Tools::getValue('slide_pager', true),
        'items_set' => Tools::getValue('items_set'),
        'home_title_1' => Tools::getValue('carousellist_title'),
        'item_category' => Tools::getValue('item_category'),
        'hometab_title' => Tools::getValue('hometab_title'),
        'items_set' => Tools::getValue('set_col_md'),
        'image_enabled' => Tools::getValue('image_enabled', true),
        'image_items' => Tools::getValue('image_items', 4),
        'image_position' => Tools::getValue('image_position', 'Horizontal'),
        'tab_width' => Tools::getValue('tab_width'),
        'products_carousel' => Tools::getValue('products_carousel', false),
        'image_banner' => Tools::getValue('image_banner', false),
        'text_banner' => Tools::getValue('text_banner', false),
        'active_image' => Tools::getValue('active_image', false),
        'text' => Tools::getValue('text'),
        'icon' => Tools::getValue('icon'),
        'link' => Tools::getValue('link'),
        'carsoul_tab_color' => Tools::getValue('carsoul_tab_color', $layoutdesigner_css_values['carsoul_tab_color']),
        'text_background_color' => Tools::getValue('text_background_color', $layoutdesigner_css_values['text_background_color']),
        'text_color' => Tools::getValue('text_color', $layoutdesigner_css_values['text_color']),
        'icon_backgroud_color' => Tools::getValue('icon_backgroud_color', $layoutdesigner_css_values['icon_backgroud_color']),
        'carsoul_tabactive_color' => Tools::getValue('carsoul_tabactive_color', $layoutdesigner_css_values['carsoul_tabactive_color']),
        'layout_border_color' => Tools::getValue('layout_border_color', $layoutdesigner_css_values['layout_border_color']),
        );
    }

    public function getcarouselFieldsVal($id_layoutsectionlist)
    {
        $dbecute = Db::getInstance()->getValue('
					SELECT carousel_details
					FROM '._DB_PREFIX_.'layoutsectionlist
					WHERE  id_layoutsectionlist = '.(int)$id_layoutsectionlist);

        $gethometablists = unserialize($dbecute);
        return array(
        'slide_width' => $gethometablists['slide_width'],
        'tab_title' => $gethometablists['tab_title'],
        'items_number' => $gethometablists['items_number'],
        'limit_cols' => $gethometablists['limit_cols'],
        'auto_play' => $gethometablists['auto_play'] ? '1' : '0',
        'carousel_enabled' => $gethometablists['carousel_enabled'] ? '1' : '0',
        'slide_pager' => $gethometablists['slide_pager'] ? '1' : '0',
        'items_set' => $gethometablists['set_col_md'],
        );
    }

    public function getimageFieldsVal($id_layoutsectionlist)
    {
        $dbecute = Db::getInstance()->getValue('
					SELECT carousel_details
					FROM '._DB_PREFIX_.'layoutsectionlist
					WHERE  id_layoutsectionlist = '.(int)$id_layoutsectionlist);

        $gethometablists = unserialize($dbecute);
        return array(
        'tab_title' => $gethometablists['tab_title'],
        'tab_width' => $gethometablists['set_col_md'],
        'image_items' => $gethometablists['image_items'],
        'image_position' => $gethometablists['image_position'],
        'image_enabled' => $gethometablists['image_enabled'] ? '1' : '0',
        'items_set' => $gethometablists['set_col_md'],
        );
    }

    public function gettextFieldsVal($id_layoutsectionlist)
    {
        $dbecute = Db::getInstance()->getValue('
					SELECT carousel_details
					FROM '._DB_PREFIX_.'layoutsectionlist
					WHERE  id_layoutsectionlist = '.(int)$id_layoutsectionlist);

        $gethometablists = unserialize($dbecute);
        //ppp($gethometablists);
        //die();
        return array(
        'tab_title' => $gethometablists['tab_title'],
        'textarea_width' => $gethometablists['set_col_md'],
        'auto_play' => $gethometablists['auto_play'] ? '1' : '0',
        );
    }

    public function getCarouselFieldsValues($id_layout_carousellist)
    {

        $getCarousellists = Db::getInstance()->getValue('
			SELECT carousellist_details
			FROM `'._DB_PREFIX_.'layout_carousellist`
			WHERE `id_layout_carousellist` = '.(int)$id_layout_carousellist.' '.Shop::addSqlRestrictionOnLang());

        $carousellist_details = unserialize($getCarousellists);
        return array(
        'hometab_title' => $carousellist_details['hometab_title'],
        'item_category' => $carousellist_details['id_category'],
        );
    }

    public function getTextarealistFieldsValues($id_layout_textarealist)
    {
        $getTextarealists = Db::getInstance()->executeS('
			SELECT `text`, `icon`, `link`, `id_hometab`
			FROM `'._DB_PREFIX_.'layout_textarealist`
			WHERE `id_layout_textarealist` = '.(int)$id_layout_textarealist);

        foreach ($getTextarealists as $getTextarealist) $textarealist = $getTextarealist;

        //ppp($textarealist);
        //die();
        return array(
        'text' => $textarealist['text'],
        'icon' => $textarealist['icon'],
        'link' => $textarealist['link']
        );
    }

    public function getProductsValues($id_layout_carousellist)
    {

        $getCarousellists = Db::getInstance()->getValue('
			SELECT carousellist_details
			FROM `'._DB_PREFIX_.'layout_carousellist`
			WHERE `id_layout_carousellist` = '.(int)$id_layout_carousellist.' '.Shop::addSqlRestrictionOnLang());
        return $carousellist_details = unserialize($getCarousellists);
    }

    public function getSpecialcatFieldsValues($id_layout_carousellist)
    {
        $getCarousellists = Db::getInstance()->getValue('
			SELECT carousellist_details
			FROM `'._DB_PREFIX_.'layout_carousellist`
			WHERE `id_layout_carousellist` = '.(int)$id_layout_carousellist.' '.Shop::addSqlRestrictionOnLang());

        $carousellist_details = unserialize($getCarousellists);
        return array(
        'hometab_title' => $carousellist_details['hometab_title'],
        'item_category' => $carousellist_details['id_category'],
        );
    }

    public function getproductById($id_product)
    {
        $context = Context::getContext();
        $id_lang = (int)$this->context->language->id;
        $active = true;
        $front = true;
        if (!in_array($context->controller->controller_type, array(
        'front',
        'modulefront'))) $front = false;

        $id_supplier = (int)Tools::getValue('id_supplier');
        $sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity'.(Combination::isFeatureActive() ? ', MAX(product_attribute_shop.id_product_attribute) id_product_attribute, MAX(product_attribute_shop.minimal_quantity) AS product_attribute_minimal_quantity'
            : '').', pl.`description`, pl.`description_short`, pl.`available_now`,
					pl.`available_later`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, MAX(image_shop.`id_image`) id_image,
					MAX(il.`legend`) as legend, m.`name` AS manufacturer_name, cl.`name` AS category_default,
					DATEDIFF(product_shop.`date_add`, DATE_SUB(NOW(),
					INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT'))
            ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).'
						DAY)) > 0 AS new, product_shop.price AS orderprice
				FROM `'._DB_PREFIX_.'category_product` cp
				LEFT JOIN `'._DB_PREFIX_.'product` p
					ON p.`id_product` = cp.`id_product`
				'.Shop::addSqlAssociation('product',
        'p').
        (Combination::isFeatureActive() ? 'LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa
				ON (p.`id_product` = pa.`id_product`)
				'.Shop::addSqlAssociation('product_attribute',
        'pa', false, 'product_attribute_shop.`default_on` = 1').'
				'.Product::sqlStock('p', 'product_attribute_shop', false,
        $context->shop) : Product::sqlStock('p', 'product', false, Context::getContext()->shop)).'
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl
					ON (product_shop.`id_category_default` = cl.`id_category`
					AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').')
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
					ON (p.`id_product` = pl.`id_product`
					AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').')
				LEFT JOIN `'._DB_PREFIX_.'image` i
					ON (i.`id_product` = p.`id_product`)'.
        Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il
					ON (image_shop.`id_image` = il.`id_image`
					AND il.`id_lang` = '.(int)$id_lang.')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m
					ON m.`id_manufacturer` = p.`id_manufacturer`
				WHERE product_shop.`id_shop` = '.(int)$context->shop->id.'
					AND p.`id_product` = '.(int)$id_product
        .($active ? ' AND product_shop.`active` = 1' : '')
        .($front ? ' AND product_shop.`visibility` IN ("both", "catalog")' : '')
        .($id_supplier ? ' AND p.id_supplier = '.(int)$id_supplier : '')
        .' GROUP BY product_shop.id_product';
        $result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql);
        //ppp($sql);
        //die();
        if (!$result) return array(
            );

        /* Modify SQL result */
        return Product::getProductsProperties($id_lang, $result);
    }

    public function hookHeader($params)
    {
        if (!isset($this->context->controller->php_self) || $this->context->controller->php_self != 'index') {
            return;
        }
        $this->context->controller->addCSS($this->_path.'views/css/style.css');
        $this->context->controller->addCSS($this->_path.'views/css/carousel.css');
        $this->context->controller->addCSS($this->_path.'views/css/tablisting.css');
        $this->context->controller->addCSS(_THEME_CSS_DIR_.'product_list.css');
        $this->context->controller->addJqueryPlugin(array(
        'scrollTo',
        'serialScroll',
        'bxslider'));
        $this->context->controller->addCSS($this->_path.'views/css/home_tab.css');
        $this->context->controller->addJs($this->_path.'views/js/home_tab.js');
        $this->context->controller->addJs($this->_path.'views/js/script.js');
        //$this->context->controller->addJs($this->_path.'views/js/jquery.lazyload.js'); 
        $this->context->controller->addCSS($this->_path.'views/css/infolinks.css');
        $this->context->controller->addCSS($this->_path.'views/css/font-awesome.css');             
    }

    public function hookDisplayHomeTabContent($params)
    {
        //return $this->hookDisplayHomeTabContent($params);
    }

    /**
     * Homepage content hook (Technical name: displayHome)
     */
    public function hookDisplayHome($params)
    {
        if (!$this->isCached('hometab_carousel.tpl', $this->getCacheId('1')))
        {

        $config = Configuration::get('carousellist_items_set');
        $id_lang = (int)Context::getContext()->language->id;
        $id_shop = (int)Context::getContext()->shop->id;
        $ids_categorys = array(
        );
        $hometabarray = array(
        );
        $home_tabs = array(
        );
        $tab_carousel_details = array(
        );
        $tab_image_details = array(
        );
        $tab_text_details = array(
        );
        $homecarouseltabarray = array(
        );
        $textlistarray = array(
        );
        $imagelistarray = array(
        );

        $hometablists = Db::getInstance()->executeS('
						SELECT id_layoutsectionlist,layoutsectionlist_title,hometab_type,carousel_details,position FROM '._DB_PREFIX_.'layoutsectionlist
						 ORDER BY `position`');

        foreach ($hometablists as $layoutsectionlist) {
            $hometab_type = $layoutsectionlist['hometab_type'];

            if ($hometab_type == 'carousel') {
                $homecarouseltabarray[$layoutsectionlist['id_layoutsectionlist']] = $this->getCarouselListContent((int)$id_shop,
                $layoutsectionlist['id_layoutsectionlist']);
                $tab_carousel_details[$layoutsectionlist['id_layoutsectionlist']] = unserialize($layoutsectionlist['carousel_details']);
            } elseif ($hometab_type == 'image') {
                $imagelistarray[$layoutsectionlist['id_layoutsectionlist']] = $this->getImageListContent((int)$id_shop,
                $layoutsectionlist['id_layoutsectionlist']);
                $tab_image_details[$layoutsectionlist['id_layoutsectionlist']] = unserialize($layoutsectionlist['carousel_details']);
            } elseif ($hometab_type == 'text') {

                $tab_text_details[$layoutsectionlist['id_layoutsectionlist']] = unserialize($layoutsectionlist['carousel_details']);
				
                $textlistarray[$layoutsectionlist['id_layoutsectionlist']] = $this->getTextareaContent((int)$id_shop,
                $layoutsectionlist['id_layoutsectionlist']);
            }
        }

        $this->getCarouselTab($homecarouseltabarray, $tab_carousel_details);

        $this->getImageTab($imagelistarray, $tab_image_details);
        $this->getTextTab($textlistarray, $tab_text_details);
        $layoutdesigner_css_values = unserialize(Configuration::get('layoutdesigner_css_value'));

        $this->smarty->assign(array(
        'hometablists' => $hometablists,
        'tab_carousel_details' => $tab_carousel_details,
        'tab_image_details' => $tab_image_details,
        'tab_text_details' => $tab_text_details,
        'layoutdesigner_css_values' => $layoutdesigner_css_values,
        ));
        }
        return $this->display(__FILE__, 'hometab_carousel.tpl',$this->getCacheId('1'));
    }

    protected function getCacheId($name = null)
    {
        return parent::getCacheId('layoutdesigner|'.$name);
    }

    public function hookAddProduct($params)
    {
        $this->_clearCache('hometab_carousel.tpl');
    }

    public function hookUpdateProduct($params)
    {
        $this->_clearCache('hometab_carousel.tpl');
    }

    public function hookDeleteProduct($params)
    {
        $this->_clearCache('hometab_carousel.tpl');
    }

    public function hookCategoryUpdate($params)
    {
        $this->_clearCache('hometab_carousel.tpl');
    }


    public function getCarouselTab($homecarouseltabarray, $tab_carousel_details)
    {
        $home_tab_ids = array(
        );
        $home_tab_titles = array(
        );
        $home_category_name = array(
        );
        $home_product_lists = array(
        );
        $home_category_link = array(
        );
        $home_tabs = array(
        );
        $id_lang = (int)Context::getContext()->language->id;
        foreach ($homecarouseltabarray as $key => $hometab_list_value) {
            $home_tabs[$key] = $hometab_list_value;
        }


        foreach ($home_tabs as $home_tab_key => $home_tab) {
            $home_category_name[$home_tab_key] = $this->get_home_tab_category($home_tab, $home_tab_key);
            //ppp($home_tab);
            $home_category_link[$home_tab_key] = $this->get_home_tab_category_link($home_tab, $home_tab_key, $id_lang);
            $home_tab_ids[$home_tab_key] = $this->get_home_tab_ids($home_tab, $home_tab_key);
            $home_tab_titles[$home_tab_key] = $this->get_home_tab_titles($home_tab, $home_tab_key);
            $items_number = $tab_carousel_details[$home_tab_key];
            $nb = $items_number['items_number'];

            $home_product_lists[$home_tab_key] = $this->get_tab_product_lists($home_tab, $home_tab_key, $nb);
        }

        return $this->smarty->assign(array(
        'home_tab_ids' => $home_tab_ids,
        'home_tab_titles' => $home_tab_titles,
        'home_category_name' => $home_category_name,
        'home_category_link' => $home_category_link,
        'home_product_lists' => $home_product_lists,
        ));
    }

    public function getImageTab($imagelistarray, $tab_image_details)
    {

        $home_image_ids = array(
        );
        $tab_image_description = array(
        );
        $tab_image_url = array(
        );
        $tab_image_lists = array(
        );
        $image_tab = array(
        );
        $id_lang = (int)Context::getContext()->language->id;

        foreach ($imagelistarray as $key => $hometab_list_value) {
            $home_image_ids[$key] = $hometab_list_value;
        }

        foreach ($home_image_ids as $home_tab_key => $home_tab) {
            $image_tab[$home_tab_key] = $this->get_image_tab($home_tab);
        }

        return $this->smarty->assign(array(
        'home_image_ids' => $home_image_ids,
        'image_tab' => $image_tab,
        ));
    }

    public function get_image_tab($home_tab)
    {
        $get_image_tab = array(
        );
        foreach ($home_tab as $key => $value) {
            $id_layout_imagelist = $value['id_layout_imagelist'];
            $get_image_tab[$id_layout_imagelist] = array(
            'image' => $value['image'],
            'imgwidth' => $value['imgwidth'],
            'url' => $value['url'],
            'description' => $value['description'],
            'id_layout_imagelist' => $value['id_layout_imagelist'],);
        }
        return $get_image_tab;
    }

    public function getTextTab($textlistarray, $tab_text_details)
    {

        $home_textarea_ids = array(
        );
        $tab_textarea_lists = array(
        );
        $textarea_tab = array(
        );
        $id_lang = (int)Context::getContext()->language->id;
        if (!empty($textlistarray))
                foreach ($textlistarray as $key => $hometab_list_value) {
                $home_textarea_ids[$key] = $hometab_list_value;
            }

        foreach ($home_textarea_ids as $text_tab_key => $text_tab) {
            $textarea_tab[$text_tab_key] = $this->get_textarea_tab($text_tab);
        }

        return $this->smarty->assign(array(
        'home_textarea_ids' => $home_textarea_ids,
        'textarea_tab' => $textarea_tab,
        ));
    }

    public function get_textarea_tab($text_tab)
    {
        $get_textarea_tab = array(
        );

        foreach ($text_tab as $key => $value) {
            $id_layout_textarealist = $value['id_layout_textarealist'];
            $get_textarea_tab[$id_layout_textarealist] = array(
            'text' => $value['text'],
            'icon' => $value['icon'],
            'link' => $value['link'],);
        }

        return $get_textarea_tab;
    }

    public function FeaturedProducts()
    {
        //return $special = Product::getRandomSpecial((int)Context::getContext()->language->id);

        $category = new Category((int)Configuration::get('HOME_FEATURED_CAT'), (int)Context::getContext()->language->id);
        $nb = (int)Configuration::get('carousellist_items_number');
        if (Configuration::get('HOME_FEATURED_RANDOMIZE'))
                return $category->getProducts((int)Context::getContext()->language->id, 1, ($nb ? $nb : 8), null, null, false, true, true,
            ($nb ? $nb : 8));
        else return $category->getProducts((int)Context::getContext()->language->id, 1, ($nb ? $nb : 8), 'position');
    }

    private function getProducts($cat_id, $nb)
    {
        $products = false;
        if (!$cat_id) return;
        $category = new Category((int)$cat_id, (int)Context::getContext()->language->id);

        $products = $category->getProducts((int)Context::getContext()->language->id, 1, ($nb ? $nb : 4));
        if (!$products) return;
        return $products;
    }

    private function getNewProducts()
    {
        if (!Configuration::get('NEW_PRODUCTS_NBR')) return;
        $newProducts = false;
        if (Configuration::get('PS_NB_DAYS_NEW_PRODUCT'))
                $newProducts = Product::getNewProducts((int)$this->context->language->id, 0, (int)Configuration::get('NEW_PRODUCTS_NBR'));

        if (!$newProducts && Configuration::get('PS_BLOCK_NEWPRODUCTS_DISPLAY')) return;
        return $newProducts;
    }

    private function getBestSellers()
    {
        if (Configuration::get('PS_CATALOG_MODE')) return false;

        $result = ProductSale::getBestSalesLight((int)$this->context->language->id, 0, (int)Configuration::get('carousellist_items_number'));

        $usetax = (Product::getTaxCalculationMethod((int)$this->context->customer->id) != PS_TAX_EXC);
        foreach ($result as &$row) $row['price'] = Tools::displayPrice(Product::getPriceStatic((int)$row['id_product'], $usetax));
        return $result;
    }

    public function getMaxPosition()
    {
        $sql = Db::getInstance()->getValue('SELECT COUNT(*)
			FROM `'._DB_PREFIX_.'layoutsectionlist`
			WHERE 1');

        return $sql + 1;
    }

    public function getPosition($id_layoutsectionlist)
    {
        $sql = Db::getInstance()->getValue('SELECT `position`
			FROM `'._DB_PREFIX_.'layoutsectionlist`
			WHERE id_layoutsectionlist ='.(int)$id_layoutsectionlist);
        return $sql;
    }

    /*
     * Configuration page form builder
     */

    public function get_home_tab_category($home_tab, $home_tab_key)
    {
        $category_name = array(
        );

        foreach ($home_tab as $key => $home_tabs) {
            $carousellist_details = unserialize($home_tabs['carousellist_details']);
            $category_str = str_replace(" ", "", $carousellist_details['category_name']);
            $category_name[$home_tabs['id_layout_carousellist']] = $category_str.'_'.$home_tab_key;
        }
        return $category_name;
    }

    public function get_home_tab_category_link($home_tab, $home_tab_key, $id_lang)
    {

        $category_link = array(
        );
        foreach ($home_tab as $key => $ids_category) {
            $carousel_type = $ids_category['carousel_type'];

            switch ($carousel_type) {
                case 'Specialcat_carousel':
                    $carousel_name = $ids_category['carousel_name'];
                    $category_link[$ids_category['id_layout_carousellist']] = $carousel_name;
                    break;
                case 'Category_carousel':
                    $carousellist_details = unserialize($ids_category['carousellist_details']);
                    $id = $carousellist_details['id_category'];
                    $c = new Category((int)$id, $id_lang);
                    $category = array(
                    );
                    $category_link[$ids_category['id_layout_carousellist']] = $c->getLink();
                    break;
                case 'Products_carousel':
                    //$carousellist_details = unserialize($ids_category['carousellist_details']);
                    //$Product_ids = $carousellist_details['Product_ids'];
                    //$Product_id =$Product_ids[0];
                    //$context = Context::getContext();
                    $category_link[$ids_category['id_layout_carousellist']] = 'Products_carousel';
                    break;
                default:
            }
        }

        return $category_link;
    }

    public function get_home_tab_ids($home_tab, $home_tab_key)
    {
        $tab_ids = array(
        );
        foreach ($home_tab as $key => $ids_category) {
            $carousellist_details = unserialize($ids_category['carousellist_details']);
            $category_str = str_replace(" ", "", $carousellist_details['category_name']);
            $tab_ids[$ids_category['id_layout_carousellist']] = $home_tab_key.$category_str.'_'.rand().time();
        }
        return $tab_ids;
    }

    public function get_home_tab_titles($home_tab, $home_tab_key)
    {
        $tab_titles = array(
        );
        foreach ($home_tab as $key => $ids_category) {
            $tab_titles[$ids_category['id_layout_carousellist']] = $ids_category['hometab_title'];
        }
        return $tab_titles;
    }

    public function get_tab_product_lists($home_tab, $home_tab_key, $nb)
    {
        $product_lists = array(
        );
        $id_category = array(
        );

        foreach ($home_tab as $key => $ids_category) {
            $carousellist_details = unserialize($ids_category['carousellist_details']);

            //$id_category[$ids_category['id_layout_carousellist']] = $carousellist_details['id_category'];
            //

			switch ($ids_category['carousel_type']) {
                case 'Category_carousel':
                    $product_lists[$ids_category['id_layout_carousellist']] = $this->getProducts($carousellist_details['id_category'], $nb);
                    break;
                case 'Specialcat_carousel':
                    switch ($carousellist_details['id_category']) {
                        case 'new_products':
                            $product_lists[$ids_category['id_layout_carousellist']] = $this->getNewProducts($nb);
                            break;
                        case 'featured_products':
                            $category = new Category((int)Configuration::get('HOME_FEATURED_CAT'), (int)Context::getContext()->language->id);

                            if (Configuration::get('HOME_FEATURED_RANDOMIZE'))
                                    $product_lists[$ids_category['id_layout_carousellist']] = $cache_products = $category->getProducts((int)Context::getContext()->language->id,
                                1, ($nb ? $nb : 8), null, null, false, true, true, ($nb ? $nb : 8));
                            else
                                    $product_lists[$ids_category['id_layout_carousellist']] = $cache_products = $category->getProducts((int)Context::getContext()->language->id,
                                1, ($nb ? $nb : 8), 'position');
                            break;
                        case 'best_sellers':
                            $product_lists[$ids_category['id_layout_carousellist']] = $this->getBestSellers();
                            break;
                        default:
                            $product_lists[$ids_category['id_layout_carousellist']] = Product::getPricesDrop((int)Context::getContext()->language->id,
                            0, Configuration::get('BLOCKSPECIALS_SPECIALS_NBR'));
                    }

                    break;
                case 'Products_carousel':

                    //ppp($carousellist_details);
                    $twoDimensionalArray = array(
                    );
                    foreach ($carousellist_details['Product_ids'] as $key => $value) {

                        $twoDimensionalArray[] = $this->getproductById($value);
                    }
                    //die();
                    $productItem_lists = array_map('current', $twoDimensionalArray);
                    //ppp($twoDimensionalArray);
                    $product_lists[$ids_category['id_layout_carousellist']] = $productItem_lists;
                    //ppp($product_lists);
                    //die();
                    break;
                default:
                //$product_lists[$ids_category['id_layout_carousellist']] = $this->getBestSellers();												
            }
        }
        //die();
        return $product_lists;
    }

    public function CategoryOptions()
    {
        $options = array(
        );
        /* $options1 = array();
          $options2 = array(
          array(
          'id' => 'new_products',
          'name' => 'New Products'),
          array(
          'id' => 'featured_products',
          'name' => 'Featured Products'),
          array(
          'id' => 'best_sellers',
          'name' => 'Best Sellers')); */
        $lang_id = (int)Context::getContext()->language->id;
        $listcatgory = Category::getCategories($lang_id);

        foreach ($listcatgory as $category) {
            foreach ($category as $childrens) {
                foreach ($childrens as $children) {
                    $options[] = array(
                    'id' => $children['id_category'],
                    'name' => $children['name']);
                }
            }
        }

        //$options = array_merge($options1, $options2);
        //ppp($options);
        //die();
        return array(
        array(
        'name' => ('Select Category'),
        'query' => $options),
        );
    }

    public function SpecialCategoryOptions()
    {
        $options = array(
        );
        $options = array(
        array(
        'id' => 'new_products',
        'name' => 'New Products'),
        array(
        'id' => 'featured_products',
        'name' => 'Featured Products'),
        array(
        'id' => 'best_sellers',
        'name' => 'Best Sellers'));
        return array(
        array(
        'name' => ('Select Category'),
        'query' => $options),
        );
    }

    public function getImageFieldsValues()
    {
        $fields = array(
        );
        $fields['active_image'] = Tools::getValue('active_image');
        $fields['image'] = Tools::getValue('image');
        $fields['title'] = Tools::getValue('title');
        $fields['url'] = Tools::getValue('url');
        $fields['description'] = Tools::getValue('description');
        $fields['imgwidth'] = Tools::getValue('imgwidth');

        return $fields;
    }

    protected function getImageFieldById($id_layout_imagelist)
    {

        $imagevalue = Db::getInstance()->executeS('
			SELECT `image`,`imgwidth`,`url`, `description`, `active_image`
			FROM `'._DB_PREFIX_.'layout_imagelist`
			WHERE `id_layout_imagelist` = '.(int)$id_layout_imagelist.' '.Shop::addSqlRestrictionOnLang());
        $fields = array(
        );

        foreach ($imagevalue as $key => $value) {

            $fields['active_image'] = $value['active_image'];
            $fields['image'] = $value['image'];
            $fields['imgwidth'] = $value['imgwidth'];
            $fields['url'] = $value['url'];
            $fields['description'] = $value['description'];
        }
        //die();


        return $fields;
    }

}
