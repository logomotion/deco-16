<?php
/**
 * Please do not edit or add any code in this file without the permission of PIBBLU
 *
 * @author    Pibblu 
 * @copyright PIBBLU
 * @license   http://www.pibblu.com  
 * Prestashop version 1.6.0.6
 * layoutdesigner 3.1.0
 * OCT 2015
 */

class LayoutDesignerModel extends ObjectModel
{

    public $id;
    public $id_shop;
    public $id_layoutsectionlist;
    public $layoutsectionlist_title;
    public $carousel_details;
    public $hometab_type;
    public $cover;
    public $position;
    public $image;
    public $id_parent;


    protected static $_links = array(
    );
    public static $definition = array(
    'table' => 'layoutsectionlist',
    'primary' => 'id_layoutsectionlist',
    //'multilang' => true,
    'fields' => array(
    'position' => array(
    'type' => self::TYPE_INT,
    'validate' => 'isUnsignedInt'),
    // Lang fields
    'layoutsectionlist_title' => array(
    'type' => self::TYPE_STRING,
    'lang' => true,
    'required' => true,
    'size' => 64),
    'hometab_type' => array(
    'type' => self::TYPE_STRING,
    'lang' => true,
    'required' => true,
    'size' => 64),
    'carousel_details' => array(
    'type' => self::TYPE_STRING,
    'lang' => true),
    ),
    );

    /* Get all CMS blocks by location */

    public static function getCMSBlocksByLocation($id_shop = false)
    {

        $sql = Db::getInstance()->executeS('
            SELECT `id_layoutsectionlist`, `layoutsectionlist_title`, `hometab_type`,`id_shop`,`id_lang`,`position`
            FROM `'._DB_PREFIX_.'layoutsectionlist`
            ORDER BY `position`');
        return $sql;
    }

    protected function gettabListContent()
    {
        $sql = Db::getInstance()->executeS('
            SELECT `id_layoutsectionlist`, `layoutsectionlist_title`, `hometab_type`,`id_shop`,`id_lang`,`position`
            FROM `'._DB_PREFIX_.'layoutsectionlist`
            ORDER BY `position` '.Shop::addSqlRestrictionOnLang());


        return $sql;
    }

    public static function updateCMSBlockPosition($id_cms_block, $position, $new_position, $location)
    {

        $query = 'UPDATE `'._DB_PREFIX_.'layoutsectionlist`
            SET `position` = '.(int)$new_position.'
            WHERE `position` = '.(int)$position;

        $sub_query = 'UPDATE `'._DB_PREFIX_.'layoutsectionlist`
            SET `position` = '.(int)$position.'
            WHERE `id_layoutsectionlist` = '.(int)$id_cms_block;

        if (Db::getInstance()->execute($query)) Db::getInstance()->execute($sub_query);
    }

    public static function getHomeTab_title($id_layoutsectionlist)
    {
        $layoutsectionlist_title = Db::getInstance()->getValue('
                    SELECT layoutsectionlist_title
                    FROM '._DB_PREFIX_.'layoutsectionlist
                    WHERE  id_layoutsectionlist = '.(int)$id_layoutsectionlist);
        return $layoutsectionlist_title;
    }

    public static function getHomeTab_type($id_layoutsectionlist)
    {
        $hometab_type = Db::getInstance()->getValue('
                    SELECT hometab_type
                    FROM '._DB_PREFIX_.'layoutsectionlist
                    WHERE  id_layoutsectionlist = '.(int)$id_layoutsectionlist);
        return $hometab_type;
    }

    public static function getImageListContent($id_lang, $id_layoutsectionlist)
    {
        return Db::getInstance()->executeS('
            SELECT `id_layout_imagelist`,`image`,`imgwidth`, `url`, `description`, `id_shop`,`id_lang`, `active_image`
            FROM `'._DB_PREFIX_.'layout_imagelist`
            WHERE `id_hometab` = '.(int)$id_layoutsectionlist.' '.Shop::addSqlRestrictionOnLang());
    }

    public static function displayStatus($id_slide, $active)
    {
        $title = ((int)$active == 0 ? 'Disabled':'Enabled');
        $icon = ((int)$active == 0 ? 'icon-remove' : 'icon-check');
        $class = ((int)$active == 0 ? 'btn-danger' : 'btn-success');
        $html = '<a class="btn '.$class.'" href="'.AdminController::$currentIndex.
        '&configure=layoutdesigner
                &token='.Tools::getAdminTokenLite('AdminModules').'
                &changeStatus&id_slide='.(int)$id_slide.'" title="'.$title.'"><i class="'.$icon.'"></i> '.$title.'</a>';

        return $html;
    }

    public static function getCarouselListContent($id_lang, $id_layoutsectionlist)
    {

        return Db::getInstance()->executeS('
            SELECT `id_layout_carousellist`,`hometab_title`, `carousel_type`,`carousel_name`, `id_shop`,`id_lang`,`carousellist_details`
            FROM `'._DB_PREFIX_.'layout_carousellist`
            WHERE `id_hometab` = '.(int)$id_layoutsectionlist.' '.Shop::addSqlRestrictionOnLang());
    }

    public static function getCarousel_type($id_layout_carousellist)
    {
        $carousel_types = Db::getInstance()->executeS('
            SELECT `hometab_title`,`carousel_type`,`id_hometab`FROM `'._DB_PREFIX_.'layout_carousellist`
            WHERE `id_layout_carousellist` = '.(int)$id_layout_carousellist.' '.Shop::addSqlRestrictionOnLang());
        foreach ($carousel_types as $value) {
            $carousel_type = $value;
        }
        return $carousel_type;
    }

    public static function getIdHomeTab($id_layoutsectionlist)
    {
        return Db::getInstance()->getValue('
            SELECT `id_hometab`
            FROM `'._DB_PREFIX_.'layout_textarealist`
            WHERE `id_layout_textarealist` = '.(int)$id_layoutsectionlist);
    }

    public static function getTextareaContent($id_lang, $id_layoutsectionlist)
    {
        return Db::getInstance()->executeS('
            SELECT `id_layout_textarealist`, `id_lang`, `id_shop`, `text`, `icon`,`link`
            FROM `'._DB_PREFIX_.'layout_textarealist`
            WHERE `id_hometab` = '.(int)$id_layoutsectionlist.' '.Shop::addSqlRestrictionOnLang());
    }

}
