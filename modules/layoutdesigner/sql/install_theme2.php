<?php
/**
 * Please do not edit or add any code in this file without the permission of PIBBLU
 *
 * @author    Pibblu 
 * @copyright PIBBLU
 * @license   http://www.pibblu.com  
 * Prestashop version 1.6.0.6
 * layoutdesigner 3.1.0
 * OCT 2015
 */

	$sql = array();

		 $sql[1] = '
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'layout_carousellist` (
				`id_layout_carousellist` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`hometab_title` VARCHAR(100) NULL,
				`carousel_type` VARCHAR(100) NULL,
				`carousel_name` VARCHAR(100) NULL,
				`id_shop` int(10) unsigned NOT NULL,
				`id_lang` int(10) unsigned NOT NULL,
				`id_hometab` int(10) unsigned NOT NULL,
				`carousellist_details` text NOT NULL,					
				PRIMARY KEY (`id_layout_carousellist`, `id_lang`,`id_shop`,`id_hometab`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;';

		 $sql[2] = '
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'layoutsectionlist` (
				`id_layoutsectionlist` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`layoutsectionlist_title` VARCHAR(100) NULL,
				`hometab_type` VARCHAR(50) NULL,
				`position` int(10) UNSIGNED NOT NULL DEFAULT 0,
				`id_shop` int(10) unsigned NOT NULL ,
				`id_lang` int(10) unsigned NOT NULL ,
				`carousel_details` text NOT NULL,					
				PRIMARY KEY (`id_layoutsectionlist`, `id_lang`,`id_shop`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;';

		$sql[3] = '
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'layout_imagelist` (
				`id_layout_imagelist` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`image` VARCHAR(100) NOT NULL,
				`imgwidth` VARCHAR(100) NOT NULL,
				`url` VARCHAR(100) NULL,
				`description` VARCHAR(100) NULL,
				`id_shop` int(10) unsigned NOT NULL ,
				`id_lang` int(10) unsigned NOT NULL ,
				`active_image` VARCHAR(30) NOT NULL ,
				`id_hometab` int(10) unsigned NOT NULL ,					
				PRIMARY KEY (`id_layout_imagelist`, `id_shop`,`id_lang`,`id_hometab`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;';

		$sql[4] = '
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'layout_textarealist` (
				`id_layout_textarealist` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`id_lang` int(10) unsigned NOT NULL,
				`id_shop` int(10) unsigned NOT NULL,
				`text` VARCHAR(300) NOT NULL,
				`icon` VARCHAR(300) NULL,
				`link` VARCHAR(300) NULL,
				`id_hometab` int(10) unsigned NOT NULL,
			PRIMARY KEY (`id_layout_textarealist`, `id_lang`,`id_shop`,`id_hometab`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;';



		$sql[5] = 'INSERT INTO '._DB_PREFIX_.'layoutsectionlist (`id_layoutsectionlist`, `layoutsectionlist_title`, `hometab_type`,`position`,`id_shop`, `id_lang`, `carousel_details`) VALUES
		(1, \'sample image banner\', \'image\',0,1,1,\'a:6:{s:11:"image_items";b:0;s:9:"tab_title";s:19:"sample image banner";s:14:"image_position";s:10:"horizontal";s:13:"image_enabled";s:4:"true";s:10:"set_col_md";s:9:"col-md-12";s:4:"type";s:5:"image";}\'),
		(2, \'Browse our Categories\', \'carousel\',1, 1, 1, \'a:9:{s:11:"slide_width";s:3:"273";s:9:"tab_title";s:21:"Browse our Categories";s:12:"items_number";s:1:"8";s:10:"limit_cols";s:1:"4";s:11:"slide_pager";s:5:"false";s:9:"auto_play";s:5:"false";s:16:"carousel_enabled";s:4:"true";s:10:"set_col_md";s:8:"col-md-6";s:4:"type";s:8:"carousel";}\'),
		(3, \'NEW PRODUCTS\', \'carousel\',2, 1, 1, \'a:9:{s:11:"slide_width";s:3:"273";s:9:"tab_title";s:12:"NEW PRODUCTS";s:12:"items_number";s:1:"8";s:10:"limit_cols";s:1:"4";s:11:"slide_pager";s:5:"false";s:9:"auto_play";s:5:"false";s:16:"carousel_enabled";s:4:"true";s:10:"set_col_md";s:8:"col-md-6";s:4:"type";s:8:"carousel";}\'),
		(4, \'banner2\', \'image\',3, 1, 1, \'a:6:{s:11:"image_items";b:0;s:9:"tab_title";s:7:"banner2";s:14:"image_position";s:10:"horizontal";s:13:"image_enabled";s:4:"true";s:10:"set_col_md";s:9:"col-md-12";s:4:"type";s:5:"image";}\'),
		(5, \'FEATURED PRODUCTS\', \'carousel\',4, 1, 1, \'a:9:{s:11:"slide_width";s:3:"280";s:9:"tab_title";s:17:"FEATURED PRODUCTS";s:12:"items_number";s:1:"8";s:10:"limit_cols";s:1:"4";s:11:"slide_pager";s:5:"false";s:9:"auto_play";s:5:"false";s:16:"carousel_enabled";s:4:"true";s:10:"set_col_md";s:9:"col-md-12";s:4:"type";s:8:"carousel";}\'),
		(6, \'banner5\', \'image\',5, 1, 1, \'a:6:{s:11:"image_items";b:0;s:9:"tab_title";s:7:"banner5";s:14:"image_position";s:10:"horizontal";s:13:"image_enabled";s:4:"true";s:10:"set_col_md";s:9:"col-md-12";s:4:"type";s:5:"image";}\'),
		(7, \'text blog\', \'text\',6,1, 1, \'a:4:{s:9:"tab_title";s:9:"text blog";s:9:"auto_play";s:4:"true";s:10:"set_col_md";s:9:"col-md-12";s:4:"type";s:4:"text";}\')';



		$sql[6] = 'INSERT INTO '._DB_PREFIX_.'layout_carousellist (`id_layout_carousellist`, `hometab_title`, `carousel_type`, `carousel_name`, `id_shop`, `id_lang`, `id_hometab`, `carousellist_details`) VALUES
		(1, \' \', \'Category_carousel\', \'Dresses\', 1, 1, 2, \'a:3:{s:13:"category_name";s:7:"Dresses";s:13:"hometab_title";s:0:"";s:11:"id_category";s:1:"8";}\'),
		(2, \'\', \'Specialcat_carousel\', \'best_sellers\', 1, 1, 3, \'a:3:{s:13:"category_name";s:12:"best_sellers";s:13:"hometab_title";s:0:"";s:11:"id_category";s:12:"best_sellers";}\'),
		(3, \'Top Rated\', \'Specialcat_carousel\', \'best_sellers\', 1, 1, 5, \'a:3:{s:13:"category_name";s:12:"best_sellers";s:13:"hometab_title";s:9:"Top Rated";s:11:"id_category";s:12:"best_sellers";}\'),
		(4, \'New Arrivals\', \'Specialcat_carousel\', \'new_products\', 1, 1, 5, \'a:3:{s:13:"category_name";s:12:"new_products";s:13:"hometab_title";s:12:"New Arrivals";s:11:"id_category";s:12:"new_products";}\'),
		(5, \'BEST SELLERS\', \'Specialcat_carousel\', \'featured_products\', 1, 1, 5, \'a:3:{s:13:"category_name";s:17:"featured_products";s:13:"hometab_title";s:12:"BEST SELLERS";s:11:"id_category";s:17:"featured_products";}\')';


		$sql[7] ='INSERT INTO '._DB_PREFIX_.'layout_imagelist (`id_layout_imagelist`, `image`, `imgwidth`, `url`, `description`, `id_shop`, `id_lang`, `active_image`, `id_hometab`) VALUES
		(1, \'09a650a2bdca5cbdb31ce8cfff9486def016854a_photo-1423483786645-576de98dcbed-879x496.jpeg\', \'col-md-8\', \'\', \'\', 1, 1, \'0\', 1),
		(2, \'c36240a1eef32695a42764b49b6d39bd4f5fc7da_banner2.png\', \'col-md-4\', \'\', \'\', 1, 1, \'0\', 1),
		(3, \'ababa4c4e11bd5dfa968c3a4adb5960faa499fe7_banner3.png\', \'col-md-4\', \'\', \'\', 1, 1, \'0\', 1),
		(4, \'4923d1ad1e4a7df4af4a8307ab92615655bb678d_banner21.png\', \'col-md-12\', \'\', \'\', 1, 1, \'0\', 4),
		(5, \'73afcc2df93ec2105b3263815b79cea0afdb79da_123-370x434432.png\', \'col-md-3\', \'\', \'\', 1, 1, \'0\', 6),
		(6, \'d8b5e0b38f6eb7268ca34ecb533a3fc399c9ee13_111-370x432.jpg\', \'col-md-3\', \'\', \'\', 1, 1, \'0\', 6),
		(7, \'b94e3b5e365e984c1fa1e0b7ac1c0122c3b87083_toys3-370x432.jpg\', \'col-md-3\', \'\', \'\', 1, 1, \'0\', 6),
		(8, \'faefcbb445875eac5402a1979c218393c265dd22_2423-370x432.png\', \'col-md-3\', \'\', \'\', 1, 1, \'0\', 6)';		


		$sql[8] = 'INSERT INTO '._DB_PREFIX_.'layout_textarealist (`id_layout_textarealist`, `id_lang`, `id_shop`, `text`, `icon`, `link`, `id_hometab`) VALUES
		(1, 1, 1, \'In-store exchange.\', \'icon-truck\', \'\', 7),
		(2, 1, 1, \'CASH ON DELIVERY\', \'icon-inr\', \'\', 7),
		(3, 1, 1, \'Free Delivery\', \'icon-truck\', \'\', 7),
		(4, 1, 1, \'ORDER ONLINE\', \'icon-tablet\', \'\', 7)';

foreach ($sql as $query)
	if (Db::getInstance()->execute($query) == false)
		return false;
