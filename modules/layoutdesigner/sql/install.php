<?php
/**
 * Please do not edit or add any code in this file without the permission of PIBBLU
 *
 * @author    Pibblu 
 * @copyright PIBBLU
 * @license   http://www.pibblu.com  
 * Prestashop version 1.6.0.6
 * layoutdesigner 3.1.0
 * OCT 2015
 */

	$sql = array();

		 $sql[1] = '
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'layout_carousellist` (
				`id_layout_carousellist` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`hometab_title` VARCHAR(100) NULL,
				`carousel_type` VARCHAR(100) NULL,
				`carousel_name` VARCHAR(100) NULL,
				`id_shop` int(10) unsigned NOT NULL,
				`id_lang` int(10) unsigned NOT NULL,
				`id_hometab` int(10) unsigned NOT NULL,
				`carousellist_details` text NOT NULL,					
				PRIMARY KEY (`id_layout_carousellist`, `id_lang`,`id_shop`,`id_hometab`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;';

		 $sql[2] = '
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'layoutsectionlist` (
				`id_layoutsectionlist` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`layoutsectionlist_title` VARCHAR(100) NULL,
				`hometab_type` VARCHAR(50) NULL,
				`position` int(10) UNSIGNED NOT NULL DEFAULT 0,
				`id_shop` int(10) unsigned NOT NULL ,
				`id_lang` int(10) unsigned NOT NULL ,
				`carousel_details` text NOT NULL,					
				PRIMARY KEY (`id_layoutsectionlist`, `id_lang`,`id_shop`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;';

		$sql[3] = '
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'layout_imagelist` (
				`id_layout_imagelist` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`image` VARCHAR(100) NOT NULL,
				`imgwidth` VARCHAR(100) NOT NULL,
				`url` VARCHAR(100) NULL,
				`description` VARCHAR(100) NULL,
				`id_shop` int(10) unsigned NOT NULL ,
				`id_lang` int(10) unsigned NOT NULL ,
				`active_image` VARCHAR(30) NOT NULL ,
				`id_hometab` int(10) unsigned NOT NULL ,
				`position` int(10) UNSIGNED NOT NULL DEFAULT 0,					
				PRIMARY KEY (`id_layout_imagelist`, `id_shop`,`id_lang`,`id_hometab`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;';

		$sql[4] = '
			CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'layout_textarealist` (
				`id_layout_textarealist` INT UNSIGNED NOT NULL AUTO_INCREMENT,
				`id_lang` int(10) unsigned NOT NULL,
				`id_shop` int(10) unsigned NOT NULL,
				`text` VARCHAR(300) NOT NULL,
				`icon` VARCHAR(300) NULL,
				`link` VARCHAR(300) NULL,
				`id_hometab` int(10) unsigned NOT NULL,
			PRIMARY KEY (`id_layout_textarealist`, `id_lang`,`id_shop`,`id_hometab`)
			) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8 ;';

		$sql[5] = 'INSERT INTO '._DB_PREFIX_.'layoutsectionlist (`id_layoutsectionlist`, `layoutsectionlist_title`, `hometab_type`,`position`, `id_shop`, `id_lang`, `carousel_details`) VALUES
		(3, \'sample image banner\', \'image\',1, 1, 1, \'a:6:{s:11:"image_items";b:0;s:9:"tab_title";s:19:"sample image banner";s:14:"image_position";s:10:"horizontal";s:13:"image_enabled";s:4:"true";s:10:"set_col_md";s:9:"col-md-12";s:4:"type";s:5:"image";}\'),
		(4, \'FEATURED PRODUCTS\', \'carousel\',2, 1, 1, \'a:9:{s:11:"slide_width";s:3:"290";s:9:"tab_title";s:17:"FEATURED PRODUCTS";s:12:"items_number";s:1:"8";s:10:"limit_cols";s:1:"4";s:11:"slide_pager";s:5:"false";s:9:"auto_play";s:4:"true";s:16:"carousel_enabled";s:4:"true";s:10:"set_col_md";s:9:"col-md-12";s:4:"type";s:8:"carousel";}\'),
		(5, \'IMAGE BANNER 02\', \'image\',3, 1, 1, \'a:6:{s:11:"image_items";b:0;s:9:"tab_title";s:15:"IMAGE BANNER 02";s:14:"image_position";s:10:"horizontal";s:13:"image_enabled";s:4:"true";s:10:"set_col_md";s:9:"col-md-12";s:4:"type";s:5:"image";}\'),
		(6, \'CATEGORY CAROUSELS\', \'carousel\',4, 1, 1, \'a:9:{s:11:"slide_width";s:3:"292";s:9:"tab_title";s:18:"CATEGORY CAROUSELS";s:12:"items_number";s:1:"8";s:10:"limit_cols";s:1:"4";s:11:"slide_pager";s:5:"false";s:9:"auto_play";s:4:"true";s:16:"carousel_enabled";s:4:"true";s:10:"set_col_md";s:9:"col-md-12";s:4:"type";s:8:"carousel";}\'),
		(7, \'IMAGE BANNER03\', \'image\',5,1, 1, \'a:6:{s:11:"image_items";b:0;s:9:"tab_title";s:14:"IMAGE BANNER03";s:14:"image_position";s:10:"horizontal";s:13:"image_enabled";s:4:"true";s:10:"set_col_md";s:9:"col-md-12";s:4:"type";s:5:"image";}\'),
		(8, \'PRODUCTS CAROUSEL\', \'carousel\',6, 1, 1, \'a:9:{s:11:"slide_width";s:3:"292";s:9:"tab_title";s:17:"PRODUCTS CAROUSEL";s:12:"items_number";s:1:"8";s:10:"limit_cols";s:1:"4";s:11:"slide_pager";s:5:"false";s:9:"auto_play";s:4:"true";s:16:"carousel_enabled";s:4:"true";s:10:"set_col_md";s:9:"col-md-12";s:4:"type";s:8:"carousel";}\'),
		(9, \'TEXT AREA\', \'text\',7, 1, 1, \'a:4:{s:9:"tab_title";s:9:"TEXT AREA";s:9:"auto_play";s:4:"true";s:10:"set_col_md";s:9:"col-md-12";s:4:"type";s:4:"text";}\')';


		$sql[6] = 'INSERT INTO '._DB_PREFIX_.'layout_carousellist (`id_layout_carousellist`, `hometab_title`, `carousel_type`, `carousel_name`, `id_shop`, `id_lang`, `id_hometab`, `carousellist_details`) VALUES
		(4, \'WE RECOMMEND\', \'Specialcat_carousel\', \'featured_products\', 1, 1, 4, \'a:3:{s:13:"category_name";s:17:"featured_products";s:13:"hometab_title";s:12:"WE RECOMMEND";s:11:"id_category";s:17:"featured_products";}\'),
		(5, \'NEW PRODUCTS\', \'Specialcat_carousel\', \'new_products\', 1, 1, 4, \'a:3:{s:13:"category_name";s:12:"new_products";s:13:"hometab_title";s:12:"NEW PRODUCTS";s:11:"id_category";s:12:"new_products";}\'),
		(6, \'BEST SELLERS\', \'Specialcat_carousel\', \'best_sellers\', 1, 1, 4, \'a:3:{s:13:"category_name";s:12:"best_sellers";s:13:"hometab_title";s:12:"BEST SELLERS";s:11:"id_category";s:12:"best_sellers";}\'),
		(7, \'WOMEN\', \'Category_carousel\', \'Women\', 1, 1, 6, \'a:3:{s:13:"category_name";s:5:"Women";s:13:"hometab_title";s:5:"WOMEN";s:11:"id_category";s:1:"3";}\'),
		(8, \'TOPS\', \'Category_carousel\', \'Tops\', 1, 1, 6, \'a:3:{s:13:"category_name";s:4:"Tops";s:13:"hometab_title";s:4:"TOPS";s:11:"id_category";s:1:"4";}\'),
		(9, \'CUSTOM PRODUCTS\', \'Products_carousel\', \'Product_ids\', 1, 1, 8, \'a:6:{s:11:"Product_ids";a:5:{i:0;s:1:"7";i:1;s:1:"3";i:2;s:1:"6";i:3;s:1:"5";i:4;s:1:"4";}s:21:"product_pack_quantity";a:5:{i:0;s:1:"1";i:1;s:1:"1";i:2;s:1:"1";i:3;s:1:"1";i:4;s:1:"1";}s:12:"product_name";a:5:{i:0;s:23:"  Printed Chiffon Dress";i:1;s:15:"  Printed Dress";i:2;s:22:"  Printed Summer Dress";i:3;s:21:" Printed Summer Dress";i:4;s:14:" Printed Dress";}s:11:"id_category";a:4:{s:2:"id";s:1:"7";s:13:"pack_quantity";s:1:"1";s:3:"ids";s:1:"7";s:4:"name";s:23:"  Printed Chiffon Dress";}s:13:"hometab_title";s:15:"CUSTOM PRODUCTS";s:13:"category_name";s:15:"CUSTOM PRODUCTS";}\')';

		$sql[7] = 'INSERT INTO '._DB_PREFIX_.'layout_imagelist (`id_layout_imagelist`, `image`, `imgwidth`, `url`, `description`, `id_shop`, `id_lang`, `active_image`, `id_hometab`,`position`) VALUES
		(4, \'5101b757e409a88f3994c7924870282a306999d4_png-imageeew1-pixels.png\', \'col-md-4\', \'\', \'\', 1, 1, \'0\', 3,1),
		(5, \'54da330a12ec8ad9dbb85e6ff1ded75f2680b208_png-imagefeixels.png\', \'col-md-4\', \'\', \'\', 1, 1, \'0\', 3,2),
		(6, \'9d114ce4e3919009fc256307cb515d62ab4ff2f5_png-imagefdpixels.png\', \'col-md-4\', \'\', \'\', 1, 1, \'0\', 3,3),
		(7, \'bda6043a6331d170d8ccda757dd7eeceec283f9b_png-imageeees.png\', \'col-md-12\', \'\', \'\', 1, 1, \'0\', 5,1),
		(8, \'b956a8599961f20c5a8934f563378f6824c57104_png-image-fdfixels.png\', \'col-md-6\', \'\', \'\', 1, 1, \'0\', 7,1),
		(9, \'a4b490fa851b335c816d12a83382ebd8a430b11b_pnggdfgsels.png\', \'col-md-6\', \'\', \'\', 1, 1, \'0\', 7,2)';

		$sql[8] = 'INSERT INTO '._DB_PREFIX_.'layout_textarealist (`id_layout_textarealist`, `id_lang`, `id_shop`, `text`, `icon`, `link`, `id_hometab`) VALUES
		(3, 1, 1, \'In-store exchange.\', \'icon-truck\', \'er\', 2),
		(4, 1, 1, \'Free Shipping.\', \'icon-money\', \'gf\', 2),
		(5, 1, 1, \'Free Shipping14\', \'icon-money\', \'gf\', 2),
		(6, 1, 1, \'Money back guarantee.\', \'icon-truck\', \'dzgdfg\', 2),
		(7, 1, 1, \'Free Delivery\', \'icon-truck\', \'\', 9),
		(8, 1, 1, \'ORDER ONLINE\', \'icon-tablet\', \'\', 9),
		(9, 1, 1, \'CASH ON DELIVERY\', \'icon-inr\', \'\', 9),
		(10, 1, 1, \'SUPPORT   CALL : 123 546 7890\', \'icon-phone-sign\', \'\', 9)';

foreach ($sql as $query)
	if (Db::getInstance()->execute($query) == false)
		return false;
