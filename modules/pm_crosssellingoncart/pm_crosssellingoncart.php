<?php
/**
 * PM_CrossSellingOnCart Merchandizing Feature
 *
 * @category merchandizing_features
 * @authors Jean-Sebastien Couvert & Stephan Obadia / Presta-Module.com <support@presta-module.com>
 * @copyright Presta-Module 2011
 * @version 2.0.6
 *
 **************************************
 **        CrossSellingOnCart         *
 **   http://www.presta-module.com    *
 **               V 2.0.6             *
 **************************************
 * +
 * +Languages: EN, FR
 * +PS version: 1.4, 1.3
 *
 * Changelog :
 * 2.0.6
 * - Fixed arrow height on Firefox Mac
 * 2.0.5
 * - Add json_encode function
 * 2.0.4
 * - Change CSS selector for avoid theme conflict
 * 2.0.3
 * - Fix script.js - missing control on MocalCart installation
 * 2.0.2
 * - Fix $postProductSelection array
 * 2.0.1
 * - Fix getAccessoriesLight function to display more than one accesory
 * 2.0
 * - Add option for accessories suggestion
 * - Interfaced with the module modalCart
 * 1.4
 * - reduction prices management
 * - core optimizations
 * - better Prestashop 1.4 support
 * 1.3
 * - Token management for enhance front-end security
 * 1.2
 * - SSL compatibility
 * 1.1
 * - add preference for product's image size
 * - add preference for bloc size
 * 1.0.1
 * - Initial release
 *
 * */

class PM_CrossSellingOnCart extends Module
{
	private $_html;
	private $errors = array ();
	public	$base_config_url;
	private $defaultLanguage;
	private $languages;
	private $iso_lang;
	private $optionTitle;
	public $prefixFieldsOptions;

	function __construct($prefixFieldsOptions = 'PM_CSOC')
	{
		$this->name = 'pm_crosssellingoncart';
		if (_PS_VERSION_ < 1.4)
			$this->tab = 'Presta Module';
		else {
			$this->author = 'Presta-Module';
			$this->tab = 'merchandizing';
		}
		$this->version = '2.0.6';

		/* The parent construct is required for translations */
		parent::__construct();

		$this->page = basename(__FILE__, '.php');
		$this->displayName = $this->l('Cross selling on cart summary');
		$this->description = $this->l('Display a selection of products on the cart summary');


		$this->prefixFieldsOptions = $prefixFieldsOptions;

		$this->_fieldsOptions = array(
			$this->prefixFieldsOptions.'_IDENTICAL_PRODUCTY' => array('title' => $this->l('Display products already in cart ?'), 'desc' => '', 'cast' => 'intval', 'type' => 'bool','default'=> false),
			$this->prefixFieldsOptions.'_NB_PRODUCT' => array('title' => $this->l('Products quantity display'),'desc' => '', 'cast' => 'intval', 'type' => 'text','default'=> 3),
			$this->prefixFieldsOptions.'_IMAGE_SIZE' => array ('title' => $this->l('Product image size'), 'desc' => '', 'type' => 'select', 'list' => array (), 'identifier' => 'name', 'default' => 'medium' ),
			$this->prefixFieldsOptions.'_TITLE_BLOC' => array('title' => $this->l('Title Block'),'desc' => '', 'type' => 'textLang'),
			$this->prefixFieldsOptions.'_BLOC_WIDTH' => array('title' => $this->l('Block Width'),'desc' => '', 'cast' => 'intval', 'type' => 'text','default'=> 556),
			$this->prefixFieldsOptions.'_ACCESSORIES' => array('title' => $this->l('Display accessories'), 'desc' => '', 'cast' => 'intval', 'type' => 'bool','default'=> false),
		);
		$this->_fieldsOptions[$this->prefixFieldsOptions.'_NB_ACCESSORIES'] = array('title' => $this->l('Maximum accessories to display'),'desc' => '', 'cast' => 'intval', 'type' => 'text','default'=> 3,'bloc_id'=> 'NB_ACCESSORIES','script'=>'$("input[name='.$this->prefixFieldsOptions.'_ACCESSORIES'.']").click(function() {if($(this).val() == 1)$("#NB_ACCESSORIES").show();else $("#NB_ACCESSORIES").hide();});if($("input[name='.$this->prefixFieldsOptions.'_ACCESSORIES'.']").val() == 1)$("#NB_ACCESSORIES").show();else $("#NB_ACCESSORIES").hide();');
		$image = PM_CrossSellingOnCart::getImageType();
		foreach ( $image as $type )
			$this->_fieldsOptions [$this->prefixFieldsOptions.'_IMAGE_SIZE'] ['list'] [] = array ('name' => $type );
		$this->optionTitle = $this->l('Settings');


	}
	function install()
	{
		if (!parent::install())
			return false;
		Configuration::updateValue($this->prefixFieldsOptions.'_LAST_VERSION', $this->version);
		Configuration::updateValue($this->prefixFieldsOptions.'_PRODUCT_SELECTION', serialize(array()));
		$this->installDefaultConfig();
		if(!$this->registerHook('shoppingCart'))
			return false;
		return true;
	}
	public function installDefaultConfig() {
		foreach ( $this->_fieldsOptions as $key => $field ) {
			$val = $field ['default'];
			if (trim($val)) {
				if (is_array($val)) {
					$val = serialize($val);
				}
				if (Configuration::get($key) === false) {
					if (! Configuration::updateValue($key, $val))
						return false;
				}
			}
		}
		return true;
	}
	function uninstall()
	{
		$this->uninstallDefaultConfig();
		Configuration::deleteByName($this->prefixFieldsOptions.'_PRODUCT_SELECTION');
		return parent::uninstall();
	}
	public function uninstallDefaultConfig() {
		foreach ( $this->_fieldsOptions as $key => $field ) {
			Configuration::deleteByName($key);
		}
		return true;
	}
	public function checkIfModuleIsUpdate($updateDb = false, $displayConfirm = true) {
	  	if (! $updateDb && $this->version != Configuration::get($this->prefixFieldsOptions.'_LAST_VERSION'))
			return false;
		if ($updateDb) {
 			unset($_GET['makeUpdate']);
			Configuration::updateValue($this->prefixFieldsOptions.'_LAST_VERSION', $this->version);
			if ($displayConfirm) {
				$this->_html .= $this->displayConfirmation($this->l('Module updated successfully'));
			}
		}
		return true;
  	}
	public function isRegisteredInHook($hook)
	{
		return Db::getInstance()->getValue('
		SELECT COUNT(*)
		FROM `'._DB_PREFIX_.'hook_module` hm
		LEFT JOIN `'._DB_PREFIX_.'hook` h ON (h.`id_hook` = hm.`id_hook`)
		WHERE h.`name` = \''.pSQL($hook).'\'
		AND hm.`id_module` = '.(int)($this->id)
		);
	}
	public function saveConfig() {
		if (Tools::getValue('submitOptions_'.$this->prefixFieldsOptions)) {
			foreach ( $this->_fieldsOptions as $key => $field ) {
				if ($field['type'] == 'textLang' || $field['type'] == 'textareaLang')
				{
					$languages = Language::getLanguages();
					$list = array();
					foreach ($languages as $language)
						$list[$language['id_lang']] = (isset($field['cast']) ? $field['cast'](Tools::getValue($key.'_'.$language['id_lang'])) : Tools::getValue($key.'_'.$language['id_lang']));

					Configuration::updateValue($key, $list, ($field['type'] == 'textareaLang'?true:false));
				} else {
					if (! isset($field ['disable']))
						Configuration::updateValue($key, (isset($field ['cast']) ? $field ['cast'](Tools::getValue($key)) : Tools::getValue($key)));
				}
			}
			$this->_html .= $this->displayConfirmation($this->l('Configuration updated successfully'));
		}
	}
	function displayTitle() {
		echo '<h2>' . $this->displayName . '</h2>';
	}
	function displaySupport() {
		$this->_html .= '<br /><fieldset><legend>PrestaModule</legend>' . $this->l('This module was developed by PrestaModule.') . '<br/>' . $this->l('Thank you to reports any bugs to:') . ' <a href="mailto:support@presta-module.com">support@presta-module.com</a></fieldset>';
	}
	function displayConfig() {
		global $cookie;
		if (! isset($this->_fieldsOptions) or ! sizeof($this->_fieldsOptions))
			return;
		$defaultLanguage = intval(Configuration::get('PS_LANG_DEFAULT'));
		$languages = Language::getLanguages();
		$arrayKeysLang = array();

		$this->_html .= '<form action="' . $this->base_config_url . '" id="formGlobal_' . $this->name . '" name="form_' . $this->name . '" method="post" class="width3">
			<fieldset><legend><img src="'.__PS_BASE_URI__.'modules/'.$this->name.'/logo.gif" /> '.$this->optionTitle .'</legend>';
		foreach ( $this->_fieldsOptions as $key => $field ) {
			if($field ['type'] == 'textLang' || $field ['type'] == 'textareaLang') {
				$arrayKeysLang[] = $key;
			}
		}
		$keysLang = implode($arrayKeysLang,'¤');
		foreach ( $this->_fieldsOptions as $key => $field ) {
			$val = Tools::getValue($key, Configuration::get($key, false));
			if(isset($field ['bloc_id']) && $field ['bloc_id']) {
				$this->_html .= '<div id="'.$field ['bloc_id'].'">';
			}
			$this->_html .= '
			<label>' . $field ['title'] . ' </label>
			<div class="margin-form">';
			switch ($field ['type']) {
				case 'select' :
					$this->_html .= '<select name="' . $key . '">';
					foreach ( $field ['list'] as $value )
						$this->_html .= '<option
							value="' . (isset($field ['cast']) ? $field ['cast']($value [$field ['identifier']]) : $value [$field ['identifier']]) . '"' . (($val === false && isset($field ['default']) && $field ['default'] === $value [$field ['identifier']]) || ($val == $value [$field ['identifier']]) ? ' selected="selected"' : '') . '>' . $value ['name'] . '</option>';
						$this->_html .= '</select>';
					break;

				case 'bool' :
					$this->_html .= '<label class="t" for="' . $key . '_on"><img src="../img/admin/enabled.gif" alt="' . $this->l('Yes') . '" title="' . $this->l('Yes') . '" /></label>
						<input type="radio" name="' . $key . '" id="' . $key . '_on" value="1"' . ($val ? ' checked="checked"' : '') . '' . (isset($field ['disable']) && $field ['disable'] ? 'disabled="disabled"' : '') . ' />
						<label class="t" for="' . $key . '_on"> ' . $this->l('Yes') . '</label>
						<label class="t" for="' . $key . '_off"><img src="../img/admin/disabled.gif" alt="' . $this->l('No') . '" title="' . $this->l('No') . '" style="margin-left: 10px;" /></label>
						<input type="radio" name="' . $key . '" id="' . $key . '_off" value="0" ' . (! $val ? 'checked="checked"' : '') . '' . (isset($field ['disable']) && $field ['disable'] ? 'disabled="disabled"' : '') . '/>
						<label class="t" for="' . $key . '_off"> ' . $this->l('No') . '</label>';
					break;

				case 'textLang' :
					foreach ( $languages as $language ) {
						$val = Tools::getValue($key . '_' . $language ['id_lang'], Configuration::get($key, $language ['id_lang']));
						$this->_html .= '
							<div id="' . $key . '_' . $language ['id_lang'] . '" style="display: ' . ($language ['id_lang'] == $defaultLanguage ? 'block' : 'none') . '; float: left;">
							<input ' . (isset($field ['size']) ? 'size="' . $field ['size'] . '"' : '' ).' type="text" name="' . $key . '_' . $language ['id_lang'] . '" value="' . $val . '" />
							</div>';
					}
					$this->_html .= $this->displayFlags($languages, $defaultLanguage, $keysLang, $key,true);
					$this->_html .= '<br style="clear:both" />';
					break;

				case 'textareaLang':
					$this->initTinyMceAtEnd = true;
					foreach ($languages as $language) {
						$val = Tools::getValue($key.'_'.$language['id_lang'], Configuration::get($key, $language['id_lang']));
						$this->_html .= '
							<div id="'.$key.'_'.$language['id_lang'].'" style="display: ' . ($language ['id_lang'] == $defaultLanguage ? 'block' : 'none') . '; float: left;">
							<textarea class="rte" cols="100" rows="20" name="'.$key.'_'.$language['id_lang'].'">'.$val.'</textarea>
							</div>';
					}
					$this->_html .= $this->displayFlags($languages, $defaultLanguage, $keysLang, $key,true);
					$this->_html .= '<br style="clear:both">';
					break;

				case 'text' :
					default :
						$this->_html .= '<input type="text" name="' . $key . '" value="' . ($val === false && isset($field ['default']) && $field ['default'] ? $field ['default'] : $val) . '" size="20" />' . (isset($field ['suffix']) ? $field ['suffix'] : '');
			}
			$this->_html .= (isset($field ['desc']) ? ' &nbsp; <span>' . $field ['desc'] . '</span>' : '');
			$this->_html .= '</div>';
			if(isset($field ['bloc_id']) && $field ['bloc_id']) {
				$this->_html .= '</div>';
			}
			if(isset($field ['script']) && $field ['script']) {
				$this->_html .= '<script type="text/javascript">'.$field ['script'].'</script>';
			}
		}
		$this->_html .= '<center>
			<input type="submit" value="' . $this->l('   Save   ') . '" name="submitOptions_'.$this->prefixFieldsOptions.'" class="button" />
			</center>
			</fieldset>
			</form><br />';
	}
	function displayShareConfig($share_base_config_url) {
		$this->base_config_url = $share_base_config_url;
		$this->initClassVar();
		$this->_postProcess();
		$this->_html .= '<script type="text/javascript" src="' . $this->_path . 'js/admin.js"></script>';
		$this->displayConfig();
		$this->displayForm();
		return $this->_html;
	}

	public function initClassVar() {
		global $cookie, $currentIndex;
		if(!$this->base_config_url)
			$this->base_config_url = $currentIndex . '&configure=' . $this->name . '&token=' . Tools::getValue('token');
		$defaultLanguage = intval(Configuration::get('PS_LANG_DEFAULT'));
		$languages = Language::getLanguages();
		$this->defaultLanguage = $defaultLanguage;
		$this->iso_lang = Language::getIsoById($cookie->id_lang);
		$this->languages = $languages;
	}
	function includeAdminCssJs() {
		if (_PS_VERSION_ >= 1.4) {
			$this->_html .= '<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.11/jquery-ui.min.js" type="text/javascript"></script>';
		}
		else {
			$this->_html .= '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js" type="text/javascript"></script>
				<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.7.2/jquery-ui.min.js" type="text/javascript"></script>';
		}
		$this->_html .= '
			<link type="text/css" rel="stylesheet" href="' . $this->_path . 'css/admin.css" />
			<script type="text/javascript" src="' . $this->_path . 'js/admin.js"></script>
			<script type="text/javascript">
				var id_language = Number(' . $this->defaultLanguage . ');
				var modulePath = "' . $this->_path . '";
				var base_config_url = "' . $this->base_config_url . '";
			</script>
			';

	}
	public function _postProcess() {
		global $cookie;
		$this->saveConfig();
		if (isset($_GET ['orderProductSelection'])) {
			$order = $_GET ['orderProductSelection'] ?$_GET ['orderProductSelection'] : array ();
			Configuration::updateValue($this->prefixFieldsOptions.'_PRODUCT_SELECTION', serialize($order));
			ob_clean();
			echo $this->l('Saved');
			die();
		}
	}
	public static function getImageType()
	{
		$image = array ();
		$img = Db::getInstance()->ExecuteS('
			SELECT it. `id_image_type`, it.`name`, it.`products`, it.`width`, it.`height`
			FROM `' . _DB_PREFIX_ . 'image_type` it
			WHERE it.`products` = 1
		');
		foreach ( $img as $k => $name ) {
			$image [$k] = $name ['name'];
		}
		return $image;
	}
	public function getContent()
	{
		$this->initClassVar();
		$this->displayTitle();
		if (Tools::getValue('makeUpdate')) {
			$this->checkIfModuleIsUpdate(true);
		}
		if (! $this->checkIfModuleIsUpdate(false)) {
			$this->_html .= '<div class="warning warn clear">' . $this->l('Your module need to be update!') . '<br /><a href="' . $this->base_config_url . '&makeUpdate=1" class="button">' . $this->l('Please click here to update '.$this->displayName) . '.</a></div>';
		}
		else {
			$this->_postProcess();
			$this->includeAdminCssJs();
			$this->displayConfig();
			$this->displayForm();
		}
		$this->displaySupport();
		return $this->_html;
	}
	public function getElementProducts($products,$id_lang,$getProductsProperties = true)
	{
		if (_PS_VERSION_ >= 1.4) {
			$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->ExecuteS('
				SELECT p.*, pa.`id_product_attribute`, pl.`description`, pl.`description_short`, pl.`available_now`, pl.`available_later`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, i.`id_image`, il.`legend`, m.`name` AS manufacturer_name, tl.`name` AS tax_name, t.`rate`, cl.`name` AS category_default, DATEDIFF(p.`date_add`, DATE_SUB(NOW(), INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY)) > 0 AS new, (p.`price` * IF(t.`rate`,((100 + (t.`rate`))/100),1)) AS orderprice
				FROM `'._DB_PREFIX_.'product` p
				LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON (p.`id_product` = pa.`id_product` AND default_on = 1)
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (p.`id_category_default` = cl.`id_category` AND cl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'tax_rule` tr ON (p.`id_tax_rules_group` = tr.`id_tax_rules_group`
				AND tr.`id_country` = '.(int)Country::getDefaultCountryId().'
				AND tr.`id_state` = 0)
				LEFT JOIN `'._DB_PREFIX_.'tax` t ON (t.`id_tax` = tr.`id_tax`)
				LEFT JOIN `'._DB_PREFIX_.'tax_lang` tl ON (t.`id_tax` = tl.`id_tax` AND tl.`id_lang` = '.(int)($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON m.`id_manufacturer` = p.`id_manufacturer`
				WHERE p.`id_product` IN('.implode(',',$products).') AND p.`active` = 1
				/*ORDER BY FIELD(p.`id_product`,'.implode(',',$products).')*/
			');
		}  else {
			$result = Db::getInstance()->ExecuteS('
				SELECT p.*, pa.`id_product_attribute`, pl.`description`, pl.`description_short`, pl.`available_now`, pl.`available_later`, pl.`link_rewrite`, pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, i.`id_image`, il.`legend`, m.`name` AS manufacturer_name, tl.`name` AS tax_name, t.`rate`, cl.`name` AS category_default, DATEDIFF(p.`date_add`, DATE_SUB(NOW(), INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY)) > 0 AS new, (p.`price` * IF(t.`rate`,((100 + (t.`rate`))/100),1) - IF((DATEDIFF(`reduction_from`, CURDATE()) <= 0 AND DATEDIFF(`reduction_to`, CURDATE()) >=0) OR `reduction_from` = `reduction_to`, IF(`reduction_price` > 0, `reduction_price`, (p.`price` * IF(t.`rate`,((100 + (t.`rate`))/100),1) * `reduction_percent` / 100)),0)) AS orderprice
				FROM `'._DB_PREFIX_.'product` p
				LEFT JOIN `'._DB_PREFIX_.'product_attribute` pa ON (p.`id_product` = pa.`id_product` AND default_on = 1)
				LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (p.`id_category_default` = cl.`id_category` AND cl.`id_lang` = '.intval($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (p.`id_product` = pl.`id_product` AND pl.`id_lang` = '.intval($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
				LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.intval($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'tax` t ON t.`id_tax` = p.`id_tax`
				LEFT JOIN `'._DB_PREFIX_.'tax_lang` tl ON (t.`id_tax` = tl.`id_tax` AND tl.`id_lang` = '.intval($id_lang).')
				LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON m.`id_manufacturer` = p.`id_manufacturer`
				WHERE p.`id_product` IN('.implode(',',$products).') AND p.`active` = 1
				/*ORDER BY FIELD(p.`id_product`,'.implode(',',$products).')*/
			');
		}
		if($getProductsProperties)
			/* Modify SQL result */
			return Product::getProductsProperties($id_lang, $result);
		else return $result;
	}
	public function displayForm()
	{
		global $currentIndex, $cookie;
		$this->_html .= '
		<style>
		a.my {
			color: #680;
		}
		a.my:hover {
			color: #f90;
		}
		.list-item {
			display: block;
			height: 35px;
			line-height: 35px;
			margin: 2px 0;
			padding-left: 10px;
			background: #dedede url('._MODULE_DIR_.$this->name.'/images/background.gif);
		}
		span.delSelection {cursor: pointer;float:right;margin-top:10px;}
		</style>
		';
		$this->_html .= '
		<form action="'.$_SERVER['REQUEST_URI'].'" method="post" name="formProductSelection" class="width3">
			<fieldset>
				<legend><img src="'.__PS_BASE_URI__.'modules/'.$this->name.'/logo.gif" /> '.$this->l('Products').'</legend>
				';

		$productSelection = array();
		$postProductSelection = unserialize(Configuration::get($this->prefixFieldsOptions.'_PRODUCT_SELECTION'));
		if (is_array($postProductSelection) && sizeof($postProductSelection))
		{
			$productSelection = $this->getElementProducts($postProductSelection,$cookie->id_lang,false);
		}
		$this->_html .= '
		<table cellspacing="0" cellpadding="0" style="width: 90%;">
		<tr>
			<td style="padding-bottom:5px;">
				<p style="text-align:center;display:none;" id="csoc_noproduct">'.$this->l('No product').'</p>';

		$this->_html .= '		<ul id="divProductSelection" class="sortable">';
		if(sizeof($productSelection))
			foreach ($productSelection as $product)
				$this->_html .= '<div id="orderProductSelection-'.$product['id_product'].'" class="table list-item"><div style="width: 35px; float: left; padding-top: 9px;" class="dragHandle"><img src="'._MODULE_DIR_.$this->name.'/images/arrow.png">
				</div>'.$product['name'].' (Stock : '.$product['quantity'].')<span onclick="delProductToSelection('.$product['id_product'].');" class="delSelection"><img src="../img/admin/delete.gif" /></span></div>';

				$this->_html .= '	</ul>
					<div class="clear">&nbsp;</div>
							<script type="text/javascript">
								var formProduct;
								var accessories = new Array();

								function fillAccessories()
								{
									$.getJSON("'.__PS_BASE_URI__.'modules/pm_crosssellingoncart/ajax.php",{ajaxProduct:1,id_lang:'.intval($cookie->id_lang).'},
										function(j) {
											for (var i = 0; i < j.length; i++)
												accessories[i] = new Array(j[i].value, j[i].text);

											formProductSelection = document.layers ? document.forms.formProductSelection : document.formProductSelection;
											formProductSelection.selectProductSelection.length = accessories.length + 1;
											for (i = 1, j = 1; i < accessories.length; i++)
											{
												if (formProductSelection.filter.value)
													if (accessories[i][1].toLowerCase().indexOf(formProductSelection.filter.value.toLowerCase()) == -1)
														continue;
												formProductSelection.selectProductSelection.options[j].value = accessories[i][0];
												formProductSelection.selectProductSelection.options[j].text = accessories[i][1];
												j++;
											}
											if (j == 1)
											{
												formProductSelection.selectProductSelection.length = 2;
												formProductSelection.selectProductSelection.options[1].value = -1;
												formProductSelection.selectProductSelection.options[1].text = \''.$this->l('No match found').'\';
												formProductSelection.selectProductSelection.options.selectedIndex = 1;
											}
											else
											{
												formProductSelection.selectProductSelection.length = j;
												formProductSelection.selectProductSelection.options.selectedIndex = (formProductSelection.filter.value == \'\' ? 0 : 1);
											}
										}
									);
								}
							</script>
							<select id="selectProductSelection" name="selectProductSelection" style="width: 380px;" onfocus="fillAccessories();">
								<option value="0" selected="selected">-- '.$this->l('Choose').' --</option>
							</select>
							<span onclick="addProductToSelection();" style="cursor: pointer;"><img src="../img/admin/add.gif" alt="'.$this->l('Add this product').'" title="'.$this->l('Add this product').'" /></span>
							<br />'.$this->l('Filter:').' <input type="text" size="25" name="filter" onkeyup="fillAccessories();" class="space" />
						</td>
					</tr></table>
			</fieldset>
			</form>';
	}

	public static function getAccessoriesLight($id_products,$limit)
	{
		if(!sizeof($id_products)) return false;
		return Db::getInstance()->ExecuteS('
		SELECT `id_product_2` AS id_product
		FROM `'._DB_PREFIX_.'accessory`
		WHERE `id_product_1` IN('.(implode(',',$id_products)).')
		ORDER BY RAND()
		LIMIT '.(int)$limit);
	}
	function hookShoppingCart($params)
	{
		//if(Tools::getValue('ajax')) return;
		global $smarty, $cookie;
		if(isset($params['products'])) $cart_products = $params['products'];
		else {
			global $cart;
			$cart_products = $cart->getProducts(true);
		}
		$static_token = Tools::getToken(true);

		$displayProductAllready = Configuration::get($this->prefixFieldsOptions.'_IDENTICAL_PRODUCTY',false);
		$nbProductSelection = Configuration::get($this->prefixFieldsOptions.'_NB_PRODUCT');
		$nbAccessories = Configuration::get($this->prefixFieldsOptions.'_NB_ACCESSORIES');
		if(!$nbAccessories) $nbAccessories = 3;
		$image_size = Configuration::get($this->prefixFieldsOptions.'_IMAGE_SIZE');
		$bloc_title = Configuration::get($this->prefixFieldsOptions.'_TITLE_BLOC',$cookie->id_lang);
		$bloc_width = Configuration::get($this->prefixFieldsOptions.'_BLOC_WIDTH');
		$postProductSelection = @unserialize(Configuration::get($this->prefixFieldsOptions.'_PRODUCT_SELECTION'));
		$productSelection = array();

		//Product already on cart
		$productsOnCart = array();
		foreach($cart_products as $k=>$v) {
			$productsOnCart[] = $v['id_product'];
		}

		$productAccessories = false;
		if(Configuration::get($this->prefixFieldsOptions.'_ACCESSORIES')) {
			$productAccessories = $this->getAccessoriesLight($productsOnCart,$nbAccessories);

			if($productAccessories && sizeof($productAccessories))
				foreach ($productAccessories AS $product) {
					$postProductSelection[] = $product['id_product'];
				}
		}

		if (is_array($postProductSelection) && sizeof($postProductSelection))
		{
			// Check if products already in cart
			if (!$displayProductAllready) {
				$postProductSelection = array_diff($postProductSelection,$productsOnCart);
			}
			//rand product array
			//shuffle($postProductSelection);
			if (is_array($postProductSelection) && sizeof($postProductSelection)) {
				$productSelection = $this->getElementProducts($postProductSelection,$cookie->id_lang);
			}
		}

		$smarty->assign(array(
			'csoc_product_selection'=>$productSelection,
			'csoc_bloc_title'=>$bloc_title,
			'csoc_bloc_width'=>$bloc_width,
			'csoc_products_quantity'=>$nbProductSelection,
			'static_token'=>$static_token,
			'csoc_prefix'=>$this->prefixFieldsOptions,
			'imageSize'=> $image_size
		));

		return $this->display(__FILE__, 'pm_crosssellingoncart.tpl');
	}
	function hookMCBelow($params)
	{
		$this->prefixFieldsOptions = 'PM_MC_CSOC';
		return $this->hookShoppingCart($params);
	}
}