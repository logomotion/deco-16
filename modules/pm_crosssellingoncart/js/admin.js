$(function() {
	$("#divProductSelection").sortable({
			axis : "y",
			opacity : 0.7,
			update : function () {
				var order = $("#divProductSelection").sortable("serialize");
				saveProductSelection(order);
			}
	});
	var order = $("#divProductSelection").sortable("serialize");
	if(!order)$('#poc_noproduct').fadeIn('fast');
	$("#divProductSelection").disableSelection();
});

function saveProductSelection(order) {
	if(!order) order = 'orderProductSelection';
	$.get(base_config_url+'&'+order, { },function(data) {
		show_info('saveorder',data);
	});
}
var queue = false;
var next = false;
function show_info(id,content) {
	//alert(content);
	if(queue){next = new Array(id,content);return;}
	queue = true;
	if($('#'+id).is("div") === false)
		$('body').append('<div id="'+id+'" class="info_screen ui-state-hover"></div>');
	else return
	$('#'+id).html(content);
	$('#'+id).slideDown('slow');

	setTimeout(function() { $('#'+id).slideUp('slow',function() {$('#'+id).remove();queue = false;if(next){show_info(next[0],next[1]);next = false;} }) },2000);
}
function addProductToSelection()
{
	var valueToAdd = $('#selectProductSelection').val();

	if (valueToAdd == '0')
		return false;

	var $divProductSelection = $('#divProductSelection');

	pos = valueToAdd.indexOf('-');
	var productId = valueToAdd.slice(0, pos);
	var productName = valueToAdd.slice(pos + 1);

	/* delete product from select + add product line to the div, input_name, input_ids elements */
	$('#selectProductSelection option[value=' + valueToAdd + ']').remove();
	$divProductSelection.html($divProductSelection.html() +'<div id="orderProductSelection-'+productId+'" class="table list-item"><div style="width: 35px; float: left; padding-top: 9px;" class="dragHandle"><img src="../modules/pm_crosssellingoncart/images/arrow.png"></div>'+ productName + ' <span onclick="delProductToSelection(' + productId + ');"  class="delSelection"><img src="../img/admin/delete.gif" /></span></div>');
	var order = $("#divProductSelection").sortable("serialize");
	saveProductSelection(order);
	$('#poc_noproduct').fadeOut('fast');
}
function delProductToSelection(id)
{
	$('#orderProductSelection-'+id).remove();
	var order = $("#divProductSelection").sortable("serialize");
	if(!order)
		$('#poc_noproduct').fadeIn('fast');
	saveProductSelection(order);
}