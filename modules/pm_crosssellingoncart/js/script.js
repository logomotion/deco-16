$(document).ready(function(){
	displayCarousel();
});
function displayCarousel() {
	if(parseInt($('#'+csoc_prefix).parents(':hidden').length)) {
		setTimeout(function() {displayCarousel();},10);
	}
	else {
		$('#'+csoc_prefix).jcarousel({
	        visible: csoc_products_quantity,
	        itemLoadCallback:function() {
				if (typeof(tb_resizeOnShow) == 'function' && $().jquery < "1.4")
					tb_resize();
			}
	    });

	}
}