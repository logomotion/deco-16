<?php
include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../init.php');

if (!function_exists('json_encode'))
{
	function json_encode($a=false)
	{
		if (is_null($a)) return 'null';
		if ($a === false) return 'false';
		if ($a === true) return 'true';
		if (is_scalar($a))
		{
			if (is_float($a))
			{
				// Always use "." for floats.
				return floatval(str_replace(",", ".", strval($a)));
			}

			if (is_string($a))
			{
				static $jsonReplaces = array(array("\\", "/", "\n", "\t", "\r", "\b", "\f", '"'), array('\\\\', '\\/', '\\n', '\\t', '\\r', '\\b', '\\f', '\"'));
				return '"' . str_replace($jsonReplaces[0], $jsonReplaces[1], $a) . '"';
			}
			else
				return $a;
		}
		$isList = true;
		for ($i = 0, reset($a); $i < count($a); $i++, next($a))
		{
			if (key($a) !== $i)
			{
				$isList = false;
				break;
			}
		}
		$result = array();
		if ($isList)
		{
			foreach ($a as $v) $result[] = json_encode($v);
			return '[' . join(',', $result) . ']';
		}
		else
		{
			foreach ($a as $k => $v) $result[] = json_encode($k).':'.json_encode($v);
			return '{' . join(',', $result) . '}';
		}
	}
}

if (isset($_GET['ajaxProduct']))
{
	$jsonArray = array();

	$products = Db::getInstance()->ExecuteS('
	SELECT p.`id_product`, pl.`name`, p.`reference`, i.`id_image`
	FROM `'._DB_PREFIX_.'product` p
	NATURAL LEFT JOIN `'._DB_PREFIX_.'product_lang` pl
	LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product` AND i.`cover` = 1)
	WHERE pl.`id_lang` = '.intval(Tools::getValue('id_lang')).'');

	foreach ($products AS $accessory) {
		$curQty = Product::getQuantity($accessory['id_product']);
		$jsonArray[] = array("value"=>intval($accessory['id_product']).'-'.addslashes($accessory['name']).'-'.addslashes($accessory['reference']).' (stock : '.$curQty.')',"text"=>intval($accessory['id_product']).' - '.addslashes($accessory['name']).' - '.addslashes($accessory['reference']).' (stock : '.$curQty.')');
	}
	echo json_encode($jsonArray);
	die();
}
?>