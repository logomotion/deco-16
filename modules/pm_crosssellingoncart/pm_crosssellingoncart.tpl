{if count($csoc_product_selection) > 0}

<script type="text/javascript">
	var csoc_products_quantity = '{if sizeof($csoc_product_selection) < $csoc_products_quantity}{$csoc_product_selection|@sizeof}{else}{$csoc_products_quantity}{/if}';
	var csoc_prefix = '{$csoc_prefix}';
</script>

<div id="csoc" style="width:{$csoc_bloc_width}px;">
	<link href="{$smarty.const.__PS_BASE_URI__}modules/pm_crosssellingoncart/css/pm_crosssellingoncart.css" rel="stylesheet" type="text/css" media="all" />
	{if $csoc_bloc_title}<h2 class="csoc">{$csoc_bloc_title}</h2>{/if}

	<div id="csoc_list" style="width:{$csoc_bloc_width}px;">
		<ul id="{$csoc_prefix}" class="csoc_carousel" style="width:{$csoc_bloc_width}px;">
			{foreach from=$csoc_product_selection item='cartProduct' name=cartProduct}
			<li class="ajax_block_product">
				<a href="{$link->getProductLink($cartProduct.id_product, $cartProduct.link_rewrite, $cartProduct.category)}" title="{$cartProduct.name|htmlspecialchars}" class="product_image">
					<img src="{$link->getImageLink($cartProduct.link_rewrite, $cartProduct.id_image, $imageSize)}" alt="{$cartProduct.name|htmlspecialchars}" />
				</a>
				<h3 class="csoc"><a href="{$link->getProductLink($cartProduct.id_product, $cartProduct.link_rewrite, $cartProduct.category)}" title="{$cartProduct.name|htmlspecialchars}">{$cartProduct.name|truncate:25:'...'|escape:'htmlall':'UTF-8'}</a></h3>
				<p class="price_container">
					{if $smarty.const._PS_VERSION_ >= 1.4}
						{if (!$PS_CATALOG_MODE AND ((isset($cartProduct.show_price) && $cartProduct.show_price) || (isset($cartProduct.available_for_order) && $cartProduct.available_for_order)))}
							{if isset($cartProduct.show_price) && $cartProduct.show_price && !isset($restricted_country_mode)}
								<span class="price" style="display: inline;">{if !$priceDisplay}{convertPrice price=$cartProduct.price}{else}{convertPrice price=$cartProduct.price_tax_exc}{/if}</span><br />
								{if $cartProduct.price_without_reduction > $cartProduct.price && isset($cartProduct.reduction) && $cartProduct.reduction}
									{if $priceDisplay >= 0 && $priceDisplay <= 2}
										<span style="text-decoration: line-through;">{convertPrice price=$cartProduct.price_without_reduction}</span>
										{if $tax_enabled && $display_tax_label == 1}
											{if $priceDisplay == 1}{l s='tax excl.' mod='pm_crosssellingoncart'}{else}{l s='tax incl.' mod='pm_crosssellingoncart'}{/if}
										{/if}
										<br />
									{/if}
								{/if}
								{if isset($cartProduct.on_sale) && $cartProduct.on_sale && isset($cartProduct.show_price) && $cartProduct.show_price && !$PS_CATALOG_MODE}
									<span class="on_sale">{l s='On sale!' mod='pm_crosssellingoncart'}</span><br/>
								{elseif isset($cartProduct.reduction) && $cartProduct.reduction && isset($cartProduct.show_price) && $cartProduct.show_price && !$PS_CATALOG_MODE}
									<span class="discount">{l s='Reduced price!' mod='pm_crosssellingoncart'}</span><br/>
								{/if}
								{if isset($cartProduct.online_only) && $cartProduct.online_only}
									<span class="online_only">{l s='Online only!' mod='pm_crosssellingoncart'}</span>
								{/if}
							{/if}
							{if isset($cartProduct.available_for_order) && $cartProduct.available_for_order && !isset($restricted_country_mode)}
								<span class="availability">
									{if ($cartProduct.allow_oosp || $cartProduct.quantity > 0)}
										{l s='Available' mod='pm_crosssellingoncart'}
									{elseif (isset($cartProduct.quantity_all_versions) && $cartProduct.quantity_all_versions > 0)}
										{l s='Product available with different options' mod='pm_crosssellingoncart'}
									{else}
										{l s='Out of stock' mod='pm_crosssellingoncart'}
									{/if}
								</span>
							{/if}
						{/if}
					{else}
						<span class="price" style="display: inline;">{if !$priceDisplay}{convertPrice price=$cartProduct.price}{else}{convertPrice price=$cartProduct.price_tax_exc}{/if}</span><br />
						{if $cartProduct.price_without_reduction > $cartProduct.price && isset($cartProduct.reduction) && $cartProduct.reduction}
							{if $priceDisplay >= 0 && $priceDisplay <= 2}
								<span style="text-decoration: line-through;">{convertPrice price=$cartProduct.price_without_reduction}</span>
								{if $tax_enabled && $display_tax_label == 1}
									{if $priceDisplay == 1}{l s='tax excl.' mod='pm_crosssellingoncart'}{else}{l s='tax incl.' mod='pm_crosssellingoncart'}{/if}
								{/if}
								<br />
							{/if}
						{/if}
						{if $cartProduct.on_sale}
							<span class="on_sale">{l s='On sale!' mod='pm_crosssellingoncart'}</span><br/>
						{elseif ($cartProduct.reduction_price != 0 || $cartProduct.reduction_percent != 0) && ($cartProduct.reduction_from == $cartProduct.reduction_to OR ($smarty.now|date_format:'%Y-%m-%d %H:%M:%S' <= $cartProduct.reduction_to && $smarty.now|date_format:'%Y-%m-%d %H:%M:%S' >= $cartProduct.reduction_from))}
							<span class="discount">{l s='Reduced price!' mod='pm_crosssellingoncart'}</span><br/>
						{/if}
						<span class="availability">{if ($cartProduct.allow_oosp OR $cartProduct.quantity > 0)}{l s='Available' mod='pm_crosssellingoncart'}{else}{l s='Out of stock' mod='pm_crosssellingoncart'}{/if}</span>
					{/if}
				</p>
				{if ($cartProduct.quantity > 0 OR $cartProduct.allow_oosp) AND $cartProduct.customizable != 2}
					<a class="exclusive ajax_add_to_cart_button" rel="ajax_id_product_{$cartProduct.id_product}" href="{$base_dir}cart.php?qty=1&amp;id_product={$cartProduct.id_product}&amp;add&amp;token={$static_token}" title="{l s='Add to cart' mod='pm_crosssellingoncart'}">{l s='Add to cart' mod='pm_crosssellingoncart'}</a>
				{else}
					<span class="exclusive">{l s='Add to cart' mod='pm_crosssellingoncart'}</span>
				{/if}
			</li>
			{/foreach}
		</ul>
		<div class="clear"></div>
	</div>

	<script type="text/javascript" src="{$smarty.const.__PS_BASE_URI__}modules/pm_crosssellingoncart/js/jquery.jcarousel.min.js" rel="forceLoad"></script>
	<script type="text/javascript" src="{$smarty.const.__PS_BASE_URI__}modules/pm_crosssellingoncart/js/script.js" rel="forceLoad"></script>
	<div class="clear"></div>
</div>
{/if}