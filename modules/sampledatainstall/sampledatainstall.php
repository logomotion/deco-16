<?php
set_time_limit(600);
if (!defined('_PS_VERSION_'))
	exit;

class SampleDataInstall extends Module
{

	private $_html = '';
	
	public function __construct()
	{
		$this->name = 'sampledatainstall';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'TemplateMonster';
		$this->need_instance = 0;

		$this->bootstrap = true;
		parent::__construct();	

		$this->displayName = $this->l('Sample Data Install');
		$this->description = $this->l('Imports sample data to make your theme look like at template live demo. Imports sample products, categories, module settings, static pages etc.');
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}

	public function install()
	{
		if (!parent::install() || 
			!$this->registerHook('displayBackOfficeHeader') || 
			!Configuration::updateValue('SDI_SHOW_MASSEGE', 1))

			return false;

		return true;
	}

	public function hookdisplayBackOfficeHeader()
	{
		$show_massege = 0;
		if (Configuration::get('SDI_SHOW_MASSEGE') == 1)
			$show_massege = 1;

		// CSS
		$this->context->controller->addCss($this->_path.'views/templates/css/styles.css');
		// JS
		$this->context->controller->addJS($this->_path.'views/templates/js/tab.js');
		$this->context->controller->addJS($this->_path.'views/templates/js/import.js');

		$this->context->smarty->assign('vars', array(
			'moduleSDIUrl' => 'index.php?tab=AdminModules&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
			'baseDir'        => _PS_BASE_URL_.__PS_BASE_URI__.'modules/sampledatainstall/sampledatainstall-ajax.php',
			'show_message' => $show_massege
		));
		return $this->display(__FILE__, 'views/templates/admin/admin-header.tpl');
	}

	public function uninstall()
	{
		if (!parent::uninstall() || 
			!Configuration::deleteByName('SDI_SHOW_MASSEGE'))
			return false;
		return true;
	}

	public function removeMessage()
	{
		Configuration::updateValue('SDI_SHOW_MASSEGE', 0);	
	}

	public function _filesUpload($path)
	{
		/*** upload files from local storage ***/
		
		if (array_key_exists('file', $_FILES))
		{
			$file_name = basename($_FILES['file']['name']);
			$file_arr = explode('.', $file_name);
			$file_ext = $file_arr[(count($file_arr) - 1)]; // file extension
			$file_name = $file_arr['0'].'.'.strrev($file_ext);

			if (strrev($file_ext) == 'sql')
				$upload_file = $path.$file_name;
			else
			{
				
				$path_tmp = explode('@', $file_name);
				$file_insert_path = str_replace('\\', '/', _PS_ROOT_DIR_.'/'.str_replace('#', '/', $path_tmp[0]).'/');
				$file_name = $path_tmp[1];
				// check for path folder
				if (!is_dir($file_insert_path))
					@mkdir($file_insert_path, 0777, true);

				$upload_file = $file_insert_path.$file_name;
			}

			move_uploaded_file($_FILES['file']['tmp_name'], $upload_file);
		}
		exit;	
	}
	
	private function _sqlInstall($filename)
	{
		/*** install dump.sql ***/
		
		$lines = file($filename);
		// Temporary variable, used to store current query
		$templine = '';
		// Loop through each line
		foreach ($lines as $line)
		{
			// Skip it if it's a comment
			if (Tools::substr($line, 0, 2) == '--' || Tools::substr($line, 0, 2) == '/*' || $line == '')
				continue;

			// Add this line to the current segment
			$templine .= $line;
			// If it has a semicolon at the end, it's the end of the query
			if (Tools::substr(trim($line), -1, 1) == ';')
			{
				// Perform the query
				Db::getInstance()->execute($templine) || $this->_html .= 'Error performing query \'<strong>'.$templine.'\': '.mysql_error().'<br /><br />';
				// Reset temp variable to empty
				$templine = '';
			}
		}
		exit ('import_end');
	}

	public function _installSample()
	{
		/*** install sample data if isset dump.sql ***/

		$sample_folder = _PS_MODULE_DIR_.$this->name.'/import';
		// read dir
		if (is_dir($sample_folder))
		{
			$files = scandir($sample_folder);
			foreach ($files as $file) 
			{
				// check for bad file name
				if ($file != '.' && $file != '..')
				{
					//check for .sql file
					$file_arr = explode('.', $file);
					// get file extension
					if ($file_arr[(count($file_arr) - 1)] == 'sql')
						$this->_html .= $this->_sqlInstall($sample_folder.'/'.$file);
				}
			}
		}
	}

	private function _convertBytes($value )
	{
		/**** convert from byte to kb, mb, gb ***/
		
		if (is_numeric( $value ))
			return $value;
		else
		{
			$value_length = Tools::strlen($value);
			$qty = Tools::substr( $value, 0, $value_length - 1 );
			$unit = Tools::strtolower(Tools::substr( $value, $value_length - 1 ));
			switch ($unit)
			{
				case 'k':
					$qty *= 1024;
					break;
				case 'm':
					$qty *= 1048576;
					break;
				case 'b':
					$qty *= 1073741824;
					break;
			}
			return $qty;
		}
	}

	private function _getBrowser()
	{
		/*** get info about curent browser ***/
		
		$u_agent = $_SERVER['HTTP_USER_AGENT'];
		$bname = 'Unknown';
		$platform = 'Unknown';
		$version = "";
	
		//First get the platform?
		if (preg_match('/linux/i', $u_agent))
			$platform = 'linux';
		elseif (preg_match('/macintosh|mac os x/i', $u_agent))
			$platform = 'mac';
		elseif (preg_match('/windows|win32/i', $u_agent))
			$platform = 'windows';

		// Next get the name of the useragent yes seperately and for good reason
		if (preg_match('/MSIE/i', $u_agent) && !preg_match('/Opera/i', $u_agent))
		{
			$bname = 'Internet Explorer';
			$ub = "MSIE";
		}
		elseif (preg_match('/Firefox/i', $u_agent))
		{
			$bname = 'Mozilla Firefox';
			$ub = "Firefox";
		}
		elseif (preg_match('/Chrome/i', $u_agent))
		{
			$bname = 'Google Chrome';
			$ub = "Chrome";
		}
		elseif (preg_match('/Safari/i', $u_agent))
		{
			$bname = 'Apple Safari';
			$ub = "Safari";
		}
		elseif (preg_match('/Opera/i', $u_agent))
		{
			$bname = 'Opera';
			$ub = "Opera";
		}
		elseif (preg_match('/Netscape/i', $u_agent))
		{
			$bname = 'Netscape';
			$ub = "Netscape";
		}

		// finally get the correct version number
		$known = array('Version', $ub, 'other');
		$pattern = '#(?<browser>'.join('|', $known).')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
		if (!preg_match_all($pattern, $u_agent, $matches))
		{
			// we have no matching number just continue
		}

		// see how many we have
		$i = count($matches['browser']);
		if ($i != 1)
		{
			//we will have two since we are not using 'other' argument yet
			//see if version is before or after the name
			if (strripos($u_agent, "Version") < strripos($u_agent, $ub))
				$version = $matches['version'][0];
			else
				$version = $matches['version'][1];
		}
		else
			$version = $matches['version'][0];

		// check if we have a number
		if ($version == null || $version == "") 
			$version = "?";

		return array(
			'userAgent' => $u_agent,
			'name'      => $bname,
			'version'   => $version,
			'platform'  => $platform,
			'pattern'    => $pattern
		);
	}

	private function _compabilityServer()
	{
		/*** Check server settings ***/
		
		// correct settings for server
		$must_settings = array(
			'safe_mode'           => 'off',
			'file_uploads'        => 'on',
			'memory_limit'        => 128,
			'post_max_size'       => 8,
			'upload_max_filesize' => 8,
			'max_input_time'      => 45,
			'max_execution_time'  => 30
		);

		// curret server settings
		$current_settings = array();

		//result array
		$result = array();

		if (ini_get('safe_mode')) $current_settings['safe_mode'] = 'on';
			else $current_settings['safe_mode'] = 'off';
		if (ini_get('file_uploads')) $current_settings['file_uploads'] = 'on';
			else $current_settings['file_uploads'] = 'off';
		$current_settings['memory_limit'] = (int)ini_get('memory_limit');
		$current_settings['post_max_size'] = (int)ini_get('post_max_size');
		$current_settings['upload_max_filesize'] = (int)ini_get('upload_max_filesize');
		$current_settings['max_input_time'] = (int)ini_get('max_input_time');
		$current_settings['max_execution_time'] = (int)ini_get('max_execution_time');

		$diff = array_diff_assoc($must_settings, $current_settings);

		if (strcmp($must_settings['safe_mode'], $current_settings['safe_mode']))
			$result['safe_mode'] = $must_settings['safe_mode'];
		if (strcmp($must_settings['file_uploads'], $current_settings['file_uploads']))
			$result['file_uploads'] = $must_settings['file_uploads'];

		foreach ($diff as $key => $value)
		{
			if ($current_settings[$key] < $value)
				$result[$key] = $value;
		}
		if (!empty($result))
		{
			$output = '';
			$count = 0;
			foreach ($result as $key => $value)
			{
				$units = '';
				if ($key == 'memory_limit' || $key == 'post_max_size' || $key == 'upload_max_filesize')
					$units = ' (Mb)';
				if ($key == 'max_input_time' || $key == 'max_execution_time')
					$units = ' (s)';
				$output .= '<tr>';
				$output .= '<td>'.$key.$units.'</td>';
				$output .= '<td class="text-center">'.$current_settings[$key].'</td>';
				$output .= '<td class="text-center">'.$must_settings[$key].'</td>';
				$count++;
				if ($count == 3)
					$output .= '</tr>';
			}

			return $output;	
		}
	}

	private function _compabilityBrowser()
	{
		/*** check browser compability ***/
		
		$response = $this->_getBrowser();
		$browser_not_supported = $response['name'] == 'Internet Explorer' && $response['version'] <= 9 || $response['name'] == 'Safari' && $response['version'] <= 6 ? true : false;
		if ($browser_not_supported)
		{
			$this->context->smarty->assign('info', array(   
				'name' => $response['name'],
				'version' => $response['version']
			));

			return $this->display(__FILE__, 'views/templates/admin/old-browser.tpl');

		}
		else
			return true;
	}

	public function getContent()
	{   
		if ($this->_compabilityBrowser() === true)
			$this->_html .= $this->renderForm();
		else
			$this->_html .= $this->_compabilityBrowser();

		return $this->_html;
	}

	public function renderForm()
	{   				
		$this->context->smarty->assign('actions', array(   
			'postAction'     => 'index.php?tab=AdminModules&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
			'max_file_size'  => $this->_convertBytes(ini_get('upload_max_filesize')),
			'max_file_size_text'  => ini_get('upload_max_filesize'),
			'baseDir'        => _PS_BASE_URL_.__PS_BASE_URI__.'modules/sampledatainstall/sampledatainstall-ajax.php',
			'regenerateDir' => $this->context->link->getAdminLink('AdminImages').'#image_type_form',
			'output' => $this->_compabilityServer()
		)); 
	
		return $this->display(__FILE__, 'views/templates/admin/admin.tpl');
	}
}