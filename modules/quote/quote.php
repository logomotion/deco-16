<?php
 if (!defined('_CAN_LOAD_FILES_'))
	exit;

class Quote extends PaymentModule
{
	private $_html = '';
	private $_postErrors = array();

	public  $details;
	public  $owner;
	public	$address;

	public function __construct()
	{
		$this->name = 'quote';
		$this->tab = 'payments_gateways';
		$this->version = '1.1';
		
		$this->currencies = true;
		$this->currencies_mode = 'checkbox';

		$config = Configuration::getMultiple(array('Q_OTE_DETAILS', 'Q_OTE_OWNER', 'Q_OTE_ADDRESS'));
		if (isset($config['Q_OTE_OWNER']))
			$this->owner = $config['Q_OTE_OWNER'];
		if (isset($config['Q_OTE_DETAILS']))
			$this->details = $config['Q_OTE_DETAILS'];
		if (isset($config['Q_OTE_ADDRESS']))
			$this->address = $config['Q_OTE_ADDRESS'];

		parent::__construct();

		$this->displayName = $this->l('Quote');
		$this->description = $this->l('Accept payments after sign of the quote');
		$this->confirmUninstall = $this->l('Are you sure you want to delete your details?');
		if (!isset($this->owner) OR !isset($this->details) OR !isset($this->address))
			$this->warning = $this->l('Account owner and details must be configured in order to use this module correctly');
		if (!sizeof(Currency::checkPaymentCurrencies($this->id)))
			$this->warning = $this->l('No currency set for this module');
	}

	public function install()
	{ 	$query = 'INSERT INTO ' . _DB_PREFIX_ . 'order_state_lang (id_order_state, id_lang, name, template)  VALUES
(2104, 2,"Awaiting back quote", "quote"),
(2104, 1, "En attente du retour devis", "quote"),
(2104, 3, "En espera de pago por el retour quote", "quote");';
		Db::getInstance()->Execute($query);
		$query = 'INSERT INTO ' . _DB_PREFIX_ . 'order_state (id_order_state, invoice, send_email, color, unremovable, hidden, logable, delivery) VALUES 
(2104, 0, 1, "lightblue", 1, 0, 0, 0);';
		Db::getInstance()->Execute($query);
		copy('../modules/quote/mails/quote_mail_fr.html', '../mails/fr/quote.txt');			
		copy('../modules/quote/mails/quote_mail_fr.html', '../mails/fr/quote.html');
				copy('../modules/quote/mails/quote_mail_en.txt', '../mails/en/quote.txt');			
		copy('../modules/quote/mails/quote_mail_en.html', '../mails/en/quote.html');
				copy('../modules/quote/mails/quote_mail_es.txt', '../mails/es/quote.txt');			
		copy('../modules/quote/mails/quote_mail_es.html', '../mails/es/quote.html');
		if (!parent::install() OR !$this->registerHook('payment') OR !$this->registerHook('paymentReturn'))
			return false;
		return true;
	}

	public function uninstall()
	{/*
			 Db::getInstance()->Execute('ALTER TABLE `' . _DB_PREFIX_ . 'carrier_zone` DROP `ups_id`') AND 
					 parent::uninstall());*/
					 unlink  ('../mails/fr/quote.html');
					 unlink  ('../mails/fr/quote.txt');
					  unlink  ('../mails/en/quote.html');
					 unlink  ('../mails/en/quote.txt');
					  unlink  ('../mails/es/quote.html');
					 unlink  ('../mails/es/quote.txt');
					 return(Db::getInstance()->Execute('DELETE FROM ' . _DB_PREFIX_ . 'order_state_lang WHERE id_order_state = 2104') AND 
					 Db::getInstance()->Execute('DELETE FROM ' . _DB_PREFIX_ . 'order_state WHERE id_order_state = 2104') AND 
					  parent::uninstall());
					 
		if (!Configuration::deleteByName('Q_OTE_DETAILS')
				OR !Configuration::deleteByName('Q_OTE_OWNER')
				OR !Configuration::deleteByName('Q_OTE_ADDRESS')
				OR !parent::uninstall())
			return false;
		return true;
	}

	private function _postValidation()
	{
		if (isset($_POST['btnSubmit']))
		{
			if (empty($_POST['details']))
				$this->_postErrors[] = $this->l('account details are required.');
			elseif (empty($_POST['owner']))
				$this->_postErrors[] = $this->l('Account owner is required.');
		}
	}

	private function _postProcess()
	{
		if (isset($_POST['btnSubmit']))
		{
			Configuration::updateValue('Q_OTE_DETAILS', $_POST['details']);
			Configuration::updateValue('Q_OTE_OWNER', $_POST['owner']);
			Configuration::updateValue('Q_OTE_ADDRESS', $_POST['address']);
		}
		$this->_html .= '<div class="conf confirm"><img src="../img/admin/ok.gif" alt="'.$this->l('ok').'" /> '.$this->l('Settings updated').'</div>';
	}

	private function _displayQuote()
	{
		$this->_html .= '<img src="../modules/quote/devisquote.png" style="float:left; margin-right:15px;"><b>'.$this->l('This module allows your custommer to create a quote.').'</b><br /><br />
		'.$this->l('If the client chooses this payment mode, the order will change its status into a \'Waiting for payment\' status.').'<br />
		'.$this->l('Therefore, you will need to manually confirm the order as soon as you receive your quote sign..').'<br /><br /><br />';
	}

	private function _displayForm()
	{
		$this->_html .=
		'<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
			<fieldset>
			<legend><img src="../img/admin/contact.gif" />'.$this->l('Contact details').'</legend>
				<table border="0" width="500" cellpadding="0" cellspacing="0" id="form">
					<tr><td colspan="2">'.$this->l('Please specify your Adress  details for customers').'.<br /><br /></td></tr>
					<tr><td width="130" style="height: 35px;">'.$this->l('Account owner').'</td><td><input type="text" name="owner" value="'.htmlentities(Tools::getValue('owner', $this->owner), ENT_COMPAT, 'UTF-8').'" style="width: 300px;" /></td></tr>
					<tr>
						<td width="130" style="vertical-align: top;">'.$this->l('Details').'</td>
						<td style="padding-bottom:15px;">
							<textarea name="details" rows="4" cols="53">'.htmlentities(Tools::getValue('details', $this->details), ENT_COMPAT, 'UTF-8').'</textarea>
							<p>'.$this->l('Such as name society, Adress, etc.').'</p>
						</td>
					</tr>
					<tr>
						<td width="130" style="vertical-align: top;">'.$this->l('Address').'</td>
						<td style="padding-bottom:15px;">
							<textarea name="address" rows="4" cols="53">'.htmlentities(Tools::getValue('address', $this->address), ENT_COMPAT, 'UTF-8').'</textarea>
						</td>
					</tr>
					<tr><td colspan="2" align="center"><input class="button" name="btnSubmit" value="'.$this->l('Update settings').'" type="submit" /></td></tr>
				</table>
			</fieldset>
		</form>';
	}

	public function getContent()
	{
		$this->_html = '<h2>'.$this->displayName.'</h2>';

		if (!empty($_POST))
		{
			$this->_postValidation();
			if (!sizeof($this->_postErrors))
				$this->_postProcess();
			else
				foreach ($this->_postErrors AS $err)
					$this->_html .= '<div class="alert error">'. $err .'</div>';
		}
		else
			$this->_html .= '<br />';

		$this->_displayQuote();
		$this->_displayForm();

		return $this->_html;
	}

	public function execPayment($cart)
	{
		if (!$this->active)
			return ;
		if (!$this->_checkCurrency($cart))
			return ;

		global $cookie, $smarty;

		$smarty->assign(array(
			'nbProducts' => $cart->nbProducts(),
			'cust_currency' => $cookie->id_currency,
			'currencies' => $this->getCurrency(),
			'total' => $cart->getOrderTotal(true, 3),
			'isoCode' => Language::getIsoById(intval($cookie->id_lang)),
			'QuoteDetails' => nl2br2($this->details),
			'QuoteAddress' => nl2br2($this->address),
			'QuoteOwner' => $this->owner,
			'this_path' => $this->_path,
			'this_path_ssl' => Tools::getHttpHost(true, true).__PS_BASE_URI__.'modules/'.$this->name.'/'
		));

		return $this->display(__FILE__, 'payment_execution.tpl');
	}

	public function hookPayment($params)
	{
		if (!$this->active)
			return ;
		if (!$this->_checkCurrency($params['cart']))
			return ;

		global $smarty;

		$smarty->assign(array(
			'this_path' => $this->_path,
			'this_path_ssl' => Tools::getHttpHost(true, true).__PS_BASE_URI__.'modules/'.$this->name.'/'
		));
		return $this->display(__FILE__, 'payment.tpl');
	}

	public function hookPaymentReturn($params)
	{
		if (!$this->active)
			return ;
//define('_PS_OS_DEVIS_', 10);
		global $smarty;
		$state = $params['objOrder']->getCurrentState();
		define('_PS_OS_QUOTE_',      2104);
		if ($state == _PS_OS_QUOTE_ OR $state == _PS_OS_OUTOFSTOCK_)
			$smarty->assign(array(
				'total_to_pay' => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
				'QuoteDetails' => nl2br2($this->details),
				'QuoteAddress' => nl2br2($this->address),
				'QuoteOwner' => $this->owner,
				'status' => 'ok',
				'id_order' => $params['objOrder']->id
			));
		else
			$smarty->assign('status', 'failed');
		return $this->display(__FILE__, 'payment_return.tpl');
	}
	
	private function _checkCurrency($cart)
	{
		$currency_order = new Currency(intval($cart->id_currency));
		$currencies_module = $this->getCurrency();
		$currency_default = Configuration::get('PS_CURRENCY_DEFAULT');
		
		if (is_array($currencies_module))
			foreach ($currencies_module AS $currency_module)
				if ($currency_order->id == $currency_module['id_currency'])
					return true;
	}
}
