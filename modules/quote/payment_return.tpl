{if $status == 'ok'}
	<p>{l s='Your order on' mod='quote'} <span class="bold">{$shop_name}</span> {l s='is complete.' mod='quote'}
		<br /><br />
	 
		<br /><br />{l s='For any questions or for further information, please contact our' mod='quote'} <a href="{$base_dir_ssl}contact-form.php">{l s='customer support' mod='quote'}</a>.
	</p>
{else}
	<p class="warning">
		{l s='We have considered your request for quote, we will notify you very quickly.' mod='quote'} 
		<a href="{$base_dir_ssl}contact-form.php">{l s='customer support' mod='quote'}</a>.
	</p>
{/if}