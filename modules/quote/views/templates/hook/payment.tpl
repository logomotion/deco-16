{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <cyrilchalamon@gmail.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}
{foreach from=$products item='product' name='myLoop'}
 {if $product.active AND ($product.allow_oosp AND $product.stock_quantity > 0)}    
 {else}
{literal}
<style>
p.payment_module{display:none;}
p#quote_payment_module{display: block;}
</style>
{/literal}
 {/if}
{/foreach}
<p class="payment_module" id="quote_payment_module">
	<a href="{$link->getModuleLink('quote', 'payment', [], true)|escape:'html'}" title="{l s='Réaliser un devis.' mod='quote'}">
		<img src="{$this_path_quote}img/quote.jpg" alt="{l s='Réaliser un devis.' mod='quote'}" width="86" height="49" />
		{l s='Réaliser un devis' mod='quote'} {l s='(sera sauvegardé sur votre compte)' mod='quote'}
	</a>
</p>



