{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <contact@prestashop.com>
*  @copyright  2007-2015 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}



<!-- BlockQuote -->

<div class="col-md-12 col-sm-12 col-xs-12">

	<h3>Réalisez votre devis</h3>



	<img src="{$modules_dir}/quote/img/quote.jpg" width="100%"/>

	<p>{l s="Placez vos produits dans votre panier" mod='quote'}</p>

   	

   	<a href="{$link->getPageLink('authentication', true)|escape:'html':'UTF-8'}">

   		<p>{l s="Inscrivez-vous sur notre site" mod='quote'}</p>

   	</a>

	

	<a href="{$link->getModuleLink('quote', 'payment')|escape:'html'}" title="{l s='Réaliser un devis' mod='quote'}">

		<p>{l s="et recevez votre devis par email !" mod='quote'}</p>

	</a>

</div>

<!-- End BlockQuote -->