{*

* 2007-2014 PrestaShop

*

* NOTICE OF LICENSE

*

* This source file is subject to the Academic Free License (AFL 3.0)

* that is bundled with this package in the file LICENSE.txt.

* It is also available through the world-wide-web at this URL:

* http://opensource.org/licenses/afl-3.0.php

* If you did not receive a copy of the license and are unable to

* obtain it through the world-wide-web, please send an email

* to license@prestashop.com so we can send you a copy immediately.

*

* DISCLAIMER

*

* Do not edit or add to this file if you wish to upgrade PrestaShop to newer

* versions in the future. If you wish to customize PrestaShop for your

* needs please refer to http://www.prestashop.com for more information.

*

*  @author PrestaShop SA <contact@prestashop.com>

*  @copyright  2007-2014 PrestaShop SA

*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)

*  International Registered Trademark & Property of PrestaShop SA

*}

<div class="alert alert-info">

<img src="../modules/quote/img/quote.jpg" style="float:left; margin-right:15px;" width="86" height="49">

<p><strong>{l s="Ce module vous permet d'accepter les devis sur votre boutique." mod='quote'}</strong></p>

<p>{l s="Si le client choisit ce mode de paiement, l'état de la commande sera 'Devis.'" mod='quote'}</p>

<p>{l s="Il pourra alors remettre les produits dans son panier dans son espace personnel et passer commande dès validation du devis." mod='quote'}</p>

</div>

