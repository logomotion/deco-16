{*
* 2007-2014 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author PrestaShop SA <cyrilchalamon@gmail.com>
*  @copyright  2007-2014 PrestaShop SA
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}


{capture name=path}{l s='Devis' mod='quote'}{/capture}
<h2>{l s='Récapitulatif de votre panier' mod='quote'}</h2>
{if isset($nbProducts) && $nbProducts <= 0}
	<p class="warning">{l s='Votre panier est vide.' mod='quote'}</p>
{else}
{if !$logged}
<p>{l s='Vous devez être connecté pour réaliser un devis' mod='quote'}</p>
{else}
<h3>{l s='Devis' mod='quote'}</h3>
<h3>{$product_from}</h3>
<h3>{$product_id}</h3>
<h3>{$product_price}</h3>
<h3>{$product_name}</h3>
<h3>{$cust_email}</h3>
<h3>{$cust_id}</h3>
<h3>{$cust_address1}</h3>
<h3>{$default_carrier}</h3>
<form action="{$link->getModuleLink('quote', 'validation', [], true)|escape:'html'}" method="post">
	<p>
		<img src="{$this_path_quote}img/quote.jpg" alt="{l s='Devis' mod='quote'}" width="86" height="49" style="float:left; margin: 0px 10px 5px 0px;" />
		{l s='Vous avez choisi de réaliser un devis.' mod='quote'}
		<br/><br />
		{l s='En voici un bref récapitulatif :' mod='quote'}
	</p>
	<!--<p style="margin-top:20px;">
		- {l s='Le montant total de votre devis est de :' mod='quote'}
		<span id="amount" class="price">{displayPrice price=$total}</span>
		{if $use_taxes == 1}
			{l s='(taxes incl.)' mod='quote'}
		{/if}
	</p>-->
	<p>	-
		{if isset($currencies) && $currencies|@count > 1}
			{l s='Nous acceptons plusieurs devises.' mod='quote'}
			<br /><br />
			{l s='Veuillez en sélectionner une :' mod='quote'}
			<select id="currency_payement" name="currency_payement" onchange="setCurrency($('#currency_payement').val());">
			{foreach from=$currencies item=currency}
				<option value="{$currency.id_currency}" {if isset($currencies) && $currency.id_currency == $cust_currency}selected="selected"{/if}>{$currency.name}</option>
			{/foreach}
			</select>
		{else}
			{l s='Nous acceptons la devise suivante sur notre boutique :' mod='quote'}&nbsp;<b>{$currencies.0.name}</b>
			<input type="hidden" name="currency_payement" value="{$currencies.0.id_currency}" />
		{/if}
	</p>
	<p>
		<br /><br />
		<b>{l s='Merci de confirmer votre devis en cliquant sur \'Je confirme mon devis\'' mod='quote'}.</b>
	</p>
	<p class="cart_navigation clearfix" id="cart_navigation">
			<button type="submit" class="button btn btn-default button-medium">
				<span>Je confirme mon devis<i class="icon-chevron-right right"></i></span>
			</button>
	</p>
</form>
{/if}
{/if}


