{capture name=path}{l s='Send the Quote' mod='quote'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}

<h2>{l s='Order summary' mod='quote'}</h2>

{assign var='current_step' value='payment'}
{include file="$tpl_dir./order-steps.tpl"}

{if $nbProducts <= 0}
	<p class="warning">{l s='Your shopping cart is empty.'}</p>
{else}

<h3>{l s='Send the Quote' mod='quote'}</h3>
<form action="{$this_path_ssl}validation.php" method="post">
<p>
	<img src="{$this_path}devisquote.png" alt="{l s='quote' mod='quote'}" style="float:left; margin: 0px 10px 5px 0px;" />
	{l s='You have chosen to send  the Quote.' mod='quote'}
	<br/><br />
	{l s='Here is a short summary of your order:' mod='quote'}
</p>
<p style="margin-top:20px;">
	- {l s='The total amount of your order is' mod='quote'}
	<span id="amount" class="price">{displayPrice price=$total}</span>
	{l s='(tax incl.)' mod='quote'}
</p>
 
<p>
	 
	<br /><br />
	<b>{l s='Please confirm your order by clicking \'I confirm my order\'' mod='quote'}.</b>
</p>
<p class="cart_navigation">
	 
	<input type="submit" name="submit" value="{l s='I confirm my order' mod='quote'}" class="exclusive_large" />
</p>
</form>
{/if}
