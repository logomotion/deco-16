

[{shop_url}] 





Merci d'avoir effectué votre devis sur {shop_name}! 



Vous avez choisi d'effectuer un devis



N'hésitez pas à contacter notre service client pour davantage d'informations



Vous pouvez accéder à tout moment au suivi de votre commande dans "Historique des commandes"

[{history_url}] de la rubrique "Mon compte"

[{my_account_url}] sur notre site. 



Si vous avez un compte invité, vous pouvez suivre votre commande

dans la section "Suivi invité"

[{guest_tracking_url}?id_order={order_name}] de notre

site. 

RÉFÉRENCE

PRODUIT

PRIX UNITAIRE

QUANTITÉ

PRIX TOTAL

{products_txt} 

{discounts} 

PRODUITS 

{total_products} 

RÉDUCTIONS 

{total_discounts} 

PAQUET CADEAU 

{total_wrapping} 

LIVRAISON 

{total_shipping} 

TVA TOTALE 

{total_tax_paid} 

TOTAL PAYÉ 

{total_paid} 

TRANSPORTEUR : {carrier}

PAIEMENT : {payment} 		 

{delivery_block_txt} 

{invoice_block_txt} 


{shop_name} [{shop_url}]



