<?php

include(dirname(__FILE__).'/../../config/config.inc.php');
include(dirname(__FILE__).'/../../header.php');
include(dirname(__FILE__).'/quote.php');

$quote = new Quote();

if ($cart->id_customer == 0 OR $cart->id_address_delivery == 0 OR $cart->id_address_invoice == 0 OR !$quote->active)
	Tools::redirectLink(__PS_BASE_URI__.'order.php?step=1');

$currency = new Currency(intval(isset($_POST['currency_payement']) ? $_POST['currency_payement'] : $cookie->id_currency));
$total = floatval($cart->getOrderTotal(true, 3));

$mailVars = array(
	'{quote_owner}' => Configuration::get('Q_OTE_OWNER'),
	'{quote_details}' => nl2br(Configuration::get('Q_OTE_DETAILS')),
	'{quote_address}' => nl2br(Configuration::get('Q_OTE_ADDRESS'))
);
define('_PS_OS_QUOTE_',      2104);
$quote->validateOrder($cart->id, _PS_OS_QUOTE_, $total, $quote->displayName, NULL, $mailVars, $currency->id);
$order = new Order($quote->currentOrder);
Tools::redirectLink(__PS_BASE_URI__.'order-confirmation.php?id_cart='.$cart->id.'&id_module='.$quote->id.'&id_order='.$quote->currentOrder.'&key='.$order->secure_key);
?>