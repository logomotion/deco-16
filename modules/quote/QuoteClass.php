<?php

class QuoteClass extends ObjectModel

{
	public $idcart;
	public $idcustomer;
	public $totalpaid;
	public $shopid;
	public $quote_vars_sql;
	public $product_id;
	public $quotecart;
	public $cust_id;
	public $home_text;
	public $sidebar_text;
	public $footer_text;
	public $product_text;
	public $quote_name;
	public $quote_address;
	public $id_shop;

	public static $definition = array(
		'table' => 'quote_text',
		'primary' => 'id_quote_texts',
		'multilang' => true,
		'fields' => array(
			'id_shop' => array('type' => self::TYPE_INT, 'validate' => 'isunsignedInt', 'required' => true),
			'home_text' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString'),
			'sidebar_text' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString'),
			'footer_text' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString'),
			'product_text' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString'),
			'quote_name' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString'),
			'quote_address' => array('type' => self::TYPE_HTML, 'lang' => true, 'validate' => 'isString'),
		)
	);

	public static function getByIdShop($id_shop)
	{
		$id = Db::getInstance()->getValue('SELECT `id_quote_texts` FROM `'._DB_PREFIX_.'quote_text` WHERE `id_shop` ='.(int)$id_shop);

		return new QuoteClass($id);
	}

	public function copyFromPost()
	{
		/* Classical fields */
		foreach ($_POST as $key => $value)
		{
			if (key_exists($key, $this) && $key != 'id_'.$this->table)
				$this->{$key} = $value;
		}

		/* Multilingual fields */
		if (count($this->fieldsValidateLang))
		{
			$languages = Language::getLanguages(false);
			foreach ($languages as $language)
			{
				foreach ($this->fieldsValidateLang as $field => $validation)
				{
					if (Tools::getIsset($field.'_'.(int)$language['id_lang']))
						$this->{$field}[(int)$language['id_lang']] = $_POST[$field.'_'.(int)$language['id_lang']];
				}
			}
		}
	}

	public static function insertNewQuote($idcart,$idcustomer,$totalpaid,$shopid)
	{
		//Insertion des ID utiles dans la table quote
		Db::getInstance()->insert('quote', array(
			'id_quote_cart' => $idcart,
			'id_quote_customer' => $idcustomer,
			'quote_total' => $totalpaid,
			'id_quote_shop' => $shopid,
		));
	}

	public static function getQuoteId($idcart)
	{
		$id_quote_sql = Db::getInstance()->query(
			'SELECT `id_quote` FROM `'._DB_PREFIX_.'quote` WHERE `id_quote_cart` = '.$idcart.'');
		$IdQuote = $id_quote_sql->fetchAll();
		return $IdQuote;
	}

	public static function getQuoteOrderState()
	{
		$sql = "SELECT * FROM `"._DB_PREFIX_."order_state` WHERE `module_name` = 'quote'";
		if ($results = Db::getInstance()->ExecuteS($sql))
		foreach ($results as $row)
        $Quote_ID =  $row['id_order_state'];
    return $Quote_ID;
	}

	public static function getQuoteProductsInfos($product_id)
	{
		$sql = "SELECT * FROM `"._DB_PREFIX_."product_lang` WHERE `id_product` = ".$product_id."";
		if ($results = Db::getInstance()->ExecuteS($sql))
		foreach ($results as $row)
			$QuoteProductsInfos = $row;
    	return $QuoteProductsInfos;
	}

	public static function getQuoteProductsPrices($product_id)
	{
		$sql = "SELECT * FROM `"._DB_PREFIX_."product` WHERE `id_product` = ".$product_id."";
		if ($results = Db::getInstance()->ExecuteS($sql))
		foreach ($results as $row)
			$QuoteProductsPrices = $row;
    	return $QuoteProductsPrices;
	}

	public static function getCustomerAdress($cust_id)
	{
		$sql = "SELECT * FROM `"._DB_PREFIX_."address` WHERE `id_customer` = ".$cust_id."";
		if ($results = Db::getInstance()->ExecuteS($sql))
		foreach ($results as $row)
			$customer_adress = $row;
    	return $customer_adress;
	}

	public static function getCustomerFromId($cust_id)
	{
		$sql = "SELECT * FROM `"._DB_PREFIX_."customer` WHERE `id_customer` = ".$cust_id."";
		if ($results = Db::getInstance()->ExecuteS($sql))
		foreach ($results as $row)
			$customer_infos = $row;
    	return $customer_infos;
	}

	public static function getDefaultCarrier()
	{
		$sql = "SELECT * FROM `"._DB_PREFIX_."carrier` WHERE `position` = 1 AND `deleted` = 0 LIMIT 1";
		if ($results = Db::getInstance()->ExecuteS($sql))
		foreach ($results as $row)
			$default_carrier = $row;
    	return $default_carrier;
	}

	public static function addToCart($quotecart)
	{
		return true;
	}

}