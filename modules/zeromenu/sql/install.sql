CREATE TABLE IF NOT EXISTS `PREFIX_zmenu` (
	`id_zmenu` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`id_shop` int(10) unsigned NOT NULL DEFAULT 1,
	`active` tinyint(1) unsigned NOT NULL DEFAULT 1,
	`rightmenu` tinyint(1) unsigned DEFAULT 0,
	`position` int(10) unsigned NOT NULL DEFAULT 0,
	`label_color` varchar(32) DEFAULT NULL,
	`drop_column` int(10) DEFAULT 0,
	`drop_bgcolor` varchar(32) DEFAULT NULL,
	`drop_bgimage` varchar(128) DEFAULT NULL,
	`bgimage_position` varchar(50) DEFAULT NULL,
	`position_x` int(10) DEFAULT 0,
	`position_y` int(10) DEFAULT 0,
	PRIMARY KEY (`id_zmenu`, `id_shop`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_zmenu_lang` (
	`id_zmenu` int(10) unsigned NOT NULL,
	`id_lang` int(10) unsigned NOT NULL,
	`name` varchar(254) NOT NULL,
	`link` varchar(254) NOT NULL DEFAULT '',
	`label` varchar(128) DEFAULT NULL,
	PRIMARY KEY (`id_zmenu`,`id_lang`),
	KEY `name` (`name`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_zdropdown` (
	`id_zdropdown` int(10) unsigned NOT NULL AUTO_INCREMENT,
	`id_zmenu` int(10) unsigned NOT NULL,
	`active` tinyint(1) unsigned NOT NULL DEFAULT 1,
	`position` int(10) unsigned NOT NULL DEFAULT 0,
	`column` int(10) DEFAULT 0,
	`content_type` varchar(50) NOT NULL,
	PRIMARY KEY (`id_zdropdown`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_zdropdown_category` (
	`id_zdropdown` int(10) unsigned NOT NULL,
	`id_category` int(10) unsigned NOT NULL,
	PRIMARY KEY (`id_zdropdown`,`id_category`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_zdropdown_product` (
	`id_zdropdown` int(10) unsigned NOT NULL,
	`id_product` int(10) unsigned NOT NULL,
	PRIMARY KEY (`id_zdropdown`,`id_product`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_zdropdown_manufacturer` (
	`id_zdropdown` int(10) unsigned NOT NULL,
	`id_manufacturer` int(10) unsigned NOT NULL,
	PRIMARY KEY (`id_zdropdown`,`id_manufacturer`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `PREFIX_zdropdown_html` (
	`id_zdropdown` int(10) unsigned NOT NULL,
	`id_lang` int(10) unsigned NOT NULL,
	`static_content` text NOT NULL,
	PRIMARY KEY (`id_zdropdown`,`id_lang`)
) ENGINE=ENGINE_TYPE DEFAULT CHARSET=utf8;