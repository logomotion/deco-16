{*
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*}

<!-- MODULE Zero - Mega Menu -->
{if $zmenus}
<div class="zmega-menu">
<div id="zmegamenu">
  <div class="mobile-title visible-xs">{l s='Menu' mod='zeromenu'}</div>
  <ul class="znav-top clearfix">
  {foreach from=$zmenus item=menu}
	<li class="m-{$menu.id_zmenu} {if $menu.dropdowns}plex{/if} {if $menu.rightmenu}right{/if}">
		<a href="{$menu.link|escape:'htmlall':'UTF-8'}">{$menu.name|escape:'htmlall':'UTF-8'}
		{if $menu.label}<sup {if $menu.label_color}style="background-color: {$menu.label_color|escape:'htmlall':'UTF-8'};"{/if}>{$menu.label|escape:'htmlall':'UTF-8'}</sup>{/if}</a>
		{if $menu.dropdowns && $menu.drop_column}
		<span class="mobile-toggle-plus"></span>
		<div class="dropdown zdropdown-{$menu.drop_column|escape:'htmlall':'UTF-8'}" {if $menu.drop_bgcolor}style="background-color: {$menu.drop_bgcolor|escape:'htmlall':'UTF-8'};"{/if}>
			<div class="dropdown-bgimage" {if $menu.drop_bgimage}style="background-image: url('{$bg_image_url|escape:'htmlall':'UTF-8'}{$menu.drop_bgimage|escape:'htmlall':'UTF-8'}'); background-position: {$menu.bgimage_position|escape:'htmlall':'UTF-8'}; {$menu.position|escape:'htmlall':'UTF-8'}"{/if}></div>
			
			{foreach from=$menu.dropdowns item=dropdown}
			{if $dropdown.content_type != 'none' && $dropdown.column}
			<div class="dropdown-content zcontent-{$dropdown.column|escape:'htmlall':'UTF-8'} d-{$dropdown.id_zdropdown}">
				{if $dropdown.content_type == 'category'}
					{if $dropdown.categories}
					{foreach from=$dropdown.categories item=category name=categories}
					{if $smarty.foreach.categories.iteration%$dropdown.column == 1 || $dropdown.column == 1}<div class="category-line">{/if}
					<div class="category-item">
						<p class="category-title"><a href="{$link->getCategoryLink($category.id_category, $category.link_rewrite)|escape:'html':'UTF-8'}" title="">{$category.name|escape:'htmlall':'UTF-8'}</a></p>
						{if $category.subcategories}
						<ul>
							{foreach from=$category.subcategories item=subcategory}
							<li><a href="{$link->getCategoryLink($subcategory.id_category, $subcategory.link_rewrite)|escape:'html':'UTF-8'}" title="">{$subcategory.name|escape:'htmlall':'UTF-8'}</a></li>
							{/foreach}
						</ul>
						{/if}
					</div>
					{if $smarty.foreach.categories.iteration%$dropdown.column == 0 || $smarty.foreach.categories.last}</div>{/if}
					{/foreach}
					{/if}				
				{elseif $dropdown.content_type == 'product'}
					{if $dropdown.products}
					{foreach from=$dropdown.products item=product name=products}
					{if $smarty.foreach.products.iteration%$dropdown.column == 1 || $dropdown.column == 1}<div class="product-line">{/if}
					<div class="product-item">
						<p><a class="product_img_link" href="{$product.link|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" title=""><img class="replace-2x img-responsive" src="{$link->getImageLink($product.link_rewrite, $product.id_image, 'home_default')|escape:'html':'UTF-8'}" alt="{$product.name|escape:'html':'UTF-8'}" title="{$product.name|escape:'html':'UTF-8'}" /></a></p>
						<h5><a class="product-name" href="{$product.link|escape:'html':'UTF-8'}" title="">{$product.name|escape:'htmlall':'UTF-8'}</a></h5>
						{if $product.show_price && !isset($restricted_country_mode) && !$PS_CATALOG_MODE}
						<p><span class="price product-price">{if $priceDisplay != 1}{displayWtPrice p=$product.price}{else}{displayWtPrice p=$product.price_tax_exc}{/if}</span></p>
						{/if}
					</div>
					{if $smarty.foreach.products.iteration%$dropdown.column == 0 || $smarty.foreach.products.last}</div>{/if}
					{/foreach}
					{/if}
				{elseif $dropdown.content_type == 'manufacturer'}
					{if $dropdown.manufacturers}
					{foreach from=$dropdown.manufacturers item=manufacturer name=manufacturers}
					{if $smarty.foreach.manufacturers.iteration%$dropdown.column == 1 || $dropdown.column == 1}<div class="manufacturer-line">{/if}
					<div class="manufacturer-item">
						<p><a href="{$link->getManufacturerLink($manufacturer.id_manufacturer, $manufacturer.link_rewrite)|escape:'html':'UTF-8'}" title=""><img src="{$img_manu_dir}{$manufacturer.image|escape:'html':'UTF-8'}-medium_default.jpg" alt="" /></a></p>
						<p><a class="manufacturer-name" href="{$link->getManufacturerLink($manufacturer, $manufacturer.link_rewrite)|escape:'html':'UTF-8'}" title="">{$manufacturer.name|escape:'htmlall':'UTF-8'}</a></p>
					</div>
					{if $smarty.foreach.manufacturers.iteration%$dropdown.column == 0 || $smarty.foreach.manufacturers.last}</div>{/if}
					{/foreach}
					{/if}
				{elseif $dropdown.content_type == 'html'}
					{if $dropdown.static_content}
					<div class="html-item rte">
						{$dropdown.static_content|default:''}
					</div>
					{/if}
				{/if}
			</div>
			{/if}
			{/foreach}
		</div>
		{/if}
	</li>
  {/foreach}
  </ul>
</div>
</div>
{/if}			
<!-- /MODULE Zero - Mega Menu -->
