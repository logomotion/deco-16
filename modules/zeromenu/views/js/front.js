/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*
* Don't forget to prefix your containers with your own identifier
* to avoid any conflicts with others containers.
*/
function updateDropdownPosition()
{
	$('#zmegamenu .dropdown').each(function(index, element) {
		i = 0 - ($(this).outerWidth() - $(this).parent().outerWidth())/2;
	
		if ( $(this).offset().left - parseInt($(this).css('margin-left')) + i + $(this).outerWidth() > $('#zmegamenu').offset().left + $('#zmegamenu').outerWidth()) {
			max = $('#zmegamenu').offset().left - $(this).parent().offset().left + $('#zmegamenu').outerWidth() - $(this).outerWidth();
			$(this).css('margin-left', max + 'px');
		} else if ($(this).offset().left - parseInt($(this).css('margin-left')) + i < $('#zmegamenu').offset().left) {
			min = $('#zmegamenu').offset().left - $(this).parent().offset().left;
			$(this).css('margin-left', min + 'px');
		} else {
			$(this).css('margin-left', i + 'px');
		}
	});
}
function mobileHoverEvent()
{
	$('#zmegamenu .mobile-toggle-plus').toggle(function() {
		$(this).next('.dropdown').slideDown('slow');
		$(this).addClass('minus');
	}, function() {
		$(this).next('.dropdown').slideUp('slow');
		$(this).removeClass('minus');
	});
}
function enableHoverMenuOnTablet()
{
	if (parseInt($('#zmegamenu .dropdown').css('top')) > 0) 
	{
		$('html').on('touchstart', function(e) {
			$('#zmegamenu .znav-top > li').removeClass('hover');
		});
		$('#zmegamenu').on('touchstart', function (e) {
			e.stopPropagation();
		});
		$('#zmegamenu .znav-top > li.plex > a').on('touchstart', function (e) {
			'use strict'; //satisfy code inspectors		
			var li = $(this).parent('li');
			if (li.hasClass('hover')) {
				return true;
			} else {
				$('#zmegamenu .znav-top > li').removeClass('hover');
				li.addClass('hover');
				e.preventDefault();
				return false;
			}
		});
	}
}

$(document).ready(function() {
	setTimeout(function(){
		updateDropdownPosition();
	}, 500);
	$(window).resize(updateDropdownPosition);
	
	$('#zmegamenu .mobile-title').toggle(function() {
	  $('#zmegamenu .znav-top').slideDown(700);
	  $(this).addClass('hover');
	}, function() {
	  $('#zmegamenu .znav-top').stop();
	  $('#zmegamenu .znav-top').slideUp(300);
	  $(this).removeClass('hover');
	});
	
	mobileHoverEvent();
	enableHoverMenuOnTablet();
});