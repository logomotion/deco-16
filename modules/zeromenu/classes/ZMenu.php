<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class ZMenu extends ObjectModel
{
	public $id;
	public $id_zmenu;
	public $id_shop = 1;
	public $name;
	public $active = 1;
	public $rightmenu = 0;
	public $position;
	public $link;
	public $label;
	public $label_color;
	public $drop_column = 0;
	public $drop_bgcolor;
	public $drop_bgimage;
	public $bgimage_position = 'right bottom';
	public $position_x = 0;
	public $position_y = 0;

	public static $definition = array(
		'table' => 'zmenu',
		'primary' => 'id_zmenu',
		'multilang' => true,
		'multilang_shop' => false,
		'fields' => array(
			'id_shop' => 	array('type' => self::TYPE_INT, 'validate' => 'isUnsignedId'),
			'label_color' => array('type' => self::TYPE_STRING, 'validate' => 'isColor', 'size' => 32),
			'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
			'rightmenu' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool'),
			'position' => array('type' => self::TYPE_INT),
			'drop_column' => array('type' => self::TYPE_INT),
			'drop_bgcolor' => array('type' => self::TYPE_STRING, 'validate' => 'isColor', 'size' => 32),
			'drop_bgimage' => array('type' => self::TYPE_STRING, 'size' => 128),
			'bgimage_position' => array('type' => self::TYPE_STRING, 'size' => 50),
			'position_x' => array('type' => self::TYPE_INT),
			'position_y' => array('type' => self::TYPE_INT),

			'name' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isCleanHtml', 'required' => true, 'size' => 254),
			'link' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isUrlOrEmpty', 'size' => 254),
			'label' => array('type' => self::TYPE_STRING, 'lang' => true, 'validate' => 'isGenericName', 'size' => 128)
		),
	);

	public function __construct($id_zmenu = null, $id_lang = null, $id_shop = null)
	{
		parent::__construct($id_zmenu, $id_lang, $id_shop);

		if (!$this->position)
			$this->position = 1 + $this->getMaxPosition();
	}

	public static function getList($id_lang, $active = true)
	{
		$id_shop = (int)Context::getContext()->shop->id;

		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT *
			FROM `'._DB_PREFIX_.'zmenu` m
			LEFT JOIN `'._DB_PREFIX_.'zmenu_lang` ml ON m.`id_zmenu` = ml.`id_zmenu`
			WHERE 1 '.($id_lang ? 'AND `id_lang` = '.(int)$id_lang : '').' AND m.`id_shop` = '.$id_shop.'
			'.($active ? 'AND `active` = 1' : '').'
			'.(!$id_lang ? 'GROUP BY m.id_zmenu' : '')
			.'ORDER BY m.`position` ASC'
		);

		return $result;
	}

	public static function getMaxPosition()
	{
		return (int)Db::getInstance()->getValue('
			SELECT MAX(m.`position`)
			FROM `'._DB_PREFIX_.'zmenu` m'
		);
	}

	public static function updatePosition($id_zmenu, $position)
	{
		$query = 'UPDATE `'._DB_PREFIX_.'zmenu`
			SET `position` = '.(int)$position.'
			WHERE `id_zmenu` = '.(int)$id_zmenu;

		Db::getInstance()->execute($query);
	}
}
