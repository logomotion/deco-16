<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

class ZDropdown extends ObjectModel
{
	public $id;
	public $id_zdropdown;
	public $id_zmenu;
	public $active = 1;
	public $position;
	public $column = 1;
	public $content_type;

	public $categories = array();
	public $products = array();
	public $manufacturers = array();
	public $custom_html;

	public static $definition = array(
		'table' => 'zdropdown',
		'primary' => 'id_zdropdown',
		'multilang' => false,
		'multilang_shop' => false,
		'fields' => array(
			'id_zmenu' => array('type' => self::TYPE_INT, 'validate' => 'isUnsignedInt'),
			'active' => array('type' => self::TYPE_BOOL, 'validate' => 'isBool', 'required' => true),
			'position' => array('type' => self::TYPE_INT),
			'column' => array('type' => self::TYPE_INT),
			'content_type' => array('type' => self::TYPE_STRING, 'size' => 50)
		),
	);

	public function __construct($id_zdropdown = null, $id_zmenu = null)
	{
		parent::__construct($id_zdropdown);

		if ($id_zmenu)
			$this->id_zmenu = $id_zmenu;

		$this->getDropdownCategories();
		$this->getDropdownProducts();
		$this->getDropdownManufacturers();
		$this->getDropdownHTML();

		if (!$this->position)
			$this->position = 1 + $this->getMaxPositionInMenu($id_zmenu);
	}

	public static function getList($id_zmenu, $id_lang, $active = true)
	{
		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT *
			FROM `'._DB_PREFIX_.'zdropdown` d
			WHERE d.`id_zmenu` = '.(int)$id_zmenu.'
			'.($active ? 'AND `active` = 1' : '').'
			ORDER BY d.`position` ASC'
		);

		foreach ($result as &$row)
		{
			$zdropdown = new ZDropdown($row['id_zdropdown']);
			$row['categories'] = $zdropdown->getCategoriesDetails($id_lang);
			$row['products'] = $zdropdown->getProductsDetails($id_lang);
			$row['manufacturers'] = $zdropdown->getManufacturersDetails($id_lang);
			$row['static_content'] = isset($zdropdown->custom_html[$id_lang]) ? $zdropdown->custom_html[$id_lang] : false;
		}

		return $result;
	}

	public static function getMaxPositionInMenu($id_zmenu)
	{
		return (int)Db::getInstance()->getValue('
			SELECT MAX(d.`position`)
			FROM `'._DB_PREFIX_.'zdropdown` d
			WHERE d.`id_zmenu` = '.(int)$id_zmenu
		);
	}

	public static function updatePosition($id_zdropdown, $position)
	{
		$query = 'UPDATE `'._DB_PREFIX_.'zdropdown`
			SET `position` = '.(int)$position.'
			WHERE `id_zdropdown` = '.(int)$id_zdropdown;

		Db::getInstance()->execute($query);
	}

	public function getDropdownCategories()
	{
		$ret = array();

		if ($this->id_zdropdown)
		{
			$row = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT `id_category` FROM `'._DB_PREFIX_.'zdropdown_category`
				WHERE `id_zdropdown` = '.(int)$this->id_zdropdown
			);

			if ($row)
				foreach ($row as $val)
					$ret[] = $val['id_category'];
		}

		$this->categories = $ret;
	}

	public function getDropdownProducts()
	{
		$ret = array();

		if ($this->id_zdropdown)
		{
			$row = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT `id_product` FROM `'._DB_PREFIX_.'zdropdown_product`
				WHERE `id_zdropdown` = '.(int)$this->id_zdropdown
			);

			if ($row)
				foreach ($row as $val)
					$ret[] = $val['id_product'];
		}

		$this->products = $ret;
	}

	public function getDropdownManufacturers()
	{
		$ret = array();

		if ($this->id_zdropdown)
		{
			$row = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT `id_manufacturer` FROM `'._DB_PREFIX_.'zdropdown_manufacturer`
				WHERE `id_zdropdown` = '.(int)$this->id_zdropdown
			);

			if ($row)
				foreach ($row as $val)
					$ret[] = $val['id_manufacturer'];
		}

		$this->manufacturers = $ret;
	}

	public function getDropdownHTML()
	{
		$ret = array();

		if ($this->id_zdropdown)
		{
			$row = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT `id_lang`, `static_content` FROM `'._DB_PREFIX_.'zdropdown_html`
				WHERE `id_zdropdown` = '.(int)$this->id_zdropdown
			);

			if ($row)
				foreach ($row as $val)
					$ret[$val['id_lang']] = $val['static_content'];
		}

		$this->custom_html = $ret;
	}

	public function getCategoriesDetails($id_lang = null, $active = true)
	{
		if (!$id_lang)
			$id_lang = Context::getContext()->language->id;

		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT c.*, cl.id_lang, cl.name, cl.description, cl.link_rewrite, cl.meta_title, cl.meta_keywords, cl.meta_description
			FROM `'._DB_PREFIX_.'zdropdown_category` dc
			LEFT JOIN `'._DB_PREFIX_.'category` c ON c.`id_category` = dc.`id_category`
			'.Shop::addSqlAssociation('category', 'c').'
			LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (c.`id_category` = cl.`id_category` AND `id_lang` = '.(int)$id_lang.' '.Shop::addSqlRestrictionOnLang('cl').')
			WHERE `id_zdropdown` = '.(int)$this->id_zdropdown.'
			'.($active ? 'AND `active` = 1' : '').'
			GROUP BY c.`id_category`
			ORDER BY `level_depth` ASC, category_shop.`position` ASC'
		);

		if (!$result)
			return false;

		foreach ($result as &$row)
		{
			$row['id_image'] = Tools::file_exists_cache(_PS_CAT_IMG_DIR_.$row['id_category'].'.jpg') ? (int)$row['id_category'] : Language::getIsoById($id_lang).'-default';
			$row['legend'] = 'no picture';
		}

		return $result;
	}

	public function getProductsDetails($id_lang = null, $active = true)
	{
		$context = Context::getContext();

		if (!$id_lang)
			$id_lang = $context->language->id;

		$sql = 'SELECT p.*, product_shop.*, stock.out_of_stock, IFNULL(stock.quantity, 0) as quantity, pl.`description`, pl.`description_short`, pl.`link_rewrite`,
			pl.`meta_description`, pl.`meta_keywords`, pl.`meta_title`, pl.`name`, pl.`available_now`, pl.`available_later`,
			MAX(image_shop.`id_image`) id_image, il.`legend`, m.`name` as manufacturer_name, cl.`name` AS category_default,
			DATEDIFF(
				p.`date_add`,
				DATE_SUB(
					NOW(),
					INTERVAL '.(Validate::isUnsignedInt(Configuration::get('PS_NB_DAYS_NEW_PRODUCT')) ? Configuration::get('PS_NB_DAYS_NEW_PRODUCT') : 20).' DAY
				)
			) > 0 AS new
		FROM `'._DB_PREFIX_.'zdropdown_product` dp
		LEFT JOIN `'._DB_PREFIX_.'product` p ON p.`id_product` = dp.`id_product`
		'.Shop::addSqlAssociation('product', 'p').'
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (
			p.`id_product` = pl.`id_product`
			AND pl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('pl').'
		)
		LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (
			product_shop.`id_category_default` = cl.`id_category`
			AND cl.`id_lang` = '.(int)$id_lang.Shop::addSqlRestrictionOnLang('cl').'
		)
		LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product`)'.
		Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
		LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)$id_lang.')
		LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON (p.`id_manufacturer`= m.`id_manufacturer`)
		'.Product::sqlStock('p', 0).'
		WHERE `id_zdropdown` = '.(int)$this->id_zdropdown.'
		'.($active ? ' AND product_shop.`active` = 1 AND product_shop.`visibility` != \'none\'' : '').'
		GROUP BY product_shop.id_product';

		if (!$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS($sql))
			return false;

		return Product::getProductsProperties($id_lang, $result);
	}

	public function getManufacturersDetails($id_lang = null, $active = true)
	{
		if (!$id_lang)
			$id_lang = Context::getContext()->language->id;

		$manufacturers = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
			SELECT m.*, ml.`description`, ml.`short_description`
			FROM `'._DB_PREFIX_.'zdropdown_manufacturer` dm
			LEFT JOIN `'._DB_PREFIX_.'manufacturer` m ON m.`id_manufacturer` = dm.`id_manufacturer`
			'.Shop::addSqlAssociation('manufacturer', 'm').'
			INNER JOIN `'._DB_PREFIX_.'manufacturer_lang` ml ON (m.`id_manufacturer` = ml.`id_manufacturer` AND ml.`id_lang` = '.(int)$id_lang.')
			WHERE `id_zdropdown` = '.(int)$this->id_zdropdown.'
			'.($active ? 'AND m.`active` = 1' : '').'
			GROUP BY m.`id_manufacturer`
			ORDER BY m.`name`'
		);

		if (!$manufacturers)
			return false;

		$rewrite_settings = (int)Configuration::get('PS_REWRITING_SETTINGS');

		foreach ($manufacturers as &$item)
		{
			$item['image'] = (!file_exists(_PS_MANU_IMG_DIR_.$item['id_manufacturer'].'-'.ImageType::getFormatedName('medium').'.jpg')) ? Context::getContext()->language->iso_code.'-default' : $item['id_manufacturer'];
			$item['link_rewrite'] = ($rewrite_settings ? Tools::link_rewrite($item['name']) : 0);
		}

		return $manufacturers;
	}

	public function getProductsAutocompleteInfo($id_lang = null)
	{
		if (!$id_lang)
			$id_lang = Context::getContext()->language->id;

		$products = array();

		if (!empty($this->products))
		{
			$rows = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
				SELECT p.`id_product`, p.`reference`, pl.name
				FROM `'._DB_PREFIX_.'product` p
				LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.`id_product` = p.`id_product` AND pl.`id_lang` = '.(int)$id_lang.')
				WHERE p.`id_product` IN ('.implode(array_map('intval', $this->products), ',').')'
			);

			foreach ($rows as $row)
				$products[$row['id_product']] = trim($row['name']).(!empty($row['reference']) ? ' (ref: '.$row['reference'].')' : '');
		}

		return $products;
	}

	public function saveDropdownCategory($categories = array())
	{
		Db::getInstance()->execute(
			'DELETE FROM `'._DB_PREFIX_.'zdropdown_category`
			WHERE `id_zdropdown` = '.(int)$this->id
		);

		if (is_array($categories))
		{
			$dropdown_cats = array();
			foreach ($categories as $id_category)
			{
				$dropdown_cats[] = array(
					'id_category' => (int)$id_category,
					'id_zdropdown' => (int)$this->id
				);
			}
			Db::getInstance()->insert('zdropdown_category', $dropdown_cats);
		}

		return true;
	}

	public function saveDropdownProduct($products = array())
	{
		Db::getInstance()->execute(
			'DELETE FROM `'._DB_PREFIX_.'zdropdown_product`
			WHERE `id_zdropdown` = '.(int)$this->id
		);

		if (is_array($products))
		{
			$dropdown_products = array();
			foreach ($products as $id_product)
			{
				$dropdown_products[] = array(
					'id_product' => (int)$id_product,
					'id_zdropdown' => (int)$this->id
				);
			}
			Db::getInstance()->insert('zdropdown_product', $dropdown_products);
		}

		return true;
	}

	public function saveDropdownManufacturer($manufacturers = array())
	{
		Db::getInstance()->execute(
			'DELETE FROM `'._DB_PREFIX_.'zdropdown_manufacturer`
			WHERE `id_zdropdown` = '.(int)$this->id
		);

		if (is_array($manufacturers))
		{
			$dropdown_manufacturer = array();
			foreach ($manufacturers as $id_manufacturer)
			{
				$dropdown_manufacturer[] = array(
					'id_manufacturer' => (int)$id_manufacturer,
					'id_zdropdown' => (int)$this->id
				);
			}
			Db::getInstance()->insert('zdropdown_manufacturer', $dropdown_manufacturer);
		}

		return true;
	}

	public function saveDropdownHTML($static_content = array())
	{
		Db::getInstance()->execute(
			'DELETE FROM `'._DB_PREFIX_.'zdropdown_html`
			WHERE `id_zdropdown` = '.(int)$this->id
		);

		if (is_array($static_content))
		{
			$dropdown_html = array();
			foreach ($static_content as $id_lang => $content)
			{
				$dropdown_html[] = array(
					'id_lang' => (int)$id_lang,
					'static_content' => pSQL(Tools::purifyHTML($content), true),
					'id_zdropdown' => (int)$this->id
				);
			}
			Db::getInstance()->insert('zdropdown_html', $dropdown_html);
		}

		return true;
	}

	public static function getSubCategories($id_category, $id_lang = null, $active = true)
	{
		if (!$id_lang)
			$id_lang = Context::getContext()->language->id;

		$sql_groups_where = '';
		$sql_groups_join = '';
		if (Group::isFeatureActive())
		{
			$sql_groups_join = 'LEFT JOIN `'._DB_PREFIX_.'category_group` cg ON (cg.`id_category` = c.`id_category`)';
			$groups = FrontController::getCurrentCustomerGroups();
			$sql_groups_where = 'AND cg.`id_group` '.(count($groups) ? 'IN ('.implode(',', $groups).')' : '='.(int)Group::getCurrent()->id);
		}

		$result = Db::getInstance(_PS_USE_SQL_SLAVE_)->executeS('
		SELECT c.*, cl.id_lang, cl.name, cl.description, cl.link_rewrite, cl.meta_title, cl.meta_keywords, cl.meta_description
		FROM `'._DB_PREFIX_.'category` c
		'.Shop::addSqlAssociation('category', 'c').'
		LEFT JOIN `'._DB_PREFIX_.'category_lang` cl ON (c.`id_category` = cl.`id_category` AND `id_lang` = '.(int)$id_lang.' '.Shop::addSqlRestrictionOnLang('cl').')
		'.$sql_groups_join.'
		WHERE `id_parent` = '.(int)$id_category.'
		'.($active ? 'AND `active` = 1' : '').'
		'.$sql_groups_where.'
		GROUP BY c.`id_category`
		ORDER BY `level_depth` ASC, category_shop.`position` ASC');

		if (!$result)
			return false;

		foreach ($result as &$row)
		{
			$row['id_image'] = Tools::file_exists_cache(_PS_CAT_IMG_DIR_.$row['id_category'].'.jpg') ? (int)$row['id_category'] : Language::getIsoById($id_lang).'-default';
			$row['legend'] = 'no picture';
		}

		return $result;
	}
}
