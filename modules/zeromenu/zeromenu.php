<?php
/**
* 2007-2015 PrestaShop
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to license@prestashop.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author    PrestaShop SA <contact@prestashop.com>
*  @copyright 2007-2015 PrestaShop SA
*  @license   http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of PrestaShop SA
*/

if (!defined('_PS_VERSION_'))
	exit;

define('_BG_IMAGES_FOLDER_', 'views/img/bg_images/');
include_once dirname(__FILE__).'/classes/ZMenu.php';
include_once dirname(__FILE__).'/classes/ZDropdown.php';

class ZeroMenu extends Module
{
	public $html = '';

	public function __construct()
	{
		$this->name = 'zeromenu';
		$this->tab = 'front_office_features';
		$this->version = '1.9.0';
		$this->author = 'Mr.Zero';
		$this->need_instance = 0;
		$this->module_key = '4d6b538a66973fd0ea6aa801736dc021';

		$this->bootstrap = true;

		parent::__construct();

		$this->displayName = $this->l('Zero - Mega Menu');
		$this->description = $this->l('Mega Menu in the top of website.');

		$this->confirmUninstall = $this->l('Are you sure you want to uninstall this module?');

		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
	}

	public function install()
	{
		if (!file_exists(dirname(__FILE__).'/sql/install.sql'))
			return false;
		else if (!$sql = Tools::file_get_contents(dirname(__FILE__).'/sql/install.sql'))
			return false;
		$sql = str_replace(array('PREFIX_', 'ENGINE_TYPE'), array(_DB_PREFIX_, _MYSQL_ENGINE_), $sql);
		$sql = preg_split("/;\s*[\r\n]+/", trim($sql));

		foreach ($sql as $query)
			if (!Db::getInstance()->execute(trim($query)))
				return false;

		return parent::install() &&
			$this->registerHook('header') &&
			$this->registerHook('actionObjectCategoryUpdateAfter') &&
			$this->registerHook('actionObjectProductUpdateAfter') &&
			$this->registerHook('actionObjectManufacturerUpdateAfter') &&
			$this->registerHook('displayTop');
	}

	public function uninstall()
	{
		$sql = 'DROP TABLE
			`'._DB_PREFIX_.'zmenu`,
			`'._DB_PREFIX_.'zmenu_lang`,
			`'._DB_PREFIX_.'zdropdown`,
			`'._DB_PREFIX_.'zdropdown_category`,
			`'._DB_PREFIX_.'zdropdown_product`,
			`'._DB_PREFIX_.'zdropdown_manufacturer`,
			`'._DB_PREFIX_.'zdropdown_html`';

		if (!Db::getInstance()->execute($sql))
			return false;

		$this->_clearCache('*');

		return parent::uninstall();
	}

	private function about()
	{
		return '
		<div class="panel">
			<h3><i class="icon icon-circle"></i> '.$this->l('About').' '.$this->displayName.'</h3>
			<p><strong>'.$this->displayName.'</strong> module is the Flexible Mega Menu for the PrestaShop store.</p>
			<p>	- Version: 1.9<br/>
				- Author: Mr.Zero<br/>
				- Documentation: read file <a href="'._MODULE_DIR_.$this->name.'/documentation.pdf" target="_blank">documentation.pdf</a> to configure this module.
			</p>
		</div>		
		';
	}

	public function getContent()
	{
		$this->context->controller->addJqueryPlugin('tablednd');
		$this->context->controller->addJS($this->_path.'views/js/position.js');
		$this->context->controller->addJS($this->_path.'views/js/back.js');
		$this->context->controller->addCSS($this->_path.'views/css/back.css');

		$about = $this->about();
		// settings
		if (Tools::isSubmit('submitSettings'))
		{
			Configuration::updateValue('ZEROMENU_CUSTOM_CSS', Tools::getValue('ZEROMENU_CUSTOM_CSS'));

			$this->_clearCache('*');

			return $this->displayConfirmation($this->l('Settings updated')).$this->html.$this->renderMenuList().$this->renderSettingsForm().$about;
		}

		// zmenu
		elseif (Tools::isSubmit('savezeromenu'))
		{
			if ($this->processSaveMenu())
				return $this->html.$this->renderMenuList().$this->renderSettingsForm().$about;
			else
				return $this->html.$this->renderMenuForm();
		}
		elseif (Tools::isSubmit('addzeromenu') || Tools::isSubmit('updatezeromenu'))
			return $this->renderMenuForm();
		elseif (Tools::isSubmit('deleteBackgroundImage'))
		{
			$id_zmenu = (int)Tools::getValue('id_zmenu');
			$zmenu = new ZMenu($id_zmenu);
			if ($zmenu->drop_bgimage)
			{
				$image_path = $this->local_path._BG_IMAGES_FOLDER_.$zmenu->drop_bgimage;

				if (file_exists($image_path))
					unlink($image_path);

				$zmenu->drop_bgimage = null;
				$zmenu->update(false);
				$this->_clearCache('*');
			}

			Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&id_zmenu='.$id_zmenu.'&updatezeromenu&token='.Tools::getAdminTokenLite('AdminModules').'&conf=7');
		}
		elseif (Tools::isSubmit('deletezeromenu'))
		{
			$zmenu = new ZMenu((int)Tools::getValue('id_zmenu'));
			$zmenu->delete();
			$this->_clearCache('*');
			Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
		}
		elseif (Tools::isSubmit('statuszeromenu'))
		{
			$zmenu = new ZMenu((int)Tools::getValue('id_zmenu'));
			$zmenu->toggleStatus();
			$this->_clearCache('*');
			Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'));
		}
		elseif (Tools::getValue('updatePositions') == 'zeromenu')
			$this->updatePositionsMenu();

		//zdropdown
		elseif (Tools::isSubmit('savezerodropdown'))
		{
			if ($this->processSaveDropdown())
				return $this->html.$this->renderDropdownList();
			else
				return $this->html.$this->renderDropdownForm();
		}
		elseif (Tools::isSubmit('addzerodropdown') || Tools::isSubmit('updatezerodropdown'))
			return $this->renderDropdownForm();
		elseif (Tools::isSubmit('deletezerodropdown'))
		{
			$zdropdown = new ZDropdown((int)Tools::getValue('id_zdropdown'));
			$zdropdown->delete();
			$this->_clearCache('*');
			Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&listzerodropdown&id_zmenu='.(int)Tools::getValue('id_zmenu').'&token='.Tools::getAdminTokenLite('AdminModules'));
		}
		elseif (Tools::isSubmit('statuszerodropdown'))
		{
			$zdropdown = new ZDropdown((int)Tools::getValue('id_zdropdown'));
			$zdropdown->toggleStatus();
			$this->_clearCache('*');
			Tools::redirectAdmin(AdminController::$currentIndex.'&configure='.$this->name.'&listzerodropdown&id_zmenu='.(int)Tools::getValue('id_zmenu').'&token='.Tools::getAdminTokenLite('AdminModules'));
		}
		elseif (Tools::getValue('updatePositions') == 'zerodropdown')
			$this->updatePositionsDropdown();
		elseif (Tools::isSubmit('listzerodropdown'))
			return $this->renderDropdownList();

		// product autocomplete
		elseif (Tools::isSubmit('ajaxProductsList'))
			$this->ajaxProductsList();
		// main view
		else
			return $this->renderMenuList().$this->renderSettingsForm().$about;
	}

	protected function ajaxProductsList()
	{
		$query = Tools::getValue('q', false);
		if (!$query || $query == '' || Tools::strlen($query) < 1)
			die();
		if ($pos = strpos($query, ' (ref:'))
			$query = Tools::substr($query, 0, $pos);

		$sql = 'SELECT p.`id_product`, pl.`link_rewrite`, p.`reference`, pl.`name`, MAX(image_shop.`id_image`) id_image, il.`legend`
		FROM `'._DB_PREFIX_.'product` p
		LEFT JOIN `'._DB_PREFIX_.'product_lang` pl ON (pl.id_product = p.id_product AND pl.id_lang = '.(int)Context::getContext()->language->id.Shop::addSqlRestrictionOnLang('pl').')
		LEFT JOIN `'._DB_PREFIX_.'image` i ON (i.`id_product` = p.`id_product`)'.
		Shop::addSqlAssociation('image', 'i', false, 'image_shop.cover=1').'
		LEFT JOIN `'._DB_PREFIX_.'image_lang` il ON (i.`id_image` = il.`id_image` AND il.`id_lang` = '.(int)Context::getContext()->language->id.')
		WHERE (pl.name LIKE \'%'.pSQL($query).'%\' OR p.reference LIKE \'%'.pSQL($query).'%\')
		GROUP BY p.id_product';

		$items = Db::getInstance()->executeS($sql);

		if ($items)
			foreach ($items as $item)
				echo trim($item['name']).(!empty($item['reference']) ? ' (ref: '.$item['reference'].')' : '').'|'.(int)$item['id_product']."\n";
		else
			Tools::jsonEncode(new stdClass);
	}

	protected function updatePositionsMenu()
	{
		$positions = Tools::getValue('zmenu');

		if (empty($positions))
			return;

		foreach ($positions as $position => $value)
		{
			$pos = explode('_', $value);

			if (isset($pos[2]))
				ZMenu::updatePosition($pos[2], $position + 1);
		}

		$this->_clearCache('*');
	}

	protected function updatePositionsDropdown()
	{
		$positions = Tools::getValue('zdropdown');

		if (empty($positions))
			return;

		foreach ($positions as $position => $value)
		{
			$pos = explode('_', $value);

			if (isset($pos[2]))
				ZDropdown::updatePosition($pos[2], $position + 1);
		}

		$this->_clearCache('*');
	}

	protected function processSaveMenu()
	{
		$id_zmenu = (int)Tools::getValue('id_zmenu');
		if ($id_zmenu)
			$zmenu = new ZMenu($id_zmenu);
		else
			$zmenu = new ZMenu();

		$zmenu->id_shop = (int)$this->context->shop->id;
		$zmenu->position = (int)Tools::getValue('position');
		$zmenu->active = (int)Tools::getValue('active');
		$zmenu->rightmenu = (int)Tools::getValue('rightmenu');
		$zmenu->label_color = Tools::getValue('label_color');
		$zmenu->drop_column = Tools::getValue('drop_column');
		$zmenu->drop_bgcolor = Tools::getValue('drop_bgcolor');
		$zmenu->bgimage_position = Tools::getValue('bgimage_position');
		$zmenu->position_x = Tools::getValue('position_x');
		$zmenu->position_y = Tools::getValue('position_y');

		if (isset($_FILES['drop_bgimage']) && isset($_FILES['drop_bgimage']['tmp_name']) && !empty($_FILES['drop_bgimage']['tmp_name']))
		{
			if ($error = ImageManager::validateUpload($_FILES['drop_bgimage'], Tools::convertBytes(ini_get('upload_max_filesize'))))
				$this->html .= '<div class="alert alert-danger conf error">'.$error.'</div>';
			else
			{
				if (move_uploaded_file($_FILES['drop_bgimage']['tmp_name'], $this->local_path._BG_IMAGES_FOLDER_.$_FILES['drop_bgimage']['name']))
					$zmenu->drop_bgimage = $_FILES['drop_bgimage']['name'];
				else
					$this->html .= '<div class="alert alert-danger conf error">'.$this->l('File upload error.').'</div>';
			}
		}

		$languages = Language::getLanguages(false);
		$id_lang_default = (int)$this->context->language->id;
		$name = array();
		$link = array();
		$label = array();
		foreach ($languages as $lang)
		{
			$name[$lang['id_lang']] = Tools::getValue('name_'.$lang['id_lang']);
			if (!$name[$lang['id_lang']])
				$name[$lang['id_lang']] = Tools::getValue('name_'.$id_lang_default);
			$link[$lang['id_lang']] = Tools::getValue('link_'.$lang['id_lang']);
			if (!$link[$lang['id_lang']])
				$link[$lang['id_lang']] = Tools::getValue('link_'.$id_lang_default);
			$label[$lang['id_lang']] = Tools::getValue('label_'.$lang['id_lang']);
			if (!$label[$lang['id_lang']])
				$label[$lang['id_lang']] = Tools::getValue('label_'.$id_lang_default);
		}
		$zmenu->name = $name;
		$zmenu->link = $link;
		$zmenu->label = $label;

		$result = $zmenu->validateFields(false) && $zmenu->validateFieldsLang(false);
		if ($result)
		{
			$zmenu->save();
			$this->_clearCache('*');
		}
		else
			$this->html .= '<div class="alert alert-danger conf error">'.$this->l('An error occurred while attempting to save Menu.').'</div>';

		return $result;
	}

	protected function processSaveDropdown()
	{
		$id_zdropdown = (int)Tools::getValue('id_zdropdown');
		if ($id_zdropdown)
			$zdropdown = new ZDropdown($id_zdropdown);
		else
			$zdropdown = new ZDropdown();

		$zdropdown->id_zmenu = (int)Tools::getValue('id_zmenu');
		$zdropdown->active = (int)Tools::getValue('active');
		$zdropdown->position = (int)Tools::getValue('position');
		$zdropdown->column = Tools::getValue('column');
		$zdropdown->content_type = Tools::getValue('content_type');

		$result = $zdropdown->validateFields(false);
		if ($result)
		{
			$zdropdown->save();

			$categories = Tools::getValue('categories', array());
			$zdropdown->saveDropdownCategory($categories);

			$products = Tools::getValue('products', array());
			$zdropdown->saveDropdownProduct($products);

			$manufacturers = Tools::getValue('manufacturers', array());
			$zdropdown->saveDropdownManufacturer($manufacturers);

			$languages = Language::getLanguages(false);
			$static_content = array();
			foreach ($languages as $lang)
				$static_content[$lang['id_lang']] = Tools::getValue('static_content_'.$lang['id_lang']);

			$zdropdown->saveDropdownHTML($static_content);

			$this->_clearCache('*');
		}
		else
			$this->html .= '<div class="alert alert-danger conf error">'.$this->l('An error occurred while attempting to save Dropdown Content.').'</div>';

		return $result;
	}

	public function renderMenuList()
	{
		$fields_list = array(
			'id_zmenu' => array(
				'title' => $this->l('Menu ID'),
				'align' => 'center',
				'class' => 'fixed-width-xs',
				'orderby' => false,
				'search' => false,
				'type' => 'zid_menu',
			),
			'header' => array(
				'title' => $this->l('Header'),
				'orderby' => false,
				'search' => false,
				'type' => 'zmenu'
			),
			'dropdown' => array(
				'title' => $this->l('Dropdown Menu'),
				'orderby' => false,
				'search' => false,
				'type' => 'zdropdown'
			),
			'position' => array(
				'title' => $this->l('Position'),
				'align' => 'center',
				'orderby' => false,
				'search' => false,
				'class' => 'fixed-width-md',
				'position' => true,
				'type' => 'zposition',
			),
			'active' => array(
				'title' => $this->l('Displayed'),
				'active' => 'status',
				'type' => 'bool',
				'class' => 'fixed-width-xs',
				'align' => 'center',
				'orderby' => false,
				'search' => false,
			)
		);

		$zmenus = ZMenu::getList((int)$this->context->language->id, false);

		$helper = new HelperList();
		$helper->shopLinkType = '';
		$helper->toolbar_btn['new'] = array(
			'href' => AdminController::$currentIndex.'&configure='.$this->name.'&addzeromenu&token='.Tools::getAdminTokenLite('AdminModules'),
			'desc' => $this->l('New')
		);
		$helper->simple_header = false;
		$helper->listTotal = count($zmenus);
		$helper->identifier = 'id_zmenu';
		$helper->table = 'zeromenu';
		$helper->actions = array('edit', 'delete');
		$helper->show_toolbar = true;
		$helper->no_link = true;
		$helper->module = $this;
		$helper->title = $this->l('Mega Menu');
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
		$helper->position_identifier = 'zmenu';
		$helper->position_group_identifier = 0;

		return $helper->generateList($zmenus, $fields_list);
	}

	public function renderDropdownList()
	{
		$fields_list = array(
			'id_zdropdown' => array(
				'title' => $this->l('Dropdown ID'),
				'align' => 'center',
				'class' => 'fixed-width-xs',
				'orderby' => false,
				'search' => false,
				'type' => 'zid_dropdown',
			),
			'content_type' => array(
				'title' => $this->l('Content Type'),
				'orderby' => false,
				'search' => false,
				'type' => 'zdropdowntype'
			),
			'column' => array(
				'title' => $this->l('Column'),
				'orderby' => false,
				'search' => false,
				'type' => 'zdropdowncolumn'
			),
			'position' => array(
				'title' => $this->l('Position'),
				'align' => 'center',
				'orderby' => false,
				'search' => false,
				'class' => 'fixed-width-md',
				'position' => true,
				'type' => 'zposition',
			),
			'active' => array(
				'title' => $this->l('Displayed'),
				'active' => 'status',
				'type' => 'bool',
				'class' => 'fixed-width-xs',
				'align' => 'center',
				'orderby' => false,
				'search' => false,
			)
		);

		$id_zmenu = (int)Tools::getValue('id_zmenu');
		$zmenu = new ZMenu($id_zmenu, (int)$this->context->language->id);
		$zdropdowns = ZDropdown::getList($id_zmenu, (int)$this->context->language->id, false);

		$helper = new HelperList();
		$helper->shopLinkType = '';
		$helper->toolbar_btn['new'] = array(
			'href' => AdminController::$currentIndex.'&configure='.$this->name.'&addzerodropdown&id_zmenu='.$id_zmenu.'&token='.Tools::getAdminTokenLite('AdminModules'),
			'desc' => $this->l('New')
		);
		$helper->toolbar_btn['back'] = array(
			'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
			'desc' => $this->l('Back to Menu List'),
			'icon' => 'process-icon-back'
		);
		$helper->simple_header = false;
		$helper->listTotal = count($zdropdowns);
		$helper->identifier = 'id_zdropdown';
		$helper->table = 'zerodropdown';
		$helper->actions = array('edit', 'delete');
		$helper->show_toolbar = true;
		$helper->no_link = true;
		$helper->module = $this;
		$helper->title = '#'.$zmenu->id_zmenu.' - '.$zmenu->name.' &nbsp;>&nbsp; '.$this->l('Dropdown Contents');
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name.'&id_zmenu='.$id_zmenu;
		$helper->position_identifier = 'zdropdown';
		$helper->position_group_identifier = $zmenu->id_zmenu;

		return $helper->generateList($zdropdowns, $fields_list);
	}

	protected function renderSettingsForm()
	{
		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Settings'),
					'icon' => 'icon-cogs',
				),
				'input' => array(
					array(
						'type' => 'textarea',
						'label' => $this->l('Custom CSS'),
						'name' => 'ZEROMENU_CUSTOM_CSS',
						'desc'     => $this->l('Add custom style for Mega Menu.'),
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->module = $this;
		$helper->default_form_language = $this->context->language->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

		$helper->identifier = $this->identifier;
		$helper->submit_action = 'submitSettings';
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');

		$helper->tpl_vars = array(
			'fields_value' => $this->getSettingsFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id,
		);

		return $helper->generateForm(array($fields_form));
	}

	protected function renderMenuForm()
	{
		$id_zmenu = (int)Tools::getValue('id_zmenu');

		$legent_title = $this->l('Add New Menu');
		if ($id_zmenu)
			$legent_title = $this->l('Edit Menu');

		$list_columns = array();
		$list_columns[0]['id'] = 'drop_0_column';
		$list_columns[0]['value'] = 0;
		$list_columns[0]['label'] = $this->l('No Dropdown');
		for ($i = 1; $i < 6; $i++)
		{
			$list_columns[$i]['id'] = 'drop_'.$i.'_column';
			$list_columns[$i]['value'] = $i;
			$list_columns[$i]['label'] = $i.($i == 1 ? $this->l('col') : $this->l('cols'));
		}

		$list_positions = array(
			'query' => array(
				array('id' => 'left top', 'name' => $this->l('left top')),
				array('id' => 'left center', 'name' => $this->l('left center')),
				array('id' => 'left bottom', 'name' => $this->l('left bottom')),
				array('id' => 'right top', 'name' => $this->l('right top')),
				array('id' => 'right center', 'name' => $this->l('right center')),
				array('id' => 'right bottom', 'name' => $this->l('right bottom')),
				array('id' => 'center top', 'name' => $this->l('center top')),
				array('id' => 'center center', 'name' => $this->l('center center')),
				array('id' => 'center bottom', 'name' => $this->l('center bottom')),
			),
			'id' => 'id',
			'name' => 'name'
		);

		$image_url = false;
		$image_size = false;
		if ($id_zmenu)
		{
			$zmenu = new ZMenu($id_zmenu);
			if ($zmenu->drop_bgimage)
			{
				$image_url = $this->_path._BG_IMAGES_FOLDER_.$zmenu->drop_bgimage;
				$image_size = filesize($this->local_path._BG_IMAGES_FOLDER_.$zmenu->drop_bgimage) / 1000;
			}
		}

		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $legent_title,
					'icon' => 'icon-book',
				),
				'input' => array(
					array(
						'type' => 'hidden',
						'name' => 'id_zmenu',
					),
					array(
						'type' => 'text',
						'label' => $this->l('Name'),
						'name' => 'name',
						'lang' => true,
						'required' => true,
					),
					array(
						'type' => 'text',
						'label' => $this->l('URL'),
						'name' => 'link',
						'lang' => true,
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Displayed'),
						'name' => 'active',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => true,
								'label' => $this->l('Enabled')
							),
							array(
								'id' => 'active_off',
								'value' => false,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Right Menu'),
						'name' => 'rightmenu',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => true,
								'label' => $this->l('Enabled')
							),
							array(
								'id' => 'active_off',
								'value' => false,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type' => 'hidden',
						'name' => 'position'
					),
					array(
						'type' => 'text',
						'label' => $this->l('Label'),
						'name' => 'label',
						'lang' => true,
						'hint' => $this->l('Label for this menu. E.g. SALE, NEW, HOT,...'),
					),
					array(
						'type' => 'color',
						'label' => $this->l('Label Background Color'),
						'name' => 'label_color',
						'lang' => true,
						'hint' => $this->l('Background color of Label. Default is #e47911. Text color is white.'),
					),
					array(
						'type' => 'radio',
						'label' => $this->l('Dropdown Menu Columns'),
						'name' => 'drop_column',
						'values' => $list_columns,
						'hint' => $this->l('The number of columns of dropdown menu'),
					),
					array(
						'type' => 'color',
						'label' => $this->l('Dropdown Background Color'),
						'name' => 'drop_bgcolor',
						'hint' => $this->l('Config the background color for dropdown menu'),
					),
					array(
						'type' => 'file',
						'label' => $this->l('Dropdown Background Image'),
						'name' => 'drop_bgimage',
						'hint' => $this->l('Upload a new background image for dropdown menu from your computer'),
						'display_image' => true,
						'image' => $image_url ? '<img src="'.$image_url.'" alt="" class="img-thumbnail" style="max-width: 100%;" />' : false,
						'size' => $image_size,
						'delete_url' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules').'&deleteBackgroundImage&id_zmenu='.$id_zmenu,
					),
					array(
						'type' => 'select',
						'label' => $this->l('Background Image Position'),
						'name' => 'bgimage_position',
						'options' => $list_positions,
						'hint' => $this->l('The starting position of a background image')
					),
					array(
						'type' => 'text',
						'label' => $this->l('Position X'),
						'name' => 'position_x',
						'desc'     => $this->l('Unit is pixel. Negative values are allowed.'),
						'hint' => $this->l('The horizontal position. Default is 0px'),
					),
					array(
						'type' => 'text',
						'label' => $this->l('Position Y'),
						'name' => 'position_y',
						'desc'     => $this->l('Unit is pixel. Negative values are allowed.'),
						'hint' => $this->l('The vertical position. Default is 0px'),
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				),
				'buttons' => array(
					array(
						'href' => AdminController::$currentIndex.'&configure='.$this->name.'&token='.Tools::getAdminTokenLite('AdminModules'),
						'title' => $this->l('Back to Menu List'),
						'icon' => 'process-icon-back'
					)
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->module = $this;
		$helper->default_form_language = $this->context->language->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

		$helper->identifier = $this->identifier;
		$helper->submit_action = 'savezeromenu';
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');

		$helper->tpl_vars = array(
			'fields_value' => $this->getMenuFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id,
		);

		return $helper->generateForm(array($fields_form));
	}

	protected function renderDropdownForm()
	{
		$id_zmenu = (int)Tools::getValue('id_zmenu');
		$zmenu = new ZMenu($id_zmenu, (int)$this->context->language->id);
		$id_zdropdown = (int)Tools::getValue('id_zdropdown');
		$zdropdown = new ZDropdown($id_zdropdown, $id_zmenu, (int)$this->context->language->id);

		$legent_title = '#'.$zmenu->id_zmenu.' - '.$zmenu->name.' &nbsp;>&nbsp; '.$this->l('Add New Dropdown Content');
		if ($id_zdropdown)
			$legent_title = '#'.$zmenu->id_zmenu.' - '.$zmenu->name.' &nbsp;>&nbsp; '.$this->l('Edit Dropdown Content');

		$list_columns = array();
		for ($i = 1; $i <= $zmenu->drop_column; $i++)
		{
			$list_columns[$i]['id'] = 'content_'.$i.'_column';
			$list_columns[$i]['value'] = $i;
			$list_columns[$i]['label'] = $i.($i == 1 ? $this->l('col') : $this->l('cols'));
		}

		$content_type_options = array(
			'query' => array(
				0 => array('id' => 'none', 'name' => ''),
				1 => array('id' => 'category', 'name' => $this->l('Category')),
				2 => array('id' => 'product', 'name' => $this->l('Product')),
				3 => array('id' => 'html', 'name' => $this->l('Custom HTML')),
				4 => array('id' => 'manufacturer', 'name' => $this->l('Manufacturer'))
			),
			'id' => 'id',
			'name' => 'name'
		);

		$manufacturers = Manufacturer::getManufacturers();
		$list_manufacturer = array();
		if ($manufacturers)
			foreach ($manufacturers as $manufacturer)
				$list_manufacturer[$manufacturer['id_manufacturer']] = $manufacturer['name'];

		$fields_form = array(
			'form' => array(
				'legend' => array(
					'title' => $legent_title,
					'icon' => 'icon-book',
				),
				'input' => array(
					array(
						'type' => 'hidden',
						'name' => 'id_zmenu',
					),
					array(
						'type' => 'hidden',
						'name' => 'id_zdropdown',
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Displayed'),
						'name' => 'active',
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => true,
								'label' => $this->l('Enabled')
							),
							array(
								'id' => 'active_off',
								'value' => false,
								'label' => $this->l('Disabled')
							)
						),
					),
					array(
						'type' => 'hidden',
						'name' => 'position'
					),
					array(
						'type' => 'radio',
						'label' => $this->l('Content Columns'),
						'name' => 'column',
						'values' => $list_columns,
						'hint' => $this->l('The number of columns of dropdown content. Maximum value is "Dropdown Menu Columns"'),
					),
					array(
						'type' => 'select',
						'label' => $this->l('Content Type'),
						'name' => 'content_type',
						'id' => 'content_type_selectbox',
						'options' => $content_type_options,
						'hint' => $this->l('Dropdown Content Type.')
					),
					array(
						'type' => 'categories',
						'label' => $this->l('Select the Root Categories'),
						'name' => 'categories',
						'hint' => $this->l('Dropdown content will display the subcategories of Root Categories.'),
						'tree' => array(
							'use_search' => false,
							'id' => 'categoryBox',
							'use_checkbox' => true,
							'selected_categories' => $zdropdown->categories,
						)
					),
					array(
						'type' => 'product_autocomplete',
						'label' => $this->l('Select the Products'),
						'name' => 'products',
						'id' => 'productBox',
						'ajax_path' => AdminController::$currentIndex.'&configure='.$this->name.'&ajax=1&ajaxProductsList&token='.Tools::getAdminTokenLite('AdminModules'),
						'hint' => $this->l('Begin typing the First Letters of the Product Name, then select the Product from the Drop-down List.')
					),
					array(
						'type' => 'textarea',
						'label' => $this->l('Custom HTML Content'),
						'name' => 'static_content',
						'id' => 'static_content_textarea',
						'autoload_rte' => true,
						'lang' => true,
						'rows' => 10,
						'cols' => 100,
					),
					array(
						'type' => 'manufacturer',
						'label' => $this->l('Select the Manufacturers'),
						'name' => 'manufacturers',
						'id' => 'manufacturerBox',
						'list_manufacturer' => $list_manufacturer,
					),
				),
				'submit' => array(
					'title' => $this->l('Save'),
				),
				'buttons' => array(
					array(
						'href' => AdminController::$currentIndex.'&configure='.$this->name.'&listzerodropdown&id_zmenu='.$id_zmenu.'&token='.Tools::getAdminTokenLite('AdminModules'),
						'title' => $this->l('Back to Dropdown List'),
						'icon' => 'process-icon-back'
					)
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->module = $this;
		$helper->default_form_language = $this->context->language->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG', 0);

		$helper->identifier = $this->identifier;
		$helper->submit_action = 'savezerodropdown';
		$helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name.'&listzerodropdown&id_zmenu='.$id_zmenu;
		$helper->token = Tools::getAdminTokenLite('AdminModules');

		$helper->tpl_vars = array(
			'fields_value' => $this->getDropdownFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id,
		);

		return $helper->generateForm(array($fields_form));
	}

	protected function getSettingsFieldsValues()
	{
		return array(
			'ZEROMENU_CUSTOM_CSS' => Tools::getValue('ZEROMENU_CUSTOM_CSS', Configuration::get('ZEROMENU_CUSTOM_CSS')),
		);
	}

	protected function getMenuFieldsValues()
	{
		$fields_value = array();

		$id_zmenu = (int)Tools::getValue('id_zmenu');
		$zmenu = new ZMenu($id_zmenu);

		$languages = Language::getLanguages(false);
		foreach ($languages as $lang)
		{
			$default_name = isset($zmenu->name[$lang['id_lang']]) ? $zmenu->name[$lang['id_lang']] : '';
			$fields_value['name'][$lang['id_lang']] = Tools::getValue('name_'.(int)$lang['id_lang'], $default_name);
			$default_link = isset($zmenu->link[$lang['id_lang']]) ? $zmenu->link[$lang['id_lang']] : '';
			$fields_value['link'][$lang['id_lang']] = Tools::getValue('link_'.(int)$lang['id_lang'], $default_link);
			$default_label = isset($zmenu->label[$lang['id_lang']]) ? $zmenu->label[$lang['id_lang']] : '';
			$fields_value['label'][$lang['id_lang']] = Tools::getValue('label_'.(int)$lang['id_lang'], $default_label);
		}

		$fields_value['id_zmenu'] = $id_zmenu;
		$fields_value['active'] = Tools::getValue('active', $zmenu->active);
		$fields_value['rightmenu'] = Tools::getValue('rightmenu', $zmenu->rightmenu);
		$fields_value['position'] = Tools::getValue('position', $zmenu->position);
		$fields_value['label_color'] = Tools::getValue('label_color', $zmenu->label_color);
		$fields_value['drop_column'] = Tools::getValue('drop_column', $zmenu->drop_column);
		$fields_value['drop_bgcolor'] = Tools::getValue('drop_bgcolor', $zmenu->drop_bgcolor);
		$fields_value['drop_bgimage'] = Tools::getValue('drop_bgimage', $zmenu->drop_bgimage);
		$fields_value['bgimage_position'] = Tools::getValue('bgimage_position', $zmenu->bgimage_position);
		$fields_value['position_x'] = Tools::getValue('position_x', $zmenu->position_x);
		$fields_value['position_y'] = Tools::getValue('position_y', $zmenu->position_y);

		return $fields_value;
	}

	protected function getDropdownFieldsValues()
	{
		$fields_value = array();

		$id_zmenu = (int)Tools::getValue('id_zmenu');
		$id_zdropdown = (int)Tools::getValue('id_zdropdown');
		$zdropdown = new ZDropdown($id_zdropdown, $id_zmenu);

		$fields_value['id_zdropdown'] = $id_zdropdown;
		$fields_value['id_zmenu'] = $zdropdown->id_zmenu;
		$fields_value['active'] = Tools::getValue('active', $zdropdown->active);
		$fields_value['position'] = Tools::getValue('position', $zdropdown->position);
		$fields_value['column'] = Tools::getValue('column', $zdropdown->column);
		$fields_value['content_type'] = Tools::getValue('content_type', $zdropdown->content_type);
		$fields_value['products'] = $zdropdown->getProductsAutocompleteInfo($this->context->language->id);
		$fields_value['manufacturers'] = $zdropdown->manufacturers;

		$languages = Language::getLanguages(false);
		foreach ($languages as $lang)
		{
			$default_static_content = isset($zdropdown->custom_html[$lang['id_lang']]) ? $zdropdown->custom_html[$lang['id_lang']] : '';
			$fields_value['static_content'][$lang['id_lang']] = Tools::getValue('static_content_'.(int)$lang['id_lang'], $default_static_content);
		}

		return $fields_value;
	}

	public function hookHeader()
	{
		$this->context->controller->addJS($this->_path.'views/js/front.js');
		$this->context->controller->addCSS($this->_path.'views/css/front.css');

		if (!$this->isCached('zeromenu_header.tpl', $this->getCacheId()))
		{
			$custom_css = Configuration::get('ZEROMENU_CUSTOM_CSS');

			$this->context->smarty->assign(array(
				'z_css' => $custom_css
			));
		}

		return $this->display(__FILE__, 'zeromenu_header.tpl');
	}

	public function hookActionObjectCategoryUpdateAfter()
	{
		$this->_clearCache('*');
	}

	public function hookActionObjectProductUpdateAfter()
	{
		$this->_clearCache('*');
	}

	public function hookActionObjectManufacturerUpdateAfter()
	{
		$this->_clearCache('*');
	}

	private function preProcess()
	{
		$id_lang = (int)$this->context->language->id;

		$zmenus = ZMenu::getList($id_lang);

		foreach ($zmenus as &$menu)
		{
			$zdropdowns = ZDropdown::getList($menu['id_zmenu'], $id_lang);

			foreach ($zdropdowns as &$dropdown)
			{
				if ($dropdown['content_type'] == 'category' && !empty($dropdown['categories']))
					foreach ($dropdown['categories'] as &$category)
						$category['subcategories'] = ZDropdown::getSubCategories($category['id_category'], $id_lang);
			}

			$menu['dropdowns'] = $zdropdowns;

			if ($menu['bgimage_position'])
			{
				$position = explode(' ', $menu['bgimage_position']);
				$menu['position'] = '';
				if ($position[0] != 'center')
					$menu['position'] .= $position[0].': '.$menu['position_x'].'px;';
				if ($position[1] != 'center')
					$menu['position'] .= $position[1].': '.$menu['position_y'].'px;';
			}
		}

		$this->smarty->assign(array(
			'zmenus' => $zmenus,
			'bg_image_url' => $this->_path._BG_IMAGES_FOLDER_
		));
	}

	public function hookDisplayTop()
	{
		if (!$this->isCached('zeromenu.tpl', $this->getCacheId()))
			$this->preProcess();

		return $this->display(__FILE__, 'zeromenu.tpl', $this->getCacheId());
	}
}
