<?php
if (!defined('_PS_VERSION_')) {
    exit;
}

class GroupSwitch extends Module
{
    public function __construct()
    {
        $this->name = 'groupswitch';

        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'lowki@logomotion';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = 'Changement de groupe';

        $this->confirmUninstall = 'Etes-vous sur de vouloir le supprimer?';
    }

    public function install()
    {

        if (!parent::install() ||
            !$this->registerHook('header') ||
            !$this->registerHook('displayTop') ||
            !$this->registerHook('leftColumn') ||
            !Configuration::updateValue('MYMODULE_NAME', 'Changement de groupe')
        ) {
            return false;
        }

        return true;
    }
    public function hookDisplayHeader($params)
    {

        $this->context->controller->addCSS($this->_path . 'css/groupswitch.css', 'all');
        $this->context->controller->addJS($this->_path . 'js/jquery.cookie.js', 'all');
        $this->context->controller->addJS($this->_path . 'js/groupswitch.js', 'all');
        $this->context->smarty->assign(
            array(
                'gs_message' => $this->l('Etes-vous un professionnel ou un particulier?'),
                'gs_btn_b2b' => $this->l('Tarifs HT'),
                'gs_btn_b2c' => $this->l('Tarifs TTC'),
                'my_module_link' => $this->context->link->getModuleLink('mymodule', 'display'),

            )
        );
        $this->context->smarty->assign('groupswitchval', 'none');
        $gs_current_name = $this->l('Tarifs HT');
        $gs_switch_name = $this->l('Tarifs TTC');
        $gs_switch_label = $this->l('b2c');
        $price_display = 1;
        if (isset($_COOKIE['ps_groupswitch'])) {
            if ($_COOKIE['ps_groupswitch'] === 'b2c') {
                $gs_current_name = $this->l('Tarifs TTC');
                $gs_switch_name = $this->l('Tarifs HT');
                $gs_switch_label = $this->l('b2b');
                $price_display = 0;
            }
        }
        $this->context->smarty->assign(
            array(

            'gs_current_name' => $gs_current_name,
            'gs_switch_name' => $gs_switch_name,
            'gs_switch_label' => $gs_switch_label,
            'priceDisplay' => $price_display
            )
        );

    }

    public function hookDisplayTop($params)
    {
        return $this->display(__FILE__, 'main.tpl');
    }

    public function hookDisplayLeftColumn($params)
    {
        return $this->hookDisplayRightColumn($params);
    }

    public function hookDisplayRightColumn($params)
    {
        $this->context->smarty->assign(
            array(
                'groupswitch' => Configuration::get('MYMODULE_NAME'),
                'my_module_link' => $this->context->link->getModuleLink('mymodule', 'display'),
            )
        );
        return $this->hookDisplayTop($params);

    }
}
