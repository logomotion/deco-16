<?php
// Security
if (!defined('_PS_VERSION_'))
	exit;

// Loading Models
//require_once(_PS_MODULE_DIR_ . 'opartfaq/models/FaqQuestion.php');

class Opartsavecart extends Module
{

	public function __construct()
	{
		$this->name = 'opartsavecart';
		$this->tab = 'front_office_features';
		$this->version = '16-06-05';
		 
		$this->author = 'Op\'art - Olivier CLEMENCE';
		$this->need_instance = 0;
		//$this->module_key="4ddbc6f52446057f4f6ff71d96990358";
		
		parent::__construct();

		$this->displayName = $this->l('Op\'art save cart');
		$this->description = $this->l('Add save cart feature for customer');

		$this->confirmUninstall = $this->l('Are you sure you want to uninstall this module ?');
		 
		if ($this->active) {
			if (Configuration::get('OPART_SAVE_CART_CONF') == '')
				$this->warning = $this->l('You have to configure your module');
		}
	}
	
	public function install()
	{
		if(version_compare(_PS_VERSION_, '1.5.0', '<'))
			return false;
	
		// Install SQL
		include(dirname(__FILE__).'/sql/install.php');
		foreach ($sql as $s)
			if (!Db::getInstance()->execute($s))
			return false;
	
		//Init
		Configuration::updateValue('OPART_SAVE_CART_CONF', 'ok');
	
		// Install Module
		//HOOK_SHOPPING_CART_EXTRA
		if (parent::install() == false
			OR !$this->registerHook('displayShoppingCart')
			OR !$this->registerHook('displayCustomerAccount')
		)
			return false;
		return true;
	}
	
	public function uninstall()
	{
		// Uninstall SQL
		include(dirname(__FILE__).'/sql/uninstall.php');
		foreach ($sql as $s)
			if (!Db::getInstance()->execute($s))
			return false;
	
		Configuration::deleteByName('OPART_SAVE_CART_CONF');

		// Uninstall Module
		if (!parent::uninstall())
			return false;
		return true;
	}
	
	public function hookDisplayCustomerAccount()
	{		
		$this->context->controller->addCSS($this->_path.'css/opartsavecart.css');
		return $this->display(__FILE__, 'views/templates/front/myaccount.tpl');
	}
	
	public function hookDisplayShoppingCart()
	{
		$cartSaved=Tools::getValue('cartSaved');
		$this->smarty->assign(array(
			'carteSaved' => $cartSaved
		));
		$this->context->controller->addJS($this->_path.'js/opartsavecart.js');
		return $this->display(__FILE__, 'views/templates/front/button.tpl');
	}
}