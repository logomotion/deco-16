<?php

	// Init
	$sql = array();

	// Create Table questions
	$sql[] = 'CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'opartsavecart` (
		  `id_cart` int(10) NOT NULL,
		  `id_customer` int(10) NOT NULL,
		  `name` varchar(256) NOT NULL,
		  `date_add` DATETIME NOT NULL,
  		PRIMARY KEY (`id_cart`,`id_customer`)
		) ENGINE='._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';