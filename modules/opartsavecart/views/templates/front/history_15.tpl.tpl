{capture name=path}<a href="{$link->getPageLink('my-account', true)|escape:'html'}">{l s='My account'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='Saved cart' mod='opartsavecart'}{/capture}
{include file="$tpl_dir./breadcrumb.tpl"}
{include file="$tpl_dir./errors.tpl"}

<h1>{l s='Saved cart' mod='opartsavecart'}</h1>
<p>{l s='Here are the carts you saved.' mod='opartsavecart'}</p>
{if isset($deleted) && $deleted=="success"}
<div class="success">{l s='Cart deleted successfully' mod='opartsavecart'}</div>
{/if}
<div class="block-center" id="block-history">
	{if $carts && count($carts)}
	<table id="order-list" class="std">
		<thead>
			<tr>
				<th class="first_item">{l s='Cart name' mod='opartsavecart'}</th>
				<th class="item">{l s='Date' mod='opartsavecart'}</th>
				<th class="item">&nbsp;</th>
				<th class="last_item">&nbsp;</th>
			</tr>
		</thead>
		<tbody>
		{foreach from=$carts item=cart name=myLoop}
			<tr class="{if $smarty.foreach.myLoop.first}first_item{elseif $smarty.foreach.myLoop.last}last_item{else}item{/if} {if $smarty.foreach.myLoop.index % 2}alternate_item{/if}">
				<td class="history_date bold">{$cart.name}</td>
				<td class="history_price"><span class="price">{dateFormat date=$cart.date_add full=1}</span></td>
				<td class="history_method"><a href="{$link->getModuleLink('opartsavecart','default',['action'=>'load','opartCartId'=>$cart.id_cart])}">{l s='load' mod='opartsavecart'}</a></td>
				<td class="history_method"><a href="{$link->getModuleLink('opartsavecart','default',['action'=>'delete','opartCartId'=>$cart.id_cart])}">{l s='delete' mod='opartsavecart'}</a></td>
			</tr>
		{/foreach}
		</tbody>
	</table>
	<div id="block-order-detail" class="hidden">&nbsp;</div>
	{else}
		<p class="warning">{l s='You have not saved cart.' mod='opartsavecart'}</p>
	{/if}
</div>

<ul class="footer_links clearfix">
	<li><a href="{$link->getPageLink('my-account', true)|escape:'html'}"><img src="{$img_dir}icon/my-account.gif" alt="" class="icon" /> {l s='Back to Your Account'}</a></li>
	<li class="f_right"><a href="{$base_dir}"><img src="{$img_dir}icon/home.gif" alt="" class="icon" /> {l s='Home'}</a></li>
</ul>
