{if $carteSaved=='true'}
	<div class="success">{l s='Cart saved successfully' mod='opartsavecart'}</div>
{/if}
{if $logged}
<div class="warning" style="display:none;" id="opartSaveCartError">{l s='Please add a name for your cart' mod='opartsavecart'}</div>
<div style="display:none; float:right; margin:10px 0 0 10px;" id="opartConteneurFormSaveCart">
	<form action="{$link->getModuleLink('opartsavecart','default',['action'=>'save'])}" method="post" onSubmit="opartTestFormSaveCart(); return false;" id="opartFormSaveCart">
		<label for="opartCartName">{l s='Please give a name for this cart' mod='opartsavecart'}:</label>
		<input type="text" name="opartCartName" id="opartCartName" />
		<input type="submit" class="exclusive" value="enregistrer" />
	</form>
</div>
<a href="#" onclick="$('#opartConteneurFormSaveCart').toggle('slow'); return false;" class="exclusive standard-checkout" title="{l s='Next' mod='opartsavecart'}" style="float:right; margin:10px 0;">{l s='Save my cart' mod='opartsavecart'}</a>
<br /><br />	
{else}
<div class="warning">{l s='Please loggin for save your cart.' mod='opartsavecart'} <a href="{$link->getPageLink('my-account', true)|escape:'html'}">{l s='Click here for login' mod='opartsavecart'}</a></div>
{/if}
