{if $carteSaved=='true'}
	<div class="alert alert-success">{l s='cart saved successfully' mod='opartsavecart'}</div>
{/if}
{if $logged}
<div class="alert alert-danger" style="display:none;" id="opartSaveCartError">{l s='Please add a name for your cart' mod='opartsavecart'}</div>
<div style="display:none; float:right; margin:10px 0 0 10px;" id="opartConteneurFormSaveCart">
	<form action="{$link->getModuleLink('opartsavecart','default',['action'=>'save'])}" method="post" onSubmit="opartTestFormSaveCart(); return false;" id="opartFormSaveCart">
		<label for="opartCartName">{l s='Please give a name for this cart' mod='opartsavecart'}:</label>
		<input type="text" name="opartCartName" id="opartCartName" />
		<input type="submit" class="" value="enregistrer" />
	</form>
</div>
<a href="#" onclick="$('#opartConteneurFormSaveCart').toggle('slow'); return false;" class="btn btn-default button button-small opartSaveCartLoadLink" title="{l s='Next' mod='opartsavecart'}" style="float:right; margin:10px 0;"><span>{l s='Sauvegarder mon panier'}</span></a>
<br /><br />	
{else}
<div class="alert alert-danger">{l s='Please loggin for save your cart.' mod='opartsavecart'} <a href="{$link->getPageLink('my-account', true)|escape:'html'}">{l s='Click here for login' mod='opartsavecart'}</a></div>
{/if}
