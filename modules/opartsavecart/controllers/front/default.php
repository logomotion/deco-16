<?php 

class OpartSaveCartDefaultModuleFrontController extends ModuleFrontController
{
	public function __construct()
	{
		parent::__construct();
		$this->display_column_left = false;
		$this->display_column_right = false;
		$this->context = Context::getContext();
	}

	public function setMedia()
	{	
		parent::setMedia();
		if(version_compare(_PS_VERSION_, '1.6.0', '<'))
			$this->addCSS(_MODULE_DIR_.$this->module->name.'/css/opartsavecart_15.css');
		else
			$this->addCSS(_MODULE_DIR_.$this->module->name.'/css/opartsavecart.css');
		
	}
	
	public function initContent()
	{
		parent::initContent();
		
		$action=Tools::getValue('action');
		
		
		if($action=="")
			Tools::redirect('index.php?controller=order');
		
		
		$idCustomer=$this->context->customer->id;
		$link=new Link();
		switch ($action) {
			case 'history':
				$sql="SELECT * FROM `"._DB_PREFIX_."opartsavecart` WHERE id_customer=".$idCustomer;
				$carts=Db::getInstance()->executeS($sql);
				$this->context->smarty->assign('carts',$carts);
				if(Tools::getValue('deleted')=="success")
					$this->context->smarty->assign('deleted',"success");
				$this->setTemplate('history.tpl');
				break;
				
			case 'save':
				if(Tools::getValue('opartCartName')=='') 
					Tools::redirect('index.php?controller=order');
				
				else {					
					//duplicate cart
					$duplicate=$this->context->cart->duplicate();
					$idCart=$duplicate['cart']->id;
					$cartName=Tools::getValue('opartCartName');
					$dateTime = date('Y-m-d H:i:s');
					//save it
					$sql="INSERT IGNORE INTO `"._DB_PREFIX_."opartsavecart` (id_cart,id_customer,name,date_add) VALUES (".(int)$idCart.",".(int)$idCustomer.",'".pSQL($cartName)."','".$dateTime."')";
					Db::getInstance()->execute($sql);					
					Tools::redirect('index.php?controller=order&cartSaved=true');
				}
				break;
				
			case 'delete':
				if(Tools::getValue('opartCartId')=='') 
					Tools::redirect('index.php?controller=order');
				else {
					$idCart=Tools::getValue('opartCartId');				
					$sql="DELETE FROM `"._DB_PREFIX_."opartsavecart` WHERE id_cart=".$idCart." AND id_customer=".$idCustomer;
					Db::getInstance()->execute($sql);										
					$redirectLink=$link->getModuleLink('opartsavecart','default',array('action'=>'history','deleted'=>'success'));
					Tools::redirect($redirectLink);
				}
				break;
				
			case 'load':
				$redirectLink=$link->getModuleLink('opartsavecart','default',array('action'=>'history'));
				if(Tools::getValue('opartCartId')=='')
					Tools::redirect($redirectLink);
				else {
					//check if cart exist for this customer
					$idCart=Tools::getValue('opartCartId');					
					$sql="SELECT * FROM `"._DB_PREFIX_."opartsavecart` WHERE id_customer=".$idCustomer." AND id_cart=".$idCart;
					$result=Db::getInstance()->getRow($sql);
					if(count($result)>0) {
						//duplicate for no update cart
						$cartObj=new Cart($idCart);
						$duplicate=$cartObj->duplicate();
						$idCart=$duplicate['cart']->id;
						global $cookie;
						$cookie->__set('id_cart',$idCart);
						Tools::redirect('index.php?controller=order');
					}
					else 
						Tools::redirect($redirectLink);
				}
				break;
			default:
				Tools::redirect('index.php?controller=order');
		}
		

	}
}