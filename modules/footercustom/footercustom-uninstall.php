<?php

/* Module Prestashop FooterCustom
 * Copyright Prestacrea
 * Author: Prestacrea
 * Website: http://www.prestacrea.com */

Db::getInstance()->Execute('
DROP TABLE `'._DB_PREFIX_.'footercustom_newsletter');

Db::getInstance()->Execute('
DROP TABLE `'._DB_PREFIX_.'footercustom_blocktitles');

Db::getInstance()->Execute('
DROP TABLE `'._DB_PREFIX_.'footercustom');

Db::getInstance()->Execute('
DROP TABLE `'._DB_PREFIX_.'footercustom_lang');

Db::getInstance()->Execute('
DROP TABLE `'._DB_PREFIX_.'footercustom_editor');

?>