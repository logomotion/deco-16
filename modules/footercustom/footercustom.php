<?php

/* Module Prestashop FooterCustom
 * Copyright Prestacrea
 * Author: Prestacrea
 * Website: http://www.prestacrea.com */

class FooterCustom extends Module
{
	function __construct()
	{
	$this->name = 'footercustom';
	$this->tab = 'PRESTACREA - Modules';
	$this->version = '2.0';
	$this->author = 'PRESTACREA';
	$this->module_key = 'ac1b7c852095415433426bad93a47c6b';
	parent::__construct();
		
      $this->page = basename(__FILE__, '.php');
	$this->displayName = ('PRESTACREA - Footer Custom');
	$this->description = $this->l('Adds a large customizable footer');
	$this->confirmUninstall = $this->l('Uninstalling the module will delete all its entries in the database');
	}

	function install()
	{
	if (file_exists(dirname(__FILE__).'/footercustom-install.php'))
	require_once(dirname(__FILE__).'/footercustom-install.php');

	if (!parent::install()
	OR !$this->registerHook('header') 
	OR !$this->registerHook('footer')
	OR !Configuration::updateValue('PS_NEWS_RAND', rand().rand())
	OR !Configuration::updateValue('FC_NEWS', 1)
	OR !Configuration::updateValue('FC_IMG', 1)
	OR !Configuration::updateValue('FC_ADDTHIS', 1)
 	OR !Configuration::updateValue('FC_LINKS', 1)
	OR !Configuration::updateValue('FC_EDITOR', 1))
	return false;
	return true;
	}

	function uninstall()
	{
	if (file_exists(dirname(__FILE__).'/footercustom-uninstall.php'))
	require_once(dirname(__FILE__).'/footercustom-uninstall.php');

	if (!parent::uninstall()
	OR !Configuration::deleteByName('FC_NEWS')
	OR !Configuration::deleteByName('FC_IMG')
	OR !Configuration::deleteByName('FC_ADDTHIS')
	OR !Configuration::deleteByName('FC_LINKS')
	OR !Configuration::deleteByName('FC_EDITOR'))
	return false;
	return true;
	}

	function getCustomers()
	{
	$rq = Db::getInstance()->ExecuteS('
	SELECT c.`id_customer`, c.`lastname`, c.`firstname`, c.`email`, c.`ip_registration_newsletter`, c.`newsletter_date_add`
	FROM `'._DB_PREFIX_.'customer` c
	WHERE 1
	'.((isset($_POST['SUSCRIBERS']) AND intval($_POST['SUSCRIBERS']) != 0) ? 'AND c.`newsletter` = '.intval($_POST['SUSCRIBERS'] - 1) : '').'
	GROUP BY c.`id_customer`');
	$header = array('id_customer', 'lastname', 'firstname', 'email', 'ip_address', 'newsletter_date_add');
	$result = (is_array($rq) ? array_merge(array($header), $rq) : $header);
	return $result;
	}

	function getFooterCustom()
	{
	$rq = Db::getInstance()->ExecuteS('
	SELECT * FROM `'._DB_PREFIX_.'footercustom_newsletter`');
	$header = array('id_customer', 'email', 'newsletter_date_add', 'ip_address');
	$result = (is_array($rq) ? array_merge(array($header), $rq) : $header);
	return $result;
	}

	function putCSV($fd, $array)
	{
	$line = implode(';', $array);
	$line .= "\n";
	if (!fwrite($fd, utf8_decode($line), 4096))
	$this->_postErrors[] = $this->l('Cannot write to').' '.dirname(__FILE__).'/'.$this->_file.' !';
	}

 	function isNewsletterRegistered($customerEmail)
 	{
 	if (Db::getInstance()->getRow('SELECT `email` FROM '._DB_PREFIX_.'footercustom_newsletter WHERE `email` = \''.pSQL($customerEmail).'\''))
 	return 1;
	if (!$registered = Db::getInstance()->getRow('SELECT `newsletter` FROM '._DB_PREFIX_.'customer WHERE `email` = \''.pSQL($customerEmail).'\''))
	return -1;
	if ($registered['newsletter'] == '1')
	return 2;
	return 0;
 	}
 	
 	function newsletterRegistration()
 	{
	$this->error = false;
	$this->valid = false;
	if (!Validate::isEmail(pSQL($_POST['email'])))
	return $this->error = $this->l('Invalid mail address');
	elseif ($_POST['action'] == '1')
	{
 	$registerStatus = $this->isNewsletterRegistered(pSQL($_POST['email']));
	if ($registerStatus < 1)
	return $this->error = $this->l('Mail address not registered');
	elseif ($registerStatus == 1)
	{
	if (!Db::getInstance()->Execute('DELETE FROM '._DB_PREFIX_.'footercustom_newsletter WHERE `email` = \''.pSQL($_POST['email']).'\''))
	return $this->error = $this->l('Error during unsubscription');
	return $this->valid = $this->l('Unsubscription successful');
	}
	elseif ($registerStatus == 2)
	{
	if (!Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'customer SET `newsletter` = 0 WHERE `email` = \''.pSQL($_POST['email']).'\''))
	return $this->error = $this->l('Error during unsubscription');
	return $this->valid = $this->l('Unsubscription successful');
	}
	}
	elseif ($_POST['action'] == '0')
	{
	$registerStatus = $this->isNewsletterRegistered(pSQL($_POST['email']));
	if ($registerStatus > 0)
	return $this->error = $this->l('Mail address already registered');
	elseif ($registerStatus == -1)
	{
	global $cookie;
	if (!Db::getInstance()->Execute('INSERT INTO '._DB_PREFIX_.'footercustom_newsletter VALUES (\'\', \''.pSQL($_POST['email']).'\', NOW(), \''.pSQL($_SERVER['REMOTE_ADDR']).'\', 
	(SELECT c.http_referer FROM '._DB_PREFIX_.'connections c WHERE c.id_guest = '.intval($cookie->id_guest).' ORDER BY c.date_add DESC LIMIT 1))'))
	return $this->error = $this->l('Error during subscription');
	return $this->valid = $this->l('Subscription successful');
	}
	elseif ($registerStatus == 0)
	{
	if (!Db::getInstance()->Execute('UPDATE '._DB_PREFIX_.'customer SET `newsletter` = 1, newsletter_date_add = NOW(), `ip_registration_newsletter` = \''.pSQL($_SERVER['REMOTE_ADDR']).'\' WHERE `email` = \''.pSQL($_POST['email']).'\''))
	return $this->error = $this->l('Error during subscription');
	return $this->valid = $this->l('Subscription successful');
	}
	}
 	}

	function addBlockTitle()
	{
	$languages = Language::getLanguages();
	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	if (!$languages)
	return false;
	foreach ($languages AS $language)
	{
	$title1 = Tools::getValue('title1_'.$language['id_lang']) ? Tools::getValue('title1_'.$language['id_lang']) : Tools::getValue('title1_'.$defaultLanguage);
	$title2 = Tools::getValue('title2_'.$language['id_lang']) ? Tools::getValue('title2_'.$language['id_lang']) : Tools::getValue('title2_'.$defaultLanguage);
	$title3 = Tools::getValue('title3_'.$language['id_lang']) ? Tools::getValue('title3_'.$language['id_lang']) : Tools::getValue('title3_'.$defaultLanguage);
	$query = 'REPLACE INTO '._DB_PREFIX_.'footercustom_blocktitles
	VALUES ('.$language['id_lang'].', \''.addslashes($title1).'\', \''.addslashes($title2).'\', \''.addslashes($title3).'\')';
	if (!Db::getInstance()->Execute($query))
	return false;
	}
	return true;
	}

	function getBlockTitle1($lang)
	{
	$result = array();
	if (!$result = Db::getInstance()->getRow('SELECT `title1` FROM '._DB_PREFIX_.'footercustom_blocktitles WHERE `id_lang`='.$lang))
	return false;
	return $result;
	}

	function getBlockTitle2($lang)
	{
	$result = array();
	if (!$result = Db::getInstance()->getRow('SELECT `title2` FROM '._DB_PREFIX_.'footercustom_blocktitles WHERE `id_lang`='.$lang))
	return false;
	return $result;
	}

	function getBlockTitle3($lang)
	{
	$result = array();
	if (!$result = Db::getInstance()->getRow('SELECT `title3` FROM '._DB_PREFIX_.'footercustom_blocktitles WHERE `id_lang`='.$lang))
	return false;
	return $result;
	}

	function addBlock()
	{
	if (!Db::getInstance()->Execute('INSERT INTO '._DB_PREFIX_.'footercustom VALUES ()') OR !$lastId = mysql_insert_id())
	return false;
	$languages = Language::getLanguages();
	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	if (!$languages)
	return false;
	foreach ($languages AS $language)
	{
	$title = Tools::getValue('title_'.$language['id_lang']) ? Tools::getValue('title_'.$language['id_lang']) : Tools::getValue('title_'.$defaultLanguage);
	$link = Tools::getValue('link_'.$language['id_lang']) ? Tools::getValue('link_'.$language['id_lang']) : Tools::getValue('link_'.$defaultLanguage);
	$inblock = Tools::getValue('inblock_'.$language['id_lang']) ? Tools::getValue('inblock_'.$language['id_lang']) : Tools::getValue('inblock_'.$defaultLanguage);
	$position = Tools::getValue('position_'.$language['id_lang']) ? Tools::getValue('position_'.$language['id_lang']) : Tools::getValue('position_'.$defaultLanguage);
	$query = 'INSERT INTO '._DB_PREFIX_.'footercustom_lang
	VALUES ('.$lastId.', '.$language['id_lang'].', \''.addslashes($title).'\' , \''.addslashes($link).'\', \''.addslashes($inblock).'\', \''.addslashes($position).'\')';
	if (!Db::getInstance()->Execute($query))
	return false;
	}
	return true;
	}

	function editBlock()
	{
	$languages = Language::getLanguages();
	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	if (!$languages)
	return false;
	foreach ($languages AS $language)
	{
	$title = Tools::getValue('title_'.$language['id_lang']) ? Tools::getValue('title_'.$language['id_lang']) : Tools::getValue('title_'.$defaultLanguage);
	$link = Tools::getValue('link_'.$language['id_lang']) ? Tools::getValue('link_'.$language['id_lang']) : Tools::getValue('link_'.$defaultLanguage);
	$inblock = Tools::getValue('inblock_'.$language['id_lang']) ? Tools::getValue('inblock_'.$language['id_lang']) : Tools::getValue('inblock_'.$defaultLanguage);
	$position = Tools::getValue('position_'.$language['id_lang']) ? Tools::getValue('position_'.$language['id_lang']) : Tools::getValue('position_'.$defaultLanguage);
	$id_block = (int)(Tools::getValue('id'));
	$query = 'REPLACE '._DB_PREFIX_.'footercustom_lang
	VALUES (\''.($id_block).'\', \''.$language['id_lang'].'\', \''.addslashes($title).'\', \''.addslashes($link).'\', \''.addslashes($inblock).'\', \''.addslashes($position).'\')';
	if (!Db::getInstance()->Execute($query))
	return false;
	}
	return true;
	}

	function deleteBlock()
	{
	$id_block = (int)(Tools::getValue('id'));
	if (Db::getInstance()->Execute('DELETE FROM '._DB_PREFIX_.'footercustom WHERE `id_block`='.$id_block))
	return Db::getInstance()->Execute('DELETE FROM '._DB_PREFIX_.'footercustom_lang WHERE `id_block`='.$id_block);
	return false;
	}

	function addEditor()
	{
	$languages = Language::getLanguages();
	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	if (!$languages)
	return false;
	foreach ($languages AS $language)
	{
	$editor = Tools::getValue('editor_'.$language['id_lang']) ? Tools::getValue('editor_'.$language['id_lang']) : Tools::getValue('editor_'.$defaultLanguage);
	$query = 'REPLACE INTO '._DB_PREFIX_.'footercustom_editor
	VALUES ('.$language['id_lang'].', \''.addslashes($editor).'\')';
	if (!Db::getInstance()->Execute($query))
	return false;
	}
	return true;
	}

	function getEditor($lang)
	{
	$result = array();
	if (!$result = Db::getInstance()->getRow('SELECT `editor` FROM '._DB_PREFIX_.'footercustom_editor WHERE `id_lang`='.$lang))
	return false;
	return $result;
	}

	function getContent()
	{
	$this->_html = '<h2>'.$this->displayName.'</h2>
	<script type="text/javascript" src="'.$this->_path.'js/footercustom.js"></script>';
	$defaultLanguage = Configuration::get('PS_LANG_DEFAULT');

      if (isset($_POST['submitExport']) AND isset($_POST['action']))
	{
	$this->_file = 'export_'.Configuration::get('PS_NEWS_RAND').'.csv';
	if ($_POST['action'] == 'customers')
	$result = $this->getCustomers();
	else
	{
	$result = $this->getFooterCustom();
	}
	if (!$nb = intval(Db::getInstance()->NumRows()))
	$this->_html .= $this->displayError($this->l('No customers were found'));
	elseif ($fd = @fopen(dirname(__FILE__).'/export/'.strval(preg_replace('#\.{2,}#', '.', $_POST['action'])).'_'.$this->_file, 'w'))
	{
	foreach ($result AS $tab)
	$this->putCSV($fd, $tab);
	fclose($fd);
	$this->_html .= $this->displayConfirmation($this->l('The CSV file has been successfully exported').' ('.$nb.' '.$this->l('customers found').')<br />>> <a href="../modules/footercustom/export/'.strval($_POST['action']).'_'.$this->_file.'"><b>'.$this->l('Download the file').'</b></a>');
	}
	else
	$this->_html .= $this->displayError($this->l('Cannot write to').' '.dirname(__FILE__).'/'.strval($_POST['action']).'_'.$this->_file.' !');
	}

	elseif (Tools::isSubmit('submitUpdate'))
	{
	if (isset($_POST['fc_news']) AND VAlidate::isBool(intval($_POST['fc_news'])))
	Configuration::updateValue('FC_NEWS', pSQL($_POST['fc_news']));
	if (isset($_POST['fc_img']) AND VAlidate::isBool(intval($_POST['fc_img'])))
	Configuration::updateValue('FC_IMG', pSQL($_POST['fc_img']));
	if (isset($_POST['fc_addthis']) AND VAlidate::isBool(intval($_POST['fc_addthis'])))
	Configuration::updateValue('FC_ADDTHIS', pSQL($_POST['fc_addthis']));
	if (isset($_POST['fc_links']) AND VAlidate::isBool(intval($_POST['fc_links'])))
	Configuration::updateValue('FC_LINKS', pSQL($_POST['fc_links']));
	if (isset($_POST['fc_editor']) AND VAlidate::isBool(intval($_POST['fc_editor'])))
	Configuration::updateValue('FC_EDITOR', pSQL($_POST['fc_editor']));
	$this->_html .= $this->displayConfirmation($this->l('Settings updated'));
	}

	elseif (Tools::isSubmit('submitAddImage'))
	{
	if (Tools::getValue('footer_img_del')=='dele')
	{
	if (file_exists(dirname(__FILE__).'/img/footer_img.png'))
	unlink (dirname(__FILE__).'/img/footer_img.png');
	}
	if (isset($_FILES['footer_img']) AND isset($_FILES['footer_img']['tmp_name']) AND !empty($_FILES['footer_img']['tmp_name']))
	{
	Configuration::set('PS_IMAGE_GENERATION_METHOD', 0);
	if (!copy($_FILES['footer_img']['tmp_name'], dirname(__FILE__).'/img/footer_img.png'))
	$this->_html .= $this->displayError($this->l('An error occurred during the image upload'));
	}
	$this->_html .= $this->displayConfirmation($this->l('Image updated'));
	}

	elseif (Tools::isSubmit('submitAddEditor'))
	{
	if ($this->addEditor())
	$this->_html .= $this->displayConfirmation($this->l('Editor updated'));
	}

	elseif (Tools::isSubmit('submitAddBlockTitle'))
	{
	if ($this->addBlockTitle())
	$this->_html .= $this->displayConfirmation($this->l('Blocks titles updated'));
	}

	elseif (Tools::isSubmit('submitAddBlock'))
	{
	$inblock = Tools::getValue('inblock_'.$defaultLanguage);
	if (!$inblock OR $inblock > 3 OR !Validate::isInt($inblock))
	$this->_html .= $this->displayError($this->l('Invalid block number : the block number must be 1, 2 or 3'));
	else
	{
	$position = Tools::getValue('position_'.$defaultLanguage);
	if (!$position OR $position <= 0 OR !Validate::isInt($position))
	$this->_html .= $this->displayError($this->l('Invalid position number'));
	else
	{
	if ($this->addBlock())
	$this->_html .= $this->displayConfirmation($this->l('The link has been added successfully'));
	else
	$this->_html .= $this->displayError($this->l('An error occured during link adding'));
	}
	}
	}

	elseif (Tools::isSubmit('submitEditBlock'))
	{
	$inblock = Tools::getValue('inblock_'.$defaultLanguage);
	if (!$inblock OR $inblock > 3 OR !Validate::isInt($inblock))
	$this->_html .= $this->displayError($this->l('Invalid block number : the block number must be 1, 2 or 3'));
	else
	{
	$position = Tools::getValue('position_'.$defaultLanguage);
	if (!$position OR $position <= 0 OR !Validate::isInt($position))
	$this->_html .= $this->displayError($this->l('Invalid position number'));
	else
	{
	if (!Tools::getValue('id') OR !is_numeric(Tools::getValue('id')) OR !$this->editBlock())
	$this->_html .= $this->displayError($this->l('An error occured during link editing'));
	else
	$this->_html .= $this->displayConfirmation($this->l('The link has been edited successfully'));
	}
	}
	}

	elseif (Tools::getValue('id'))
	{
	if (!is_numeric(Tools::getValue('id')) OR !$this->deleteBlock())
	$this->_html .= $this->displayError($this->l('An error occurred during link deletion'));
	else
	$this->_html .= $this->displayConfirmation($this->l('The link has been deleted successfully'));
	}

	$this->displayForm();
	return $this->_html;
	}
    
	function displayForm()
	{
	global $cookie;
	$iso = Language::getIsoById((int)($cookie->id_lang));
	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	$languages = Language::getLanguages();
	$divLangName = 'title¤link';
	$divLangNameTitles = 'title1¤title2¤title3';
	$divLangNameEditor = 'editor';
	$this->_fieldsExport = array(
	'SUSCRIBERS' => array(
	'title' => $this->l('Customers'),
	'type' => 'select',
	'value' => array(0 => $this->l('All customers'), 2 => $this->l('Subscribers'), 1 => $this->l('Non-subscribers')),
	'value_default' => 2));
	$this->_html.= '
	<script type="text/javascript">id_language = Number('.$defaultLanguage.');</script>

      <form action="'.$_SERVER['REQUEST_URI'].'" method="post">
      <fieldset style="float:left;clear:both;width:430px;margin-right:10px;height:110px"><legend><img src="'.$this->_path.'logo.gif"/>'.$this->l('EXPORT NEWSLETTER SUBSCRIBERS').'</legend>
	<input type="hidden" name="action" value="footerCustom">
	'.$this->l('Generate a CSV file based on FooterCustom data').'<br /><br />';
	$this->_html .= '
	<div class="margin-form" style="padding:0"><input type="submit" class="button" style="width:150px" name="submitExport" value="'.$this->l('Export CSV file').'"/></div>
	<div class="margin-form" style="padding:0">
	<p>'.$this->l('Warning : do not use the default block newsletter of Prestashop if you use the newsletter of the FooterCustom').'</p>
	</div>
      </fieldset>
	</form>

      <form action="'.$_SERVER['REQUEST_URI'].'" method="post">
	<fieldset style="float:left;width:430px;height:110px"><legend><img src="'.$this->_path.'logo.gif"/>'.$this->l('EXPORT CUSTOMERS').'</legend>
	<input type="hidden" name="action" value="customers">
	'.$this->l('Generate a CSV file from customers account data').'<br /><br />';
	foreach ($this->_fieldsExport as $key => $field)
	{
	$this->_html .= '
	<label style="padding:0;text-align:left;width:90px">'.$field['title'].' </label>
	<div class="margin-form" style="padding-left:80px">';
	switch ($field['type'])
	{
	case 'select':
	$this->_html .= '<select name="'.$key.'">';
	foreach ($field['value'] AS $k => $value)
	$this->_html .= '<option value="'.$k.'"'.(($k == Tools::getValue($key, $field['value_default'])) ? ' selected="selected"' : '').'>'.$value.'</option>';
	$this->_html .= '</select>';
	break;
	default:
	break;
	}
	if (isset($field['example']) AND !empty($field['example']))
	$this->_html .= '<p style="clear: both;">'.$field['example'].'</p>';
	$this->_html .= '
	</div>';
	}
	$this->_html .= '
	<div class="margin-form" style="padding-left:0"><input type="submit" class="button" style="width:150px" name="submitExport" value="'.$this->l('Export CSV file').'"/></div>
      </fieldset>
	</form>

	<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
	<fieldset class="space" style="width:220px;height:220px;float:left;margin-right:10px"><legend><img src="'.$this->_path.'logo.gif"/>'.$this->l('SETTINGS').'</legend>

	<label style="text-align:left;width:120px;padding:0">'.$this->l('Newsletter').'</label>
	<div class="margin-form" style="font-weight:bold;padding:0 0 0 120px">
	<input type="radio" style="margin-right:5px" name="fc_news" value="1" '.(Configuration::get('FC_NEWS') ? 'checked="checked" ' : '').'/>'.$this->l('Yes').'
	<input type="radio" style="margin-right:5px;margin-left:5px" name="fc_news" value="0" '.(!Configuration::get('FC_NEWS') ? 'checked="checked" ' : '').'/>'.$this->l('No').'
	<p style="font-weight:normal;margin:2px 0 0 -120px;padding:0 0 2px">'.$this->l('Display or not the newsletter').'</p>
	</div>

	<label style="text-align:left;width:120px;padding:0">'.$this->l('Image').'</label>
	<div class="margin-form" style="font-weight:bold;padding:0 0 0 120px">
	<input type="radio" style="margin-right:5px" name="fc_img" value="1" '.(Configuration::get('FC_IMG') ? 'checked="checked" ' : '').'/>'.$this->l('Yes').'
	<input type="radio" style="margin-right:5px;margin-left:5px" name="fc_img" value="0" '.(!Configuration::get('FC_IMG') ? 'checked="checked" ' : '').'/>'.$this->l('No').'
	<p style="font-weight:normal;margin:2px 0 0 -120px;padding:0 0 2px">'.$this->l('Display or not the image').'</p>
	</div>

	<label style="text-align:left;width:120px;padding:0">'.$this->l('AddThis').'</label>
	<div class="margin-form" style="font-weight:bold;padding:0 0 0 120px">
	<input type="radio" style="margin-right:5px" name="fc_addthis" value="1" '.(Configuration::get('FC_ADDTHIS') ? 'checked="checked" ' : '').'/>'.$this->l('Yes').'
	<input type="radio" style="margin-right:5px;margin-left:5px" name="fc_addthis" value="0" '.(!Configuration::get('FC_ADDTHIS') ? 'checked="checked" ' : '').'/>'.$this->l('No').'
	<p style="font-weight:normal;margin:2px 0 0 -120px;padding:0 0 2px">'.$this->l('Display or not AddThis').'</p>
	</div>

	<label style="text-align:left;width:120px;padding:0">'.$this->l('Links').'</label>
	<div class="margin-form" style="font-weight:bold;padding:0 0 0 120px">
	<input type="radio" style="margin-right:5px" name="fc_links" value="1" '.(Configuration::get('FC_LINKS') ? 'checked="checked" ' : '').'/>'.$this->l('Yes').'
	<input type="radio" style="margin-right:5px;margin-left:5px" name="fc_links" value="0" '.(!Configuration::get('FC_LINKS') ? 'checked="checked" ' : '').'/>'.$this->l('No').'
	<p style="font-weight:normal;margin:2px 0 0 -120px;padding:0 0 2px">'.$this->l('Display or not the links').'</p>
	</div>

	<label style="text-align:left;width:120px;padding:0">'.$this->l('Editor').'</label>
	<div class="margin-form" style="font-weight:bold;padding:0 0 10px 120px">
	<input type="radio" style="margin-right:5px" name="fc_editor" value="1" '.(Configuration::get('FC_EDITOR') ? 'checked="checked" ' : '').'/>'.$this->l('Yes').'
	<input type="radio" style="margin-right:5px;margin-left:5px" name="fc_editor" value="0" '.(!Configuration::get('FC_EDITOR') ? 'checked="checked" ' : '').'/>'.$this->l('No').'
	<p style="font-weight:normal;margin:2px 0 0 -120px;padding:0 0 2px">'.$this->l('Display or not the editor').'</p>
	</div>

	<div class="margin-form" style="padding:0"><input type="submit" name="submitUpdate" value="'.$this->l('Update').'" class="button" style="width:150px"/></div>
	</fieldset>
	</form>

	<form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
	<fieldset class="space" style="width:220px;height:220px;float:left;margin-right:10px"><legend><img src="'.$this->_path.'logo.gif"/>'.$this->l('IMAGE').'</legend>
	<div class="margin-form" style="padding:0">
	<img src="'.$this->_path.'img/footer_img.png" style="width:160px;height:80px"/><br />
	<input type="file" name="footer_img" />
	<p>'.$this->l('Set the image of the footer').'<br/>'.$this->l('Accepted formats :').' JPG, GIF, PNG</p>
	</div>
	<div class="margin-form" style="padding:0">
	<input type="checkbox" name="footer_img_del" value="dele" >&nbsp;'.$this->l('Delete image').'<br>
	</div>
	<div class="clear pspace"></div>
	<div class="margin-form" style="padding:0"><input type="submit" name="submitAddImage" value="'.$this->l('Update').'" class="button" style="width:150px"/></div>
	</fieldset>
	</form>

	<form action="'.$_SERVER['REQUEST_URI'].'" method="post">
	<fieldset class="space"  style="float:left;width:382px;height:220px"><legend><img src="'.$this->_path.'logo.gif"/>'.$this->l('ADD OR EDIT A LINK').'</legend>

	<label style="width:80px;text-align:left;padding:0">'.$this->l('Title').'</label>
	<div class="margin-form" style="padding:0 0 10px 80px">';
	foreach ($languages as $language)
	$this->_html.= '
	<div id="title_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
	<input type="text" size="40" name="title_'.$language['id_lang'].'" id="titleInput_'.$language['id_lang'].'" value=""/>
	<p>'.$this->l('Set the title of the link').'</p>
	</div>';
	$this->_html.= $this->displayFlags($languages, $defaultLanguage, $divLangName, 'title', true);
	$this->_html.= '
	<div class="clear"></div>
	</div>

	<label style="width:80px;text-align:left;padding:0">'.$this->l('URL').'</label>
	<div class="margin-form" style="padding:0 0 10px 80px">';
	foreach ($languages as $language)
	$this->_html.= '
	<div id="link_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
	<input type="text" size="40" name="link_'.$language['id_lang'].'" id="linkInput_'.$language['id_lang'].'" value=""/>
	<p>'.$this->l('Set the url of the link').'</p>
	</div>';
	$this->_html.= $this->displayFlags($languages, $defaultLanguage, $divLangName, 'link', true);
	$this->_html.= '
	<div class="clear"></div>
	</div>

	<label style="width:80px;text-align:left;padding:2px 0 0">'.$this->l('Location').'</label>
	<div class="margin-form" style="font-weight:bold;padding:0 0 10px 80px">';
	foreach ($languages as $language)
	$this->_html.= '
	<div id="inblock_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
	<span>'.$this->l('Block N&ordm;').'</span>
	<input type="text" size="1" name="inblock_'.$language['id_lang'].'" id="inblockInput_'.$language['id_lang'].'" value=""/>
	<span style="margin-left:15px">'.$this->l('Position N&ordm;').'</span>
	<input type="text" size="1" name="position_'.$language['id_lang'].'" id="positionInput_'.$language['id_lang'].'" value=""/>
	<p style="font-weight:normal">'.$this->l('Set the location of the link').'</p>
	</div>';
	$this->_html.= '
	<div class="clear"></div>
	</div>

	<div class="margin-form" style="padding:0 0 10px 80px">
	<input type="hidden" name="id" id="id" value="'.(isset($_POST['id']) ? $_POST['id'] : '').'"/>
	<input type="submit" id="submitAddBlock" name="submitAddBlock" value="'.$this->l('Add this link').'" class="button"/>
	<input type="submit" id="submitEditBlock" name="submitEditBlock" value="'.$this->l('Edit this link').'" class="button" style="display:none"/>
	</div>

	</fieldset>
	</form>';

	$this->listBlocks();

	$this->_html.= '
      <form action="'.$_SERVER['REQUEST_URI'].'" method="post">

	<fieldset class="space" style="width:274px;float:left;margin-right:10px"><legend><img src="'.$this->_path.'logo.gif"/>'.$this->l('BLOCK N&ordm;1').'</legend>
	<label style="width:40px;text-align:left;padding:0">'.$this->l('Title').'</label>
	<div class="margin-form" style="padding:0 0 0 40px">';
	foreach ($languages as $language)
	{
	$blocktitle1 = $this->getBlockTitle1($language['id_lang']);
	$this->_html.= '
	<div id="title1_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
	<input type="text" size="25" name="title1_'.$language['id_lang'].'" id="title1Input_'.$language['id_lang'].'" value="'.$blocktitle1['title1'].'"/>
	<p>'.$this->l('Set the title of the block').'</p>
	</div>';
	}
	$this->_html.= $this->displayFlags($languages, $defaultLanguage, $divLangNameTitles, 'title1', true);
	$this->_html.= '
	<div class="clear"></div>
	</div>
	<div class="margin-form" style="padding:0 0 10px 40px"><input type="submit" name="submitAddBlockTitle" value="'.$this->l('Update').'" class="button" style="width:150px"/></div>';
	$this->displayBlocksList1();
	$this->_html.= '
	</fieldset>

	<fieldset class="space" style="width:274px;float:left;margin-right:10px"><legend><img src="'.$this->_path.'logo.gif"/>'.$this->l('BLOCK N&ordm;2').'</legend>
	<label style="width:40px;text-align:left;padding:0">'.$this->l('Title').'</label>
	<div class="margin-form" style="padding:0 0 0 40px">';
	foreach ($languages as $language)
	{
	$blocktitle2 = $this->getBlockTitle2($language['id_lang']);
	$this->_html.= '
	<div id="title2_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
	<input type="text" size="25" name="title2_'.$language['id_lang'].'" id="title2Input_'.$language['id_lang'].'" value="'.$blocktitle2['title2'].'"/>
	<p>'.$this->l('Set the title of the block').'</p>
	</div>';
	}
	$this->_html.= $this->displayFlags($languages, $defaultLanguage, $divLangNameTitles, 'title2', true);
	$this->_html.= '
	<div class="clear"></div>
	</div>
	<div class="margin-form" style="padding:0 0 10px 40px"><input type="submit" name="submitAddBlockTitle" value="'.$this->l('Update').'" class="button" style="width:150px"/></div>';
	$this->displayBlocksList2();
	$this->_html.= '
	</fieldset>

	<fieldset class="space" style="width:274px;float:left"><legend><img src="'.$this->_path.'logo.gif"/>'.$this->l('BLOCK N&ordm;3').'</legend>
	<label style="width:40px;text-align:left;padding:0">'.$this->l('Title').'</label>
	<div class="margin-form" style="padding:0 0 0 40px">';
	foreach ($languages as $language)
	{
	$blocktitle3 = $this->getBlockTitle3($language['id_lang']);
	$this->_html.= '
	<div id="title3_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').'; float: left;">
	<input type="text" size="25" name="title3_'.$language['id_lang'].'" id="title3Input_'.$language['id_lang'].'" value="'.$blocktitle3['title3'].'"/>
	<p>'.$this->l('Set the title of the block').'</p>
	</div>';
	}
	$this->_html.= $this->displayFlags($languages, $defaultLanguage, $divLangNameTitles, 'title3', true);
	$this->_html.= '
	<div class="clear"></div>
	</div>
	<div class="margin-form" style="padding:0 0 10px 40px"><input type="submit" name="submitAddBlockTitle" value="'.$this->l('Update').'" class="button" style="width:150px"/></div>';
	$this->displayBlocksList3();
	$this->_html.= '
	</fieldset>
	<div class="clear"></div>

	</form>

	<form action="'.$_SERVER['REQUEST_URI'].'" method="post" enctype="multipart/form-data">
	<fieldset class="space"><legend><img src="'.$this->_path.'logo.gif"/>'.$this->l('EDITOR').'</legend>
	<label style="text-align:left;padding:0">'.$this->l('Insert the informations of your choice (Images, texts, links...)').'</label>
	<div class="margin-form">';
	foreach ($languages as $language)
	{
	$editor = $this->getEditor($language['id_lang']);
	$this->_html.= '
	<div id="editor_'.$language['id_lang'].'" style="display: '.($language['id_lang'] == $defaultLanguage ? 'block' : 'none').';float: left;">
	<textarea class="rte" cols="80" rows="30" id="editorInput_'.$language['id_lang'].'" name="editor_'.$language['id_lang'].'">'.$editor['editor'].'</textarea>
	</div>';}
	$this->_html.= $this->displayFlags($languages, $defaultLanguage, $divLangNameEditor, 'editor', true);
	$this->_html.= '
	<p class="clear"></p>
	</div>
	<div class="margin-form"><input type="submit" name="submitAddEditor" value="'.$this->l('Update').'" class="button" style="width:150px"/></div>
	</fieldset>
	</form>';

	if (version_compare(_PS_VERSION_, '1.4.1.0', '>=')){
	$isoTinyMCE = (file_exists(_PS_ROOT_DIR_.'/js/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en');
	$ad = dirname($_SERVER["PHP_SELF"]);
	$this->_html.= '
	<script type="text/javascript">	
	var iso = \''.$isoTinyMCE.'\' ;
	var pathCSS = \''._THEME_CSS_DIR_.'\' ;
	var ad = \''.$ad.'\' ;
	</script>
	<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce/tiny_mce.js"></script>
	<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>';
	}
	else{
	$this->_html.= '
	<script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
	<script type="text/javascript">
	tinyMCE.init({
	mode : "textareas",
	theme : "advanced",
	plugins : "safari,pagebreak,style,layer,table,advimage,advlink,inlinepopups,media,searchreplace,contextmenu,paste,directionality,fullscreen",
	theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
	theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,,|,forecolor,backcolor",
	theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media,|,ltr,rtl,|,fullscreen",
	theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,pagebreak",
	theme_advanced_toolbar_location : "top",
	theme_advanced_toolbar_align : "left",
	theme_advanced_statusbar_location : "bottom",
	theme_advanced_resizing : false,
	content_css : "'.__PS_BASE_URI__.'themes/'._THEME_NAME_.'/css/global.css",
	document_base_url : "'.__PS_BASE_URI__.'",
	width: "600",
	height: "auto",
	font_size_style_values : "8pt, 10pt, 12pt, 14pt, 18pt, 24pt, 36pt",
	template_external_list_url : "lists/template_list.js",
	external_link_list_url : "lists/link_list.js",
	external_image_list_url : "lists/image_list.js",
	media_external_list_url : "lists/media_list.js",
	file_browser_callback : "ajaxfilemanager",
	entity_encoding: "raw",
	convert_urls : false,
	language : "'.(file_exists(_PS_ROOT_DIR_.'/js/tinymce/jscripts/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en').'"});
	</script>';
	}
	}

	function getBlocks($id_lang = NULL)
	{
	$result = array();
	if (!$blocks = Db::getInstance()->ExecuteS('SELECT `id_block` FROM '._DB_PREFIX_.'footercustom'))
	return false;
	$i = 0;
	foreach ($blocks AS $block)
	{
	$result[$i]['id'] = $block['id_block'];
	$sql = 'SELECT `id_lang`, `title`, `link`, `inblock`, `position`
	FROM '._DB_PREFIX_.'footercustom_lang
	WHERE `id_block`='.(int)($block['id_block']);
	if (isset($id_lang) AND is_numeric($id_lang) AND (int)($id_lang) > 0)
	$sql .= ' AND `id_lang` = '.(int)($id_lang);
	if (!$texts = Db::getInstance()->ExecuteS($sql))
	return false;
	foreach ($texts AS $text)
	{
	$result[$i]['title_'.$text['id_lang']] = $text['title'];
	$result[$i]['link_'.$text['id_lang']] = $text['link'];
	$result[$i]['inblock_'.$text['id_lang']] = $text['inblock'];
	$result[$i]['position_'.$text['id_lang']] = $text['position'];
	}
	$i++;
	}
	return $result;
	}

	function listBlocks()
	{
	$blocks = $this->getBlocks();
	global $currentIndex, $cookie, $adminObj;
	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	$languages = Language::getLanguages();
	if ($blocks)
	{
	$this->_html.= '
	<script type="text/javascript">
	var currentUrl = \''.$currentIndex.'&configure='.$this->name.'\';
	var token=\''.$adminObj->token.'\';
	var blocks = new Array();';
	$var='';
	foreach ($blocks AS $block)
	{
	$var .= 'blocks['.$block['id'].'] = new Array(';
	$i=0;
	foreach ($languages AS $language)
	{
	if ($i>0)
	$var .= ",";
	$var .= $language['id_lang'];
	$title = isset($block['title_'.$language['id_lang']]) ? $block['title_'.$language['id_lang']] : '';
	$var .= ',\''.addslashes($title).'\'';
	$link = isset($block['link_'.$language['id_lang']]) ? $block['link_'.$language['id_lang']] : '';
	$var .= ',\''.addslashes($link).'\'';
	$inblock = isset($block['inblock_'.$language['id_lang']]) ? $block['inblock_'.$language['id_lang']] : '';
	$var .= ',\''.addslashes($inblock).'\'';
	$position = isset($block['position_'.$language['id_lang']]) ? $block['position_'.$language['id_lang']] : '';
	$var .= ',\''.addslashes($position).'\'';
	$i++;
	}
	$var .= ');';
	}
	$var .= '</script>';
	$this->_html.= $var;
	}
	}

	function displayBlocksList1()
	{
	$blocks = $this->getBlocks();
	global $cookie;
	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	$languages = Language::getLanguages();
	$this->_html.= '
	<table class="table" cellspacing="0" style="background:#FFF;border-bottom:none;width:274px;font-size:13px">
	<tr>
	<th style="border-bottom:1px solid #DFD5C3;width:20px;font-weight:bold;text-align:center">'.$this->l('Position').'</th>
	<th style="border-bottom:1px solid #DFD5C3;border-left:1px solid #DFD5C3;font-weight:bold">'.$this->l('Title').'</th>
	<th style="border-bottom:1px solid #DFD5C3;border-left:1px solid #DFD5C3;width:45px;font-weight:bold;text-align:center">'.$this->l('Actions').'</th>
	</tr>';
	foreach ($blocks AS $block)
	{
	$position = isset($block['position_'.$cookie->id_lang]) ? $block['position_'.$cookie->id_lang] : $block['position_'.$defaultLanguage];
	$title = isset($block['title_'.$cookie->id_lang]) ? $block['title_'.$cookie->id_lang] : $block['title_'.$defaultLanguage];
	if ($block['inblock_'.$cookie->id_lang] == 1){
	$this->_html.= '
	<tr>
	<td style="border-bottom:1px solid #DFD5C3;text-align:center">'.$position.'</td>
	<td style="border-bottom:1px solid #DFD5C3;border-left:1px solid #DFD5C3">'.$title.'</td>
	<td style="border-bottom:1px solid #DFD5C3;border-left:1px solid #DFD5C3;text-align:center">
	<img src="'._PS_ADMIN_IMG_.'edit.gif" onclick="blockEdit('.$block['id'].')" style="cursor:pointer;padding:0" title="'.$this->l('Edit').'"/>
	<img src="'._PS_ADMIN_IMG_.'delete.gif" onclick="blockDelete('.$block['id'].')" style="cursor:pointer;padding:0" title="'.$this->l('Delete').'"/>
	</td>
	</tr>';}
	}
	$this->_html.= '
	</table>
	<input type="hidden" id="languageFirst" value="'.$languages[0]['id_lang'].'"/>
	<input type="hidden" id="languageNb" value="'.sizeof($languages).'"/>';
	}

	function displayBlocksList2()
	{
	$blocks = $this->getBlocks();
	global $cookie;
	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	$languages = Language::getLanguages();
	$this->_html.= '
	<table class="table" cellspacing="0" style="background:#FFF;border-bottom:none;width:274px;font-size:13px">
	<tr>
	<th style="border-bottom:1px solid #DFD5C3;width:20px;font-weight:bold;text-align:center">'.$this->l('Position').'</th>
	<th style="border-bottom:1px solid #DFD5C3;border-left:1px solid #DFD5C3;font-weight:bold">'.$this->l('Title').'</th>
	<th style="border-bottom:1px solid #DFD5C3;border-left:1px solid #DFD5C3;width:45px;font-weight:bold;text-align:center">'.$this->l('Actions').'</th>
	</tr>';
	foreach ($blocks AS $block)
	{
	$position = isset($block['position_'.$cookie->id_lang]) ? $block['position_'.$cookie->id_lang] : $block['position_'.$defaultLanguage];
	$title = isset($block['title_'.$cookie->id_lang]) ? $block['title_'.$cookie->id_lang] : $block['title_'.$defaultLanguage];
	if ($block['inblock_'.$cookie->id_lang] == 2){
	$this->_html.= '
	<tr>
	<td style="border-bottom:1px solid #DFD5C3;text-align:center">'.$position.'</td>
	<td style="border-bottom:1px solid #DFD5C3;border-left:1px solid #DFD5C3">'.$title.'</td>
	<td style="border-bottom:1px solid #DFD5C3;border-left:1px solid #DFD5C3;text-align:center">
	<img src="'._PS_ADMIN_IMG_.'edit.gif" onclick="blockEdit('.$block['id'].')" style="cursor:pointer;padding:0" title="'.$this->l('Edit').'"/>
	<img src="'._PS_ADMIN_IMG_.'delete.gif" onclick="blockDelete('.$block['id'].')" style="cursor:pointer;padding:0" title="'.$this->l('Delete').'"/>
	</td>
	</tr>';}
	}
	$this->_html.= '
	</table>
	<input type="hidden" id="languageFirst" value="'.$languages[0]['id_lang'].'"/>
	<input type="hidden" id="languageNb" value="'.sizeof($languages).'"/>';
	}

	function displayBlocksList3()
	{
	$blocks = $this->getBlocks();
	global $cookie;
	$defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
	$languages = Language::getLanguages();
	$this->_html.= '
	<table class="table" cellspacing="0" style="background:#FFF;border-bottom:none;width:274px;font-size:13px">
	<tr>
	<th style="border-bottom:1px solid #DFD5C3;width:20px;font-weight:bold;text-align:center">'.$this->l('Position').'</th>
	<th style="border-bottom:1px solid #DFD5C3;border-left:1px solid #DFD5C3;font-weight:bold">'.$this->l('Title').'</th>
	<th style="border-bottom:1px solid #DFD5C3;border-left:1px solid #DFD5C3;width:45px;font-weight:bold;text-align:center">'.$this->l('Actions').'</th>
	</tr>';
	foreach ($blocks AS $block)
	{
	$position = isset($block['position_'.$cookie->id_lang]) ? $block['position_'.$cookie->id_lang] : $block['position_'.$defaultLanguage];
	$title = isset($block['title_'.$cookie->id_lang]) ? $block['title_'.$cookie->id_lang] : $block['title_'.$defaultLanguage];
	if ($block['inblock_'.$cookie->id_lang] == 3){
	$this->_html.= '
	<tr>
	<td style="border-bottom:1px solid #DFD5C3;text-align:center">'.$position.'</td>
	<td style="border-bottom:1px solid #DFD5C3;border-left:1px solid #DFD5C3">'.$title.'</td>
	<td style="border-bottom:1px solid #DFD5C3;border-left:1px solid #DFD5C3;text-align:center">
	<img src="'._PS_ADMIN_IMG_.'edit.gif" onclick="blockEdit('.$block['id'].')" style="cursor:pointer;padding:0" title="'.$this->l('Edit').'"/>
	<img src="'._PS_ADMIN_IMG_.'delete.gif" onclick="blockDelete('.$block['id'].')" style="cursor:pointer;padding:0" title="'.$this->l('Delete').'"/>
	</td>
	</tr>';}
	}
	$this->_html.= '
	</table>
	<input type="hidden" id="languageFirst" value="'.$languages[0]['id_lang'].'"/>
	<input type="hidden" id="languageNb" value="'.sizeof($languages).'"/>';
	}

	function hookHeader($params)
	{
	global $smarty;
	$smarty->assign(array(
	'this_path' => $this->_path));
	if (version_compare(_PS_VERSION_, '1.4.0.0', '>=')){
	Tools::addCSS($this->_path.'css/footercustom.css', 'all');}
	else{
	return $this->display(__FILE__, 'header.tpl');}
	}

	function hookFooter($params)
	{
	global $smarty, $cookie;

	if (Tools::isSubmit('submitNewsletter'))
	{
	$this->newsletterRegistration();
	if ($this->error)
	{
	$smarty->assign(array(
	'msg' => $this->error,
	'nw_value' => isset($_POST['email']) ? pSQL($_POST['email']) : false,											'nw_error' => true,
	'action' => $_POST['action']));
	}
	elseif ($this->valid)
	{
	$smarty->assign(array(
	'msg' => $this->valid,
	'nw_error' => false));
	}
	}

	$blocktitle1 = $this->getBlockTitle1($cookie->id_lang);
	$blocktitle2 = $this->getBlockTitle2($cookie->id_lang);
	$blocktitle3 = $this->getBlockTitle3($cookie->id_lang);
	$sql1 = 'SELECT f.`id_block` AS id, fl.`title` AS title, fl.`link` AS link
  	FROM `'._DB_PREFIX_.'footercustom` f
    	JOIN `'._DB_PREFIX_.'footercustom_lang` fl ON (f.`id_block` = fl.`id_block` AND fl.`id_lang` = '.intval($cookie->id_lang).')
  	WHERE `inblock` = 1
	ORDER BY `position`';
	$blocks1 = Db::getInstance()->ExecuteS($sql1);
	$sql2 = 'SELECT f.`id_block` AS id, fl.`title` AS title, fl.`link` AS link
  	FROM `'._DB_PREFIX_.'footercustom` f
    	JOIN `'._DB_PREFIX_.'footercustom_lang` fl ON (f.`id_block` = fl.`id_block` AND fl.`id_lang` = '.intval($cookie->id_lang).')
  	WHERE `inblock` = 2
	ORDER BY `position`';
	$blocks2 = Db::getInstance()->ExecuteS($sql2);
	$sql3 = 'SELECT f.`id_block` AS id, fl.`title` AS title, fl.`link` AS link
  	FROM `'._DB_PREFIX_.'footercustom` f
    	JOIN `'._DB_PREFIX_.'footercustom_lang` fl ON (f.`id_block` = fl.`id_block` AND fl.`id_lang` = '.intval($cookie->id_lang).')
  	WHERE `inblock` = 3
	ORDER BY `position`';
	$blocks3 = Db::getInstance()->ExecuteS($sql3);
	$editor = $this->getEditor($cookie->id_lang);
	$smarty->assign(array(
	'fc_news' => Configuration::get('FC_NEWS'),
	'fc_img' => Configuration::get('FC_IMG'),
	'fc_addthis' => Configuration::get('FC_ADDTHIS'),
	'fc_links' => Configuration::get('FC_LINKS'),
	'fc_editor' => Configuration::get('FC_EDITOR'),
	'blocktitle1' => $blocktitle1['title1'],
	'blocktitle2' => $blocktitle2['title2'],
	'blocktitle3' => $blocktitle3['title3'],
  	'blocks1' => $blocks1,
  	'blocks2' => $blocks2,
  	'blocks3' => $blocks3,
	'editor' => $editor['editor'],
	'ps_version' => _PS_VERSION_));
	return $this->display(__FILE__, 'footercustom.tpl');
	}
}
?>