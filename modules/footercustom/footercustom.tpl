{* Module Prestashop FooterCustom
 * Copyright Prestacrea
 * Author: Prestacrea
 * Website: http://www.prestacrea.com *}

<div id="footercustom">

{if $fc_news == 1}
<div id="footernewsletter_container">
<div id="footernewsletter">
<form {if $ps_version >= '1.4.0.0'}action="{$link->getPageLink('index.php')}"{else}action="{$base_dir_ssl}"{/if} method="post">
<p><span>{l s='Newsletter' mod='footercustom'} !</span>
{if isset($msg) && $msg}
<span class="{if $nw_error}footerwarning{else}footersuccess{/if}">{$msg}</span>
{else}
{l s='Subscribe to our newsletter' mod='footercustom'}
{/if}
<input type="text" class="footerinput" name="email" size="18" value="{l s='Your mail address' mod='footercustom'}" onfocus="javascript:if(this.value=='{l s='Your mail address' mod='footercustom'}')this.value='';" onblur="javascript:if(this.value=='')this.value='{l s='Your mail address' mod='footercustom'}';"/>
<select class="footerselect" name="action">
<option value="0"{if isset($action) && $action == 0} selected="selected"{/if}>{l s='Subscribe' mod='footercustom'}</option>
<option value="1"{if isset($action) && $action == 1} selected="selected"{/if}>{l s='Unsubscribe' mod='footercustom'}</option>
</select>
<input type="submit" value="{l s='Validate' mod='footercustom'}" class="button" name="submitNewsletter"/>
</p>
</form>
</div>
</div>
{/if}

{if $fc_links == 1}
<div id="footerlinks_container">

<div id="footerlinks">

<div id="footerimage_container">

{if $fc_img == 1}
<div id="footerimage">
<img src="{$base_dir_ssl}modules/footercustom/img/footer_img.png" alt=""/>
</div>
{/if}

{if $fc_addthis == 1}
<div id="footeraddthis">
<div class="addthis_toolbox addthis_default_style ">
  <a class="addthis_button_facebook"></a>
  <a class="addthis_button_twitter"></a>
  <a class="addthis_button_email"></a>
  <a class="addthis_button_google"></a>
  <a class="addthis_button_compact"></a>
</div>
<script type="text/javascript" src="https://s7.addthis.com/js/250/addthis_widget.js#pubid=xa-4debecd65fac86f2"></script>
</div>
{/if}

</div>

<div id="footerlinks_ul">

<ul class="footerinformations">
<li class="footerinformations"><span>{$blocktitle1}</span></li>
{foreach from=$blocks1 item=block name=loop}
<li>{if $block.link}<a href="{$block.link}" title="{$block.title}">{/if}{if $block.title}{$block.title}{/if}{if $block.link}</a>{/if}
</li>
{/foreach}
</ul>

<ul class="footerproducts">
<li class="footerproducts"><span>{$blocktitle2}</span></li>
{foreach from=$blocks2 item=block name=loop}
<li>{if $block.link}<a href="{$block.link}" title="{$block.title}">{/if}{if $block.title}{$block.title}{/if}{if $block.link}</a>{/if}
</li>
{/foreach}
</ul>

<ul class="footercompany">
<li class="footercompany"><span>{$blocktitle3}</span></li>
{foreach from=$blocks3 item=block name=loop}
<li>{if $block.link}<a href="{$block.link}" title="{$block.title}">{/if}{if $block.title}{$block.title}{/if}{if $block.link}</a>{/if}
</li>
{/foreach}
</ul>

</div>

</div>
</div>
{/if}

{if $fc_editor == 1}
<div id="footereditor_container">
<div id="footereditor">
{foreach from=$editor item=content name=loop}
{$editor}
{/foreach}
</div>
</div>
{/if}

</div>