/* Module Prestashop FooterCustom
 * Copyright Prestacrea
 * Author: Prestacrea
 * Website: http://www.prestacrea.com */

function blockEdit(blockId)
{
	getE('id').value = blockId;
	var fc = parseInt(getE('languageNb').value - 1);
	for (var i=0; i<=fc; i++) {
	getE('titleInput_' + blocks[blockId][i * 5]).value = blocks[blockId][i * 5 + 1];
	getE('linkInput_' + blocks[blockId][i * 5]).value = blocks[blockId][i * 5 + 2];
	getE('inblockInput_' + blocks[blockId][i * 5]).value = blocks[blockId][i * 5 + 3];
	getE('positionInput_' + blocks[blockId][i * 5]).value = blocks[blockId][i * 5 + 4];

	}

	if (document.all){
	getE('submitAddBlock').style.setAttribute('cssText', 'display:none');
	getE('submitEditBlock').style.setAttribute('cssText', '');
	}
	getE('submitAddBlock').setAttribute('style', 'display:none');
	getE('submitEditBlock').setAttribute('style', '');
}

function blockDelete(blockId)
{
	document.location.replace(currentUrl+'&id='+blockId+'&token='+token);
}