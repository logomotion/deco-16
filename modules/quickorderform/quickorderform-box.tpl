<!-- MODULE QuickOrderForm -->
<div id="quickorderform_block_left" class="block">
	<h4>{l s='Order quicker !' mod='quickorderform'}</h4>
	<div class="block_content quickorderform_block">
		<p class="quickorder_image">
			<a href="{$this_path}quickorderform-form.php" title="{l s='Go to Quick Order Form' mod='quickorderform'}"><img src="{$image}" {if $qof_title}alt="{$qof_title}" title="{$qof_title}"{/if} width="155"  height="163" /></a>
		</p>
		<p>
			<a href="{$this_path}quickorderform-form.php" title="{l s='Quick Order' mod='quickorderform'}">{l s='Quick Order' mod='quickorderform'}</a>
		</p>
	</div>
</div>
<!-- /MODULE QuickOrderForm -->
