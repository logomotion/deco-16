<!-- MODULE QuickOrderForm 15 -->
<div id="quickorderform_block_left" class="block">
    <p class="title_block">
        <a href="{$link->getModuleLink('quickorderform')}" title="{l s='Quick Order' mod='quickorderform'}">
            {l s='Order quicker !' mod='quickorderform'}
        </a>
    </p>
	<div class="block_content quickorderform_block">
		<p class="quickorder_image">
			<a href="{$link->getModuleLink('quickorderform')}" title="{l s='Go to Quick Order Form' mod='quickorderform'}"><img src="{$image}" {if $qof_title}alt="{$qof_title}" title="{$qof_title}"{/if} width="155" height="163" class="img-responsive" /></a>
		</p>
        <div>
            <a class="btn btn-default button button-small" href="{$link->getModuleLink('quickorderform')}" title="{l s='Quick Order' mod='quickorderform'}">
                <span>{l s='Quick Order' mod='quickorderform'}<i class="icon-chevron-right right"></i></span>
            </a>
        </div>
	</div>
</div>
<!-- /MODULE QuickOrderForm 15 -->
