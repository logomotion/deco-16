<?php

class QuickOrderFormDefaultModuleFrontController extends ModuleFrontController
{
	public $ssl = true;

	public function initContent()
	{
		if (QuickOrderForm::hasAccess())
		{
			parent::initContent();

			$this->context->smarty->assign(array('search_ssl'       => (int)Tools::usingSecureMode(),
												 'quantity_buttons' => Configuration::get('BLOCKQUICKORDER_QUANTITY'),
												 'qof_empty' => Configuration::get('BLOCKQUICKORDER_EMPTY'),
												 'qof_nblines' => Configuration::get('BLOCKQUICKORDER_NBLINES')-1,
												 'ps'             => (_PS_VERSION_ >= 1.6) ? 'ps16' : 'ps15'));

			$this->setTemplate('quickorderform-form.tpl');
		}
		else
			Tools::redirect(__PS_BASE_URI__);

	}
}