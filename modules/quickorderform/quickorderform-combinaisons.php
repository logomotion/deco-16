<?php

require('../../config/config.inc.php');
require('../../init.php');

global $cookie;
$return = '';

$monproduct = new Product($_POST['id_product'], true, (int)$cookie->id_lang);
if (isset($_POST['id_product_attribute']) && $_POST['id_product_attribute'])
	$monattrib = $_POST['id_product_attribute'];

if (!empty($monproduct))
{
	// combinaison
	$attributesGroups = $monproduct->getAttributesGroups((int) ($cookie->id_lang));
}
else
{
	echo false;
	exit();
}

if(!empty($attributesGroups))
{
	// prod with combinaison
	if (is_array($attributesGroups) AND $attributesGroups) {
		foreach ($attributesGroups AS $k => $row) {
			if ( (isset($monattrib)) && ($row['id_product_attribute'] == $monattrib) )
			{
				echo $row['minimal_quantity'];
				exit();
			}
			else
			{
				$combinations[$row['id_product_attribute']]['attributes_values'][$row['id_attribute_group']] = $row['attribute_name'];
				$combinations[$row['id_product_attribute']]['minimal_quantity'] = $row['minimal_quantity'];
				$combinations[$row['id_product_attribute']]['default_on'] = $row['default_on'];
			}
		}
	}
}
else
{
	// prod without combinaison
	echo "ProdSansDecli##".$monproduct->minimal_quantity;
	exit();
}

foreach ($combinations as $kCombination => $vCombination) {
	$mastring = $kCombination.'#';
	foreach ($vCombination["attributes_values"] as $composant) {
		$mastring .= $composant. ' - ';
	}
	$mastring = rtrim($mastring,' - ');
	$mastring .= '#'.$vCombination["minimal_quantity"];
	$mastring .= '#'.$vCombination["default_on"];
	$return .= $mastring."##";
}
$return = rtrim($return,'##');

echo ( $return );