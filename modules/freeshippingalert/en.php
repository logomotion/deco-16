<?php

global $_MODULE;
$_MODULE = array();
$_MODULE['<{freeshippingalert}prestashop>blockfreeship_2d8f3d3ca086eb77793eef99a902ec15'] = 'Minimum order';
$_MODULE['<{freeshippingalert}prestashop>blockfreeship_bbf5bc49e8b4e675757e66f95c6e4e1e'] = 'Minimum order is defined';
$_MODULE['<{freeshippingalert}prestashop>blockfreeship_1ec33b80127419bbc4f8f743f151c1a1'] = 'purchases';
$_MODULE['<{freeshippingalert}prestashop>blockfreeship_e0c59b98715123d23ce9ae1870b154b1'] = 'You still have to order';
$_MODULE['<{freeshippingalert}prestashop>freeshippingalert_665ee5fccac789e654964e0f111b4679'] = 'Minimum cart';
$_MODULE['<{freeshippingalert}prestashop>freeshippingalert_654e887157351401e1e870bdfb84f9c2'] = 'Minimum cart';
$_MODULE['<{freeshippingalert}prestashop>freeshippingalert_4c96619779761c1b4b03d075d8e5bb1f'] = 'Minimum amount to order';
$_MODULE['<{freeshippingalert}prestashop>freeshippingalert_dad1f8d794ee0dd7753fe75e73b78f31'] = 'Zones';
$_MODULE['<{freeshippingalert}prestashop>freeshippingalert_81f620545faeb419cae4932272def201'] = 'Groups';
$_MODULE['<{freeshippingalert}prestashop>freeshippingalert_1c3aef2983b7c25b9a14b2b40481ddb9'] = 'URL redirection';
$_MODULE['<{freeshippingalert}prestashop>freeshippingalert_aed88202e2432ee2a613f9120ef7486b'] = 'Save';
