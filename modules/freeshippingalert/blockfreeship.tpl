{*
 *  @author logomotion <production@logomotion.fr>
 *  @copyright  2015 logomotion
 *  @version  1.0
 *}
{assign var='blockfreeship_flag' value='Cart::BOTH_WITHOUT_SHIPPING'|constant}
{if $cookie->isLogged()}
<div id="blockfreeship" class="block{if $freeship_mini-$cart->getOrderTotal(true, $blockfreeship_flag)<=0 or $cart->getOrderTotal(true, $blockfreeship_flag) == 0} hidden{/if}">
	<h4>{l s='Livraison gratuite' mod='freeshippingalert'}</h4>
	<div class="block_content">
			<p>{l s='Vous devez encore commander' mod='freeshippingalert'} <span class="freeship_diff">{convertPrice price=$freeship_mini-$cart->getOrderTotal(true, $blockfreeship_flag)}</span> {l s='pour une livraison gratuite' mod='freeshippingalert'}
			</p>
			<p><small>{l s='Limité à la France métropolitaine' mod='freeshippingalert'}</small></p>
	</div>
</div>
{/if}