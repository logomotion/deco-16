<script>
{literal}
	$(document).ready(function(){
{/literal}
{if $isOrder}
	{literal}
			var alertText = "{/literal}{l s='Le minimum de commande est définie à' mod='freeshippingalert'} {$freeship_mini}€ {l s="d'achats" mod='freeshippingalert'}{literal}";
			$('.step').after('<div class="alert alert-danger freeship-danger hidden"><p>' + alertText + '</p></div>');
		});
	{/literal}
{/if}
{if $diff_freeship>0}
{literal}
	$('.standard-checkout').addClass('hidden');
	$('.freeship-danger').removeClass('hidden');
{/literal}
{/if}
	}
</script>