<?php
/*
 *  @author logomotion <production@logomotion.fr>
 *  @copyright  2015 logomotion
 *  @version  1.0
 */

if (!defined('_CAN_LOAD_FILES_'))
	exit;

class FreeShippingAlert extends Module{

	public function __construct(){
		$this->name = 'freeshippingalert';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'Logomotion';
		$this->need_instance = 1;

		parent::__construct();

		$this->displayName = $this->l('Livraison gratuite');
		$this->description = $this->l("Permet d'afficher un message indiquant combien un client doit commander avant la livraison gratuite.");
	}

	public function install(){
		if(!parent::install() ||
			!$this->installDB() ||
			!$this->registerHook('header') ||
			!$this->registerHook('footer') ||
			!$this->registerHook('rightColumn') ||
			!Configuration::updateValue('FREESHIPPINGALERT_URL', ''))
			return false;
		return true;
	}

	public function installDB(){
		$sql1 = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."e_freeship` (
		  `id_freeship` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `fk_id_zone` int(8) NOT NULL,
		  `fk_id_group` int(8) NOT NULL,
		  `freeship_mini` double(10,2) NOT NULL DEFAULT '0.00',
		  PRIMARY KEY (`id_freeship`)
		) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
		if(!Db::getInstance()->Execute($sql1)){
			return false;
		}
		return true;
	}

	public function uninstall(){
		if (!parent::uninstall() ||
			!$this->uninstallDB() ||
			!Configuration::deleteByName('FREESHIPPINGALERT_URL'))
			return false;
		return true;
	}

	public function uninstallDB(){
		$sql1 = "DROP TABLE ps_e_freeship";
		if(!Db::getInstance()->Execute($sql)){
			return false;
		}
		return true;
	}

	public function getContent(){
		global $currentIndex, $cookie;
		if(isset($_POST['saveFreeship']) && is_array($_POST['tab_freeship'])){
			foreach($_POST['tab_freeship'] as $key_group => $val_group){
				if(is_array($val_group)){
					foreach($val_group as $key_zone => $val_zone){
						if($this->freeshipExist($key_group,$key_zone)){
							$this->updateFreeship($key_group,$key_zone,$val_zone);
						}
						else{
							$this->insertFreeship($key_group,$key_zone,$val_zone);
						}
					}
				}
			}
			Configuration::updateValue('FREESHIPPINGALERT_URL',Tools::getValue('FREESHIPPINGALERT_URL'));
		}
		$groups = Group::getGroups($cookie->id_lang);
		$zones = Zone::getZones(true);
		$tab_freeship = $this->getFreeships();
		$str = '
		</fieldset><br />
		<form method="POST" action="'.Tools::htmlentitiesUTF8($_SERVER['REQUEST_URI']).'">
		<fieldset>
			<legend>'.$this->l('Panier minimum').'</legend>
			<h3>'.$this->l('Montant minimum avant livraison gratuite').'</h3>';
			$str .= '<table class="table" cellspacing="0" cellpadding="0">';
			// GROUPS
			$str .= '<tr>
						<th>'.$this->l('Zones').' / '.$this->l('Groupes').'</th>';
						if(is_array($groups)){
							foreach($groups as $key_g => $val_g){
								$str .= '<th>'.$val_g['name'].'</th>';
							}
						}
			$str .= '</tr>';
			// ZONES
			if(is_array($zones)){
				foreach($zones as $key_z => $val_z){
					$str .= '<tr>
								<td>'.$val_z['name'].'</td>';
								if(is_array($groups)){
									foreach($groups as $key_g => $val_g){
										$str .= '<td><input type="text" name="tab_freeship['.$val_g['id_group'].']['.$val_z['id_zone'].']" value="'.(isset($tab_freeship[$val_g['id_group']][$val_z['id_zone']])?$tab_freeship[$val_g['id_group']][$val_z['id_zone']]['freeship_mini']:0).'" size="8" style="text-align:right;" /> €</td>';
									}
								}
					$str .= '</tr>';
				}
			}
			$str .= '</table>';
			$str .= '<br />
					<label>'.$this->l('Url de redirection').'</label>
					<div class="margin-form">
						<input type="text" name="FREESHIPPINGALERT_URL" value="'.Tools::getValue('FREESHIPPINGALERT_URL',Configuration::get('FREESHIPPINGALERT_URL')).'" size="80" />
					</div>
					<center>
						<input type="submit" name="saveFreeship" value="'.$this->l('Enregistrer').'" class="button" />
					</center>';
		$str .= '</fieldset>
		</form>';
		return $str;
	}

	public function hookHeader($params){
		Tools::addCSS($this->_path . 'freeshippingalert.css', 'screen');
		global $smarty;
		$trans = array('.php'=>'');
			if(isset($params['cart']->id) && $params['cart']->id>0){
				if($params['cookie']->isLogged()){
					$id_group = Customer::getDefaultGroupId($params['cookie']->id_customer);
					$id_zone = Country::getIdZone(Customer::getCurrentCountry($params['cookie']->id_customer));
				}
				else{
					$id_group = 1;
					$id_zone = Country::getIdZone(Configuration::get('PS_COUNTRY_DEFAULT'));
				}
				$freeship_mini = $this->getMontantFreeship($id_group,$id_zone);
				$total_products_cart = $params['cart']->getOrderTotal(true,Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING);
				$diff_freeship = $freeship_mini - $total_products_cart;
				$smarty->assign(array(
					'freeship_mini' => $freeship_mini,
					'isOrder' => (get_class($this->context->controller) === 'OrderController'),
					'diff_freeship' => $diff_freeship
					));
				return $this->display(__FILE__, 'blockfreeshipheader.tpl');
			}

	}
	public function hookRightColumn($params){
		global $smarty;
		if($params['cookie']->isLogged()){
			$id_group = Customer::getDefaultGroupId($params['cookie']->id_customer);
			$id_zone = Country::getIdZone(Customer::getCurrentCountry($params['cookie']->id_customer));
		}
		else{
			$id_group = 1;
			$id_zone = Country::getIdZone(Configuration::get('PS_COUNTRY_DEFAULT'));
		}
		$freeship_mini = $this->getMontantFreeship($id_group,$id_zone);
		$total_products_cart = $params['cart']->getOrderTotal(true,Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING);
		$diff_freeship = $freeship_mini - $total_products_cart;
		$smarty->assign(array(
			'freeship_mini' => $freeship_mini,
			'diff_freeship' => $diff_freeship,
			'cart' => $params['cart']
		));
		return $this->display(__FILE__, 'blockfreeship.tpl');
	}

	public function hookLeftColumn($params){
		return $this->hookRightColumn($params);
	}

	public function hookFooter($params){
		global $smarty;
		

	}

	##### FUNCTIONS #####

	function freeshipExist($id_group,$id_zone){
		$res = Db::getInstance()->getRow("SELECT COUNT(id_freeship) as nb_item FROM "._DB_PREFIX_."e_freeship WHERE fk_id_group='$id_group' AND fk_id_zone='$id_zone' ");
		if(isset($res['nb_item']) && $res['nb_item']>0){
			return true;
		}
		else{
			return false;
		}
	}

	function getFreeships(){
		$tab_freeship = array();
		$sql = "SELECT id_freeship,fk_id_zone,fk_id_group,freeship_mini
				FROM "._DB_PREFIX_."e_freeship ";
		$res = Db::getInstance()->ExecuteS($sql);
		if(is_array($res)){
			foreach($res as $key => $val){
				$tab_freeship[$val['fk_id_group']][$val['fk_id_zone']] = $val;
			}
		}
		return $tab_freeship;
	}

	function getMontantFreeship($id_group,$id_zone){
		$sql = "SELECT id_freeship,freeship_mini
				FROM "._DB_PREFIX_."e_freeship
				WHERE fk_id_group='$id_group' AND fk_id_zone='$id_zone' ";
		$res = Db::getInstance()->getRow($sql);
		return $res['freeship_mini'];
	}

	function insertFreeship($id_group,$id_zone,$freeship_mini){
		$sql = "INSERT INTO "._DB_PREFIX_."e_freeship
				(fk_id_zone,fk_id_group,freeship_mini)
				VALUES
				('$id_zone','$id_group','$freeship_mini') ";
		Db::getInstance()->Execute($sql);
	}

	function updateFreeship($id_group,$id_zone,$freeship_mini){
		$sql = "UPDATE "._DB_PREFIX_."e_freeship
				SET freeship_mini='$freeship_mini'
				WHERE fk_id_zone='$id_zone' AND fk_id_group='$id_group' ";
		Db::getInstance()->Execute($sql);
	}

}

?>