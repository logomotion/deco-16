{*
* 2007-2014 PrestaShop
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2014 PrestaShop SA
* @license   http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
* International Registered Trademark & Property of PrestaShop SA
*}

<div class="payment_module atos">
	{if $atos}
		<p class="bold teaser">{l s='Payer par carte bancaire' mod='atos'}</p>
		{$atos}
	{else}
		{l s='Your order total must be greater than' mod='atos'} {displayPrice price=1} {l s='in order to pay by credit card.' mod='atos'}
	{/if}
</div>