{*
* 2007-2014 PrestaShop
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
* @author    PrestaShop SA <contact@prestashop.com>
* @copyright 2007-2014 PrestaShop SA
* @license   http://addons.prestashop.com/en/content/12-terms-and-conditions-of-use
* International Registered Trademark & Property of PrestaShop SA
*}

{literal}
<script>
	ps_version = '{/literal}{$ps_version|intval}{literal}';
	multishop = '{/literal}{$multishop|intval}{literal}';
	debug_mode = '{/literal}{$debug_mode|intval}{literal}';
{/literal}
</script>

{if $ps_version == 0}
<div class="bootstrap">
	<!-- Beautiful header -->
	{include file="./header.tpl"}
{/if}
	<!-- Module content -->
	<div id="modulecontent" class="clearfix" role="{$is_submit|intval}">

		{if $safe_mode || !$exec || !$request || !$response || !$shop_enable}
		<div class="alert alert-danger" style="margin-left:15px; margin-right: 15px;">
			<b>{l s='You have a misconfiguration' mod='atos'}</b>
			<p>
				{if $safe_mode}<li>{l s='safe_mode is enabled, contact your hoster.' mod='atos'}</li>{/if}
			</p>
			<p>
				{if !$exec}<li>{l s='exec() function is forbidden, contact your hoster.' mod='atos'}</li>{/if}
			</p>
			<p>
				{if !$request}<li>{$bin_dir|escape:'htmlall':'UTF-8'}request{if $is_win|intval == 1}.exe{/if} {l s='is not executable, contact your hoster' mod='atos'}</li>{/if}
			</p>
			<p>
				{if !$response}<li>{$bin_dir|escape:'htmlall':'UTF-8'}response{if $is_win|intval == 1}.exe{/if} {l s='is not executable, contact your hoster' mod='atos'}</li>{/if}
			</p>
			<p>
				{if !$shop_enable}<li>{l s="Your shop is under maintenance. So your bank can't access to the orders validation file." mod='atos'}</li>{/if}
			</p>
		</div>
		{/if}

		<!-- Nav tabs -->
		<div class="col-lg-2">
			<div class="list-group">
				<a href="#documentation" class="list-group-item active" data-toggle="tab"><i class="icon-book"></i> {l s='Documentation' mod='atos'}</a>
				<a href="#conf" class="list-group-item" data-toggle="tab"><i class="icon-cogs"></i> {l s='Configuration' mod='atos'}</a>
				<a href="#contacts" class="contactus list-group-item" data-toggle="tab"><i class="icon-envelope"></i> {l s='Contact' mod='atos'}</a>
			</div>
			<div class="list-group">
				<a class="list-group-item"><i class="icon-info"></i> {l s='Version' mod='atos'} {$module_version|escape:'htmlall':'UTF-8'}</a>
			</div>
		</div>
		<!-- Tab panes -->
		<div class="tab-content col-lg-10">
			<div class="tab-pane active panel" id="documentation">
				{include file="./tabs/documentation.tpl"}
			</div>
			<div class="tab-pane panel" id="conf">
				{include file="./tabs/conf.tpl"}
			</div>
			{include file="./tabs/contact.tpl"}
		</div>
	</div>
{if $ps_version == 0}
	<!-- Manage translations -->
	{include file="./translations.tpl"}
</div>
{/if}