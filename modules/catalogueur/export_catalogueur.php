<?php

include(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');

ini_set('display_errors', 0);

function clean_string_input($string){
	$search = array(
        '/[\x00-\x1f]/i', // Caract�res de contr�le
		'/[\x7F]/i', // DEL
    );
    $replace = array(
		' ',
		' ',
    );
	return html_entity_decode(htmlspecialchars(strip_tags(preg_replace($search,$replace,$string))));
}

function print_product($cookie,$name_category_1,$name_category_2,$name_category_3,$name_category_4,$name_category_5,$name_category_6,$marque,$id,$name,$description,$prix_promo,$prix,$url,$image="",$meta_title,$meta_keywords,$meta_description,$references){

	echo "<product>\n";
		echo "<categorie1><![CDATA[".clean_string_input($name_category_1)."]]></categorie1>\n";
		echo "<categorie2><![CDATA[".clean_string_input($name_category_2)."]]></categorie2>\n";
		echo "<categorie3><![CDATA[".clean_string_input($name_category_3)."]]></categorie3>\n";
		echo "<categorie4><![CDATA[".clean_string_input($name_category_4)."]]></categorie4>\n";
		echo "<categorie5><![CDATA[".clean_string_input($name_category_5)."]]></categorie5>\n";
		echo "<categorie6><![CDATA[".clean_string_input($name_category_6)."]]></categorie6>\n";
		echo "<marque><![CDATA[".clean_string_input($marque)."]]></marque>\n";
		echo "<identifiant_unique><![CDATA[".$id."]]></identifiant_unique>\n";
		echo "<titre><![CDATA[".stripslashes(clean_string_input($name))."]]></titre>\n";
		echo "<meta_title><![CDATA[".stripslashes(clean_string_input($meta_title))."]]></meta_title>\n";
		echo "<meta_keyword><![CDATA[".stripslashes(clean_string_input($meta_keywords))."]]></meta_keyword>\n";
		echo "<meta_description><![CDATA[".stripslashes(clean_string_input($meta_description))."]]></meta_description>\n";
		echo "<description><![CDATA[".strip_tags(clean_string_input($description))."]]></description>\n";
		echo "<prixBarre currency=\"EUR\"><![CDATA[".number_format($prix_promo,2,".","")."]]></prixBarre >\n";
		echo "<prix currency=\"EUR\"><![CDATA[".number_format($prix,2,".","")."]]></prix>\n";
		echo "<url_produit><![CDATA[".$url."]]></url_produit>\n";
		echo "<url_image><![CDATA[".$image."]]></url_image>\n";
		echo "<references>\n";
		foreach($references as $reference){
			echo "<ref><![CDATA[".clean_string_input($reference)."]]></ref>\n";
		}
		echo "</references>\n";
	echo "</product>\n";
}

header("Content-Type:text/xml; charset=utf-8");
echo '<?xml version="1.0" encoding="UTF-8"?>'."\n";
?>
<catalogue lang="FR">
<?php

$affiliate = (Tools::getValue('ac') ? '?ac='.Tools::getValue('ac') : '');
$products = Product::getProducts(strval($cookie->id_lang), 0, 0, 'id_product', 'ASC', false, true);

foreach ($products AS $product){

	if($product["id_category_default"]!=1){
		$main_category = new Category($product["id_category_default"]);
	}else{
		$tab_categories = Product::getIndexedCategories($product['id_product']);
		$all_categories = array();
		foreach($tab_categories as $id_categorie["id_category"]){
			$categorie = new Category($id_categorie["id_category"]["id_category"], strval($cookie->id_lang));
			if($categorie->active && $categorie->checkAccess(0)){
				$all_categories[$categorie->calcLevelDepth()] = $categorie->id_category;
			}
		}
		$main_category = new Category(end($all_categories));
	}
	
	$tab_categories = $main_category->getParentsCategories(strval($cookie->id_lang));

	$categories = array_fill(1, 6, "");
	
	foreach($tab_categories as $categorie){
		$categories[$categorie["level_depth"]] = preg_replace("#^[[:space:]]*[0-9]+[.][[:space:]]*#", "", $categorie["name"]);
	}	
	
	// dans certains cas la m�thode getName renvoie un tableau
	$main_category_name=(is_array($main_category->getName()))?current($main_category->getName()):$main_category->getName();
	
	$categories[$main_category->calcLevelDepth()] = preg_replace("#^[[:space:]]*[0-9]+[.][[:space:]]*#", "", $main_category_name);
	
	// attention � reprendre les index dans la fonction print_product
	$categories = array_values($categories);
	
	if(empty($categories)){
		$categories = array(1 => "/");
	}

	$prix = Product::getPriceStatic($product['id_product'],true,null,6,null,false,false,1,false);
	$prix_promo = Product::getPriceStatic($product['id_product'],true,null,6,null,false,true,1,false);
	
	$image = Image::getImages(strval($cookie->id_lang), $product['id_product']);
	if(file_exists("../img/p/".$image[0]['id_product']."-".$image[0]['id_image']."-large.jpg")){
		$image =  _PS_BASE_URL_.__PS_BASE_URI__."img/p/".$image[0]['id_product']."-".$image[0]['id_image']."-home.jpg";
	}else{
		$image = "";
	}
	
	$cat_link_rew = Category::getLinkRewrite($product['id_category_default'], strval($cookie->id_lang));
	$product_link = $link->getProductLink($product['id_product'] , $product['link_rewrite'], $cat_link_rew);
	
	$obj_product = new Product($product['id_product']);
	
	$references = array();
	
	// reference du produit
	if(is_null($product['reference']) || $product['reference'] == "NULL" || trim($product['reference']) == ""){
		if (Db::getInstance()->Execute('
			UPDATE `'._DB_PREFIX_.'product`
			SET `reference` = CONCAT(`reference`,"cat'.$product['id_product'].'")
			WHERE `id_product` = '.intval($product['id_product']).' AND (`reference` IS NULL OR `reference` = "NULL" OR TRIM(`reference`) = "")')) {
				$references[] = "cat".$product['id_product'];
		}
	}else{
		$references[] = $product['reference'];
	}
	
	// $attributes=$obj_product->getAttributeCombinaisons(strval($cookie->id_lang));	
	
	// references attributs
	/* if(is_array($attributes) && !empty($attributes)){
		foreach($attributes as $attribute){
			if($attribute['reference'] != ""){
				$references[] = $attribute['reference'];
			}elseif($product['name']!="" && $attribute['group_name'] != "" && $attribute['attribute_name'] != ""){
				$references[] = $product['name']." - ".$attribute['group_name']." : ".$attribute['attribute_name'];
			}
		}
	} */
	
	$references = array_unique($references);
	
	print_product($cookie,$categories[0],$categories[1],$categories[2],$categories[3],$categories[4],$categories[5],$product['manufacturer_name'],$product['id_product'],$product['name'],$product['description'],$prix_promo,$prix,htmlspecialchars($product_link).$affiliate,$image,$product['meta_title'],$product['meta_keywords'],$product['meta_description'],$references);
}

?>
</catalogue>