<?php

if (!defined('_CAN_LOAD_FILES_'))
	exit;
	
class Catalogueur extends Module
{
	public function __construct()
	{
		$this->name = 'catalogueur';
		$this->tab = 'seo';
		$this->version = 1.0;
		$this->author = '';
		
		parent::__construct();
		
		$this->displayName = $this->l('Catalogueur');
		$this->description = $this->l('Solution d\'optimisation assistée et automatique du référencement de votre catalogue en ligne');
	}

	public function install()
	{
		if (!parent::install())return false;
		return true;
	}
	
	public function getContent()
	{
		chmod("../modules/catalogueur", 0755);
		chmod("../modules/catalogueur/application.key.php", 0755);
		chmod("../modules/catalogueur/application.php", 0755);
		chmod("../modules/catalogueur/catalogueur_update_meta.php", 0755);
		chmod("../modules/catalogueur/catalogueur_update_product.php", 0755);
		chmod("../modules/catalogueur/export_catalogueur.php", 0755);
		
		$html = '';
		
		include_once("../modules/catalogueur/application.key.php");
		
		if (Tools::isSubmit('submit')){
			if(($_POST['cle1'] == '') AND ($_POST['cle2'] == '')){
				$html .= $this->displayError($this->l('Erreur, il faut renseigner les deux champs du formulaire'));
			}
			else{
				$ouvre=fopen('../modules/catalogueur/application.key.php','w');
				$_POST['cle1']=stripslashes($_POST['cle1']);
				$_POST['cle2']=stripslashes($_POST['cle2']);
				fwrite($ouvre,"<?php\r\n");
				fwrite($ouvre,"\$cle1=\"".$_POST['cle1']."\";\r\n");
				fwrite($ouvre,"\$cle2=\"".$_POST['cle2']."\";\r\n");
				fwrite($ouvre,"?>");
				fclose($ouvre);
				$html .= $this->displayConfirmation($this->l('Changement reussi'));
			}
		}
		
		$html .= '<h2>Catalogueur</h2>
		<form action="'.$_SERVER['REQUEST_URI'].'" method="post" >
			<fieldset>
				<legend><img src="'.$this->_path.'logo.gif" alt="" title="" />Paramètres</legend>
				<label>Clé 1 : </label>
				<div class="margin-form">
					<input type="text" name="cle1" size=42 value="'.$cle1.'"/>
				</div>
				<label>Clé 2 : </label>
				<div class="margin-form">
					<input type="text" name="cle2" size=42 value="'.$cle2.'"/>
				</div>
				<center><input type="submit" value="Envoyer" name="submit" class="button"/></center>
     		</fieldset>
		</form>';
	
		return $html;
	}
}
?>