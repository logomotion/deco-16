<?php

/**
 * Script updater function.
 * Updates the script files of an application.
 *
 * Based on :
 *
 *  http://www.catalogueur.com
 *  By Groupe Soledis <contact@groupe-soledis.com>
 *
 */

ini_set('display_errors', 1);

$host = "dev.app.catalogueur.com";

/**
 * Test if internet connexion exists
 * @param url string : url to test
 * @return bool : connexion exists or not
 */
function connexion($url = "http://www.google.fr") {
	ini_set('allow_url_fopen', '1');
	if (@fclose(@fopen($url, 'r'))) { return true; }
	else { return false; }
}

/**
 * Uncrypt text for update script
 * @param text string : text to decrypt
 * @param key string : key for decryption
 * @return uncrypted data
 */
function decrypter($text,$key,$ivi=0) {
	// method iv included
	if($ivi!=0){
		list($iv, $data) = explode('@@', base64_decode($text));
		
		$decrypt = mcrypt_decrypt(MCRYPT_XTEA, $key, $data, MCRYPT_MODE_ECB, $iv);
	}
	else{
		$iv_size = mcrypt_get_iv_size(MCRYPT_XTEA, MCRYPT_MODE_ECB);
		$iv = mcrypt_create_iv($iv_size, MCRYPT_RAND);
		
		$decrypt = mcrypt_decrypt(MCRYPT_XTEA, $key, base64_decode($text), MCRYPT_MODE_ECB, $iv);
	}
    return preg_replace('/[\x00-\x08]/i','',$decrypt);
}

/**
 * Scripts updater
 * - IP and params controls
 * - get file_content (for each)
 * - unlink existing file
 * - paste file_content in new PHP file
 */
if($_SERVER["REMOTE_ADDR"]==gethostbyname($host) && ($_GET["tne"]!="" || $_GET["rue"]!="") && $_GET["tpi"]!="" && $_GET["yek"]!=""){
	$web_service = "http://".$host."/application/application.php";
	if(connexion($web_service)){
	
		$call_url=$web_service."?tpi=".$_GET["tpi"]."&tne=".$_GET["tne"]."&rue=".$_GET["rue"]."&edo=".$_GET["edo"];
	
		$script=@file_get_contents($call_url);
		
		if(strlen($script)>0){
			if($_GET["edo"]!="nocry"){
				$ivi=($_GET["edo"]=="ivinc")?1:0;
				$script = decrypter($script, $_GET["yek"],$ivi);
			}
			if(preg_match("/---check---/",$script)) {
				if(file_exists("./".$_GET["tpi"].".php")) {
					unlink("./".$_GET["tpi"].".php");
				}else{
					echo "error 5 <!--./".$_GET["tpi"].".php"."-->";
				}
				
				$handle = fopen("./".$_GET["tpi"].".php", "w");
				if(fwrite($handle,$script)){
					$log=@file_get_contents($web_service."?log=true&tpi=".$_GET["tpi"]."&tne=".$_GET["tne"]."&rue=".$_GET["rue"]."&edo=".$_GET["edo"]);
				}else{
					echo "error 6 <!--".$log."-->";
				}
				fclose($handle);
			}else{
				echo "error 4 <!--".$script."-->";
			}
		}else{
			echo "error 3 <!--".$call_url."-->";
		}
	}else{
		echo "error 2 <!--".$web_service."-->";
	}
}else{
	echo "error 1 <!--".$_SERVER["REMOTE_ADDR"]." - ".gethostbyname($host)."-->";
}
?>