<?php
require_once (dirname(__FILE__).'/../../config/config.inc.php');
require_once dirname(__FILE__).'/cartabandonment.php';
require_once _PS_MODULE_DIR_.'cartabandonment/classes/template.php';

if(isset($_GET['tnd']))
        $token = $_GET['tnd'];
else $token = $argv[1];
if (!defined('_PS_BASE_URL_'))
        define('_PS_BASE_URL_', Tools::getShopDomain(true));

$obj = new Cartabandonment();
if ($obj->validateToken($token))
{
        $obj->addLog("Begin sending \t".date(DATE_RFC822));
        echo $result = $obj->sendEmails();
        $obj->addLog("End sending \t".date(DATE_RFC822));
}