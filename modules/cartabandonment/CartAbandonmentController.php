<?php

class CartAbandonmentController extends FrontController
{     
        public function preProcess()
        {
                parent::preProcess();
                
                $action = Tools::getValue('op');
                $id_cart = intval(Tools::getValue('ac'));
                $secure_key = pSQL(Tools::getValue('ks'));
                $id_customer = intval(Tools::getValue('uc'));
                $num_sending = intval(Tools::getValue('mun'));
                
                $module = Module::getInstanceByName('cartabandonment');
                
                if(in_array($action, array('UNS', 'RMD', 'CA', 'RD')) AND $id_cart AND $secure_key AND $id_customer AND $num_sending)
                {
                        $query = '
                                SELECT `lastname`, `firstname`, `passwd`, `email`, `id_currency`, `id_cart` 
                                FROM `'._DB_PREFIX_.'customer` `cu`
                                LEFT JOIN `'._DB_PREFIX_.'cart` `ca`
                                        ON `ca`.`id_customer` = `cu`.`id_customer` AND `ca`.`secure_key` = `cu`.`secure_key` 
                                WHERE `ca`.`id_cart`='.$id_cart.' AND `ca`.`secure_key`="'.$secure_key.'" AND `ca`.`id_customer`='.$id_customer;
                        
                        
                        $result = DB::getInstance()->getRow($query);

                        if($result)
                        {
                                if($action == 'RD')
                                {
                                        $module->reminderRead($id_cart, $secure_key, $id_customer, $num_sending);
                                        Tools::redirect('index.php?read=1');
                                }
                                
                                $customer = new Customer($id_customer);
                                $cart = new Cart($id_cart);
                                
                                self::$cookie->id_customer = (int)($customer->id);
                                self::$cookie->customer_lastname = $customer->lastname;
                                self::$cookie->customer_firstname = $customer->firstname;
                                self::$cookie->passwd = $customer->passwd;
                                self::$cookie->logged = 1;
                                self::$cookie->email = $customer->email;
                                self::$cookie->is_guest = $customer->is_guest;;

                                self::$cookie->id_cart = $id_cart;

                                if($action == 'CA')
                                        Tools::redirect(__PS_BASE_URI__.'order.php?step=0&token='.Tools::getToken(false));
                        }
                }
                else Tools::redirect();
        }
        
        public function displayContent()
	{
                $action = Tools::getValue('op');
                $id_cart = intval(Tools::getValue('ac'));
                $secure_key = pSQL(Tools::getValue('ks'));
                $id_customer = intval(Tools::getValue('uc'));
                $num_sending = intval(Tools::getValue('mun'));
                
                $module = Module::getInstanceByName('cartabandonment');
		
                
                if(in_array($action, array('UNS', 'RMD')) AND $id_cart AND $secure_key AND $id_customer AND $num_sending)
                {
                        if($action =='UNS')
                        {
                                $module->unsubscribe($id_cart, $secure_key, $id_customer, $num_sending);
                                self::$smarty->display(dirname(__FILE__).'/tpl/unsubscribe.tpl');
                        }

                        elseif($action =='RMD')
                        {
                                $output = $module->viewReminder($id_cart, $secure_key, $id_customer, $num_sending);
                                echo $output['msg'];
                        }     
                }
	}
        
        
        public function displayHeader()
        {
                $action = Tools::getValue('op');
                if($action != 'RMD')
                        parent::displayHeader();
        }
        
        
        public function displayFooter()
        {
                $action = Tools::getValue('op');
                if($action != 'RMD')
                        parent::displayFooter();
        }
        
}