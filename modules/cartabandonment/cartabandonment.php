<?php
class CartAbandonment extends Module
{
        public function __construct()
        {
                $this->name = 'cartabandonment';
		$this->version = '1.9.9';
		$this->author = 'PrestaShop';
                $this->module_key = '14fe56914027cb4cd50a7dc83283f457';

                if(version_compare(_PS_VERSION_, '1.4.0.0') >= 0)
			$this->tab = 'advertising_marketing';
		else
			$this->tab = 'Advertisement';

                parent::__construct();
                $this->displayName = $this->l('Cart abandonment');
		$this->description = $this->l('Remind your clients who have abandoned their shopping cart by automatic e-mail');
                $this->all_type_of_colors = array('main_color' => $this->l('Background main color'),
                                        'fh_bg_color' => $this->l('Footer and header background color'),
                                        'title_color' => $this->l('Font color title'),
                                        'body_color' => $this->l('Font color body'),
                                        'fh_txt_color' => $this->l('Footer and header text color'));
                $this->_html = '';
        }


        
        public function install()
        {
                $return = true;
                $query = array ();
                $query[] = '
                        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'cart_abandonment_stats`(
                                `id_cart` INT UNSIGNED NOT NULL,
                                `num_sending` INT UNSIGNED DEFAULT 0,
                                `transformed` TINYINT(1) DEFAULT 0,
                                `active` TINYINT(1) DEFAULT "1",
                                `id_discount` INT UNSIGNED,
                                `date_sending` TIMESTAMP,
                                `date_transforming` TIMESTAMP,
                                `template_id` VARCHAR(255) NOT NULL,
                                `unsubscribe` TINYINT(1) DEFAULT 0,
                                `date_unsubscribing` TIMESTAMP,
                                KEY (`id_cart`, `num_sending`)) ENGINE = '._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';
                
                $query[] = '
                        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'cart_abandonment_config_discount`(
                                `id_config` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                                `from` FLOAT DEFAULT "00.0",
                                `to` FLOAT DEFAULT "00.0",
                                `value` FLOAT DEFAULT "0.0",
                                `type` VARCHAR(127) NOT NULL,
                                PRIMARY KEY (`id_config`)) ENGINE = '._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';
                
                $query[] = '
                        CREATE TABLE IF NOT EXISTS `'._DB_PREFIX_.'cart_abandonment_template`(
                                `id_template` INT UNSIGNED NOT NULL AUTO_INCREMENT,
                                `name` VARCHAR(255) NOT NULL,
                                `id_lang` INT UNSIGNED DEFAULT 1,
                                `title_cart_content` VARCHAR(255),
                                `title` VARCHAR(255),
                                `main_color` VARCHAR(16),
                                `header_font_color` VARCHAR(16),
                                `header_background_color` VARCHAR(16),
                                `title_font_color` VARCHAR(16),
                                `body_font_color` VARCHAR(16),
                                `num_sending` INT UNSIGNED DEFAULT 1,
                                `conclusion` TEXT,
                                `body` TEXT,
                                `discount_text` TEXT,
                                `discount` TINYINT(1) DEFAULT 0,
                                `with_cart_content` TINYINT(1) DEFAULT 1,
                                `active` TINYINT(1) DEFAULT 1,
                                `header` TEXT,
                                `footer` TEXT,
                                `left_column` TEXT,
                                `services` TEXT,
                                `date_add` TIMESTAMP,
                                `date_upd` TIMESTAMP,
                                PRIMARY KEY (`id_template`)) ENGINE = '._MYSQL_ENGINE_.' DEFAULT CHARSET=utf8';
                


                $this->registerHook('AdminStatsModules');
                
                $db = DB::getInstance();
                foreach ($query as $value)
                        $return = ($return AND $db->Execute($value));

                

                if($return)
                        $return = (bool)parent::install();
                
                $date = date('Y-m-d H:i:s');
                Configuration::updateValue('GIVEN_UP_CART_DATE_INSTALL', $date);
                Configuration::updateValue('GIVEN_UP_CART_ENABLE_DISCOUNT', 'enable');
                Configuration::updateValue('CART_REMINDER_ACTIVE', 'enable');
                Configuration::updateValue('GIVEN_UP_CART_NBR_RANGE_DISCOUNT', 1);
                Configuration::updatevalue('NBR_OF_REMINDERS', 1);
                Configuration::updatevalue('HR_OF_SENDING_WEEK', '20:00');
                Configuration::updatevalue('HR_OF_SENDING_WE', '15:00');
                Configuration::updateValue('TND_DEADLINE_FOR_FIRST_2', 'H');
                Configuration::updateValue('TND_TIME_BETWEEN_2', 'D');
                Configuration::updateValue('TND_DEADLINE_FOR_FIRST', '10');
                Configuration::updateValue('TND_TIME_BETWEEN', '2');
                Configuration::updateValue('TND_VOUCHER_EXPIRE', '15');       
                Configuration::updateValue('TND_SHOP_DOMAIN', Tools::getShopDomain(true));
                Configuration::updateValue('TND_HTTP_HOST', Tools::htmlentitiesUTF8($_SERVER['HTTP_HOST']));
                Configuration::updateValue('CART_REMINDER_FROM', Configuration::get('PS_SHOP_EMAIL'));
                Configuration::updateValue('TND_NBR_CART_PAGINATION', '20');
                Configuration::updateValue('TND_TOKEN', md5(microtime()));
                file_put_contents(dirname(__FILE__).'/log.txt', $date.' '.$this->l('New installation'));
                
                
                return $return;
        }

        
        public function uninstall()
        {
                Configuration::deleteByName('GIVEN_UP_CART_DATE_INSTALL');
                Configuration::deleteByName('GIVEN_UP_CART_ENABLE_DISCOUNT');
                Configuration::deleteByName('CART_REMINDER_ACTIVE');
                Configuration::deleteByName('GIVEN_UP_CART_NBR_RANGE_DISCOUNT');
                Configuration::deleteByName('NBR_OF_REMINDERS');
                Configuration::deleteByName('HR_OF_SENDING_WEEK');
                Configuration::deleteByName('HR_OF_SENDING_WE');
                Configuration::deleteByName('TND_DEADLINE_FOR_FIRST_2');
                Configuration::deleteByName('TND_TIME_BETWEEN_2');
                Configuration::deleteByName('TND_DEADLINE_FOR_FIRST');
                Configuration::deleteByName('TND_TIME_BETWEEN');
                Configuration::deleteByName('TND_VOUCHER_EXPIRE');
                Configuration::deleteByName('TND_TOKEN');
                Configuration::deleteByName('TND_SHOP_DOMAIN');
                Configuration::deleteByName('TND_MAIN_COLOR');
                Configuration::deleteByName('TND_FH_BG_COLOR');
                Configuration::deleteByName('TND_FH_TXT_COLOR');
                Configuration::deleteByName('TND_TITLE_COLOR');
                Configuration::deleteByName('TND_BODY_COLOR');
                Configuration::deleteByName('TND_HTTP_HOST');
                Configuration::deleteByName('TND_DEADLINE_FOR_FIRST');

                $db = DB::getInstance();
                return(
                        $db->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'cart_abandonment_stats`')
                                AND
                        $db->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'cart_abandonment_config_discount`')
                                AND
                        $db->Execute('DROP TABLE IF EXISTS `'._DB_PREFIX_.'cart_abandonment_template`')
                                AND
                        parent::uninstall());
        }



        public function getContent()
        {
                require_once _PS_MODULE_DIR_.'cartabandonment/classes/template.php';
                global $cookie;
			
                $date = date('Y-m-d H:i:s');
                        
                if (!Configuration::get('CART_REMINDER_FROM'))
                        Configuration::updateValue('CART_REMINDER_FROM', Configuration::get('PS_SHOP_EMAIL'));
                
                if (!Configuration::get('TND_HTTP_HOST'))
                        Configuration::updateValue('TND_HTTP_HOST', Tools::htmlentitiesUTF8($_SERVER['HTTP_HOST']));
                
                if (!Configuration::get('GIVEN_UP_CART_DATE_INSTALL'))
                        Configuration::updateValue('GIVEN_UP_CART_DATE_INSTALL', $date);
                
                if (!Configuration::get('GIVEN_UP_CART_ENABLE_DISCOUNT'));
                        Configuration::updateValue('GIVEN_UP_CART_ENABLE_DISCOUNT', 'enable');
                        
                if (!Configuration::get('CART_REMINDER_ACTIVE'))
                        Configuration::updateValue('CART_REMINDER_ACTIVE', 'enable');
                
                if (!Configuration::get('GIVEN_UP_CART_NBR_RANGE_DISCOUNT'))
                        Configuration::updateValue('GIVEN_UP_CART_NBR_RANGE_DISCOUNT', 1);
                
                if (!Configuration::get('NBR_OF_REMINDERS'))
                        Configuration::updatevalue('NBR_OF_REMINDERS', 1);
                
                if (!Configuration::get('HR_OF_SENDING_WEEK'))
                        Configuration::updatevalue('HR_OF_SENDING_WEEK', '20:00');
                
                if (!Configuration::get('HR_OF_SENDING_WE'))
                        Configuration::updatevalue('HR_OF_SENDING_WE', '15:00');
                
                if (!Configuration::get('TND_DEADLINE_FOR_FIRST_2'))
                        Configuration::updateValue('TND_DEADLINE_FOR_FIRST_2', 'H');
                
                if (!Configuration::get('TND_TIME_BETWEEN_2'))
                        Configuration::updateValue('TND_TIME_BETWEEN_2', 'D');
                
                if (!Configuration::get('TND_DEADLINE_FOR_FIRST'))
                        Configuration::updateValue('TND_DEADLINE_FOR_FIRST', '10');
                
                if (!Configuration::get('TND_TIME_BETWEEN'))
                        Configuration::updateValue('TND_TIME_BETWEEN', '2');
                
                if (!Configuration::get('TND_VOUCHER_EXPIRE')) 
                        Configuration::updateValue('TND_VOUCHER_EXPIRE', '15'); 
                
                $defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));
		$languages = Language::getLanguages();
		$iso = Language::getIsoById($defaultLanguage);
                $ad = dirname($_SERVER["PHP_SELF"]);
                
                if(!in_array(basename($ad), scandir(_PS_ROOT_DIR_)))
                        return ;            

                
                if(version_compare(_PS_VERSION_, '1.4.1') >= 0)
                        $tiny = '
                                <script type="text/javascript" src="'.__PS_BASE_URI__.'js/tiny_mce/tiny_mce.js"></script>
                                <script type="text/javascript" src="'.__PS_BASE_URI__.'js/tinymce.inc.js"></script>
                                <script type="text/javascript">
                                        function initTinyMCE(){
                                        tinyMCE.init({
                                                mode : "specific_textareas",
                                                theme : "advanced",
                                                skin:"cirkuit",
                                                editor_selector : "rte",
                                                editor_deselector : "noEditor",
                                                plugins : "safari,pagebreak,style,table,advimage,advlink,inlinepopups,media,contextmenu,paste,fullscreen,xhtmlxtras,preview",
                                                // Theme options
                                                theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
                                                theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,,|,forecolor,backcolor",
                                                theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media,|,ltr,rtl,|,fullscreen",
                                                theme_advanced_buttons4 : "styleprops,|,cite,abbr,acronym,del,ins,attribs,pagebreak",
                                                theme_advanced_toolbar_location : "top",
                                                theme_advanced_toolbar_align : "left",
                                                theme_advanced_statusbar_location : "bottom",
                                                theme_advanced_resizing : false,
                                                content_css : pathCSS+"global.css",
                                                document_base_url : ad,
                                                width: "600",
                                                height: "auto",
                                                font_size_style_values : "8pt, 10pt, 12pt, 14pt, 18pt, 24pt, 36pt",
                                                elements : "nourlconvert,ajaxfilemanager",
                                                file_browser_callback : "ajaxfilemanager",
                                                entity_encoding: "raw",
                                                convert_urls : false,
                                        language : iso

                                        });

                                }
                                </script>
                        ';
                else $tiny = '
                                <script type="text/javascript" src="'.Tools::getShopDomain(true).__PS_BASE_URI__.'js/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
                                <script type="text/javascript">
                                        function initTinyMCE(){
                                                tinyMCE.init({
                                                        mode : "textareas",
                                                        theme : "advanced",
                                                        plugins : "safari,pagebreak,style,layer,table,advimage,advlink,inlinepopups,media,searchreplace,contextmenu,paste,directionality,fullscreen",
                                                        // Theme options
                                                        theme_advanced_buttons1 : 							"newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,styleselect,formatselect,fontselect,fontsizeselect",
                                                        theme_advanced_buttons2 : 							"cut,copy,paste,pastetext,pasteword,|,search,replace,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,anchor,image,cleanup,help,code,,|,forecolor,backcolor",
                                                        theme_advanced_buttons3 : "tablecontrols,|,hr,removeformat,visualaid,|,sub,sup,|,charmap,media,|,ltr,rtl,|,fullscreen",
                                                        theme_advanced_buttons4 : "insertlayer,moveforward,movebackward,absolute,|,styleprops,|,cite,abbr,acronym,del,ins,attribs,|,pagebreak",
                                                        theme_advanced_toolbar_location : "top",
                                                        theme_advanced_toolbar_align : "left",
                                                        theme_advanced_statusbar_location : "bottom",
                                                        theme_advanced_resizing : false,
                                                        content_css : pathCSS+"global.css",
                                                        document_base_url : ad,
                                                        width: "600",
                                                        height: "auto",
                                                        font_size_style_values : "8pt, 10pt, 12pt, 14pt, 18pt, 24pt, 36pt",
                                                        // Drop lists for link/image/media/template dialogs
                                                        template_external_list_url : "lists/template_list.js",
                                                        external_link_list_url : "lists/link_list.js",
                                                        external_image_list_url : "lists/image_list.js",
                                                        media_external_list_url : "lists/media_list.js",
                                                        elements : "nourlconvert,ajaxfilemanager",
                                                        file_browser_callback : "ajaxfilemanager",
                                                        convert_urls : false,
                                                        language : iso

                                                });
                                        }
                                </script>
                                <!--script language="javascript">id_language = Number('.$defaultLanguage.');</script-->';

                $this->_html .= '<h2><img src="'.__PS_BASE_URI__.'modules/'.$this->name.'/logo.gif"/>'.' '.$this->displayName.'</h2>';
                $this->_html .= '
                                <link type="text/css" rel="stylesheet" href="'.__PS_BASE_URI__.'modules/'.$this->name.'/css/colorpicker/colorpicker.css"/>
                                <link type="text/css" href="'.__PS_BASE_URI__.'modules/'.$this->name.'/css/'.$this->name.'.css" rel="stylesheet" />
                                <link type="text/css" href="'.__PS_BASE_URI__.'modules/'.$this->name.'/js/timepicker/jquery.ui.timepicker.css" rel="stylesheet" />
                                
                                <script type="text/javascript">
                                        var baseUri = "'.__PS_BASE_URI__.'modules/'.$this->name.'/tools.php?token='.Configuration::get('TND_TOKEN').'";
                                        var iso = "'.(file_exists(_PS_ROOT_DIR_.'/js/tinymce/jscripts/tiny_mce/langs/'.$iso.'.js') ? $iso : 'en').'" ;
                                        var pathCSS = "'._THEME_CSS_DIR_.'" ;
                                        var ad = "'.Tools::htmlentitiesUTF8($ad).'" ;
                                        var msgTPL = "'.$this->l('Please create at least one template').'";
                                        var employee_id_lang = '.(int)$cookie->id_lang.';
                                        var _hourText = "'.$this->l('Hours').'";
                                        var _minuteText = "'.$this->l('Minutes').'";
                                </script>
                                '.$tiny.'
                                <script type="text/javascript" src="'.__PS_BASE_URI__.'modules/'.$this->name.'/js/jquery-ui-1.8.14.custom.min.js"></script>
                                <script type="text/javascript" src="'.__PS_BASE_URI__.'modules/'.$this->name.'/js/timepicker/jquery.ui.timepicker.js"></script>
                                <script type="text/javascript" src="'.__PS_BASE_URI__.'modules/'.$this->name.'/js/colorpicker/colorpicker.js"></script>
                                <script type="text/javascript" src="'.__PS_BASE_URI__.'modules/'.$this->name.'/js/colorpicker/eye.js"></script>
                                <script type="text/javascript" src="'.__PS_BASE_URI__.'modules/'.$this->name.'/js/colorpicker/utils.js"></script>
                                <script type="text/javascript" src="'.__PS_BASE_URI__.'modules/'.$this->name.'/js/colorpicker/layout.js?ver=1.0.2"></script>
                                <script type="text/javascript" src="'.__PS_BASE_URI__.'modules/'.$this->name.'/js/'.$this->name.'.js"></script>';
                                

                $this->updateStats();
                if(!is_callable('unlink'))
                        $this->_html .= $this->displayError($this->l('Function "unlink" is not callable. You can not use this module.'));                
                
                $this->_html .= $this->displayMainActions();
                
                return $this->_html;
        }    
        
        
        private function displayMainActions()
        {
                return '
                        <div>
                                <div class="tab-row">
                                        <h4 class="tab selected">
                                                <a href="#" class="user_guide">'.$this->l('User guide').'</a>
                                        </h4>
                                        <h4 class="tab">
                                                <a href="#" class="config_templates" style="display:block;">'.$this->l('Create and edit Templates').'</a>
                                        </h4>
                                        <h4 class="tab">
                                                <a href="#" class="config_mails" style="display:block;">'.$this->l('Configure the sendings of e-mails').'</a>
                                        </h4>
                                        <h4 class="tab">
                                                <a href="#" class="config_carts" style="display:block;">'.$this->l('Carts, discounts and stats').'</a>
                                        </h4>
                                </div>
                                

                                <div class="tab-page user_guide" style="border-width-">
                                        '.$this->getUserGuide().'
                                </div>
                                <div class="tab-page config_templates" style="display:none;"></div>
                                <div class="tab-page config_carts" style="display:none;"></div>
                                <div class="tab-page config_mails" style="display:none;"></div>
                        </div>
                ';
        }
 
        
        
        public function getUserGuide()
        {
                return '
                        <div class="hint" style="display:block">
                                <b>'.$this->l('How to use this module').'</b><br/><br/>
                                <b>'.$this->l('This module allows you to automatically send e-mails to remind your customers who have not completed the order process on your online shop.').' '
                                    .$this->l('The e-mails are customizable: you can add a discount code, adjust the contents of the mail to the customer\'s language, choose the delay before sending the first reminder, schedule several reminders ...').
                                '</b><br/>
                                <ol>
                                        <li>
                                                <b>'.$this->l('Edit a template for sending e-mails automatically').'</b>
                                                <p>
                                                        '.$this->l('First, write the name of the template, and select the language in which you want to send the e-mail (a French model and a English model are already available).')
                                                        .' '.$this->l('To fill in these fields, please do not use the following characters: {} "] [/ @ |').'
                                                </p>
                                                <p>
                                                        '.$this->l('The table presents you all the templates you have created. Here you can choose to associate a discount to a template, and set the number for each e-mail (if a 1st reminder, a 2nd reminder ...).')
                                                         .' '.$this->l('The buttons to the right allow you to edit, customize and duplicate each template.').'
                                                </p>
                                                <p>
                                                        '.$this->l('Then, for each template, customize the title of the e-mail, the body of the e-mail, the header, footer, the services provided and set the colors for text and blocks.')
                                                        .' '.$this->l('Make sure that the language of the contents of each mail matches the language selected for the template. Create a reminder template for each active language in your shop.').'
                                                </p>
                                                <p>
                                                        '.$this->l('If you want multiple reminder emails are sent, you must create as many e-mail reminders for each language (otherwise the e-mail will be sent a reminder at random).')
                                                        .' '.$this->l('To do this, feel free to duplicate a model that you have previously customized and then modify the text to suit each language (templates of different languages ​​can have the same name).').'
                                                </p>
                                                <p>
                                                        '.$this->l('The "duplicate" function brings consistency between all the e-mail reminder you send.')
                                                         .' '.$this->l('For a same language, all your e-mails will have the same footer, the same header and the same services block.').'
                                                </p>
                                                <p>
                                                        '.$this->l('Note: It is necessary to use variables in place so that the client receives e-mail with personal information (with this customer number, his name, the contents of the cart he wanted to order ...).').'
                                                </p>
                                        </li>
                                        <li>
                                                <b>'.$this->l('Configure the sendings of e-mails').'</b>
                                                <p>
                                                        '.$this->l('First, define the time between the day the customer adds items to their cart without finalizing the order, and the day the mail is a reminder is sent. If you want to send several reminders, you can also determine the number of e-mails and the number of days between each sending.').'
                                                </p>
                                                <p>
                                                        '.$this->l('Delays before sending each e-mail can be configured in days, hours or minutes. If you choose a period of days, you can then set the time you want when the e-mail is sent during the week and the the time when it is sent during the weekend.').'
                                                </p>
                                        </li>
                                        <li>
                                                <b>'.$this->l('Carts, discounts and stats').'</b>
                                                <p>
                                                        '.$this->l('In this tab, you can find the abandoned shopping carts and those who were converted after your reminders.')
                                                         .$this->l('You can also set the discounts associated with email reminders. These discounts can be realized as a percentage or currency, and be different price according to the ranges you choose.').'
                                                </p>
                                        </li>
                                </ol>
                                <b><p>
                                        '.$this->l('CAUTION: To ensure that e-mails are sent automatically, it is necessary to have previously set a cron task, and the sending of e-mails from your online shop.')
                                        .' '.$this->l('Please contact your host to configure it.').'
                                </p></b>
                                <div class="crontab"></div>
                        </div>
                ';
        }
        
        
        
        public function getContentTemplates()
        {
                return '
                        <div>'.$this->warnIsNotWritable('tpl').'</div>
                        <div class="clear">&nbsp;</div>
                        <div class="hint" style="display:block;">
                                <b>'.$this->l('The e-mail is automatically personalized with those variables:').'</b>

                                <br/><br/><b>'.$this->l('How to customize this template').'</b><br/>
                                <ul style="list-style:none;">
                                        <li><b>%SHOP_NAME%</b> : '.$this->l('Online shop name').'</li>
                                        <li><b>%FIRSTNAME%, $LASTNAME</b> : '.$this->l('First name and last name of the client').'</li>
                                        <li><b>%QUANTITY%</b> : '.$this->l('Number of items of the cart').'</li>
                                        <li><b>%VOUCHER%, %VOUCHER_VALUE%, %VOUCHER_EXPIRE%</b> : '.$this->l('Name, value and duration of validity (in days) of the voucher offered to the customer. These two variables are used only if you offer coupons with this template.').'</li>
                                        <li><b>%CART_OPEN_LINK%, %CART_CLOSE_LINK%</b> : '.$this->l('Those two variables have to be set on both sides of the item you want to link to your cart.').'</li>
                                </ul><br/>
                                <p>
                                        '.$this->l('For each template of automatic e-mail reminders you create, write the name of it, the language in which you want to send it (if it is a first, a 2nd mail reminder ...), add a to the e-mail if you want, and set the number for each e-mail (if a 1st reminder, a 2nd reminder ...).').'
                                </p>
                                <p>
                                        '.$this->l('The buttons to the right allow you to edit, customize and duplicate each template.').'
                                </p>
                                <p>
                                        '.$this->l('To show the content of the cart, the short description es used.').'
                                <p>
                        </div>
                        <div class="clear">&nbsp;</div>
                '.$this->configChoiceOfTemplates();
        }
        
        
        
        public function getContentCarts()
        {
                $max = Configuration::get('TND_NBR_CART_PAGINATION');
                $this->updateStats();
                $tmp ='';
                $out = '<fieldset>
                                <legend>'.$this->l('List of given up carts').'</legend>
                                <div id="display_given_up_carts2">'.$this->displayActionForPagination(0, $max).'</div>
                                <div id="display_given_up_carts">'.$this->displayGivenUpCarts(0, $max).'</div>
                                <div class="clear">&nbsp;</div>
                        </fieldset>
                        <div class="clear">&nbsp;</div>
                                '.$this->displayTransformedCarts();               

                if(Configuration::get('GIVEN_UP_CART_ENABLE_DISCOUNT') == 'enable')
                        $tmp .= $this->displayConfigDiscount();
                
                $out .= '<div class="clear">&nbsp;</div>
                        <div id="edit_discount">'.$tmp.'</div>';
                
                return $out;
        }
        
        
        
        public function getContentEmails()
        {
                $output = $this->configHowToSendfEmails();
                $output .= '<div id="validate_all_templates">'.$this->validateAllTemplates().'</div>';
                $output .= $this->configSendingOfEmails();
                
                return $output;
        }
        


        public function updateNbrOfCartPagination($value)
        {
                if(is_numeric($value))
                        Configuration::updateValue('TND_NBR_CART_PAGINATION', intval($value));
        }

        public function displayActionForPagination($max_prev = 0, $min_next = 0)
        {
                $part1 = $part2 = $part4 = '';
                $count = $this->getGivenUpCarts(null, false, true, 0, true, true);
                $nbr = Configuration::get('TND_NBR_CART_PAGINATION');
                if ($max_prev - $nbr >= 0)
                        $part1 = '
                                <td>
                                        <a href="javascript:DisplayCarts(0, '.$nbr.');">
                                                <img src="'.Tools::getShopDomain(true).__PS_BASE_URI__.'/img/admin/list-prev2.gif" alt=""/>
                                        </a>
                                </td>
                                <td>
                                        <a href="javascript:DisplayCarts('.($max_prev - $nbr).', '.$max_prev.');">
                                                <img src="'.Tools::getShopDomain(true).__PS_BASE_URI__.'/img/admin/list-prev.gif" alt=""/>
                                        </a>
                                </td>';


                if($min_next >= $nbr AND $min_next < $count)
                        $part4 = '
                                <td>
                                        <a href="javascript:DisplayCarts('.($min_next).', '.($min_next + $nbr).');">
                                                <img src="'.Tools::getShopDomain(true).__PS_BASE_URI__.'/img/admin/list-next.gif" alt=""/>
                                        </a>
                                </td>
                                <td>
                                        <a href="javascript:DisplayCarts('.($count - $nbr).', '.$count.');">
                                                <img src="'.Tools::getShopDomain(true).__PS_BASE_URI__.'/img/admin/list-next2.gif" alt=""/>
                                        </a>
                                </td>
                                ';

                $array = array(10, 20, 50, 100);
                $part2 = '<td>'.$this->l('Display').' <select onChange="javascript:UpdateNbrOfCartPagination($(this).attr(\'value\'));">';
                foreach ($array as $value)
                {
                        $part2 .= '<option '.($value == $nbr ? 'selected="selected"':'').'>'.$value.'</option>';
                }
                $part2 .= '</select> '.$this->l('abandoned carts').'</td> ';

                $part3 = '<td>'.' '.$max_prev.($min_next >= $nbr ? '-'.($max_prev + $nbr) : '').'/'.$count.'</td>';
                
                return '<table>'.$part1.$part2.$part3.$part4.'</table>';
        }

        
        public function validateAllTemplates()
        {
                $db = DB::getInstance();
                $languages = Language::getLanguages();
                $output = $this->warnIsNotWritable('log.txt');
                foreach ($languages as $lang)
                {
                        for($i = 1; $i <= Configuration::get('NBR_OF_REMINDERS'); $i++)
                        {
                                $query = 'SELECT `id_template` FROM `'._DB_PREFIX_.'cart_abandonment_template`
                                        WHERE `id_lang`='.(int)$lang['id_lang'].' AND `num_sending`='.intval($i).' AND `active` = 1';

                                $count = count($db->ExecuteS($query));
                                
                                if($count > 1)
                                        $output .= $this->displayError($lang['name'].', '.$this->l('Order of sending').' '.$i.' : '.$this->l('Too much active templates'));
                                if($count < 1)
                                        $output .= $this->displayError($lang['name'].', '.$this->l('Order of sending').' '.$i.' : '.$this->l('No active template'));
                        }
                        
                }
                return $output;
        }
        
        
        
        private function warnIsNotWritable($file)
        {
                $out = '';
                $all_files = scandir(dirname(__FILE__));
                if(in_array($file, $all_files))
                        $file = dirname(__FILE__).'/'.$file;
                
                if(!is_dir($file) && !is_file($file))
                        return $out;
                
                if(!is_writable($file))
                {
                        if(!chmod($file, 0777))
                                $validate = chmod($file, 0755);
                        else $validate = true;
                        
                        if(!$validate)
                                $out = $this->displayError($file.' '.$this->l('is not writable. Please check your permission.'));
                }
                
                return $out;
        }



        
        public function updateVoucherValidity($value)
        {
                if(is_numeric($value))
                        Configuration::updateValue('TND_VOUCHER_EXPIRE', intval($value));
        }
        
        
        private function displayConfigDiscount()
        {
                if(Configuration::get('GIVEN_UP_CART_ENABLE_DISCOUNT'))
                return '
                        <fieldset>
                                        <legend>'.$this->l('Discount with reminders of carts').'</legend>
                                        
                                        <label>'.$this->l('Duration of validity of the voucher').'</label>
                                        <input type="text" size="2" value="'.Configuration::get('TND_VOUCHER_EXPIRE').'"
                                                onChange="javascript:UpdateVoucherValidity($(this).attr(\'value\'))"></input>&nbsp;'.$this->l('Days').'
                                                
                                        <div class="clear">&nbsp;</div>
                                        <div class="clear">&nbsp;</div>
                                        
                                        <form id="edit_ranges_discount" name="edit_ranges_discount">
                                                <table class="table" cellspacing=0 cellpadding=0>
                                                        <tbody id="display_ranges_discount">
                                                                '.$this->displayRangesForDiscount().'
                                                        </tbody>
                                                </table>
                                                <div class="clear">&nbsp;</div>
                                                
                                                <div class="clear">&nbsp;</div>
                                                <input type="button"  style="float:right"
                                                        class="button"
                                                        value="'.$this->l('Save').'"
                                                        id="save_ranges_discount"
                                                        onClick="javascript:SaveRanges(\'true\')"/>
                                                </form>
                        </fieldset>';
        }
        
        
        
        public function isTransformedCart($id_cart)
        {
                return DB::getInstance()->getRow('SELECT * FROM `'._DB_PREFIX_.'cart_abandonment_stats` WHERE `id_cart`='.intval($id_cart).' AND `transformed`="1"');
        }
        
        
        public function validateToken($token)
        {
                return Configuration::get('TND_TOKEN') == $token;
        }


        private function createVoucher($id_cart, $id_customer)
        {
                if(Configuration::get('GIVEN_UP_CART_ENABLE_DISCOUNT') == 'enable' AND Configuration::get('CART_REMINDER_ACTIVE')== 'enable')
                {
                        $rules = 'SELECT * FROM `'._DB_PREFIX_.'cart_abandonment_config_discount`';
                        $rules = DB::getInstance()->ExecuteS($rules);
                
                        $vouch = NULL;
                        $cart = new Cart($id_cart);
                        $total = $cart->getOrderTotal(true);
                        foreach ($rules AS $rule)
                                if($rule['from'] <= $total AND ($rule['to'] > $total OR $rule['to'] == 0))
                                        $vouch = $rule;

                        if (!empty($vouch))
                        {
                                $name = 'TND'.substr(sha1(microtime()), 6, 5);
                                $voucher = new Discount();
                                $voucher->id_customer = intval($id_customer);
                                $voucher->name = $name;
                                $voucher->quantity = 1;
                                $voucher->quantity_per_user = 1;
                                $voucher->active = 1;
                                $voucher->cumulable = 0;
                                $voucher->cart_display = 0;
                                $voucher->cumulable_reduction = 1;
                                $now = time();
                                $voucher->date_from = date('Y-m-d H:i:s', $now);
                                $voucher->date_to = date('Y-m-d H:i:s', $now + (3600 * 24 * 15));
                        
				$voucher->description[(int)(Configuration::get('PS_LANG_DEFAULT'))] = $this->displayName.' '.$name;
                                $voucher->value = floatval($vouch['value']);
                                $voucher->id_discount_type = intval($vouch['type']);
                                $voucher->minimal = $vouch['from'];
                        
                                if($voucher->id_discount_type == 2)
                                        $voucher->id_currency = Configuration::get('PS_CURRENCY_DEFAULT');

                                $voucher->add();
                                
                                return $voucher;
                        }
                }

                return false;
        }
        


        public function displayGivenUpCarts($begin = 0, $max = true)
        {
                $results = $this->getGivenUpCarts(null, false, true, $begin, $max);
                $output = '<table class="table" cellspacing=0 cellpadding=0>
                                <thead>
                                        <tr>
                                                <th>'.$this->l('Cart Id').'</th>
                                                <th>'.$this->l('Firstname').'</th>
                                                <th>'.$this->l('Lastname').'</th>
                                                <th>'.$this->l('Nbr products').'</th>
                                                <th>'.$this->l('Amount (wt)').'</th>
                                                <th>'.$this->l('Date add').'</th>
                                        </tr>
                                </thead><tbody>';

                if (is_array($results))
                        foreach ($results as $value)
                        {
                                $output .= '
                                        <tr>
                                                <td>'.$value['id_cart'].'</td>
                                                <td>'.ucfirst($value['firstname']).'</td>
                                                <td>'.strtoupper($value['lastname']).'</td>
                                                <td>'.$value['nbr_products'].'</td>
                                                <td>'.$value['amount_wt'].'</td>
                                                <td>'.$value['date_add'].'</td>
                                        </tr>';
                        }

                $output .= '</tbody></table>
                        <div class="clear">&nbsp;</div>
                        <!--input type="button" class="button" value="'.$this->l('Deactive').'" onclick="javascript:DeactiveCarts()"></input>
                        <div class="clear">&nbsp;</div-->';

                return $output;
        }


        private function getCustomerIdentity($id_cart)
        {
                $query = 'SELECT * FROM `'._DB_PREFIX_.'customer` `cu`
                          LEFT JOIN `'._DB_PREFIX_.'cart` `ca`
                          ON `cu`.`id_customer` = `ca`.`id_customer`
                          WHERE `cu`.`id_customer`='.intval($id_cart);
                
                return DB::getInstance()->getRow($query);
        }


        private function displayRangesForDiscount()
        {
                
                $result = DB::getInstance()->ExecuteS('SELECT * FROM `'._DB_PREFIX_.'cart_abandonment_config_discount`');
                $nbr = max(count($result), intval(Configuration::get('GIVEN_UP_CART_NBR_RANGE_DISCOUNT')));
                $output = '';
                for($i=0; $i<$nbr; $i++)
                {
                        if($i == $nbr-1)
                                $to = 'value="+"';
                        else
                        {
                                if(isset($result[$i]['to']))
                                        $to = 'value="'.$result[$i]['to'].'"';
                                else $to = 'value="0"';
                        }
                        
                        if(isset($result[$i]['from']))
                                        $from = 'value="'.$result[$i]['from'].'"';
                        else $from = 'value="0"';
                        
                        if(isset($result[$i]['value']))
                                        $value = 'value="'.$result[$i]['value'].'"';
                        else $value = 'value="0"';
                                
                        
                        $output .='
                                <tr id="range_'.$i.'" class="range_discount">
                                        <td>'.$this->l('Min').'
                                                <input type="text" name="from_'.$i.'" '.$from.'/>
                                        </td>
                                        
                                        <td>'.$this->l('Max').'
                                                <input type="text" name="to_'.$i.'" '.$to.'/>                                
                                        </td>
                                        
                                        <td>'.$this->l('Value').'
                                                <input type="text" name="value_'.$i.'" '.$value.'/>
                                        </td>
                                        
                                        <td>'.$this->l('Type').'
                                                <select name="type_'.$i.'">
                                                        <option value=1>'.$this->l('Percentage').'</option>
                                                        <option value=2>'.$this->l('Currency').'</option>
                                                </select>
                                        </td>
                                        
                                        <td>
                                                <table>
                                                        <td>
                                                                <a href="javascript:DeleteRange(\'range_'.$i.'\', '.$nbr.');">
                                                                        <img src="'.Tools::getShopDomain(true).__PS_BASE_URI__.'/img/admin/forbbiden.gif" alt="delete this range"/>
                                                                </a>
                                                        </td>
                                                        <td>
                                                                <a href="javascript:AddNewRange(\'range_'.$i.'\', '.$nbr.');">
                                                                        <img src="'.Tools::getShopDomain(true).__PS_BASE_URI__.'/img/admin/add.gif" alt="delete this range"/>
                                                                </a>
                                                        </td>
                                                </table>
                                        </td>
                                </tr>';
                }
                return $output;
        }
        
        
        private function updateNbrRangesForDiscount($action)
        {
                $nbr = intval(Configuration::get('GIVEN_UP_CART_NBR_RANGE_DISCOUNT'));
                
                if($action == 'up')
                        $nbr++;
                elseif($action == 'down') $nbr--;

                Configuration::updateValue('GIVEN_UP_CART_NBR_RANGE_DISCOUNT', $nbr);

                return $nbr;
        }
        
        
        
        /*****************************************************************************************/
        public function switchActive($id_template)
        {
                $template = new Template($id_template);
                $template->switchActive();
                return $this->displayAllTemplates();
        }


        public function displayTemplateIsActive($id_tpl)
        {
                $tpl = Template::get(intval($id_tpl));
                if($tpl->active)
                        return '<img title="'.$this->l('Enabled').'" title="'.$this->l('Enabled').'" src="'.Tools::getShopDomain(true).__PS_BASE_URI__.'/img/admin/enabled.gif">';
                return '<img title="'.$this->l('Disabled').'" title="'.$this->l('Disabled').'" src="'.Tools::getShopDomain(true).__PS_BASE_URI__.'/img/admin/disabled.gif">';
        }

        
        public function displayAllTemplates($iso = NULL)
        {
                $nbr = Configuration::get('NBR_OF_REMINDERS');
                $results = Template::get(NULL, $iso, false);
                $output = '<table class="table" style="float:left;" cellspacing=0 cellpadding=0>
                                <thead>
                                        <tr>
                                                <th>'.$this->l('Name').'</th>
                                                <th>'.$this->l('Language').'</th>
                                                <th>'.$this->l('Order of sending').'</th>
                                                <th>'.$this->l('With discount').'</th>
                                                <th>'.$this->l('Display content of cart').'</th>
                                                <th>'.$this->l('Actions').'</th>
                                        </tr>
                                </thead><tbody>';

                if(is_array($results) AND !empty($results))
                        foreach ($results AS $template)
                        {
                                $select_sending = $select_discount = '';
                                for($i=1; $i<=$nbr; $i++)
                                {
                                        $select_sending .= '<option '.(($i == $template->num_sending) ? 'selected': '').'>'.$i.'</option>';
                                }
                                $select_sending = '<select name="num_sending" class="'.$template->id.'"
                                                onchange="javascript:UpdateNumOfSending(\''.$template->id.'\', $(this).attr(\'value\'))">'.$select_sending.'</select>';
                        
                                $select_discount = '
                                        <select name="discount" onchange="javascript:UpdateTemplateWithDiscount(\''.$template->id.'\', $(this).attr(\'value\'))">
                                                <option '.($template->discount ? 'selected' : '').' value="1">'.$this->l('Yes').'</option>
                                                <option '.(!$template->discount ? 'selected' : '').' value="0">'.$this->l('No').'</option>
                                        </select>';
                                
                                $select_display_content = '
                                        <select name="num_sending" class="'.$template->id.'"
                                                onchange="javascript:UpdateTemplateWithContentOfCart(\''.$template->id.'\', $(this).attr(\'value\'))">
                                                <option '.($template->with_cart_content ? 'selected' : '').' value="1">'.$this->l('Yes').'</option>
                                                <option '.(!$template->with_cart_content ? 'selected' : '').' value="0">'.$this->l('No').'</option>
                                        </select>';
                        

                                $lang = Language::getLanguage(Language::getIdByIso($template->lang));
                                $output .= '
                                        <tr>
                                                <td>'.$template->name.'</td>
                                                <td>'.$lang['name'].'</td>
                                                <td><center>'.$select_sending.'</center></td>
                                                <td><center>'.$select_discount.'</center></td>
                                                <td><center>'.$select_display_content.'</center></td>
                                                <td>
                                                        <table>
                                                                <td>
                                                                        <a href="javascript:EditTemplate(\''.$template->id.'\', \'preview\')">
                                                                                <img title="'.$this->l('Preview and Edit').'" alt="'.$this->l('Preview and edit').'" src="'.Tools::getShopDomain(true)._PS_ADMIN_IMG_.'edit.gif"/>
                                                                        </a>
                                                                </td>
                                                                <td>
                                                                        <a href="javascript:SwitchActive(\''.$template->id.'\', $(this))">
                                                                                '.$this->displayTemplateIsActive($template->id).'
                                                                        </a>
                                                                </td>
                                                                <td>
                                                                        <a href="javascript:DuplicateTemplate(\''.$template->id.'\')">
                                                                                <img title="'.$this->l('Duplicate').'" alt="'.$this->l('Duplicate').'" src="'.Tools::getShopDomain(true)._PS_ADMIN_IMG_.'duplicate.png"/>
                                                                        </a>
                                                                </td>
                                                                <td>
                                                                        <a href="javascript:DeleteTemplate(\''.$template->id.'\')">
                                                                                <img title="'.$this->l('Delete').'" alt="'.$this->l('Delete').'" src="'.Tools::getShopDomain(true)._PS_ADMIN_IMG_.'delete.gif"/>
                                                                        </a>
                                                                </td>
                                                        </table>
                                                </td>
                                        </tr>';
                        }

  
                $output .= '</tbody></table>
                        <div class="clear">&nbsp;</div>
                        <input type="button" class="button" value="'.$this->l('Add new template').'"
                                onclick="javascript:AddNewTemplate()" />';
                


                return '<fieldset>
                                <legend>'.$this->l('All templates').'</legend>
                                
                                <form id="display_all_templates">'.$output.'</form>
                        </fieldset>';
        }
        
        public function saveTemplate($data, $id_template = NULL)
        {
                if (!empty($data))
                {
                        if (array_key_exists('id_lang', $data))
                                $id_lang = $data['id_lang'];
                        else $id_lang = Configuration::get('PS_LANG_DEFAULT');
                        
                        if (!empty($id_template))
                                $template = new Template($id_template, $id_lang);
                        elseif (array_key_exists('id_template', $data))
                                $template = new Template($data['id_template'], $id_lang);
                        else $template = new Template(NULL, $id_lang);
                        
                        foreach ($data AS $key=>$value)
                                $template->{$key} = $value;
                        $template->save();
                        return array('all_templates' => $this->displayAllTemplates(), 'edit_template' => $this->editTemplate($template->id));
                }
        }

        public function editTemplate($id_template = null)
        {
                if (!empty($id_template))
                        $template = new Template($id_template);
                else $template = new Template();
                $languages = Language::getLanguages();
                
                global $smarty;

                $smarty->assign(array('admin_img' => _PS_ADMIN_IMG_, 'template' => $template, 'languages' => $languages));
                return $this->display(__FILE__, 'tpl/edit_template.tpl');  
        }


        /*****************Traitement des regles de promotion*******************/     
        public function saveRanges($data)
        {
                $data = explode('&', $data);
                $db = DB::getInstance();
                $query = '';
                
                foreach($data AS $d)
                {
                        // $d is like "value_0=10" 
                        $tmp = explode('=', $d);
                        $tmp2 = explode('_', $tmp[0]);
                        $key = intval($tmp2[1]);
                        ${$tmp2[0]}[$key] = $tmp[1]; 
                }
                
                $db->Execute('TRUNCATE TABLE `'._DB_PREFIX_.'cart_abandonment_config_discount`');
                if (isset($to) AND is_array($to))
                        foreach($to AS $key=>$val)
                        {

                                $query = 'INSERT INTO `'._DB_PREFIX_.'cart_abandonment_config_discount`
                                        (`FROM`, `TO`, `VALUE`, `TYPE`)
                                        VALUE("'.intval($from[$key]).'", "'.intval($to[$key]).'", "'.intval($value[$key]).'", "'.pSQL($type[$key]).'");';


                                $db->Execute($query);
                        }
                
                return Configuration::updateValue('GIVEN_UP_CART_NBR_RANGE_DISCOUNT', count($to));
        } 

        /********************************Traitement des templates*********************************/
        public function displayDescriptionOfNewTemplate()
        {
                $buttons = '
                        <input type="button" class="button" value="'.$this->l('Save template').'" style="float:right"
                                onclick="javascript:AddNewTemplate(1)"/>';
                
                $languages = Language::getLanguages();
                $form_language = '';
                
                foreach($languages AS $lang)
                        $form_language .= '<option value="'.$lang['id_lang'].'" >'.$lang['name'].'</option>'; 
                
                
                return '<div id="description_of_new_template">
                        <fieldset>
                                <legend>'.$this->l('Description of template').'</legend>
                                <form class="add_new_template">
                                        <label>'.$this->l('Name of template').'</label>
                                        <input type="text" name="name"/>
                                        <div class="clear">&nbsp;</div>
                                                
                                        <label>'.$this->l('Language of template').'</label>
                                        <select name="id_lang">
                                                '.$form_language.'
                                        </select>
                                        <div class="clear">&nbsp;</div>
                                                '.$buttons.'
                                </form>
                        </fieldset></div>';
        }
     
        
        public function deleteTemplate($id_template)
        {
                $template = new Template($id_template);
                $template->delete();
                return $this->displayAllTemplates();
        }
        
        
        public function duplicateTemplate($id_template)
        {
                $template = new Template($id_template);
                $template->duplicate();
                return $this->displayAllTemplates();
        }
        
        
        public function updateTemplate()
        {
                $id_template = Tools::getValue('template');
                $form = Tools::getValue('form');
                
                if($id_template)
                {  
                        if(Template::get($id_template)->update($form))
                                return $this->displayAllTemplates();
                }
        }
        
        
        public function addBlock()
        {
                $this->loadTemplate();
                $form = Tools::getValue('form');
                $form = urldecode($form);
                $explode = explode('&', $form);
                
                $data = explode('=', $explode[0]);
                $name = $data[1];
                $data = explode('=', $explode[1]);
                $type = $data[1];
                $data = explode('=', $explode[2]);
                $position = $data[1];
                
                $this->loadTemplate();
                $this->template->addBlock(new Block($name, $type), $position);
                
                return $this->displayTemplate();
        }
        
        public function saveBlock()
        {
                $form = Tools::getValue('form');
                $id_block = Tools::getValue('id');
                $block = Block::get($id_block);
                if($block->update($form))
                        return Template::get($block->id_template)->displayTPL();;
        }


        
        
        /********************************************************************************************/
        public function configHowToSendfEmails()
        {
                $output = '
                        <div id="config_of_sending_of_emails_2">
                        <form id="config_of_sending_of_emails">
                                <fieldset>
                                <legend>'.$this->l('Configure reminders').'</legend>
                                
                                <div class="clear">&nbsp;</div>
                                <div style="width:45%; float:left;">
                                        <label>'.$this->l('Deadline for the first reminder').'</label>
                                        <table>
                                                <td><input type="text" size="2" name="tnd_deadline_for_first" value="'.Configuration::get('TND_DEADLINE_FOR_FIRST').'"/></td>
                                                <td>
                                                        <select name="tnd_deadline_for_first_2">
                                                                <option value="D" '.(Configuration::get('TND_DEADLINE_FOR_FIRST_2') == 'D' ? "selected" : "").'>'.$this->l('Days').'</option>
                                                                <option value="H" '.(Configuration::get('TND_DEADLINE_FOR_FIRST_2') == 'H' ? "selected" : "").'>'.$this->l('Hours').'</option>
                                                                <option value="M" '.(Configuration::get('TND_DEADLINE_FOR_FIRST_2') == 'M' ? "selected" : "").'>'.$this->l('Minutes').'</option>
                                                        </select>
                                                </td>
                                        </table>
                                        <div class="clear">&nbsp;</div>
                                
                                        <label>'.$this->l('Time between each reminder').'</label>
                                        <table>
                                                <td>
                                                        <input type="text" size="2" name="tnd_time_between" value="'.Configuration::get('TND_TIME_BETWEEN').'"/>
                                                </td>
                                                <td>
                                                        <select name="tnd_time_between_2">
                                                                <option value="D" '.(Configuration::get('TND_TIME_BETWEEN_2') == 'D' ? "selected" : "").'>'.$this->l('Days').'</option>
                                                                <option value="H" '.(Configuration::get('TND_TIME_BETWEEN_2') == 'H' ? "selected" : "").'>'.$this->l('Hours').'</option>
                                                                <option value="M" '.(Configuration::get('TND_TIME_BETWEEN_2') == 'M' ? "selected" : "").'>'.$this->l('Minutes').'</option>
                                                        </select>
                                                </td>
                                        </table>
                                        <div class="clear">&nbsp;</div>
                                
                                        <label>'.$this->l('Number of reminders').'</label>
                                        <input type="text" size="2" name="nbr_of_reminders" value="'.Configuration::get('NBR_OF_REMINDERS').'"/>
                                        <div class="clear">&nbsp;</div>
                                </div>
                                
                                <div style="width:50%; float:right;">
                                        <label>'.$this->l('The hour of sending in the week').'</label>
                                        <input type="text" size="5" name="hr_of_sending_week" value="'.Configuration::get('HR_OF_SENDING_WEEK').'" class="hr_of_sending"/>
                                        <div class="clear">&nbsp;</div>
                                        
                                        <label>'.$this->l('The hour of sending in the week end').'</label>
                                        <input type="text" size="5" name="hr_of_sending_we" value="'.Configuration::get('HR_OF_SENDING_WE').'" class="hr_of_sending"/>
                                        <div class="clear">&nbsp;</div>
                                </div>
                                
                                <div class="clear">&nbsp;</div>
                                <input type="button" class="button" value="'.$this->l('Save configuration').'"
                                        onclick="javascript:SaveConfigOfSendingEmails()">
                                </fieldset>
                        </form>
                        <div class="clear">&nbsp;</div>      
                ';
                
                return $output;
        }
        
        
        public function configChoiceOfTemplates()
        {
                $output = '
                        <div class="all_templates">
                                '.$this->displayAllTemplates().'
                        </div>
                        <div class="clear">&nbsp;</div>
                        <div class="new_template"></div>
                        <div class="clear">&nbsp;</div>
                        <div class="one_template"></div>
                        <div class="clear">&nbsp;</div>';
                return $output;
        }
        
        

        public function configSendingOfEmails()
        {

                $output = '
                        <fieldset>
                                <legend>'.$this->l('Configure sending of emails').'</legend>
                                        
                                <label>'.$this->l('From (e-mail address of sender)').'</label>
                                <input id="mail_from" type="text" size="70" value="'.Configuration::get('PS_SHOP_EMAIL').'"></input>
                                
                                <div class="clear">&nbsp;</div>
                                
                                <label>'.$this->l('To (e-mail address of receiver)').'</label>
                                <input id="mail_to" type="text" size="70" value="'.Configuration::get('PS_SHOP_EMAIL').'"></input>
                                <div class="margin-form">
                                        '.$this->l('Before enabling the automation of reminders, we invite you to test the e-mail you have set. Enter in this field the email address where you want to receive the test (the content of this test will be based on the first abandoned cart on your online store).').' 
                                </div>
                                        
                                <div class="clear">&nbsp;</div>
                                <input type="button" class="button" value="'.$this->l('Test reminders').'" 
                                        onclick="javascript:TestSendingOfEmails($(\'#mail_from\').attr(\'value\'), $(\'#mail_to\').attr(\'value\'))"></input>
                                <div id="disable_enable_reminders">'.$this->disable_enable_reminders().'</div>
                        </fieldset>
                        
                        <div class="clear">&nbsp;</div>
                        <div class="crontab">'.$this->configCrontab().'</div>
                ';
                $output .= '<div class="clear">&nbsp;</div>';

                return $output;
        }
        
        
        public function disable_enable_reminders()
        {
                $out = '';
                if(Configuration::get('CART_REMINDER_ACTIVE') == 'enable')
                {
                        $out = '<input type="button" id = "activate_reminders" class="button" value="'.$this->l('Activate reminders').'"
                                        onclick="javascript:ActivateReminders($(\'#mail_from\').attr(\'value\'));" style="float:right"  disabled="disabled"></input>
                                
                                <input type="button" class="button" id = "deactivate_reminders" value="'.$this->l('Deactivate reminders').'"
                                        onclick="javascript:DeactivateReminders();" style="float:right; margin-right:5px;"></input>';        
                }
                elseif(Configuration::get('CART_REMINDER_ACTIVE') == 'disable')
                {
                        $out = '<input type="button" id = "activate_reminders" class="button" value="'.$this->l('Activate reminders').'"
                                onclick="javascript:ActivateReminders($(\'#mail_from\').attr(\'value\'));" style="float:right"></input>
                                
                                <input type="button" class="button" id = "deactivate_reminders" value="'.$this->l('Deactivate reminders').'"
                                        onclick="javascript:DeactivateReminders();" style="float:right; margin-right:5px;"  disabled="disabled"></input>';
                }
                
                return $out;
                
        }
        
        
        public function configCrontab()
        {
                $defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));;
                $iso = Language::getIsoById($defaultLanguage);
                
                $week = Configuration::get('HR_OF_SENDING_WEEK');
                $we = Configuration::get('HR_OF_SENDING_WE');
                $first = Configuration::get('TND_DEADLINE_FOR_FIRST');

                if(Configuration::get('TND_DEADLINE_FOR_FIRST_2') == 'M')
                        $output = $data = '*/'.intval($first).' * * * * php -f '.dirname(__FILE__).'/send.php '.Configuration::get('TND_TOKEN');
                elseif(Configuration::get('TND_DEADLINE_FOR_FIRST_2') == 'H')
                        $output = $data = '*/15 * * * * php -f '.dirname(__FILE__).'/send.php '.Configuration::get('TND_TOKEN');
                else
                {
                        $first_tab = '* * * * 0,6 php -f '.dirname(__FILE__).'/send_first.php '.Configuration::get('TND_TOKEN');
                        $week = explode(':', $week);
                        $we = explode(':', $we);
                      
                        if (count($we) > 1)
                        {
                                $we_tab = intval($we[1]).' '.intval($we[0]).' * * '.'0,6 php -f '.dirname(__FILE__).'/send.php '.Configuration::get('TND_TOKEN');
                                $week_tab = intval($week[1]).' '.intval($week[0]).' * * '.'1-5 php -f '.dirname(__FILE__).'/send.php '.Configuration::get('TND_TOKEN');

                                $data = $week_tab."\n".$we_tab."\n".$first_tab;
                                $output = $week_tab.'</br>'.$we_tab.'</br>'.$first_tab;
                        }
                        else $output = '';
                }


                $output = '
                                <b>'.$this->l('CAUTION: To recall that the mails are sent automatically, it is necessary to have previously set a cron task, and sending e-mails from your online shop.')
                                        .' '.$this->l('Please contact your host to configure these two points.').'</b>
                                <b>'.$this->l('The following lines are to be added to crontab:').'</b>
                                <p>'.$output.'</p>
                                <p></p>
                                <p><b>'.$this->l('Link since a browser').'</b><br/>'
                                        .Tools::getShopDomain(true).__PS_BASE_URI__.'modules/cartabandonment/send.php?tnd='.Configuration::get('TND_TOKEN').'</p>
                                <b>'.$this->l('For more information on cron task, see the following page:').' http://'.$iso.'.wikipedia.org/wiki/Cron</b>
                ';
                return '
                        <fieldset>
                                <legend>'.$this->l('Crontab').'</legend>
                                <div class="hint" style="display:block;">'.$output.'</div>
                       </fieldset>';
        }   
        
        
        /********************************************************************************************/
        private function _displayAbandonedCart($carts)
        {

                $output = '<fieldset class="width3"><legend><img src="../modules/'.$this->name.'/logo.gif" />'.$this->l('Abandoned cart').'</legend>';
                $output .= '<table cellpadding="0" cellspacing="0" class="table space">';
                $output .= '<thead>
                                <tr>
                                        <th>'.$this->l('Id cart').'</th>
                                        <th>'.$this->l('Date').'</th>
                                        <th>'.$this->l('Price Taxed').'</th>
                                        <th>'.$this->l('Price untaxed').'</th>
                                        <th>'.$this->l('Firstname').'</th>
                                        <th>'.$this->l('Lastname').'</th>
                                        <th>'.$this->l('Birthday').'</th>
                                </tr>
                          </thead>';

                $output .= '<tbody>';
                foreach ($carts as $value) {
                        $output .= '<tr>';
                        $output .= '<td>'.$value['id_cart'].'</td>';
                        $output .= '<td>'.$value['date_add'].'</td>';
                        $output .= '<td>'.$value['price_taxed'].'</td>';
                        $output .= '<td>'.$value['price_untaxed'].'</td>';
                        $output .= '<td>'.$value['firstname'].'</td>';
                        $output .= '<td>'.$value['lastname'].'</td>';
                        $output .= '<td>'.$value['birthday'].'</td>';
                        $output .= '</tr>';
                }
                $output .= '</tbody>';
                
                $output .= '</table>';
                $output .= '</fieldset>';

                return $output;
        }


        
        private function _get($id_cart = null, $transformed = false, $begin = 0, $max = true)
        {
                $query = '
                        SELECT * from `'._DB_PREFIX_.'cart` `cart`
                        LEFT JOIN `'._DB_PREFIX_.'customer` `cu`
                        ON `cu`.`id_customer` = `cart`.`id_customer
                        WHERE `cart`.`id_cart` NOT IN (
                                SELECT `ca`.`id_cart` FROM `'._DB_PREFIX_.'cart` `ca`
                                LEFT JOIN `'._DB_PREFIX_.'orders` `od`
                                ON `od`.`id_cart` = `ca`.`id_cart`
                                WHERE `od`.`valid` <> 0
                        )';

                if (!is_null($id_cart))
                {
                        $query .= ' AND `cart`.`id_cart` = '.intval($id_cart);
                        $carts = DB::getInstance()->getRow($query);
                }
                else
                {
                        if ($max !== true AND is_numeric($max) AND $begin < $max)
                                $query .= ' LIMIT '.intval($begin).','.(intval($max)-1);
                        $carts = DB::getInstance()->ExecuteS($query);
                }

                foreach ($carts as $key => $value) {
                        $id_cart = intval($value['id_cart']);
                        $obj_cart = new Cart($id_cart);

                        $carts[$key]['all_products'] = $obj_cart->getProducts();
                        $carts[$key]['nbr_of_products'] = $obj_cart->nbProducts();
                        $carts[$key]['price_taxed'] = Tools::displayPrice($obj_cart->getOrderTotal(true));
                        $carts[$key]['price_untaxed'] = Tools::displayPrice($obj_cart->getOrderTotal(false));
                }

                return $carts;
        }
        
        
        /*******************************************************************************************/
        public function saveConfigOfSendingEmails()
        {
                $form = Tools::getValue('form');
                
                foreach($form AS $key=>$value)
                {
                        Configuration::updateValue(strtoupper($key), pSql($value));      
                }
                
                return $this->configCrontab();
        }
        
        
        public function updateNumOfSending($id_template, $num_sending)
        {
                $template = new Template((int)$id_template);
                $template->num_sending = (int)$num_sending;
                $template->save();
                return array('all_templates' => $this->displayAllTemplates(), 'edit_template' => $this->editTemplate($template->id));
        }
        
        
        public function updateTemplateWithDiscount($id_template, $discount)
        {
                $template = new Template((int)$id_template);
                $template->discount = (bool)$discount;
                $template->save();
                return array('all_templates' => $this->displayAllTemplates(), 'edit_template' => $this->editTemplate($template->id));
        }
        
        
        public function updateTemplateWithContentOfCart($id_template, $with_content_of_cart)
        {
                $template = new Template((int)$id_template);
                $template->with_cart_content = (bool)$with_content_of_cart;
                $template->save();
                return array('all_templates' => $this->displayAllTemplates(), 'edit_template' => $this->editTemplate($template->id));
        }
        
        /*****************************************************************************************/
        
        
        public function getGivenUpCarts($id_cart=null, $afterNDays=false, $active = true, $begin = 0, $max = true, $count = false)
        {
                $date_install = Configuration::get('GIVEN_UP_CART_DATE_INSTALL');
                // 604800 s = 7 days

                if ($count)
                        $query = 'SELECT COUNT(`ca`.`id_cart`) FROM `'._DB_PREFIX_.'cart` `ca`';
                else $query = 'SELECT `ca`.*, `cu`.`firstname`, `cu`.`lastname` FROM `'._DB_PREFIX_.'cart` `ca`';

                $query .= ' 
                          LEFT JOIN `'._DB_PREFIX_.'customer` `cu` ON `cu`.`id_customer` = `ca`.`id_customer`
                          WHERE `ca`.`id_cart` NOT IN (SELECT `id_cart` FROM `'._DB_PREFIX_.'orders`)
                                AND `ca`.`id_customer` > 0
                                AND (ABS(UNIX_TIMESTAMP(`ca`.`date_add`) - '.strtotime($date_install).') <= 604800
                                OR UNIX_TIMESTAMP(`ca`.`date_add`) - UNIX_TIMESTAMP("'.$date_install.'") > 604800)';
               
                
                if($afterNDays)
                {
                        if(Configuration::get('TND_DEADLINE_FOR_FIRST_2') == 'M')
                        {
                                $query .= ' AND ((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(`ca`.`date_add`)) >= '.intval(Configuration::get('TND_DEADLINE_FOR_FIRST'))*60 .' )';
                        }
                        elseif(Configuration::get('TND_DEADLINE_FOR_FIRST_2') == 'H')
                        {
                                $query .= ' AND ((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(`ca`.`date_add`)) >= '.intval(Configuration::get('TND_DEADLINE_FOR_FIRST'))*3600 .' )';
                        }
                        else $query .= ' AND (TO_DAYS(NOW()) - TO_DAYS(`ca`.`date_add`) >= '.intval(Configuration::get('TND_DEADLINE_FOR_FIRST')).' )';
                }



                if($active)
                        $query .= ' AND (`ca`.`id_cart` NOT IN (SELECT `id_cart` FROM `'._DB_PREFIX_.'cart_abandonment_stats` WHERE NOT `active`))';

                if(is_numeric($id_cart))
                {
                        $query .= ' AND (`ca`.`id_cart`='.intval($id_cart).' )';
                        $result = DB::getInstance()->getRow($query);

                        $cart = new Cart($result['id_cart']);
                        $result['amount_wt'] = Tools::displayPrice($cart->getOrderTotal(false));
                        $result['nbr_products'] = $cart->nbProducts();

                        return $result;
                }

                if($count)
                        return DB::getInstance()->getValue($query);

                if ($max !== true AND is_numeric($max))
                        $query .= ' LIMIT '.intval($begin).','.(intval($max));

                $results = DB::getInstance()->ExecuteS($query);
                
                
                if (is_array($results))
                        foreach ($results as $key=>$result)
                        {
                                $cart = new Cart($result['id_cart']);
                                
                                $results[$key]['nbr_products'] = $cart->nbProducts();
                                if ($cart->getOrderTotal(false) <  1)
                                        unset($results[$key]);
                                else $results[$key]['amount_wt'] = Tools::displayPrice($cart->getOrderTotal(false));
                        }

                return $results;

        }
        
        private function _getOneCart()
        {
                $query = 'SELECT * FROM `'._DB_PREFIX_.'cart` `ca`';

                $result = DB::getInstance()->getRow($query);

                $cart = new Cart($result['id_cart']);
                $result['amount_wt'] = $cart->getOrderTotal(false);
                $result['nbr_products'] = $cart->nbProducts();

                return $result;

        }
        
        
        
        private function _isEligibleInNSending($num_sending, $id_cart)
        {
                if($this->_isPreviousSending($num_sending, $id_cart) AND !$this->isTransformedCart($id_cart))
                {
                        $opt = '';
                        if(Configuration::get('TND_TIME_BETWEEN_2') == 'M')
                        {
                                $opt .= ' AND ((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(`date_sending`)) >= '.intval(Configuration::get('TND_TIME_BETWEEN'))*70 .' )';
                        }
                        elseif(Configuration::get('TND_TIME_BETWEEN_2') == 'H')
                        {
                                $opt .= ' AND ((UNIX_TIMESTAMP() - UNIX_TIMESTAMP(`date_sending`)) >= '.intval(Configuration::get('TND_TIME_BETWEEN'))*3700 .' )';
                        }
                        else $opt .= ' AND (TO_DAYS(NOW()) - TO_DAYS(`date_sending`) >= '.intval(Configuration::get('TND_TIME_BETWEEN')).' )';
                        
                        
                        $return = DB::getInstance()->getRow('SELECT * 
                                        FROM `'._DB_PREFIX_.'cart_abandonment_stats`
                                        WHERE `id_cart` = '.intval($id_cart).$opt.
                                        ' ORDER BY `num_sending` DESC');
                      
                        
                        return ($return AND $num_sending - $return['num_sending'] == 1);
                }
                else if($num_sending == 1)
                        return !(bool)DB::getInstance()->getRow('SELECT * 
                                        FROM `'._DB_PREFIX_.'cart_abandonment_stats`
                                        WHERE `id_cart` = '.intval($id_cart));
                
                return false;
        }
        
        
        
        
        private function _isPreviousSending($num_sending, $id_cart)
        {
                if($num_sending > 1)
                        return (bool)DB::getInstance()->getValue('SELECT * FROM `'._DB_PREFIX_.'cart_abandonment_stats` WHERE `id_cart` = '.intval($id_cart).' AND `num_sending` >= '.(intval($num_sending) - 1));
                return false;
        }
        
        
        private function _formatCart($id_cart, $test=false)
        {
                $cartObj = new Cart(intval($id_cart));

                if(!$cartObj->id_customer AND !$test)
                        return false;
                
                $customerObj = new Customer($cartObj->id_customer);
                
                if($test)
                        $result = $this->_getOneCart();
                else 
                        $result = $this->getGivenUpCarts(intval($id_cart));
                
                $result['firstname'] = $customerObj->firstname;
                $result['lastname'] = $customerObj->lastname;
                $result['email'] = $customerObj->email;
                $result['secure_key'] = $customerObj->secure_key;

                if(!empty($customerObj->id_lang))
                        $result['iso'] = Language::getIsoById($cartObj->id_lang);
                else
                {
                        $defaultLanguage = (int)(Configuration::get('PS_LANG_DEFAULT'));;
                        $result['iso'] = Language::getIsoById($defaultLanguage);
                }
                return $result;
        }
        
        
        private function _generateEmail($cart_detail, $test = false, $first = false)
        {
                $languages = Language::getLanguages();
                $output = $tpl = $num_sending = null;
                $max_sending = Configuration::get('NBR_OF_REMINDERS');

                if (!strlen($cart_detail['iso']))
                        $cart_detail['iso'] = null;
                
                if ($test)
                {
                        $res = array ();
                        $tpl = Template::get();
                        foreach ($tpl as $value)
                        {
                                $output = $this->viewReminder($cart_detail['id_cart'], $cart_detail['secure_key'], $cart_detail['id_customer'], $num_sending, $test, true, $value->id);
                                $title = $output['title'];
                                $output = $output['msg']; 
                                $title = str_replace('%FIRSTNAME%', ucfirst($cart_detail['firstname']), $title);
                                $title = str_replace('%LASTNAME%', strtoupper($cart_detail['lastname']), $title);
                                $title = str_replace('%SHOP_NAME%', Configuration::get('PS_SHOP_NAME'), $title);
                                $res[] = array('msg' => $output, 'num_sending' => $num_sending, 'template_id' => $value->id, 'title' => 'Test template '.$value->name.' : '.$title);
                        }
                        return $res;
                }
                
                if ($first AND $this->_isEligibleInNSending(1, $cart_detail['id_cart']))
                {
                        $tpl = Template::getByNumSending(1, $cart_detail['iso']);

                        if(is_array($tpl) AND count($tpl))
                                $tpl = $tpl[0];
                        elseif(is_null($tpl))
                                return $this->l('Please create at least one template');
                }
                
                elseif(!$first AND !$test)
                {
                        for ($i=1; ($i<=$max_sending) AND !($tpl instanceof Template); $i++)
                        {
                                if($this->_isEligibleInNSending($i, $cart_detail['id_cart']))
                                {
                                        $tpl = Template::getByNumSending($i, $cart_detail['iso']);
                                        if (is_array($tpl) AND count($tpl))
                                                $tpl = $tpl[0];
                                        elseif (empty($tpl))
                                                return $this->l('Please create at least one template');
                                        
                                        $num_sending = $i;
                                } 
                        }
                }
                
                if (!(empty($num_sending) OR empty($tpl)))
                {
                        $output = $this->viewReminder($cart_detail['id_cart'], $cart_detail['secure_key'], $cart_detail['id_customer'], $num_sending, $test, true, $tpl->id);

                        $title = $output['title'];
                        $output = $output['msg']; 
                        $title = str_replace('%FIRSTNAME%', ucfirst($cart_detail['firstname']), $title);
                        $title = str_replace('%LASTNAME%', strtoupper($cart_detail['lastname']), $title);
                        $title = str_replace('%SHOP_NAME%', Configuration::get('PS_SHOP_NAME'), $title);
                        $output = array('msg' => $output, 'num_sending' => $num_sending, 'template_id' => $tpl->id, 'title' => $title);
                }

                return $output;
        }
        
        
        
        public function replaceLink($name, $link, $msg = NULL)
        {
                $open = strtolower($name).'_open_link';
                $close = strtolower($name).'_close_link';
                
                if(!is_null($msg))
                {
                        $msg = str_replace('$'.$open, '<a href="'.$link.'">', $msg);
                        $msg = str_replace('$'.$close, '</a>', $msg);
                        return $msg;
                }
                
                return array($open => '<a href="'.$link.'">', $close => '</a>');
                
        }
        
        
        public function deactivateReminders()
        {
                Configuration::updateValue('CART_REMINDER_ACTIVE', 'disable');
                return $this->disable_enable_reminders();
        }
        
        
        public function activateReminders()
        {
                Configuration::updateValue('CART_REMINDER_ACTIVE', 'enable');
                return $this->disable_enable_reminders();
        }
        
        public function remindCarts($from, $to)
        {
                if (Tools::getValue('test'))
                        if (!$this->sendEmails($to))
                                return;
                else
                {
                        $templates = Template::get();
                        if (empty($templates));
                                return $this->displayError($this->l('Please create at least one template'));
    
                        Configuration::updateValue('CART_REMINDER_ACTIVE', 'enable');
                        Configuration::updateValue('CART_REMINDER_FROM', $from);
                        
                        return $this->displayConfirmation($this->l('The reminder of e-mails was activated'));
                }
        }
        

        
        public function sendEmails($to = null, $first = false)
        {
                $this->updateStats();
                $db = DB::getInstance();
 
                $from = Configuration::get('CART_REMINDER_FROM');
                $return = $no_log = false;
                $msg = '';

                if (empty($to) AND Configuration::get('CART_REMINDER_ACTIVE')== 'enable')
                {
                        $begin = 0;
                        $max = 100;

                        while($all_carts = $this->getGivenUpCarts($id_cart=null, $afterNDays=true, $active = true, $begin, $max))
                        {
                                $begin += $max;
                                foreach($all_carts AS $cart)
                                {
                                        $cart_data = $this->_formatCart($cart['id_cart']);
                                        if ($cart_data)
                                        {
                                                $out = $this->_generateEmail($cart_data, false, $first);
                                                if (is_array($out) AND array_key_exists('msg', $out))
                                                        $return = $this->send($from, $cart_data['email'], $out['msg'], $out['title']);
                                        }
                                        else $no_log = true;
                                        
                                        if ($return AND is_array($out) AND array_key_exists('num_sending', $out))
                                        {
                                                if ($db->getRow('SELECT * FROM `'._DB_PREFIX_.'cart_abandonment_stats`WHERE `id_cart`='.$cart['id_cart'].' AND `num_sending`='.$out['num_sending']))
                                                {
                                                        $query = '
                                                                UPDATE `'._DB_PREFIX_.'cart_abandonment_stats`
                                                                SET `date_sending`= NOW(), `template_id`="'.$out['template_id'].'"
                                                                WHERE `id_cart`='.$cart['id_cart'].' AND `num_sending`='.$out['num_sending'];
                                                }
                                                else{
                                                        $query = '
                                                                INSERT INTO `'._DB_PREFIX_.'cart_abandonment_stats`
                                                                (`date_sending`, `id_cart`, `template_id`, `num_sending`)
                                                                VALUES (NOW(),"'.$cart['id_cart'].'", "'.$out['template_id'].'", "'.$out['num_sending'].'")';
                                                }
                                                DB::getInstance()->Execute($query);
                                                $msg .= "\n OK : ".$cart_data['email'];
                                        }

                                        if (!$return AND !$no_log)
                                        {
                                                $msg .= "\n KO : ".$cart_data['email'];
                                        }
                                }
                        }

                }
                elseif (Validate::isEmail($to))
                {
                        $return = true;
                        $cart = $this->_getOneCart();
                        $cart_data = $this->_formatCart($cart['id_cart'], true);
                        $out = $this->_generateEmail($cart_data, true);

                        if (!is_array($out) AND !is_object($out))
                                return;

                        foreach ($out AS $data)
                                $return = ($return AND $this->send($from, $to, $data['msg'], $data['title']));

                        if($return)
                                $msg = "\n Test OK : ".$to;
                        else $msg = "\n Test OK : ".$to;
                }
                else $msg = "\n KO : ";
                
                $this->addLog($msg);
                
                if($return)
                {
                        echo $this->displayConfirmation('OK');
                        return true;
                }
                else echo $this->displayError('KO');
        }
        
        
        
        private static function _getConnection()
	{
		$configuration = Configuration::getMultiple(array('PS_SHOP_EMAIL', 'PS_MAIL_METHOD', 'PS_MAIL_SERVER', 'PS_MAIL_USER', 'PS_MAIL_PASSWD', 'PS_SHOP_NAME', 'PS_MAIL_SMTP_ENCRYPTION', 'PS_MAIL_SMTP_PORT', 'PS_MAIL_METHOD', 'PS_MAIL_TYPE'));
		if (intval($configuration['PS_MAIL_METHOD']) == 2)
		{
			include_once(_PS_SWIFT_DIR_.'Swift/Connection/SMTP.php');
			$connection = new Swift_Connection_SMTP($configuration['PS_MAIL_SERVER'], $configuration['PS_MAIL_SMTP_PORT'], ($configuration['PS_MAIL_SMTP_ENCRYPTION'] == "ssl") ? Swift_Connection_SMTP::ENC_SSL : (($configuration['PS_MAIL_SMTP_ENCRYPTION'] == "tls") ? Swift_Connection_SMTP::ENC_TLS : Swift_Connection_SMTP::ENC_OFF));
			$connection->setTimeout(4);
			if (!$connection)
				return false;
			if (!empty($configuration['PS_MAIL_USER']) AND !empty($configuration['PS_MAIL_PASSWD']))
			{
				$connection->setUsername($configuration['PS_MAIL_USER']);
				$connection->setPassword($configuration['PS_MAIL_PASSWD']);
			}
		}
		else
		{
			include_once(_PS_SWIFT_DIR_.'Swift/Connection/NativeMail.php');
			$connection = new Swift_Connection_NativeMail();
		}
		return ($connection);
	}
        
        
        
        
        public function send($from, $to, $msg, $title)
        {
                $return = false;
                if (Validate::isEmail($from) AND Validate::isEmail($to))
                {
                        require_once(_PS_SWIFT_DIR_.'Swift.php');
                        
                        if (!$connection = self::_getConnection())
                                return false;

                        @ini_set('max_execution_time', '7200');
                        $begin = time();
                        do
                        {
                                try {
                                        $swift = new Swift($connection);
                                        $connected = true;
                                }
                                catch(Swift_ConnectionException $e)
                                {
                                        if(time() > ($begin + 70))
                                                return $this->displayError($this->l('Connection to the server of e-mails failed'));
                                        $connected = false;
                                }

                        } while(!$connected);
                        
                        if($connected)
                        {
                                $swift_msg = new Swift_message($title, $msg, 'text/html', '8bit', 'utf-8');
                                $return = $swift->Send($swift_msg, new Swift_Address($to), new Swift_Address($from));       
                        }
                }
                
                return $return;
        }
        
        
        private function createLink($action, $num_sending, $id_customer, $id_cart, $secure_key)
        {
                $output = Tools::getShopDomain(true).__PS_BASE_URI__.'modules/'.$this->name.'/index.php?ks='.$secure_key.'&uc='.intval($id_customer).'&ac='.intval($id_cart).'&mun='.intval($num_sending);
                
                if($action == 'shop')
                {
                        return Tools::getShopDomain(true).__PS_BASE_URI__;
                }
                if($action == 'unsubscribe')
                {
                        return $output.'&op=UNS';
                }
                if($action == 'cart')
                {
                        return $output.'&op=CA';
                }
                if($action == 'reminder')
                {
                        return $output.'&op=RMD';
                }
                if($action == 'read')
                {
                        return $output.'&op=RD';
                }
        }
                                                
        public function displayProductsOfCarts($id_cart, $id_template)
        {
                $cart = new Cart($id_cart);
                $all_products = $cart->getProducts();
                $images = Tools::getShopDomain(true).__PS_BASE_URI__.'modules/'.$this->name.'/images';
                $output = '';
                $template = new Template((int)$id_template);
                $four_products = array_chunk($all_products, 4);
               
                foreach($four_products AS $products)
                {
                        $count = count($products);
                        $out = '';
                        $i = 0;
                        foreach($products AS $product)
                        {
                                $i++;
                                $add = '';
                                if($i != $count)
                                        $add = '<td width="10" valign="top">&nbsp;</td>';
                                
                                $link = new Link();

                                $id_lang = $template->id_lang;
                                $language = new Language($id_lang);

                                if (!$language->active OR empty($language->id))
                                        $id_lang = (int)(Configuration::get('PS_LANG_DEFAULT'));
                                
                                $my_product = new Product($product['id_product'], false);
                                $product_image = $link->getImageLink($product['link_rewrite'], $product['id_image'], 'home');
                                if (strpos($product_image, 'http') !== 0)
                                        $product_image = (Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://').$product_image;
                                $product_page = $link->getProductLink($my_product, NULL, NULL, NULL, $template->id);

                                
                                $my_product->name[$id_lang];
                                $template->body_font_color;
                                $out .='
        <td width="132" valign="top">
		<table width="132" border="0" cellspacing="0" cellpadding="0">
			<tr>
				<td height="1" width="1" bgcolor="#d9d9d9"><img src="'.$images.'/p.png" alt="" height="1" width="1" style="display:block" /></td>
				<td height="1" width="130" bgcolor="#d9d9d9"><img src="'.$images.'/p.png" alt="" height="1" width="130" style="display:block" /></td>
				<td height="1" width="1" bgcolor="#d9d9d9"><img src="'.$images.'/p.png" alt="" height="1" width="1" style="display:block" /></td>
			</tr>
                        <tr>
                                <td bgcolor="#d9d9d9"></td>
                                <td height="130" align="center">
                                        <a href="'.$product_page.'">
                                                <img src="'.$product_image.'" style="display:block; border:none;"/>
                                        </a>
                                </td>
                                <td bgcolor="#d9d9d9"></td>
                                
                        </tr>
                        <tr>
                                <td height="1" bgcolor="#d9d9d9"></td>
                                <td height="1" bgcolor="#d9d9d9"></td>
                                <td height="1" bgcolor="#d9d9d9"></td>
                        </tr>
                </table>
                <a href="'.$product_page.'">
                        <img src="'.$images.'/p.png" alt="" height="10" width="1" style="display:block" />
                        <font face="Arial, Verdana, sans-serif"  color="'.Configuration::get('TND_BODY_COLOR').'">
                                <span style="font-size:14px; color:#'.$template->body_font_color.'"><b>'.$my_product->name[$id_lang].'</b></span>
                                <img src="'.$images.'/p.png" alt="" height="8" width="1" style="display:block" />
                                <span style="font-size:12px; margin-right:10px; color:#'.$template->body_font_color.'">'.$my_product->description_short[$id_lang].'</span>
                                <img src="'.$images.'/p.png" alt="" height="15" width="1" style="display:block" />
                        </font>
                </a>
        </td>'.$add;
                                
                        }
                        $output .= '<tr>'.$out.'</tr>';
                }
                
                return $output;
        }
        
        
        
        public function viewReminder($id_cart, $secure_key, $id_customer, $num_sending, $test = false, $title = false, $id_template = NULL)
        {
                if ($test)
                {
                        $valide = true;
                        $cart_detail = $this->_formatCart($id_cart, true);
                }
                else {
                        $cart_detail = $this->getGivenUpCarts($id_cart);
                        $valide = (($cart_detail['id_customer'] == $id_customer) AND ($cart_detail['id_cart'] == $id_cart) AND ($cart_detail['secure_key'] == $secure_key));
                }

                if ($valide)
                {
                        if (empty($id_template))
                        {        
                                $query = 'SELECT `template_id`
                                        FROM `'._DB_PREFIX_.'cart_abandonment_stats`
                                        WHERE `id_cart`='.intval($id_cart).' AND `num_sending`='.intval($num_sending); 
                        
                                $id_template = (int)DB::getInstance()->getValue($query);

                                $tpl = new Template($id_template);
                        }
                        else $tpl = new Template($id_template);
                        
                        if (empty($tpl->id))
                                return $this->l('Please create at least one template');
                        
                        $link_ca = $this->createLink('cart', $num_sending, $cart_detail['id_customer'], $cart_detail['id_cart'], $cart_detail['secure_key']);
                        $link_rmd = $this->createLink('reminder', $num_sending, $cart_detail['id_customer'], $cart_detail['id_cart'], $cart_detail['secure_key']);
                        $link_rd = $this->createLink('read', $num_sending, $cart_detail['id_customer'], $cart_detail['id_cart'], $cart_detail['secure_key']);
                        $link_uns = $this->createLink('unsubscribe', $num_sending, $cart_detail['id_customer'], $cart_detail['id_cart'], $cart_detail['secure_key']);
                        $link_sp = $this->createLink('shop', $num_sending, $cart_detail['id_customer'], $cart_detail['id_cart'], $cart_detail['secure_key']);

                        $title = $tpl->title;
                        $title = str_replace('%FIRSTNAME%', ucfirst($cart_detail['firstname']), $title);
                        $title = str_replace('%LASTNAME%', strtoupper($cart_detail['lastname']), $title);
                        $array = array(
                                'firstname' => ucfirst($cart_detail['firstname']),
                                'lastname' =>  strtoupper($cart_detail['lastname']),
                                'quantity'=> $cart_detail['nbr_products'],
                                'id_customer' => $cart_detail['id_customer'],
                                'cart_content'=> ($tpl->with_cart_content ? $this->displayProductsOfCarts($cart_detail['id_cart'], $tpl->id) : ''),
                                'shop_name'=> '<a href="'.Tools::getShopDomain(true).__PS_BASE_URI__.'">'.Configuration::get('PS_SHOP_NAME').'</a>',
                                'date_sending' => Tools::displayDate($cart_detail['date_upd'], $cart_detail['id_lang'], true),
                                'num_sending'=> $num_sending,
                                'title' => $title);
                        
                        $array = array_merge($this->replaceLink('RMD', $link_rmd), $array);
                        $array = array_merge($this->replaceLink('READ', $link_rd), $array);
                        $array = array_merge($this->replaceLink('UNSUBSCRIBE', $link_uns), $array);
                        $array = array_merge($this->replaceLink('CART', $link_ca), $array);
                        $array = array_merge($this->replaceLink('SHOP', $link_sp), $array);

                        
                         if ($tpl->discount)
                         {
                                $voucher = $this->createVoucher($cart_detail['id_cart'], $cart_detail['id_customer']);
                                if($voucher)
                                {
                                        $array['voucher'] = $voucher->name;
                                        
                                        if($voucher->id_discount_type == 2)
                                                $array['voucher_value'] = Tools::displayPrice($voucher->value);
                                        elseif($voucher->id_discount_type == 1)
                                                $array['voucher_value'] = $voucher->value.'%';
                                        
                                                                
                                        $query = '
                                                INSERT INTO `'._DB_PREFIX_.'cart_abandonment_stats`
                                                        (`id_discount`, `id_cart`, `num_sending`)
                                                        VALUES ('.Discount::getIdByName($voucher->name).','.intval($id_cart).','.$num_sending.')';
                                        if(!$test)
                                                DB::getInstance()->Execute($query);
                                }
                                else
                                        $array['voucher'] = $array['voucher_value'] = false;
                        }
                        $msg = $tpl->view($array);
                        if ($title)
                                return array('msg' => $msg, 'title' => $title);
                        else return $msg;
                }
        }
        
        
        public function unsubscribe($id_cart, $secure_key, $id_customer, $num_sending)
        {
                $cart_detail = $this->getGivenUpCarts($id_cart, false, false);
                
                if($cart_detail['id_customer']==$id_customer AND $cart_detail['id_cart']==$id_cart AND $cart_detail['secure_key']==$secure_key)
                {
                        $query = 'UPDATE `'._DB_PREFIX_.'cart_abandonment_stats`
                                        SET `date_unsubscribing` = NOW(), `unsubscribe` ="1", `active`="0"
                                        WHERE `id_cart`='.intval($id_cart).' AND `num_sending`='.intval($num_sending);
                        

                        
                        DB::getInstance()->Execute($query);
                }      
        }
        
        
        private function _getUnsubscribe()
        {
                $query = 'SELECT DISTINCT `id_cart` FROM `'._DB_PREFIX_.'cart_abandonment_stats` WHERE `unsubscribe` = "1"';
                return DB::getInstance()->ExecuteS($query);
        }
        
        
        public function addLog($msg)
        {
                file_put_contents(dirname(__FILE__).'/log.txt', date(DATE_RFC822)."\n".$msg ,FILE_APPEND);
        }
        
        
        private function updateStats()
        {
                $res = DB::getInstance()->ExecuteS('SELECT DISTINCT `id_cart`
                                FROM `'._DB_PREFIX_.'cart_abandonment_stats`
                                WHERE `num_sending` >= '.Configuration::get('NBR_OF_REMINDERS'));

                if (!empty($res))
                {
                        foreach ($res AS $r)
                                $tmp[] = $r['id_cart'];
                        $res = implode(',', $tmp);
                
                        DB::getInstance()->Execute(
                                'UPDATE `'._DB_PREFIX_.'cart_abandonment_stats`
                                SET `active`=0
                                WHERE `id_cart` IN ('.$res.')');
                }
                


                $res = DB::getInstance()->ExecuteS('SELECT o.`id_cart`, `date_upd`
                                                        FROM `'._DB_PREFIX_.'orders` o
                                                        INNER JOIN `'._DB_PREFIX_.'cart_abandonment_stats` cas ON cas.`id_cart` = o.`id_cart`
                                                        WHERE `transformed` = 0
                                                        LIMIT 100');
                $tmp = array();
                if (!empty($res))
                        foreach ($res AS $r)
                                DB::getInstance()->Execute(
                                'UPDATE `'._DB_PREFIX_.'cart_abandonment_stats`
                                SET `transformed`="1", `date_transforming`= "'.$r['date_upd'].'", `active` = 0
                                WHERE `id_cart` = '.$r['id_cart']);
        }
        
        
        
        private function getTranformedCarts($max=0, $act = 'next')
        {
                $max = intval($max);
                if($act == 'prev')
                {
                        $up = $max;
                        $down = $max-30;
                }
                else
                {
                        $up = $max+30;
                        $down = $max;
                }
                
                return DB::getInstance()->ExecuteS(
                        '
                                SELECT DISTINCT `ca`.`id_cart`, `cu`.`id_customer`, `transformed`, `lastname`, `firstname`, `date_transforming`, `num_sending` AS `num_send`, `email`, `total_discounts`, `total_paid`
                                FROM `'._DB_PREFIX_.'cart_abandonment_stats` `ca`
                                LEFT JOIN `'._DB_PREFIX_.'orders` `o`
                                ON `o`.`id_cart` = `ca`.`id_cart`
                                LEFT JOIN `'._DB_PREFIX_.'customer` `cu`
                                ON `cu`.`id_customer` = `o`.`id_customer`
                                WHERE `num_sending` = (SELECT MAX(`c`.`num_sending`) FROM `'._DB_PREFIX_.'cart_abandonment_stats` `c` WHERE `ca`.`id_cart` = `c`.`id_cart`)
                                GROUP BY `ca`.`id_cart`
                                HAVING `transformed`="1"
                                LIMIT '.$down.','.$up.'
                                
                        ');
        }
        
        
        public function displayTransformedCarts($max = 0, $act = 'next')
        {    
                if (is_numeric(Tools::getValue('maxL')))
                {
                        $max = Tools::getValue('maxL');
                        $act = Tools::getValue('act');
                }
                $max = intval($max);

                $res = $this->getTranformedCarts($max, $act);

                $tmp = '';
                if(count($res) > 30)
                {
                        $tmp = '<input type="button" style="float:right" class="button" value="'.$this->l('Next').'" onclick="javascript:displayNextTransformed('.($max+30).')"/>';
                }
                if($max > 30)
                        $tmp .= '<input type="button" class="button" value="'.$this->l('Next').'" onclick="javascript:displayNextTransformed('.($max+30).', \'prev\')"/>';
                
                
                $output = 
                        '<script type="text/javascript">
                                function displayNextTransformed(maxL, act)
                                {
                                        $.post("'.(Configuration::get('PS_SSL_ENABLED') ? 'https://' : 'http://').Configuration::get('TND_HTTP_HOST').__PS_BASE_URI__.'modules/'.$this->name.'/tools.php?token='.Configuration::get('TND_TOKEN').'",
                                                {
                                                        action: "display_transformed_carts",
                                                        maxL : maxL,
                                                        act : act
                                                },
                                                function(data)
                                                {
                                                        if(data)
                                                                $("#cart_abandonment_stats").html(data);
;                                                });
                                }
                        </script>';
                
                $output .= '<table class="table" cellspacing=0 cellpadding=0>
                                <thead>
                                        <tr>
                                                <th>'.$this->l('ID Customer').'</th>
                                                <th>'.$this->l('Lastname').'</th>
                                                <th>'.$this->l('Firstname').'</th>
                                                <th>'.$this->l('Date transformed').'</th>
                                                <th>'.$this->l('Num sending').'</th>
                                                <th>'.$this->l('Email').'</th>
                                                <th>'.$this->l('Total Discounts').'</th>
                                                <th>'.$this->l('Total paid').'</th>
                                        </tr>
                                </thead>
                                </tbody>';
                
                foreach($res AS $r)
                {
                        $output .= '<tr><td>'.$r['id_customer'].'</td>';
                        $output .= '<td>'.$r['lastname'].'</td>';
                        $output .= '<td>'.$r['firstname'].'</td>';
                        $output .= '<td>'.$r['date_transforming'].'</td>';
                        $output .= '<td>'.$r['num_send'].'</td>';
                        $output .= '<td>'.$r['email'].'</td>';
                        $output .= '<td>'.Tools::displayPrice($r['total_discounts']).'</td>';
                        $output .= '<td>'.Tools::displayPrice($r['total_paid']).'</td></tr>';
                }
                
                $output .= '</tbody></table>';
                
                
                $output = '
                        <fieldset id="cart_abandonment_stats">
                                <legend>'.$this->l('Transformed carts').'</legend>
                                <div class="clear">&nbsp;</div>
                                '.$output.'
                                <div class="clear">&nbsp;</div>
                                '.$tmp.'
                        </fieldset>';
                
                if($res)
                        return $output;
        }
        
        
              
}
