<script>
{literal}
	$(document).ready(function(){
{/literal}
{if $isOrder}
	{literal}
			var alertText = "{/literal}{l s='Le minimum de commande est définie à' mod='toolcartmin'} {$montant_mini}€ {l s="d'achats" mod='toolcartmin'}{literal}";
			$('.step').after('<div class="alert alert-danger cartmin-danger hidden"><p>' + alertText + '</p></div>');
		});
	{/literal}
{/if}
{if $diff_cartmin>0}
{literal}
	$('.standard-checkout').addClass('hidden');
	$('.cartmin-danger').removeClass('hidden');
{/literal}
{/if}
	}
</script>