{*
 *  @author Boostervente <ppactol@boostervente.com>
 *  @copyright  2012 Boostervente
 *  @version  1.0
 *}
		{assign var='blockcartmin_flag' value='Cart::BOTH_WITHOUT_SHIPPING'|constant}
		<div id="blockcartmin" class="block{if $diff_cartmin<=0 and $cart->getOrderTotal(true, $blockcartmin_flag) != 0 or $cart->getOrderTotal(true, $blockcartmin_flag) == 0} hidden{/if}">
			<h4>{l s='Minimum de commande' mod='toolcartmin'}</h4>
			<div class="block_content">
				<p>{l s='Le minimum de commande est définie à' mod='toolcartmin'} {$montant_mini}€ {l s='d\'achats' mod='toolcartmin'}</p>
					<hr />
					<p>{l s='Vous devez encore commander' mod='toolcartmin'}
						<span class="diff_cartmin">
							{convertPrice price=$montant_mini-$cart->getOrderTotal(true, $blockcartmin_flag)}
						</span>
					</p>
			</div>
		</div>
<script>
	{if $diff_cartmin>0 and $cart->getOrderTotal(true, $blockcartmin_flag) != 0}
		$('#blockcartmin').removeClass('hidden');
		$('#button_order_cart').addClass('hidden');
	{/if}
</script>