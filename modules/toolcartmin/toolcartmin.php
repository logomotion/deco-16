<?php
/*
 *  @author Boostervente <ppactol@boostervente.com>
 *  @copyright  2012 Boostervente
 *  @version  1.0
 */

if (!defined('_CAN_LOAD_FILES_'))
	exit;

class ToolCartMin extends Module{

	public function __construct(){
		$this->name = 'toolcartmin';
		$this->tab = 'front_office_features';
		$this->version = '1.0';
		$this->author = 'Boostervente';
		$this->need_instance = 1;

		parent::__construct();

		$this->displayName = $this->l('Panier mini');
		$this->description = $this->l("Permet de bloquer le processus de commande si le montant n'est pas atteint.");
	}

	public function install(){
		if(!parent::install() ||
			!$this->installDB() ||
			!$this->registerHook('header') ||
			!$this->registerHook('footer') ||
			!$this->registerHook('rightColumn') ||
			!Configuration::updateValue('TOOLCARTMIN_URL', ''))
			return false;
		return true;
	}

	public function installDB(){
		$sql1 = "CREATE TABLE IF NOT EXISTS `"._DB_PREFIX_."e_cartmin` (
		  `id_cartmin` int(10) unsigned NOT NULL AUTO_INCREMENT,
		  `fk_id_zone` int(8) NOT NULL,
		  `fk_id_group` int(8) NOT NULL,
		  `montant_mini` double(10,2) NOT NULL DEFAULT '0.00',
		  PRIMARY KEY (`id_cartmin`)
		) ENGINE=MyISAM DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;";
		if(!Db::getInstance()->Execute($sql1)){
			return false;
		}
		return true;
	}

	public function uninstall(){
		if (!parent::uninstall() ||
			!$this->uninstallDB() ||
			!Configuration::deleteByName('TOOLCARTMIN_URL'))
			return false;
		return true;
	}

	public function uninstallDB(){
		$sql1 = "DROP TABLE ps_e_cartmin";
		if(!Db::getInstance()->Execute($sql)){
			return false;
		}
		return true;
	}

	public function getContent(){
		global $currentIndex, $cookie;
		if(isset($_POST['saveCartmin']) && is_array($_POST['tab_cartmin'])){
			foreach($_POST['tab_cartmin'] as $key_group => $val_group){
				if(is_array($val_group)){
					foreach($val_group as $key_zone => $val_zone){
						if($this->cartminExist($key_group,$key_zone)){
							$this->updateCartmin($key_group,$key_zone,$val_zone);
						}
						else{
							$this->insertCartmin($key_group,$key_zone,$val_zone);
						}
					}
				}
			}
			Configuration::updateValue('TOOLCARTMIN_URL',Tools::getValue('TOOLCARTMIN_URL'));
		}
		$groups = Group::getGroups($cookie->id_lang);
		$zones = Zone::getZones(true);
		$tab_cartmin = $this->getCartmins();
		$str = '
		</fieldset><br />
		<form method="POST" action="'.Tools::htmlentitiesUTF8($_SERVER['REQUEST_URI']).'">
		<fieldset>
			<legend>'.$this->l('Panier minimum').'</legend>
			<h3>'.$this->l('Montant minimum pour passer une commande').'</h3>';
			$str .= '<table class="table" cellspacing="0" cellpadding="0">';
			// GROUPS
			$str .= '<tr>
						<th>'.$this->l('Zones').' / '.$this->l('Groupes').'</th>';
						if(is_array($groups)){
							foreach($groups as $key_g => $val_g){
								$str .= '<th>'.$val_g['name'].'</th>';
							}
						}
			$str .= '</tr>';
			// ZONES
			if(is_array($zones)){
				foreach($zones as $key_z => $val_z){
					$str .= '<tr>
								<td>'.$val_z['name'].'</td>';
								if(is_array($groups)){
									foreach($groups as $key_g => $val_g){
										$str .= '<td><input type="text" name="tab_cartmin['.$val_g['id_group'].']['.$val_z['id_zone'].']" value="'.(isset($tab_cartmin[$val_g['id_group']][$val_z['id_zone']])?$tab_cartmin[$val_g['id_group']][$val_z['id_zone']]['montant_mini']:0).'" size="8" style="text-align:right;" /> €</td>';
									}
								}
					$str .= '</tr>';
				}
			}
			$str .= '</table>';
			$str .= '<br />
					<label>'.$this->l('Url de redirection').'</label>
					<div class="margin-form">
						<input type="text" name="TOOLCARTMIN_URL" value="'.Tools::getValue('TOOLCARTMIN_URL',Configuration::get('TOOLCARTMIN_URL')).'" size="80" />
					</div>
					<center>
						<input type="submit" name="saveCartmin" value="'.$this->l('Enregistrer').'" class="button" />
					</center>';
		$str .= '</fieldset>
		</form>';
		return $str;
	}

	public function hookHeader($params){
		Tools::addCSS($this->_path . 'toolcartmin.css', 'screen');
		global $smarty;
		$trans = array('.php'=>'');
			if(isset($params['cart']->id) && $params['cart']->id>0){
				if($params['cookie']->isLogged()){
					$id_group = Customer::getDefaultGroupId($params['cookie']->id_customer);
					$id_zone = Country::getIdZone(Customer::getCurrentCountry($params['cookie']->id_customer));
				}
				else{
					$id_group = 1;
					$id_zone = Country::getIdZone(Configuration::get('PS_COUNTRY_DEFAULT'));
				}
				$montant_mini = $this->getMontantCartmin($id_group,$id_zone);
				$total_products_cart = $params['cart']->getOrderTotal(false,Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING);
				$diff_cartmin = $montant_mini - $total_products_cart;
				$smarty->assign(array(
					'montant_mini' => $montant_mini,
					'isOrder' => (get_class($this->context->controller) === 'OrderController'),
					'diff_cartmin' => $diff_cartmin
					));
				return $this->display(__FILE__, 'blockcartminheader.tpl');
			}

	}
	public function hookRightColumn($params){
		global $smarty;
		if($params['cookie']->isLogged()){
			$id_group = Customer::getDefaultGroupId($params['cookie']->id_customer);
			$id_zone = Country::getIdZone(Customer::getCurrentCountry($params['cookie']->id_customer));
		}
		else{
			$id_group = 1;
			$id_zone = Country::getIdZone(Configuration::get('PS_COUNTRY_DEFAULT'));
		}
		$montant_mini = $this->getMontantCartmin($id_group,$id_zone);
		$total_products_cart = $params['cart']->getOrderTotal(true,Cart::ONLY_PHYSICAL_PRODUCTS_WITHOUT_SHIPPING);
		$diff_cartmin = $montant_mini - $total_products_cart;
		$smarty->assign(array(
			'montant_mini' => $montant_mini,
			'diff_cartmin' => $diff_cartmin,
			'cart' => $params['cart']
		));
		return $this->display(__FILE__, 'blockcartmin.tpl');
	}

	public function hookLeftColumn($params){
		return $this->hookRightColumn($params);
	}

	public function hookFooter($params){
		global $smarty;
		

	}

	##### FUNCTIONS #####

	function cartminExist($id_group,$id_zone){
		$res = Db::getInstance()->getRow("SELECT COUNT(id_cartmin) as nb_item FROM "._DB_PREFIX_."e_cartmin WHERE fk_id_group='$id_group' AND fk_id_zone='$id_zone' ");
		if(isset($res['nb_item']) && $res['nb_item']>0){
			return true;
		}
		else{
			return false;
		}
	}

	function getCartmins(){
		$tab_cartmin = array();
		$sql = "SELECT id_cartmin,fk_id_zone,fk_id_group,montant_mini
				FROM "._DB_PREFIX_."e_cartmin ";
		$res = Db::getInstance()->ExecuteS($sql);
		if(is_array($res)){
			foreach($res as $key => $val){
				$tab_cartmin[$val['fk_id_group']][$val['fk_id_zone']] = $val;
			}
		}
		return $tab_cartmin;
	}

	function getMontantCartmin($id_group,$id_zone){
		$sql = "SELECT id_cartmin,montant_mini
				FROM "._DB_PREFIX_."e_cartmin
				WHERE fk_id_group='$id_group' AND fk_id_zone='$id_zone' ";
		$res = Db::getInstance()->getRow($sql);
		return $res['montant_mini'];
	}

	function insertCartmin($id_group,$id_zone,$montant_mini){
		$sql = "INSERT INTO "._DB_PREFIX_."e_cartmin
				(fk_id_zone,fk_id_group,montant_mini)
				VALUES
				('$id_zone','$id_group','$montant_mini') ";
		Db::getInstance()->Execute($sql);
	}

	function updateCartmin($id_group,$id_zone,$montant_mini){
		$sql = "UPDATE "._DB_PREFIX_."e_cartmin
				SET montant_mini='$montant_mini'
				WHERE fk_id_zone='$id_zone' AND fk_id_group='$id_group' ";
		Db::getInstance()->Execute($sql);
	}

}

?>