<?php

// if (!defined('_CAN_LOAD_FILES_'))
//     exit;

class QuickOrderFormOverride extends QuickOrderForm
{
    public function install()
    {
        Configuration::updateGlobalValue('BLOCKQUICKORDER_TITLE', $this->l('Quick order form'));
        Configuration::updateGlobalValue('BLOCKQUICKORDER_QUANTITY', 0);
        Configuration::updateGlobalValue('BLOCKQUICKORDER_NBLINES', 5);
        Configuration::updateGlobalValue('BLOCKQUICKORDER_EMPTY', 0);

        foreach (scandir(_PS_MODULE_DIR_.$this->name.'/img/') as $file)
            if (in_array($file, array('quickorderform.jpg', 'quickorderform.gif', 'quickorderform.png')))
                Configuration::updateGlobalValue('BLOCKQUICKORDER_IMG_EXT', Tools::substr($file, strrpos($file, '.') + 1));

        $groups = Group::getGroups($this->context->language->id);
        $list_group = '';
        foreach ($groups as $group)
            $list_group .= intval($group['id_group']).'-';
        $list_group = rtrim($list_group, '-');
        Configuration::updateGlobalValue('BLOCKQUICKORDER_GROUPS', $list_group);

        return (parent::install() && $this->registerHook('displayLeftColumn') && $this->registerHook('displayHeader') && $this->registerHook('displayTop'));
    }

    public function hookDisplayTop()
    {
        if (QuickOrderForm::hasAccess()) {
            $this->context->controller->addCSS($this->getTemplatePath().'css/quickorderformtop.css', 'all');
            $this->smarty->assign(array('image'     => $this->context->link->protocol_content.$this->qof_img,
                                        'qof_title' => $this->qof_title));

            return $this->display(__FILE__, 'top.tpl');
        }
    }
}