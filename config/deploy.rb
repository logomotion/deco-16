require 'mina/bundler'
require 'mina/rails'
require 'mina/git'

account = 'deco'

set :repository, 'git@bitbucket.com:logomotion/deco.git'
set :branch, 'master'

set :domain, 'ns3.logomotion-serveur.com'
set :user, 'root'

set :forward_agent, true
set :term_mode, nil

set :deploy_to, "/home/#{account}/src_dev"

set :shared_dirs, ['log','cache/smarty/compile', 'cache/smarty/cache',  'config/atos-config', 'img', 'modules/atos/pathfile']
set :shared_files, ['config/settings.inc.php', '.htaccess', 'robots.txt']

task :deploy => :environment do
	deploy do
		invoke :'git:clone'
		invoke :'deploy:cleanup'
		invoke :'deploy:link_shared_paths'
		on :launch do
			command "find #{fetch(:deploy_to)} -type d -exec chmod 755 {} +"
			command "find #{fetch(:deploy_to)} -type f -exec chmod 644 {} +"
			command "chown -R #{account}:#{account} #{fetch(:deploy_to)}"
		end
	end
end